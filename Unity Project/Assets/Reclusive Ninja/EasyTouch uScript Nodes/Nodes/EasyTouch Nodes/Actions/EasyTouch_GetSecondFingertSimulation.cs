﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if Second Finger Simulation is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Second Finger Simulation", "Returns if Second Finger Simulation is enabled/disabled.")]
public class EasyTouch_GetSecondFingertSimulation : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Is Second Finger Simulation Enabled?", "Returns if Second Finger Simulation is enabled/disabled.")]
		out bool b_SecondFingerSimulation)
	{
		b_SecondFingerSimulation = HedgehogTeam.EasyTouch.EasyTouch.GetSecondeFingerSimulation();
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}