﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables AutoUpdateUI.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Auto Update UI", "Enables/Disables AutoUpdateUI.")]
public class EasyTouch_SetAutoUpdateUI : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable AutoUpdateUI", "Enables/Disables AutoUpdateUI.")]
        bool b_EnableAutoUpdateUI)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetAutoUpdateUI(b_EnableAutoUpdateUI);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}