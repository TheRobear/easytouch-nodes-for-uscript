﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables Pinch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enable Pinch", "Enables/Disables Pinch.")]
public class EasyTouch_SetEnablePinch : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Enable Pinch", "Enables/Disables Pinch.")]
		bool b_EnablePinch)
	{
        HedgehogTeam.EasyTouch.EasyTouch.SetEnablePinch(b_EnablePinch);
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}