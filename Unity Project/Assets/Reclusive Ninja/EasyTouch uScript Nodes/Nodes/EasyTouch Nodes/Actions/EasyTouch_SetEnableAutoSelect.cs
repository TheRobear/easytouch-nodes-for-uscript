﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables SetEnableAutoSelect.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enable Auto Select", "Enables/Disables SetEnableAutoSelect.")]
public class EasyTouch_SetEnableAutoSelect : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable SetEnableAutoSelect", "Enables/Disables SetEnableAutoSelect.")]
        bool b_EnableAutoSelect)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetEnableAutoSelect(b_EnableAutoSelect);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}