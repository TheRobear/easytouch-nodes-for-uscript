﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Long Tap Time for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Long Tap Time", "Sets the Long Tap Time for EasyTouch.")]
public class EasyTouch_SetLongTapTime : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Long Tap Time", "Sets the Long Tap Time for EasyTouch.")]
        float f_LongTapTime
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetLongTapTime(f_LongTapTime);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}