﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Swipe Tolerance for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Swipe Tolerance", "Returns the Swipe Tolerance for EasyTouch.")]
public class EasyTouch_GetSwipeTolerance : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Swipe Tolerance", "Returns the Swipe Tolerance of EasyTouch.")]
        out float f_SwipeTolerance)
    {
        f_SwipeTolerance = HedgehogTeam.EasyTouch.EasyTouch.GetSwipeTolerance();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}