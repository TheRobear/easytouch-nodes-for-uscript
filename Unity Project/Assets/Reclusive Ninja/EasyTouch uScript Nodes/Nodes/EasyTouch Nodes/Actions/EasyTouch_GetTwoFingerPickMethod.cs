﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Two Finger Pick Method for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Two Finger Pick Method", "Returns the Two Finger Pick Method for EasyTouch.")]
public class EasyTouch_GetTwoFingerPickMethod : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Two Finger Pick Method", "Returns the Two Finger Pick Method for EasyTouch.")]
        out HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod ettfpm_TwoFingPickMethod)
    {
        ettfpm_TwoFingPickMethod = HedgehogTeam.EasyTouch.EasyTouch.GetTwoFingerPickMethod();
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}
