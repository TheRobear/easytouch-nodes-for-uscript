﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Min Pinch Length.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Min Pinch Length", "Sets the Min Pinch Length.")]
public class EasyTouch_SetMinPinchLength : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Min Pinch Length", "Sets the Min Pinch Length.")]
		float f_MinPichLength)
	{
        HedgehogTeam.EasyTouch.EasyTouch.SetMinPinchLength(f_MinPichLength);
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}