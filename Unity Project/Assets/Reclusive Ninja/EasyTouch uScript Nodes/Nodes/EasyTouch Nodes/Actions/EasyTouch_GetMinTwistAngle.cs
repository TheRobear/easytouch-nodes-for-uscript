﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Min Twist Angle.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Min Twist Angle", "Returns the Min Twist Angle.")]
public class EasyTouch_GetMinTwistAngle : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Min Twist Angle", "Returns the Min Twist Angle.")]
		out float f_MinTwistAngle)
	{
        f_MinTwistAngle = HedgehogTeam.EasyTouch.EasyTouch.GetMinTwistAngle();
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}