﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables 2 Fingers Gestures for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enable 2 Fingers Gesture", "Enables/Disables 2 Fingers Gestures for EasyTouch.")]
public class EasyTouch_SetEnable2FingersGesture : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable 2 Fingers Gesture", "Enables/Disables 2 Fingers Gestures for EasyTouch.")]
        bool b_Enable2FingerGesture)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetEnable2FingersGesture(b_Enable2FingerGesture);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}