﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the gesture priority for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Gesture Priority", "Sets the gesture priority for EasyTouch.")]
public class EasyTouch_SetGesturePriority : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Gesture Priority", "Sets the gesture priority for EasyTouch.")]
        HedgehogTeam.EasyTouch.EasyTouch.GesturePriority etgp_GesturePriority)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetGesturePriority(etgp_GesturePriority);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}