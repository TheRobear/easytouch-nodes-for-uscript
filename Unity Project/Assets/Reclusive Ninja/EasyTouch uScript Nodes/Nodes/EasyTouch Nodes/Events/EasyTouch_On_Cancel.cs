﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Events")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Uses EasyTouch On_Cancel to handle EasyTouch On_Cancel events.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch - On Cancel", "Uses EasyTouch On_Cancel to handle EasyTouch On_Cancel events.")]
public class EasyTouch_On_Cancel : uScriptEvent
{
#region uScript Event Handlers ------------------------------------------------------------------------
    //uScript event delcaration.
    public delegate void uScriptEventHandler(object sender, System.EventArgs args);

    //EasyTouch event delcaration.
    [FriendlyName("On_Cancel")]
    public event uScriptEventHandler OnCancel;
#endregion

#region Unity Related Functions ------------------------------------------------------------------------
    //Subscribe to the EasyTouch On_Cancel delegate call when the object is enabled.
    void OnEnable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel += On_Cancel;
    }

    //------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_Cancel delegate call when the object is disabled.
    void OnDisable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel -= On_Cancel;
    }

    //------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_Cancel delegate call when the object is destroyed.
    void OnDestroy()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel -= On_Cancel;
    }
#endregion

#region EasyTouch Functions ------------------------------------------------------------------------
    //Calls the EasyTouch On_Cancel delecare call.
    public void On_Cancel(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        TouchGesture();
    }

//------------------------------------------------------------------------
    //Sends the event to the node chain connected to this event.
    void TouchGesture()
    {
        if (OnCancel != null)
        {
            OnCancel(this, new System.EventArgs());
        }
    }

#endregion
}