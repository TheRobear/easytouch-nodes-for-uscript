//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class UIWIndowBehaviour : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_Delta_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_7 = null;
   UnityEngine.GameObject owner_Connection_8 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_3 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_3 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_3 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_3 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_3 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_3 = true;
   //pointer to script instanced logic node
   RN_SetAsLastSibling logic_RN_SetAsLastSibling_RN_SetAsLastSibling_9 = new RN_SetAsLastSibling( );
   UnityEngine.GameObject logic_RN_SetAsLastSibling_go_GameObject_9 = default(UnityEngine.GameObject);
   bool logic_RN_SetAsLastSibling_Out_9 = true;
   
   //event nodes
   UnityEngine.EventSystems.PointerEventData.InputButton event_UnityEngine_GameObject_ib_Button_0 = UnityEngine.EventSystems.PointerEventData.InputButton.Left;
   System.Int32 event_UnityEngine_GameObject_i_ClickCount_0 = (int) 0;
   System.Single event_UnityEngine_GameObject_f_ClickTime_0 = (float) 0;
   UnityEngine.EventSystems.BaseInputModule event_UnityEngine_GameObject_bim_CurrentInput_0 = default(UnityEngine.EventSystems.BaseInputModule);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Delta_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Delta_0 = new Vector3( );
   System.Boolean event_UnityEngine_GameObject_b_Dragging_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_EligibleForClick_0 = (bool) false;
   UnityEngine.Camera event_UnityEngine_GameObject_c_EnterEventCam_0 = default(UnityEngine.Camera);
   System.Collections.Generic.List<UnityEngine.GameObject> event_UnityEngine_GameObject_l_Hovered_0 = default(System.Collections.Generic.List<UnityEngine.GameObject>);
   System.Boolean event_UnityEngine_GameObject_b_IsPointerMoving_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsScrolling_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_LastPress_0 = default(UnityEngine.GameObject);
   UnityEngine.EventSystems.RaycastResult event_UnityEngine_GameObject_rc_PointerCurrentRaycast_0 = default(UnityEngine.EventSystems.RaycastResult);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerDrag_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerEnter_0 = default(UnityEngine.GameObject);
   System.Int32 event_UnityEngine_GameObject_i_PointerID_0 = (int) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerPress_0 = default(UnityEngine.GameObject);
   UnityEngine.EventSystems.RaycastResult event_UnityEngine_GameObject_rc_PointerPressRaycast_0 = default(UnityEngine.EventSystems.RaycastResult);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Camera event_UnityEngine_GameObject_c_PressEventCamera_0 = default(UnityEngine.Camera);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_PressPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_PressPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_RawPointerPress_0 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_ScrollDelta_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_ScrollData_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_SelectedObject_0 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_Used_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_UsedDragThreshold_0 = (bool) false;
   UnityEngine.EventSystems.PointerEventData.InputButton event_UnityEngine_GameObject_ib_Button_6 = UnityEngine.EventSystems.PointerEventData.InputButton.Left;
   System.Int32 event_UnityEngine_GameObject_i_ClickCount_6 = (int) 0;
   System.Single event_UnityEngine_GameObject_f_ClickTime_6 = (float) 0;
   UnityEngine.EventSystems.BaseInputModule event_UnityEngine_GameObject_bim_CurrentInput_6 = default(UnityEngine.EventSystems.BaseInputModule);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Delta_6 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Delta_6 = new Vector3( (float)0, (float)0, (float)0 );
   System.Boolean event_UnityEngine_GameObject_b_Dragging_6 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_EligibleForClick_6 = (bool) false;
   UnityEngine.Camera event_UnityEngine_GameObject_c_EnterEventCam_6 = default(UnityEngine.Camera);
   System.Collections.Generic.List<UnityEngine.GameObject> event_UnityEngine_GameObject_l_Hovered_6 = default(System.Collections.Generic.List<UnityEngine.GameObject>);
   System.Boolean event_UnityEngine_GameObject_b_IsPointerMoving_6 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsScrolling_6 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_LastPress_6 = default(UnityEngine.GameObject);
   UnityEngine.EventSystems.RaycastResult event_UnityEngine_GameObject_rc_PointerCurrentRaycast_6 = default(UnityEngine.EventSystems.RaycastResult);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerDrag_6 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerEnter_6 = default(UnityEngine.GameObject);
   System.Int32 event_UnityEngine_GameObject_i_PointerID_6 = (int) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PointerPress_6 = default(UnityEngine.GameObject);
   UnityEngine.EventSystems.RaycastResult event_UnityEngine_GameObject_rc_PointerPressRaycast_6 = default(UnityEngine.EventSystems.RaycastResult);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_6 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_6 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Camera event_UnityEngine_GameObject_c_PressEventCamera_6 = default(UnityEngine.Camera);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_PressPosition_6 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_PressPosition_6 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_RawPointerPress_6 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_ScrollDelta_6 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_ScrollData_6 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_SelectedObject_6 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_Used_6 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_UsedDragThreshold_6 = (bool) false;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               RN_IDragHandler component = owner_Connection_1.GetComponent<RN_IDragHandler>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<RN_IDragHandler>();
               }
               if ( null != component )
               {
                  component.On_Drag += Instance_On_Drag_0;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
      if ( null == owner_Connection_7 || false == m_RegisteredForEvents )
      {
         owner_Connection_7 = parentGameObject;
         if ( null != owner_Connection_7 )
         {
            {
               RN_IPointerHandler component = owner_Connection_7.GetComponent<RN_IPointerHandler>();
               if ( null == component )
               {
                  component = owner_Connection_7.AddComponent<RN_IPointerHandler>();
               }
               if ( null != component )
               {
                  component.On_PointerDown += Instance_On_PointerDown_6;
               }
            }
         }
      }
      if ( null == owner_Connection_8 || false == m_RegisteredForEvents )
      {
         owner_Connection_8 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               RN_IDragHandler component = owner_Connection_1.GetComponent<RN_IDragHandler>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<RN_IDragHandler>();
               }
               if ( null != component )
               {
                  component.On_Drag += Instance_On_Drag_0;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_7 )
         {
            {
               RN_IPointerHandler component = owner_Connection_7.GetComponent<RN_IPointerHandler>();
               if ( null == component )
               {
                  component = owner_Connection_7.AddComponent<RN_IPointerHandler>();
               }
               if ( null != component )
               {
                  component.On_PointerDown += Instance_On_PointerDown_6;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            RN_IDragHandler component = owner_Connection_1.GetComponent<RN_IDragHandler>();
            if ( null != component )
            {
               component.On_Drag -= Instance_On_Drag_0;
            }
         }
      }
      if ( null != owner_Connection_7 )
      {
         {
            RN_IPointerHandler component = owner_Connection_7.GetComponent<RN_IPointerHandler>();
            if ( null != component )
            {
               component.On_PointerDown -= Instance_On_PointerDown_6;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_3.SetParent(g);
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_9.SetParent(g);
      owner_Connection_1 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_7 = parentGameObject;
      owner_Connection_8 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_On_Drag_0(object o, RN_IDragHandler.OnDragEventData e)
   {
      //fill globals
      event_UnityEngine_GameObject_ib_Button_0 = e.ib_Button;
      event_UnityEngine_GameObject_i_ClickCount_0 = e.i_ClickCount;
      event_UnityEngine_GameObject_f_ClickTime_0 = e.f_ClickTime;
      event_UnityEngine_GameObject_bim_CurrentInput_0 = e.bim_CurrentInput;
      event_UnityEngine_GameObject_v2_Delta_0 = e.v2_Delta;
      event_UnityEngine_GameObject_v3_Delta_0 = e.v3_Delta;
      event_UnityEngine_GameObject_b_Dragging_0 = e.b_Dragging;
      event_UnityEngine_GameObject_b_EligibleForClick_0 = e.b_EligibleForClick;
      event_UnityEngine_GameObject_c_EnterEventCam_0 = e.c_EnterEventCam;
      event_UnityEngine_GameObject_l_Hovered_0 = e.l_Hovered;
      event_UnityEngine_GameObject_b_IsPointerMoving_0 = e.b_IsPointerMoving;
      event_UnityEngine_GameObject_b_IsScrolling_0 = e.b_IsScrolling;
      event_UnityEngine_GameObject_go_LastPress_0 = e.go_LastPress;
      event_UnityEngine_GameObject_rc_PointerCurrentRaycast_0 = e.rc_PointerCurrentRaycast;
      event_UnityEngine_GameObject_go_PointerDrag_0 = e.go_PointerDrag;
      event_UnityEngine_GameObject_go_PointerEnter_0 = e.go_PointerEnter;
      event_UnityEngine_GameObject_i_PointerID_0 = e.i_PointerID;
      event_UnityEngine_GameObject_go_PointerPress_0 = e.go_PointerPress;
      event_UnityEngine_GameObject_rc_PointerPressRaycast_0 = e.rc_PointerPressRaycast;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_c_PressEventCamera_0 = e.c_PressEventCamera;
      event_UnityEngine_GameObject_v2_PressPosition_0 = e.v2_PressPosition;
      event_UnityEngine_GameObject_v3_PressPosition_0 = e.v3_PressPosition;
      event_UnityEngine_GameObject_go_RawPointerPress_0 = e.go_RawPointerPress;
      event_UnityEngine_GameObject_v2_ScrollDelta_0 = e.v2_ScrollDelta;
      event_UnityEngine_GameObject_v3_ScrollData_0 = e.v3_ScrollData;
      event_UnityEngine_GameObject_go_SelectedObject_0 = e.go_SelectedObject;
      event_UnityEngine_GameObject_b_Used_0 = e.b_Used;
      event_UnityEngine_GameObject_b_UsedDragThreshold_0 = e.b_UsedDragThreshold;
      //relay event to nodes
      Relay_On_Drag_0( );
   }
   
   void Instance_On_PointerDown_6(object o, RN_IPointerHandler.OnPointerDownEventData e)
   {
      //fill globals
      event_UnityEngine_GameObject_ib_Button_6 = e.ib_Button;
      event_UnityEngine_GameObject_i_ClickCount_6 = e.i_ClickCount;
      event_UnityEngine_GameObject_f_ClickTime_6 = e.f_ClickTime;
      event_UnityEngine_GameObject_bim_CurrentInput_6 = e.bim_CurrentInput;
      event_UnityEngine_GameObject_v2_Delta_6 = e.v2_Delta;
      event_UnityEngine_GameObject_v3_Delta_6 = e.v3_Delta;
      event_UnityEngine_GameObject_b_Dragging_6 = e.b_Dragging;
      event_UnityEngine_GameObject_b_EligibleForClick_6 = e.b_EligibleForClick;
      event_UnityEngine_GameObject_c_EnterEventCam_6 = e.c_EnterEventCam;
      event_UnityEngine_GameObject_l_Hovered_6 = e.l_Hovered;
      event_UnityEngine_GameObject_b_IsPointerMoving_6 = e.b_IsPointerMoving;
      event_UnityEngine_GameObject_b_IsScrolling_6 = e.b_IsScrolling;
      event_UnityEngine_GameObject_go_LastPress_6 = e.go_LastPress;
      event_UnityEngine_GameObject_rc_PointerCurrentRaycast_6 = e.rc_PointerCurrentRaycast;
      event_UnityEngine_GameObject_go_PointerDrag_6 = e.go_PointerDrag;
      event_UnityEngine_GameObject_go_PointerEnter_6 = e.go_PointerEnter;
      event_UnityEngine_GameObject_i_PointerID_6 = e.i_PointerID;
      event_UnityEngine_GameObject_go_PointerPress_6 = e.go_PointerPress;
      event_UnityEngine_GameObject_rc_PointerPressRaycast_6 = e.rc_PointerPressRaycast;
      event_UnityEngine_GameObject_v2_Position_6 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_6 = e.v3_Position;
      event_UnityEngine_GameObject_c_PressEventCamera_6 = e.c_PressEventCamera;
      event_UnityEngine_GameObject_v2_PressPosition_6 = e.v2_PressPosition;
      event_UnityEngine_GameObject_v3_PressPosition_6 = e.v3_PressPosition;
      event_UnityEngine_GameObject_go_RawPointerPress_6 = e.go_RawPointerPress;
      event_UnityEngine_GameObject_v2_ScrollDelta_6 = e.v2_ScrollDelta;
      event_UnityEngine_GameObject_v3_ScrollData_6 = e.v3_ScrollData;
      event_UnityEngine_GameObject_go_SelectedObject_6 = e.go_SelectedObject;
      event_UnityEngine_GameObject_b_Used_6 = e.b_Used;
      event_UnityEngine_GameObject_b_UsedDragThreshold_6 = e.b_UsedDragThreshold;
      //relay event to nodes
      Relay_On_PointerDown_6( );
   }
   
   void Relay_On_Drag_0()
   {
      local_Delta_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_Delta_0;
      Relay_In_3();
   }
   
   void Relay_In_3()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_3.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_3, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_3[ index++ ] = owner_Connection_4;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_3 = local_Delta_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_3.In(logic_uScriptAct_SetGameObjectPosition_Target_3, logic_uScriptAct_SetGameObjectPosition_Position_3, logic_uScriptAct_SetGameObjectPosition_AsOffset_3, logic_uScriptAct_SetGameObjectPosition_AsLocal_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_On_PointerDown_6()
   {
      Relay_In_9();
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_RN_SetAsLastSibling_go_GameObject_9 = owner_Connection_8;
            
         }
      }
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_9.In(logic_RN_SetAsLastSibling_go_GameObject_9);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
