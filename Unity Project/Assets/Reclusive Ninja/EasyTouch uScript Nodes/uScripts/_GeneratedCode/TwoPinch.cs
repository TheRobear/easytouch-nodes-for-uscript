//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoPinch : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_35_System_String = "Delta Pinch : ";
   System.String local_66_System_String = "Delta Pinch : ";
   UnityEngine.GameObject local_93_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_93_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_97_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_97_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_99_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_99_UnityEngine_GameObject_previous = null;
   System.Single local_DeltaPinch_System_Single = (float) 0;
   System.String local_DeltaPinchValue_System_String = "";
   System.Single local_DeltaTime_System_Single = (float) 0;
   UnityEngine.Vector3 local_NewScale_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_NewScaleX_System_Single = (float) 0;
   System.Single local_NewScaleY_System_Single = (float) 0;
   System.Single local_NewScaleZ_System_Single = (float) 0;
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   System.Single local_ScaleX_System_Single = (float) 0;
   System.Single local_ScaleY_System_Single = (float) 0;
   System.Single local_ScaleZ_System_Single = (float) 0;
   System.Single local_Zoom_System_Single = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_31 = null;
   UnityEngine.GameObject owner_Connection_41 = null;
   UnityEngine.GameObject owner_Connection_64 = null;
   UnityEngine.GameObject owner_Connection_68 = null;
   UnityEngine.GameObject owner_Connection_75 = null;
   UnityEngine.GameObject owner_Connection_77 = null;
   UnityEngine.GameObject owner_Connection_82 = null;
   UnityEngine.GameObject owner_Connection_87 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectScale logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_4 = new uScriptAct_GetGameObjectScale( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectScale_Target_4 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_GetGameObjectScale_Scale_4;
   System.Single logic_uScriptAct_GetGameObjectScale_X_4;
   System.Single logic_uScriptAct_GetGameObjectScale_Y_4;
   System.Single logic_uScriptAct_GetGameObjectScale_Z_4;
   bool logic_uScriptAct_GetGameObjectScale_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_9 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_9 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_9;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_9;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractFloat logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_13 = new uScriptAct_SubtractFloat( );
   System.Single logic_uScriptAct_SubtractFloat_A_13 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_B_13 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_FloatResult_13;
   System.Int32 logic_uScriptAct_SubtractFloat_IntResult_13;
   bool logic_uScriptAct_SubtractFloat_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractFloat logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_17 = new uScriptAct_SubtractFloat( );
   System.Single logic_uScriptAct_SubtractFloat_A_17 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_B_17 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_FloatResult_17;
   System.Int32 logic_uScriptAct_SubtractFloat_IntResult_17;
   bool logic_uScriptAct_SubtractFloat_Out_17 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractFloat logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_21 = new uScriptAct_SubtractFloat( );
   System.Single logic_uScriptAct_SubtractFloat_A_21 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_B_21 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_FloatResult_21;
   System.Int32 logic_uScriptAct_SubtractFloat_IntResult_21;
   bool logic_uScriptAct_SubtractFloat_Out_21 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_25 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_25 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_25 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_25 = true;
   //pointer to script instanced logic node
   RN_FloatsToVector3 logic_RN_FloatsToVector3_RN_FloatsToVector3_26 = new RN_FloatsToVector3( );
   System.Single logic_RN_FloatsToVector3_f_XValue_26 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_YValue_26 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_ZValue_26 = (float) 0;
   UnityEngine.Vector3 logic_RN_FloatsToVector3_v3_Result_26;
   bool logic_RN_FloatsToVector3_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_33 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_33 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_33 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_33 = "";
   System.String logic_uScriptAct_Concatenate_Result_33;
   bool logic_uScriptAct_Concatenate_Out_33 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_38 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_38 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_38 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_38;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_38;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_38 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectScale logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_42 = new uScriptAct_GetGameObjectScale( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectScale_Target_42 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_GetGameObjectScale_Scale_42;
   System.Single logic_uScriptAct_GetGameObjectScale_X_42;
   System.Single logic_uScriptAct_GetGameObjectScale_Y_42;
   System.Single logic_uScriptAct_GetGameObjectScale_Z_42;
   bool logic_uScriptAct_GetGameObjectScale_Out_42 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_45 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_45 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_45 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_45;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_45;
   bool logic_uScriptAct_AddFloat_v2_Out_45 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_46 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_46 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_46 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_46;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_46;
   bool logic_uScriptAct_AddFloat_v2_Out_46 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_47 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_47 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_47 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_47;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_47;
   bool logic_uScriptAct_AddFloat_v2_Out_47 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_59 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_59 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_59 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_59 = true;
   //pointer to script instanced logic node
   RN_FloatsToVector3 logic_RN_FloatsToVector3_RN_FloatsToVector3_62 = new RN_FloatsToVector3( );
   System.Single logic_RN_FloatsToVector3_f_XValue_62 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_YValue_62 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_ZValue_62 = (float) 0;
   UnityEngine.Vector3 logic_RN_FloatsToVector3_v3_Result_62;
   bool logic_RN_FloatsToVector3_Out_62 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_65 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_65 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_65 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_65 = "";
   System.String logic_uScriptAct_Concatenate_Result_65;
   bool logic_uScriptAct_Concatenate_Out_65 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_69 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_69 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_69 = new Vector3( (float)2, (float)2, (float)2 );
   bool logic_uScriptAct_SetGameObjectScale_Out_69 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_71 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_71 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_71 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_71 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_72 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_72 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_72 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_72 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_72 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_72 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_72 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_78 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_78 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_78 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_78 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_78 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_78 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_78 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_78 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_80 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_80 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_80 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_80 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_83 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_83 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_83 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_83 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_83 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_83 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_83 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_83 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_85 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_85 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_85 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_85 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_88 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_88 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_88 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_88 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_88 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_88 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_88 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_88 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_90 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_90 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_90 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_90 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_92 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_92 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_92 = "Pinch Me";
   bool logic_RN_SetTextMeshText_Out_92 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_96 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_96 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_96 = "";
   bool logic_RN_SetTextMeshText_Out_96 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_101 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_101 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_101 = "";
   bool logic_RN_SetTextMeshText_Out_101 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnableTwist logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_239 = new EasyTouch_SetEnableTwist( );
   System.Boolean logic_EasyTouch_SetEnableTwist_b_EnableTwist_239 = (bool) false;
   bool logic_EasyTouch_SetEnableTwist_Out_239 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnablePinch logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_241 = new EasyTouch_SetEnablePinch( );
   System.Boolean logic_EasyTouch_SetEnablePinch_b_EnablePinch_241 = (bool) true;
   bool logic_EasyTouch_SetEnablePinch_Out_241 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnablePinch logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_243 = new EasyTouch_SetEnablePinch( );
   System.Boolean logic_EasyTouch_SetEnablePinch_b_EnablePinch_243 = (bool) true;
   bool logic_EasyTouch_SetEnablePinch_Out_243 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_227 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_227 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_227 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_227 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_227 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_227 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_227 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_227 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_227 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_227 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_227 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_227 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_227 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_227 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_227 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_227 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_227 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_227 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_227 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_227 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_227 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_227 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_227 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_227 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_227 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_227 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_227 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_227 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_227 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_227 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_231 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_231 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_231 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_231 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_231 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_231 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_231 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_231 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_231 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_231 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_231 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_231 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_231 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_231 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_231 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_231 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_231 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_231 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_231 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_231 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_231 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_231 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_231 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_231 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_231 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_231 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_231 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_231 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_231 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_231 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_93_UnityEngine_GameObject = GameObject.Find( "Sphere_Pinch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_97_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_97_UnityEngine_GameObject = GameObject.Find( "Sphere_Pinch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_97_UnityEngine_GameObject_previous != local_97_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_97_UnityEngine_GameObject_previous = local_97_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_99_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_99_UnityEngine_GameObject = GameObject.Find( "Sphere_Pinch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_99_UnityEngine_GameObject_previous != local_99_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_99_UnityEngine_GameObject_previous = local_99_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_231;
                  component.OnPinchIn += Instance_OnPinchIn_231;
                  component.OnPinchOut += Instance_OnPinchOut_231;
                  component.OnPinchEnd += Instance_OnPinchEnd_231;
               }
            }
         }
      }
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_227;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_227;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_227;
               }
            }
         }
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
      }
      if ( null == owner_Connection_31 || false == m_RegisteredForEvents )
      {
         owner_Connection_31 = parentGameObject;
      }
      if ( null == owner_Connection_41 || false == m_RegisteredForEvents )
      {
         owner_Connection_41 = parentGameObject;
      }
      if ( null == owner_Connection_64 || false == m_RegisteredForEvents )
      {
         owner_Connection_64 = parentGameObject;
      }
      if ( null == owner_Connection_68 || false == m_RegisteredForEvents )
      {
         owner_Connection_68 = parentGameObject;
      }
      if ( null == owner_Connection_75 || false == m_RegisteredForEvents )
      {
         owner_Connection_75 = parentGameObject;
      }
      if ( null == owner_Connection_77 || false == m_RegisteredForEvents )
      {
         owner_Connection_77 = parentGameObject;
      }
      if ( null == owner_Connection_82 || false == m_RegisteredForEvents )
      {
         owner_Connection_82 = parentGameObject;
      }
      if ( null == owner_Connection_87 || false == m_RegisteredForEvents )
      {
         owner_Connection_87 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_97_UnityEngine_GameObject_previous != local_97_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_97_UnityEngine_GameObject_previous = local_97_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_99_UnityEngine_GameObject_previous != local_99_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_99_UnityEngine_GameObject_previous = local_99_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_231;
                  component.OnPinchIn += Instance_OnPinchIn_231;
                  component.OnPinchOut += Instance_OnPinchOut_231;
                  component.OnPinchEnd += Instance_OnPinchEnd_231;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_227;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_227;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_227;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
            if ( null != component )
            {
               component.OnPinch -= Instance_OnPinch_231;
               component.OnPinchIn -= Instance_OnPinchIn_231;
               component.OnPinchOut -= Instance_OnPinchOut_231;
               component.OnPinchEnd -= Instance_OnPinchEnd_231;
            }
         }
      }
      if ( null != owner_Connection_1 )
      {
         {
            EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
            if ( null != component )
            {
               component.OnTouchStart2Fingers -= Instance_OnTouchStart2Fingers_227;
               component.OnTouchDown2Fingers -= Instance_OnTouchDown2Fingers_227;
               component.OnTouchUp2Fingers -= Instance_OnTouchUp2Fingers_227;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.SetParent(g);
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_13.SetParent(g);
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_17.SetParent(g);
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_21.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_25.SetParent(g);
      logic_RN_FloatsToVector3_RN_FloatsToVector3_26.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_33.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_38.SetParent(g);
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_42.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_45.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_46.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_47.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_59.SetParent(g);
      logic_RN_FloatsToVector3_RN_FloatsToVector3_62.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_65.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_69.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_71.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_78.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_80.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_83.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_85.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_88.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_90.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_92.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_96.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_101.SetParent(g);
      logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_239.SetParent(g);
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_241.SetParent(g);
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_243.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_1 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_31 = parentGameObject;
      owner_Connection_41 = parentGameObject;
      owner_Connection_64 = parentGameObject;
      owner_Connection_68 = parentGameObject;
      owner_Connection_75 = parentGameObject;
      owner_Connection_77 = parentGameObject;
      owner_Connection_82 = parentGameObject;
      owner_Connection_87 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart2Fingers_227(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_227 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_227 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_227 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_227 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_227 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_227 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_227 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_227 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_227 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_227 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_227 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_227 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_227 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_227 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_227 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_227 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_227 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_227 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_227 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_227 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_227 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_227 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_227 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_227 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_227 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_227 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_227 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_227 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_227 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_227 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_227 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_227 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_227 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_227 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_227 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_227 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_227 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_227 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_227 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_227 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_227 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_227 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_227 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart2Fingers_227( );
   }
   
   void Instance_OnTouchDown2Fingers_227(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_227 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_227 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_227 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_227 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_227 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_227 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_227 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_227 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_227 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_227 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_227 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_227 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_227 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_227 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_227 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_227 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_227 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_227 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_227 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_227 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_227 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_227 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_227 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_227 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_227 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_227 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_227 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_227 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_227 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_227 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_227 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_227 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_227 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_227 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_227 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_227 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_227 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_227 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_227 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_227 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_227 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_227 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_227 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown2Fingers_227( );
   }
   
   void Instance_OnTouchUp2Fingers_227(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_227 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_227 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_227 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_227 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_227 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_227 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_227 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_227 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_227 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_227 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_227 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_227 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_227 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_227 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_227 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_227 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_227 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_227 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_227 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_227 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_227 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_227 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_227 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_227 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_227 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_227 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_227 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_227 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_227 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_227 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_227 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_227 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_227 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_227 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_227 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_227 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_227 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_227 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_227 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_227 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_227 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_227 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_227 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp2Fingers_227( );
   }
   
   void Instance_OnPinch_231(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_231 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_231 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_231 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_231 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_231 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_231 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_231 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_231 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_231 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_231 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_231 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_231 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_231 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_231 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_231 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_231 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_231 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_231 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_231 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_231 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_231 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_231 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_231 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_231 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_231 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_231 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_231 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_231 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_231 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_231 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_231 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_231 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_231 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_231 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_231 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_231 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_231 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_231 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_231 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_231 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_231 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_231 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_231 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinch_231( );
   }
   
   void Instance_OnPinchIn_231(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_231 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_231 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_231 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_231 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_231 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_231 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_231 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_231 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_231 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_231 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_231 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_231 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_231 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_231 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_231 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_231 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_231 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_231 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_231 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_231 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_231 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_231 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_231 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_231 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_231 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_231 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_231 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_231 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_231 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_231 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_231 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_231 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_231 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_231 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_231 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_231 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_231 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_231 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_231 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_231 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_231 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_231 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_231 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchIn_231( );
   }
   
   void Instance_OnPinchOut_231(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_231 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_231 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_231 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_231 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_231 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_231 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_231 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_231 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_231 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_231 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_231 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_231 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_231 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_231 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_231 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_231 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_231 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_231 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_231 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_231 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_231 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_231 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_231 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_231 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_231 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_231 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_231 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_231 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_231 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_231 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_231 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_231 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_231 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_231 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_231 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_231 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_231 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_231 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_231 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_231 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_231 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_231 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_231 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchOut_231( );
   }
   
   void Instance_OnPinchEnd_231(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_231 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_231 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_231 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_231 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_231 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_231 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_231 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_231 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_231 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_231 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_231 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_231 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_231 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_231 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_231 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_231 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_231 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_231 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_231 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_231 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_231 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_231 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_231 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_231 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_231 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_231 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_231 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_231 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_231 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_231 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_231 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_231 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_231 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_231 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_231 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_231 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_231 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_231 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_231 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_231 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_231 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_231 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_231 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchEnd_231( );
   }
   
   void Relay_In_4()
   {
      {
         {
            logic_uScriptAct_GetGameObjectScale_Target_4 = owner_Connection_5;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_4.In(logic_uScriptAct_GetGameObjectScale_Target_4, out logic_uScriptAct_GetGameObjectScale_Scale_4, out logic_uScriptAct_GetGameObjectScale_X_4, out logic_uScriptAct_GetGameObjectScale_Y_4, out logic_uScriptAct_GetGameObjectScale_Z_4);
      local_ScaleX_System_Single = logic_uScriptAct_GetGameObjectScale_X_4;
      local_ScaleY_System_Single = logic_uScriptAct_GetGameObjectScale_Y_4;
      local_ScaleZ_System_Single = logic_uScriptAct_GetGameObjectScale_Z_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_4.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_9 = local_DeltaTime_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_9 = local_DeltaPinch_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.In(logic_uScriptAct_MultiplyFloat_v2_A_9, logic_uScriptAct_MultiplyFloat_v2_B_9, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_9, out logic_uScriptAct_MultiplyFloat_v2_IntResult_9);
      local_Zoom_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptAct_SubtractFloat_A_13 = local_ScaleX_System_Single;
            
         }
         {
            logic_uScriptAct_SubtractFloat_B_13 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_13.In(logic_uScriptAct_SubtractFloat_A_13, logic_uScriptAct_SubtractFloat_B_13, out logic_uScriptAct_SubtractFloat_FloatResult_13, out logic_uScriptAct_SubtractFloat_IntResult_13);
      local_NewScaleX_System_Single = logic_uScriptAct_SubtractFloat_FloatResult_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
            logic_uScriptAct_SubtractFloat_A_17 = local_ScaleY_System_Single;
            
         }
         {
            logic_uScriptAct_SubtractFloat_B_17 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_17.In(logic_uScriptAct_SubtractFloat_A_17, logic_uScriptAct_SubtractFloat_B_17, out logic_uScriptAct_SubtractFloat_FloatResult_17, out logic_uScriptAct_SubtractFloat_IntResult_17);
      local_NewScaleY_System_Single = logic_uScriptAct_SubtractFloat_FloatResult_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_21();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            logic_uScriptAct_SubtractFloat_A_21 = local_ScaleZ_System_Single;
            
         }
         {
            logic_uScriptAct_SubtractFloat_B_21 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_21.In(logic_uScriptAct_SubtractFloat_A_21, logic_uScriptAct_SubtractFloat_B_21, out logic_uScriptAct_SubtractFloat_FloatResult_21, out logic_uScriptAct_SubtractFloat_IntResult_21);
      local_NewScaleZ_System_Single = logic_uScriptAct_SubtractFloat_FloatResult_21;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_21.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_25()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectScale_Target_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_25, index + 1);
            }
            logic_uScriptAct_SetGameObjectScale_Target_25[ index++ ] = owner_Connection_31;
            
         }
         {
            logic_uScriptAct_SetGameObjectScale_Scale_25 = local_NewScale_UnityEngine_Vector3;
            
         }
      }
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_25.In(logic_uScriptAct_SetGameObjectScale_Target_25, logic_uScriptAct_SetGameObjectScale_Scale_25);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_25.Out;
      
      if ( test_0 == true )
      {
         Relay_In_33();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_RN_FloatsToVector3_f_XValue_26 = local_NewScaleX_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_YValue_26 = local_NewScaleY_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_ZValue_26 = local_NewScaleZ_System_Single;
            
         }
         {
         }
      }
      logic_RN_FloatsToVector3_RN_FloatsToVector3_26.In(logic_RN_FloatsToVector3_f_XValue_26, logic_RN_FloatsToVector3_f_YValue_26, logic_RN_FloatsToVector3_f_ZValue_26, out logic_RN_FloatsToVector3_v3_Result_26);
      local_NewScale_UnityEngine_Vector3 = logic_RN_FloatsToVector3_v3_Result_26;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FloatsToVector3_RN_FloatsToVector3_26.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_In_33()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_33.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_33, index + 1);
            }
            logic_uScriptAct_Concatenate_A_33[ index++ ] = local_35_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_33.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_33, index + 1);
            }
            logic_uScriptAct_Concatenate_B_33[ index++ ] = local_DeltaPinch_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_33.In(logic_uScriptAct_Concatenate_A_33, logic_uScriptAct_Concatenate_B_33, logic_uScriptAct_Concatenate_Separator_33, out logic_uScriptAct_Concatenate_Result_33);
      local_DeltaPinchValue_System_String = logic_uScriptAct_Concatenate_Result_33;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_33.Out;
      
      if ( test_0 == true )
      {
         Relay_In_96();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_38 = local_DeltaTime_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_38 = local_DeltaPinch_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_38.In(logic_uScriptAct_MultiplyFloat_v2_A_38, logic_uScriptAct_MultiplyFloat_v2_B_38, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_38, out logic_uScriptAct_MultiplyFloat_v2_IntResult_38);
      local_Zoom_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_38;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_38.Out;
      
      if ( test_0 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_In_42()
   {
      {
         {
            logic_uScriptAct_GetGameObjectScale_Target_42 = owner_Connection_41;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_42.In(logic_uScriptAct_GetGameObjectScale_Target_42, out logic_uScriptAct_GetGameObjectScale_Scale_42, out logic_uScriptAct_GetGameObjectScale_X_42, out logic_uScriptAct_GetGameObjectScale_Y_42, out logic_uScriptAct_GetGameObjectScale_Z_42);
      local_ScaleX_System_Single = logic_uScriptAct_GetGameObjectScale_X_42;
      local_ScaleY_System_Single = logic_uScriptAct_GetGameObjectScale_Y_42;
      local_ScaleZ_System_Single = logic_uScriptAct_GetGameObjectScale_Z_42;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_42.Out;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_In_45()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_45 = local_ScaleX_System_Single;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_45 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_45.In(logic_uScriptAct_AddFloat_v2_A_45, logic_uScriptAct_AddFloat_v2_B_45, out logic_uScriptAct_AddFloat_v2_FloatResult_45, out logic_uScriptAct_AddFloat_v2_IntResult_45);
      local_NewScaleX_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_45;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_45.Out;
      
      if ( test_0 == true )
      {
         Relay_In_46();
      }
   }
   
   void Relay_In_46()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_46 = local_ScaleY_System_Single;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_46 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_46.In(logic_uScriptAct_AddFloat_v2_A_46, logic_uScriptAct_AddFloat_v2_B_46, out logic_uScriptAct_AddFloat_v2_FloatResult_46, out logic_uScriptAct_AddFloat_v2_IntResult_46);
      local_NewScaleY_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_46;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_46.Out;
      
      if ( test_0 == true )
      {
         Relay_In_47();
      }
   }
   
   void Relay_In_47()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_47 = local_ScaleZ_System_Single;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_47 = local_Zoom_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_47.In(logic_uScriptAct_AddFloat_v2_A_47, logic_uScriptAct_AddFloat_v2_B_47, out logic_uScriptAct_AddFloat_v2_FloatResult_47, out logic_uScriptAct_AddFloat_v2_IntResult_47);
      local_NewScaleZ_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_47;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_47.Out;
      
      if ( test_0 == true )
      {
         Relay_In_62();
      }
   }
   
   void Relay_In_59()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectScale_Target_59.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_59, index + 1);
            }
            logic_uScriptAct_SetGameObjectScale_Target_59[ index++ ] = owner_Connection_64;
            
         }
         {
            logic_uScriptAct_SetGameObjectScale_Scale_59 = local_NewScale_UnityEngine_Vector3;
            
         }
      }
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_59.In(logic_uScriptAct_SetGameObjectScale_Target_59, logic_uScriptAct_SetGameObjectScale_Scale_59);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_59.Out;
      
      if ( test_0 == true )
      {
         Relay_In_65();
      }
   }
   
   void Relay_In_62()
   {
      {
         {
            logic_RN_FloatsToVector3_f_XValue_62 = local_NewScaleX_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_YValue_62 = local_NewScaleY_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_ZValue_62 = local_NewScaleZ_System_Single;
            
         }
         {
         }
      }
      logic_RN_FloatsToVector3_RN_FloatsToVector3_62.In(logic_RN_FloatsToVector3_f_XValue_62, logic_RN_FloatsToVector3_f_YValue_62, logic_RN_FloatsToVector3_f_ZValue_62, out logic_RN_FloatsToVector3_v3_Result_62);
      local_NewScale_UnityEngine_Vector3 = logic_RN_FloatsToVector3_v3_Result_62;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FloatsToVector3_RN_FloatsToVector3_62.Out;
      
      if ( test_0 == true )
      {
         Relay_In_59();
      }
   }
   
   void Relay_In_65()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_65.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_65, index + 1);
            }
            logic_uScriptAct_Concatenate_A_65[ index++ ] = local_66_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_65.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_65, index + 1);
            }
            logic_uScriptAct_Concatenate_B_65[ index++ ] = local_DeltaPinch_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_65.In(logic_uScriptAct_Concatenate_A_65, logic_uScriptAct_Concatenate_B_65, logic_uScriptAct_Concatenate_Separator_65, out logic_uScriptAct_Concatenate_Result_65);
      local_DeltaPinchValue_System_String = logic_uScriptAct_Concatenate_Result_65;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_65.Out;
      
      if ( test_0 == true )
      {
         Relay_In_101();
      }
   }
   
   void Relay_In_69()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectScale_Target_69.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_69, index + 1);
            }
            logic_uScriptAct_SetGameObjectScale_Target_69[ index++ ] = owner_Connection_68;
            
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_69.In(logic_uScriptAct_SetGameObjectScale_Target_69, logic_uScriptAct_SetGameObjectScale_Scale_69);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_69.Out;
      
      if ( test_0 == true )
      {
         Relay_In_92();
      }
   }
   
   void Relay_In_71()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_71.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_71, index + 1);
            }
            logic_uScriptCon_IsNull_Target_71[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_71.In(logic_uScriptCon_IsNull_Target_71);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_71.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_72();
      }
   }
   
   void Relay_In_72()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_72 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_72 = owner_Connection_75;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.In(logic_uScriptCon_CompareGameObjects_A_72, logic_uScriptCon_CompareGameObjects_B_72, logic_uScriptCon_CompareGameObjects_CompareByTag_72, logic_uScriptCon_CompareGameObjects_CompareByName_72, logic_uScriptCon_CompareGameObjects_ReportNull_72);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.Same;
      
      if ( test_0 == true )
      {
         Relay_In_239();
      }
   }
   
   void Relay_In_78()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_78 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_78 = owner_Connection_77;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_78.In(logic_uScriptCon_CompareGameObjects_A_78, logic_uScriptCon_CompareGameObjects_B_78, logic_uScriptCon_CompareGameObjects_CompareByTag_78, logic_uScriptCon_CompareGameObjects_CompareByName_78, logic_uScriptCon_CompareGameObjects_ReportNull_78);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_78.Same;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_80()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_80.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_80, index + 1);
            }
            logic_uScriptCon_IsNull_Target_80[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_80.In(logic_uScriptCon_IsNull_Target_80);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_80.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_78();
      }
   }
   
   void Relay_In_83()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_83 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_83 = owner_Connection_82;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_83.In(logic_uScriptCon_CompareGameObjects_A_83, logic_uScriptCon_CompareGameObjects_B_83, logic_uScriptCon_CompareGameObjects_CompareByTag_83, logic_uScriptCon_CompareGameObjects_CompareByName_83, logic_uScriptCon_CompareGameObjects_ReportNull_83);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_83.Same;
      
      if ( test_0 == true )
      {
         Relay_In_38();
      }
   }
   
   void Relay_In_85()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_85.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_85, index + 1);
            }
            logic_uScriptCon_IsNull_Target_85[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_85.In(logic_uScriptCon_IsNull_Target_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_85.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_83();
      }
   }
   
   void Relay_In_88()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_88 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_88 = owner_Connection_87;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_88.In(logic_uScriptCon_CompareGameObjects_A_88, logic_uScriptCon_CompareGameObjects_B_88, logic_uScriptCon_CompareGameObjects_CompareByTag_88, logic_uScriptCon_CompareGameObjects_CompareByName_88, logic_uScriptCon_CompareGameObjects_ReportNull_88);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_88.Same;
      
      if ( test_0 == true )
      {
         Relay_In_69();
      }
   }
   
   void Relay_In_90()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_90.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_90, index + 1);
            }
            logic_uScriptCon_IsNull_Target_90[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_90.In(logic_uScriptCon_IsNull_Target_90);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_90.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_88();
      }
   }
   
   void Relay_In_92()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_92 = local_93_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_92.In(logic_RN_SetTextMeshText_go_GameObject_92, logic_RN_SetTextMeshText_s_Text_92);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetTextMeshText_RN_SetTextMeshText_92.Out;
      
      if ( test_0 == true )
      {
         Relay_In_243();
      }
   }
   
   void Relay_In_96()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_97_UnityEngine_GameObject_previous != local_97_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_97_UnityEngine_GameObject_previous = local_97_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_96 = local_97_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_96 = local_DeltaPinchValue_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_96.In(logic_RN_SetTextMeshText_go_GameObject_96, logic_RN_SetTextMeshText_s_Text_96);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_101()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_99_UnityEngine_GameObject_previous != local_99_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_99_UnityEngine_GameObject_previous = local_99_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_101 = local_99_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_101 = local_DeltaPinchValue_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_101.In(logic_RN_SetTextMeshText_go_GameObject_101, logic_RN_SetTextMeshText_s_Text_101);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart2Fingers_227()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_227;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_71();
   }
   
   void Relay_OnTouchDown2Fingers_227()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_227;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnTouchUp2Fingers_227()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_227;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnPinch_231()
   {
      local_DeltaTime_System_Single = event_UnityEngine_GameObject_f_DeltaTime_231;
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_231;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_231;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnPinchIn_231()
   {
      local_DeltaTime_System_Single = event_UnityEngine_GameObject_f_DeltaTime_231;
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_231;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_231;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_80();
   }
   
   void Relay_OnPinchOut_231()
   {
      local_DeltaTime_System_Single = event_UnityEngine_GameObject_f_DeltaTime_231;
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_231;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_231;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_85();
   }
   
   void Relay_OnPinchEnd_231()
   {
      local_DeltaTime_System_Single = event_UnityEngine_GameObject_f_DeltaTime_231;
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_231;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_231;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_90();
   }
   
   void Relay_In_239()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_239.In(logic_EasyTouch_SetEnableTwist_b_EnableTwist_239);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_239.Out;
      
      if ( test_0 == true )
      {
         Relay_In_241();
      }
   }
   
   void Relay_In_241()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_241.In(logic_EasyTouch_SetEnablePinch_b_EnablePinch_241);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_243()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_243.In(logic_EasyTouch_SetEnablePinch_b_EnablePinch_243);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
