//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class UITwistWindow : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Boolean local_OverUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_PickedUIElement_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedUIElement_UnityEngine_GameObject_previous = null;
   System.Single local_TwistAngle_System_Single = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_8 = null;
   UnityEngine.GameObject owner_Connection_11 = null;
   UnityEngine.GameObject owner_Connection_13 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_4 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_4 = true;
   bool logic_uScriptCon_CompareBool_False_4 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_6 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_6 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_6 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_6 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_6 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_6 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_6 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_6 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_9 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_9 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_9 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_9 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_9 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_12 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_12 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_12 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_12 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_12 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_12 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_12 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_12 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_12 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_12 = (bool) true;
   bool logic_uScriptAct_SetGameObjectRotation_Out_12 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_31 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_31 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_31 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_31 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_31 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_31 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_31 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_31 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_31 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_31 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_31 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_31 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_31 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_31 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_31 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_31 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_31 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_31 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_31 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_31 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_31 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_31 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_31 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_31 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_31 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_31 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_31 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_31 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_31 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_31 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_31;
                  component.OnTwistEnd += Instance_OnTwistEnd_31;
               }
            }
         }
      }
      if ( null == owner_Connection_8 || false == m_RegisteredForEvents )
      {
         owner_Connection_8 = parentGameObject;
      }
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
      if ( null == owner_Connection_13 || false == m_RegisteredForEvents )
      {
         owner_Connection_13 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_31;
                  component.OnTwistEnd += Instance_OnTwistEnd_31;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
            if ( null != component )
            {
               component.OnTwist -= Instance_OnTwist_31;
               component.OnTwistEnd -= Instance_OnTwistEnd_31;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_6.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_9.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_12.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_8 = parentGameObject;
      owner_Connection_11 = parentGameObject;
      owner_Connection_13 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTwist_31(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_31 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_31 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_31 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_31 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_31 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_31 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_31 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_31 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_31 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_31 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_31 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_31 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_31 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_31 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_31 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_31 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_31 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_31 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_31 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_31 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_31 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_31 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_31 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_31 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_31 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_31 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_31 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_31 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_31 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_31 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_31 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_31 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_31 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_31 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_31 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_31 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_31 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_31 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_31 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_31 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_31 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_31 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_31 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwist_31( );
   }
   
   void Instance_OnTwistEnd_31(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_31 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_31 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_31 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_31 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_31 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_31 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_31 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_31 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_31 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_31 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_31 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_31 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_31 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_31 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_31 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_31 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_31 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_31 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_31 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_31 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_31 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_31 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_31 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_31 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_31 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_31 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_31 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_31 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_31 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_31 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_31 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_31 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_31 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_31 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_31 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_31 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_31 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_31 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_31 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_31 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_31 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_31 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_31 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwistEnd_31( );
   }
   
   void Relay_In_4()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_4 = local_OverUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.In(logic_uScriptCon_CompareBool_Bool_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.True;
      
      if ( test_0 == true )
      {
         Relay_In_6();
         Relay_In_9();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_6 = local_PickedUIElement_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_6 = owner_Connection_8;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_6.In(logic_uScriptCon_CompareGameObjects_A_6, logic_uScriptCon_CompareGameObjects_B_6, logic_uScriptCon_CompareGameObjects_CompareByTag_6, logic_uScriptCon_CompareGameObjects_CompareByName_6, logic_uScriptCon_CompareGameObjects_ReportNull_6);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_6.Same;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_9 = local_PickedUIElement_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_9 = owner_Connection_11;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_9.In(logic_uScriptCon_IsChild_Child_9, logic_uScriptCon_IsChild_Parent_9);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_9.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_12, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_12[ index++ ] = owner_Connection_13;
            
         }
         {
         }
         {
         }
         {
            logic_uScriptAct_SetGameObjectRotation_ZDegrees_12 = local_TwistAngle_System_Single;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_12.In(logic_uScriptAct_SetGameObjectRotation_Target_12, logic_uScriptAct_SetGameObjectRotation_XDegrees_12, logic_uScriptAct_SetGameObjectRotation_YDegrees_12, logic_uScriptAct_SetGameObjectRotation_ZDegrees_12, logic_uScriptAct_SetGameObjectRotation_IgnoreX_12, logic_uScriptAct_SetGameObjectRotation_IgnoreY_12, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_12, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_12, logic_uScriptAct_SetGameObjectRotation_AsOffset_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTwist_31()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_31;
      local_OverUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_31;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_31;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_4();
   }
   
   void Relay_OnTwistEnd_31()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_31;
      local_OverUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_31;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_31;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
}
