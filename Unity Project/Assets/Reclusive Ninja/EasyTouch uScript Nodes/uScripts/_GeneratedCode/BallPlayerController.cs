//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class BallPlayerController : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_50_System_Single = (float) 400;
   UnityEngine.GameObject local_59_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_59_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_85_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_85_UnityEngine_GameObject_previous = null;
   UnityEngine.Vector3 local_CurrentPosition_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_DeltaTime_System_Single = (float) 0;
   System.Single local_FloatResult01_System_Single = (float) 0;
   System.Boolean local_IsJump_System_Boolean = (bool) false;
   UnityEngine.Vector3 local_MoveDirection_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_MoveDirectionX_System_Single = (float) 0;
   System.Single local_MoveDirectionY_System_Single = (float) 0;
   System.Single local_MoveDirectionZ_System_Single = (float) 0;
   System.Single local_Multiply01_System_Single = (float) 0;
   UnityEngine.Vector3 local_ObjectDirection_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_PosX_System_Single = (float) 0;
   System.Single local_PosY_System_Single = (float) 0;
   System.Single local_PosZ_System_Single = (float) 0;
   UnityEngine.Vector3 local_Result01_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_Right_UnityEngine_Vector3 = new Vector3( (float)1, (float)0, (float)0 );
   UnityEngine.Vector3 local_RightResult_UnityEngine_Vector3 = new Vector3( (float)1, (float)0, (float)0 );
   UnityEngine.Vector3 local_StartPosition_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.String local_SwipeDir_System_String = "";
   public System.Boolean StartBall = (bool) false;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_2 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_33 = null;
   UnityEngine.GameObject owner_Connection_35 = null;
   UnityEngine.GameObject owner_Connection_39 = null;
   UnityEngine.GameObject owner_Connection_48 = null;
   UnityEngine.GameObject owner_Connection_64 = null;
   UnityEngine.GameObject owner_Connection_70 = null;
   UnityEngine.GameObject owner_Connection_79 = null;
   UnityEngine.GameObject owner_Connection_89 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_3 = (float) 10;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_3;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6;
   bool logic_uScriptAct_GetDeltaTime_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_8;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_8 = true;
   //pointer to script instanced logic node
   RN_Vector3ToFloats logic_RN_Vector3ToFloats_RN_Vector3ToFloats_12 = new RN_Vector3ToFloats( );
   UnityEngine.Vector3 logic_RN_Vector3ToFloats_v3_Vector3_12 = new Vector3( );
   System.Single logic_RN_Vector3ToFloats_f_XValue_12;
   System.Single logic_RN_Vector3ToFloats_f_YValue_12;
   System.Single logic_RN_Vector3ToFloats_f_ZValue_12;
   bool logic_RN_Vector3ToFloats_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_17 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_17 = (float) 9.81;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_17 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_17;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_17;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_17 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_20 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_20 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_20 = true;
   bool logic_uScriptCon_CompareBool_False_20 = true;
   //pointer to script instanced logic node
   uScriptAct_SetFloat logic_uScriptAct_SetFloat_uScriptAct_SetFloat_22 = new uScriptAct_SetFloat( );
   System.Single logic_uScriptAct_SetFloat_Value_22 = (float) 8;
   System.Single logic_uScriptAct_SetFloat_Target_22;
   bool logic_uScriptAct_SetFloat_Out_22 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_24 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_24;
   bool logic_uScriptAct_SetBool_Out_24 = true;
   bool logic_uScriptAct_SetBool_SetTrue_24 = true;
   bool logic_uScriptAct_SetBool_SetFalse_24 = true;
   //pointer to script instanced logic node
   RN_FloatsToVector3 logic_RN_FloatsToVector3_RN_FloatsToVector3_26 = new RN_FloatsToVector3( );
   System.Single logic_RN_FloatsToVector3_f_XValue_26 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_YValue_26 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_ZValue_26 = (float) 0;
   UnityEngine.Vector3 logic_RN_FloatsToVector3_v3_Result_26;
   bool logic_RN_FloatsToVector3_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_34 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_34 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_34 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_34;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_34;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_34;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_34;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_34;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_34;
   bool logic_uScriptAct_GetPositionAndRotation_Out_34 = true;
   //pointer to script instanced logic node
   RN_Vector3ToFloats logic_RN_Vector3ToFloats_RN_Vector3ToFloats_37 = new RN_Vector3ToFloats( );
   UnityEngine.Vector3 logic_RN_Vector3ToFloats_v3_Vector3_37 = new Vector3( );
   System.Single logic_RN_Vector3ToFloats_f_XValue_37;
   System.Single logic_RN_Vector3ToFloats_f_YValue_37;
   System.Single logic_RN_Vector3ToFloats_f_ZValue_37;
   bool logic_RN_Vector3ToFloats_Out_37 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_38 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_38 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_38 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_38;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_38;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_38;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_38;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_38;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_38;
   bool logic_uScriptAct_GetPositionAndRotation_Out_38 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareFloat logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_45 = new uScriptCon_CompareFloat( );
   System.Single logic_uScriptCon_CompareFloat_A_45 = (float) 0;
   System.Single logic_uScriptCon_CompareFloat_B_45 = (float) 0.5;
   bool logic_uScriptCon_CompareFloat_GreaterThan_45 = true;
   bool logic_uScriptCon_CompareFloat_GreaterThanOrEqualTo_45 = true;
   bool logic_uScriptCon_CompareFloat_EqualTo_45 = true;
   bool logic_uScriptCon_CompareFloat_NotEqualTo_45 = true;
   bool logic_uScriptCon_CompareFloat_LessThanOrEqualTo_45 = true;
   bool logic_uScriptCon_CompareFloat_LessThan_45 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_47 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_47 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_47 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_47 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_47 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_47 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_51 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_51 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_51 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_51;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_51;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_51 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractFloat logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_55 = new uScriptAct_SubtractFloat( );
   System.Single logic_uScriptAct_SubtractFloat_A_55 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_B_55 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_FloatResult_55;
   System.Int32 logic_uScriptAct_SubtractFloat_IntResult_55;
   bool logic_uScriptAct_SubtractFloat_Out_55 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_60 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_60 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_60 = true;
   bool logic_uScriptCon_CompareBool_False_60 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_62 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_62;
   bool logic_uScriptAct_SetBool_Out_62 = true;
   bool logic_uScriptAct_SetBool_SetTrue_62 = true;
   bool logic_uScriptAct_SetBool_SetFalse_62 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_66 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_66 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_66 = true;
   bool logic_uScriptCon_CompareBool_False_66 = true;
   //pointer to script instanced logic node
   uScriptCon_StringSwitch logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68 = new uScriptCon_StringSwitch( );
   System.String[] logic_uScriptCon_StringSwitch_Targets_68 = new System.String[] {};
   System.String logic_uScriptCon_StringSwitch_A_68 = "DownLeft";
   System.String logic_uScriptCon_StringSwitch_B_68 = "UpLeft";
   System.String logic_uScriptCon_StringSwitch_C_68 = "Left";
   System.String logic_uScriptCon_StringSwitch_D_68 = "";
   bool logic_uScriptCon_StringSwitch_None_68 = true;
   bool logic_uScriptCon_StringSwitch_Any_68 = true;
   bool logic_uScriptCon_StringSwitch_All_68 = true;
   bool logic_uScriptCon_StringSwitch_AMatch_68 = true;
   bool logic_uScriptCon_StringSwitch_BMatch_68 = true;
   bool logic_uScriptCon_StringSwitch_CMatch_68 = true;
   bool logic_uScriptCon_StringSwitch_DMatch_68 = true;
   //pointer to script instanced logic node
   uScriptCon_StringSwitch logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71 = new uScriptCon_StringSwitch( );
   System.String[] logic_uScriptCon_StringSwitch_Targets_71 = new System.String[] {};
   System.String logic_uScriptCon_StringSwitch_A_71 = "DownRight";
   System.String logic_uScriptCon_StringSwitch_B_71 = "UpRight";
   System.String logic_uScriptCon_StringSwitch_C_71 = "Right";
   System.String logic_uScriptCon_StringSwitch_D_71 = "";
   bool logic_uScriptCon_StringSwitch_None_71 = true;
   bool logic_uScriptCon_StringSwitch_Any_71 = true;
   bool logic_uScriptCon_StringSwitch_All_71 = true;
   bool logic_uScriptCon_StringSwitch_AMatch_71 = true;
   bool logic_uScriptCon_StringSwitch_BMatch_71 = true;
   bool logic_uScriptCon_StringSwitch_CMatch_71 = true;
   bool logic_uScriptCon_StringSwitch_DMatch_71 = true;
   //pointer to script instanced logic node
   uScriptCon_StringSwitch logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_73 = new uScriptCon_StringSwitch( );
   System.String[] logic_uScriptCon_StringSwitch_Targets_73 = new System.String[] {};
   System.String logic_uScriptCon_StringSwitch_A_73 = "Up";
   System.String logic_uScriptCon_StringSwitch_B_73 = "";
   System.String logic_uScriptCon_StringSwitch_C_73 = "";
   System.String logic_uScriptCon_StringSwitch_D_73 = "";
   bool logic_uScriptCon_StringSwitch_None_73 = true;
   bool logic_uScriptCon_StringSwitch_Any_73 = true;
   bool logic_uScriptCon_StringSwitch_All_73 = true;
   bool logic_uScriptCon_StringSwitch_AMatch_73 = true;
   bool logic_uScriptCon_StringSwitch_BMatch_73 = true;
   bool logic_uScriptCon_StringSwitch_CMatch_73 = true;
   bool logic_uScriptCon_StringSwitch_DMatch_73 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_75 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_75;
   bool logic_uScriptAct_SetBool_Out_75 = true;
   bool logic_uScriptAct_SetBool_SetTrue_75 = true;
   bool logic_uScriptAct_SetBool_SetFalse_75 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_77 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_77 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_77 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_77 = (float) -90;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_77 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_77 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_77 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_77 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_77 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_77 = (bool) true;
   bool logic_uScriptAct_SetGameObjectRotation_Out_77 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_78 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_78 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_78 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_78 = (float) 90;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_78 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_78 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_78 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_78 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_78 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_78 = (bool) true;
   bool logic_uScriptAct_SetGameObjectRotation_Out_78 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_80 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_80 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_80 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_80;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_80 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_87 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_87;
   bool logic_uScriptAct_SetBool_Out_87 = true;
   bool logic_uScriptAct_SetBool_SetTrue_87 = true;
   bool logic_uScriptAct_SetBool_SetFalse_87 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_88 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_88 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_88 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_88 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_88 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_88 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_88 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_88 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_88 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_88 = (bool) false;
   bool logic_uScriptAct_SetGameObjectRotation_Out_88 = true;
   //pointer to script instanced logic node
   RN_ReturnTransformDirection logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_90 = new RN_ReturnTransformDirection( );
   UnityEngine.GameObject logic_RN_ReturnTransformDirection_go_GameObject_90 = default(UnityEngine.GameObject);
   RN_ReturnTransformDirection.Direction logic_RN_ReturnTransformDirection_d_Direction_90 = RN_ReturnTransformDirection.Direction.Forward;
   UnityEngine.Vector3 logic_RN_ReturnTransformDirection_v3_Direction_90;
   bool logic_RN_ReturnTransformDirection_Out_90 = true;
   //pointer to script instanced logic node
   RN_MoveCharacterController logic_RN_MoveCharacterController_RN_MoveCharacterController_93 = new RN_MoveCharacterController( );
   UnityEngine.GameObject logic_RN_MoveCharacterController_go_GameObject_93 = default(UnityEngine.GameObject);
   UnityEngine.CollisionFlags logic_RN_MoveCharacterController_cf_CollisionFlags_93;
   UnityEngine.Vector3 logic_RN_MoveCharacterController_v3_Motion_93 = new Vector3( );
   bool logic_RN_MoveCharacterController_Out_93 = true;
   //pointer to script instanced logic node
   RN_TransformRotate logic_RN_TransformRotate_RN_TransformRotate_94 = new RN_TransformRotate( );
   UnityEngine.GameObject logic_RN_TransformRotate_go_GameObject_94 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_RN_TransformRotate_v3_EulerAngles_94 = new Vector3( );
   bool logic_RN_TransformRotate_Out_94 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_128 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_128 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_128 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_128 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_128 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_128 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_128 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_128 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_128 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_128 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_128 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_128 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_128 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_128 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_128 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_128 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_128 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_128 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_128 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_128 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_128 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_128 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_128 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_128 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_128 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_128 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_128 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_128 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_128 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_128 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_59_UnityEngine_GameObject = GameObject.Find( "BallMesh" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_59_UnityEngine_GameObject_previous != local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_59_UnityEngine_GameObject_previous = local_59_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_85_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_85_UnityEngine_GameObject = GameObject.Find( "Start_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_85_UnityEngine_GameObject_previous != local_85_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_85_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_85_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_84;
               }
            }
         }
         
         local_85_UnityEngine_GameObject_previous = local_85_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_85_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_85_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_85_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_84;
               }
            }
         }
      }
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Update component = owner_Connection_1.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
      if ( null == owner_Connection_2 || false == m_RegisteredForEvents )
      {
         owner_Connection_2 = parentGameObject;
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
      if ( null == owner_Connection_33 || false == m_RegisteredForEvents )
      {
         owner_Connection_33 = parentGameObject;
         if ( null != owner_Connection_33 )
         {
            {
               uScript_Global component = owner_Connection_33.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_33.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_32;
                  component.uScriptLateStart += Instance_uScriptLateStart_32;
               }
            }
         }
      }
      if ( null == owner_Connection_35 || false == m_RegisteredForEvents )
      {
         owner_Connection_35 = parentGameObject;
      }
      if ( null == owner_Connection_39 || false == m_RegisteredForEvents )
      {
         owner_Connection_39 = parentGameObject;
      }
      if ( null == owner_Connection_48 || false == m_RegisteredForEvents )
      {
         owner_Connection_48 = parentGameObject;
      }
      if ( null == owner_Connection_64 || false == m_RegisteredForEvents )
      {
         owner_Connection_64 = parentGameObject;
         if ( null != owner_Connection_64 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_64.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_64.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_128;
                  component.OnSwipe += Instance_OnSwipe_128;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_128;
               }
            }
         }
      }
      if ( null == owner_Connection_70 || false == m_RegisteredForEvents )
      {
         owner_Connection_70 = parentGameObject;
      }
      if ( null == owner_Connection_79 || false == m_RegisteredForEvents )
      {
         owner_Connection_79 = parentGameObject;
      }
      if ( null == owner_Connection_89 || false == m_RegisteredForEvents )
      {
         owner_Connection_89 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_59_UnityEngine_GameObject_previous != local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_59_UnityEngine_GameObject_previous = local_59_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_85_UnityEngine_GameObject_previous != local_85_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_85_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_85_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_84;
               }
            }
         }
         
         local_85_UnityEngine_GameObject_previous = local_85_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_85_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_85_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_85_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_84;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Update component = owner_Connection_1.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_33 )
         {
            {
               uScript_Global component = owner_Connection_33.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_33.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_32;
                  component.uScriptLateStart += Instance_uScriptLateStart_32;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_64 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_64.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_64.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_128;
                  component.OnSwipe += Instance_OnSwipe_128;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_128;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_85_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_85_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_84;
            }
         }
      }
      if ( null != owner_Connection_1 )
      {
         {
            uScript_Update component = owner_Connection_1.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
      if ( null != owner_Connection_33 )
      {
         {
            uScript_Global component = owner_Connection_33.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_32;
               component.uScriptLateStart -= Instance_uScriptLateStart_32;
            }
         }
      }
      if ( null != owner_Connection_64 )
      {
         {
            EasyTouch_On_Swipe component = owner_Connection_64.GetComponent<EasyTouch_On_Swipe>();
            if ( null != component )
            {
               component.OnSwipeStart -= Instance_OnSwipeStart_128;
               component.OnSwipe -= Instance_OnSwipe_128;
               component.OnSwipeEnd -= Instance_OnSwipeEnd_128;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.SetParent(g);
      logic_RN_Vector3ToFloats_RN_Vector3ToFloats_12.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_17.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_20.SetParent(g);
      logic_uScriptAct_SetFloat_uScriptAct_SetFloat_22.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_24.SetParent(g);
      logic_RN_FloatsToVector3_RN_FloatsToVector3_26.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_34.SetParent(g);
      logic_RN_Vector3ToFloats_RN_Vector3ToFloats_37.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_38.SetParent(g);
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_45.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_47.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_51.SetParent(g);
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_55.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_60.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_62.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_66.SetParent(g);
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68.SetParent(g);
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71.SetParent(g);
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_73.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_75.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_77.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_78.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_80.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_87.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_88.SetParent(g);
      logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_90.SetParent(g);
      logic_RN_MoveCharacterController_RN_MoveCharacterController_93.SetParent(g);
      logic_RN_TransformRotate_RN_TransformRotate_94.SetParent(g);
      owner_Connection_1 = parentGameObject;
      owner_Connection_2 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_33 = parentGameObject;
      owner_Connection_35 = parentGameObject;
      owner_Connection_39 = parentGameObject;
      owner_Connection_48 = parentGameObject;
      owner_Connection_64 = parentGameObject;
      owner_Connection_70 = parentGameObject;
      owner_Connection_79 = parentGameObject;
      owner_Connection_89 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Instance_uScriptStart_32(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_32( );
   }
   
   void Instance_uScriptLateStart_32(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_32( );
   }
   
   void Instance_OnButtonClick_84(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_84( );
   }
   
   void Instance_OnSwipeStart_128(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_128 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_128 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_128 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_128 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_128 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_128 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_128 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_128 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_128 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_128 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_128 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_128 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_128 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_128 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_128 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_128 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_128 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_128 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_128 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_128 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_128 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_128 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_128 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_128 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_128 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_128 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_128 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_128 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_128 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_128 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_128 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_128 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_128 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_128 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_128 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_128 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_128 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_128 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_128 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_128 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_128 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_128 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_128 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart_128( );
   }
   
   void Instance_OnSwipe_128(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_128 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_128 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_128 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_128 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_128 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_128 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_128 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_128 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_128 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_128 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_128 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_128 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_128 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_128 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_128 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_128 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_128 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_128 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_128 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_128 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_128 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_128 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_128 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_128 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_128 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_128 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_128 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_128 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_128 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_128 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_128 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_128 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_128 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_128 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_128 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_128 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_128 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_128 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_128 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_128 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_128 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_128 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_128 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe_128( );
   }
   
   void Instance_OnSwipeEnd_128(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_128 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_128 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_128 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_128 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_128 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_128 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_128 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_128 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_128 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_128 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_128 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_128 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_128 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_128 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_128 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_128 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_128 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_128 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_128 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_128 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_128 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_128 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_128 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_128 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_128 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_128 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_128 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_128 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_128 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_128 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_128 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_128 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_128 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_128 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_128 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_128 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_128 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_128 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_128 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_128 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_128 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_128 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_128 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd_128( );
   }
   
   void Relay_OnUpdate_0()
   {
      Relay_In_6();
      Relay_In_38();
   }
   
   void Relay_OnLateUpdate_0()
   {
   }
   
   void Relay_OnFixedUpdate_0()
   {
   }
   
   void Relay_In_3()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3 = local_ObjectDirection_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_3, out logic_uScriptAct_MultiplyVector3WithFloat_Result_3);
      local_Result01_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_3;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.Out;
      
      if ( test_0 == true )
      {
         Relay_In_8();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_6, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6);
      local_DeltaTime_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_60();
      }
   }
   
   void Relay_In_8()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8 = local_Result01_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8 = local_DeltaTime_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8, out logic_uScriptAct_MultiplyVector3WithFloat_Result_8);
      local_MoveDirection_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_8;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.Out;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            logic_RN_Vector3ToFloats_v3_Vector3_12 = local_MoveDirection_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_Vector3ToFloats_RN_Vector3ToFloats_12.In(logic_RN_Vector3ToFloats_v3_Vector3_12, out logic_RN_Vector3ToFloats_f_XValue_12, out logic_RN_Vector3ToFloats_f_YValue_12, out logic_RN_Vector3ToFloats_f_ZValue_12);
      local_MoveDirectionX_System_Single = logic_RN_Vector3ToFloats_f_XValue_12;
      local_MoveDirectionY_System_Single = logic_RN_Vector3ToFloats_f_YValue_12;
      local_MoveDirectionZ_System_Single = logic_RN_Vector3ToFloats_f_ZValue_12;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_Vector3ToFloats_RN_Vector3ToFloats_12.Out;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_17 = local_DeltaTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_17.In(logic_uScriptAct_MultiplyFloat_v2_A_17, logic_uScriptAct_MultiplyFloat_v2_B_17, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_17, out logic_uScriptAct_MultiplyFloat_v2_IntResult_17);
      local_Multiply01_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_55();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_20 = local_IsJump_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_20.In(logic_uScriptCon_CompareBool_Bool_20);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_20.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_20.False;
      
      if ( test_0 == true )
      {
         Relay_In_22();
      }
      if ( test_1 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_22()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_SetFloat_uScriptAct_SetFloat_22.In(logic_uScriptAct_SetFloat_Value_22, out logic_uScriptAct_SetFloat_Target_22);
      local_MoveDirectionY_System_Single = logic_uScriptAct_SetFloat_Target_22;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetFloat_uScriptAct_SetFloat_22.Out;
      
      if ( test_0 == true )
      {
         Relay_False_24();
      }
   }
   
   void Relay_True_24()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_24.True(out logic_uScriptAct_SetBool_Target_24);
      local_IsJump_System_Boolean = logic_uScriptAct_SetBool_Target_24;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_24.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_False_24()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_24.False(out logic_uScriptAct_SetBool_Target_24);
      local_IsJump_System_Boolean = logic_uScriptAct_SetBool_Target_24;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_24.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_RN_FloatsToVector3_f_XValue_26 = local_MoveDirectionX_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_YValue_26 = local_MoveDirectionY_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_ZValue_26 = local_MoveDirectionZ_System_Single;
            
         }
         {
         }
      }
      logic_RN_FloatsToVector3_RN_FloatsToVector3_26.In(logic_RN_FloatsToVector3_f_XValue_26, logic_RN_FloatsToVector3_f_YValue_26, logic_RN_FloatsToVector3_f_ZValue_26, out logic_RN_FloatsToVector3_v3_Result_26);
      local_MoveDirection_UnityEngine_Vector3 = logic_RN_FloatsToVector3_v3_Result_26;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FloatsToVector3_RN_FloatsToVector3_26.Out;
      
      if ( test_0 == true )
      {
         Relay_In_93();
      }
   }
   
   void Relay_uScriptStart_32()
   {
      Relay_In_34();
   }
   
   void Relay_uScriptLateStart_32()
   {
   }
   
   void Relay_In_34()
   {
      {
         {
            logic_uScriptAct_GetPositionAndRotation_Target_34 = owner_Connection_35;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_34.In(logic_uScriptAct_GetPositionAndRotation_Target_34, logic_uScriptAct_GetPositionAndRotation_GetLocal_34, out logic_uScriptAct_GetPositionAndRotation_Position_34, out logic_uScriptAct_GetPositionAndRotation_Rotation_34, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_34, out logic_uScriptAct_GetPositionAndRotation_Forward_34, out logic_uScriptAct_GetPositionAndRotation_Up_34, out logic_uScriptAct_GetPositionAndRotation_Right_34);
      local_StartPosition_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_34;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_37()
   {
      {
         {
            logic_RN_Vector3ToFloats_v3_Vector3_37 = local_CurrentPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_Vector3ToFloats_RN_Vector3ToFloats_37.In(logic_RN_Vector3ToFloats_v3_Vector3_37, out logic_RN_Vector3ToFloats_f_XValue_37, out logic_RN_Vector3ToFloats_f_YValue_37, out logic_RN_Vector3ToFloats_f_ZValue_37);
      local_PosX_System_Single = logic_RN_Vector3ToFloats_f_XValue_37;
      local_PosY_System_Single = logic_RN_Vector3ToFloats_f_YValue_37;
      local_PosZ_System_Single = logic_RN_Vector3ToFloats_f_ZValue_37;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_Vector3ToFloats_RN_Vector3ToFloats_37.Out;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            logic_uScriptAct_GetPositionAndRotation_Target_38 = owner_Connection_39;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_38.In(logic_uScriptAct_GetPositionAndRotation_Target_38, logic_uScriptAct_GetPositionAndRotation_GetLocal_38, out logic_uScriptAct_GetPositionAndRotation_Position_38, out logic_uScriptAct_GetPositionAndRotation_Rotation_38, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_38, out logic_uScriptAct_GetPositionAndRotation_Forward_38, out logic_uScriptAct_GetPositionAndRotation_Up_38, out logic_uScriptAct_GetPositionAndRotation_Right_38);
      local_CurrentPosition_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_38;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_38.Out;
      
      if ( test_0 == true )
      {
         Relay_In_37();
      }
   }
   
   void Relay_In_45()
   {
      {
         {
            logic_uScriptCon_CompareFloat_A_45 = local_PosY_System_Single;
            
         }
         {
         }
      }
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_45.In(logic_uScriptCon_CompareFloat_A_45, logic_uScriptCon_CompareFloat_B_45);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_45.LessThan;
      
      if ( test_0 == true )
      {
         Relay_In_47();
      }
   }
   
   void Relay_In_47()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_47.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_47, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_47[ index++ ] = owner_Connection_48;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_47 = local_StartPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_47.In(logic_uScriptAct_SetGameObjectPosition_Target_47, logic_uScriptAct_SetGameObjectPosition_Position_47, logic_uScriptAct_SetGameObjectPosition_AsOffset_47, logic_uScriptAct_SetGameObjectPosition_AsLocal_47);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_47.Out;
      
      if ( test_0 == true )
      {
         Relay_In_88();
      }
   }
   
   void Relay_In_51()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_51 = local_DeltaTime_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_51 = local_50_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_51.In(logic_uScriptAct_MultiplyFloat_v2_A_51, logic_uScriptAct_MultiplyFloat_v2_B_51, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_51, out logic_uScriptAct_MultiplyFloat_v2_IntResult_51);
      local_FloatResult01_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_51;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_51.Out;
      
      if ( test_0 == true )
      {
         Relay_In_80();
      }
   }
   
   void Relay_In_55()
   {
      {
         {
            logic_uScriptAct_SubtractFloat_A_55 = local_MoveDirectionY_System_Single;
            
         }
         {
            logic_uScriptAct_SubtractFloat_B_55 = local_Multiply01_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_55.In(logic_uScriptAct_SubtractFloat_A_55, logic_uScriptAct_SubtractFloat_B_55, out logic_uScriptAct_SubtractFloat_FloatResult_55, out logic_uScriptAct_SubtractFloat_IntResult_55);
      local_MoveDirectionY_System_Single = logic_uScriptAct_SubtractFloat_FloatResult_55;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_55.Out;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_In_60()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_60 = StartBall;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_60.In(logic_uScriptCon_CompareBool_Bool_60);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_60.True;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_True_62()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_62.True(out logic_uScriptAct_SetBool_Target_62);
      StartBall = logic_uScriptAct_SetBool_Target_62;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_62()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_62.False(out logic_uScriptAct_SetBool_Target_62);
      StartBall = logic_uScriptAct_SetBool_Target_62;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_66()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_66 = StartBall;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_66.In(logic_uScriptCon_CompareBool_Bool_66);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_66.True;
      
      if ( test_0 == true )
      {
         Relay_In_68();
         Relay_In_71();
         Relay_In_73();
      }
   }
   
   void Relay_In_68()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptCon_StringSwitch_Targets_68.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_StringSwitch_Targets_68, index + 1);
            }
            logic_uScriptCon_StringSwitch_Targets_68[ index++ ] = local_SwipeDir_System_String;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68.In(logic_uScriptCon_StringSwitch_Targets_68, logic_uScriptCon_StringSwitch_A_68, logic_uScriptCon_StringSwitch_B_68, logic_uScriptCon_StringSwitch_C_68, logic_uScriptCon_StringSwitch_D_68);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68.AMatch;
      bool test_1 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68.BMatch;
      bool test_2 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_68.CMatch;
      
      if ( test_0 == true )
      {
         Relay_In_77();
      }
      if ( test_1 == true )
      {
         Relay_In_77();
      }
      if ( test_2 == true )
      {
         Relay_In_77();
      }
   }
   
   void Relay_In_71()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptCon_StringSwitch_Targets_71.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_StringSwitch_Targets_71, index + 1);
            }
            logic_uScriptCon_StringSwitch_Targets_71[ index++ ] = local_SwipeDir_System_String;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71.In(logic_uScriptCon_StringSwitch_Targets_71, logic_uScriptCon_StringSwitch_A_71, logic_uScriptCon_StringSwitch_B_71, logic_uScriptCon_StringSwitch_C_71, logic_uScriptCon_StringSwitch_D_71);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71.AMatch;
      bool test_1 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71.BMatch;
      bool test_2 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_71.CMatch;
      
      if ( test_0 == true )
      {
         Relay_In_78();
      }
      if ( test_1 == true )
      {
         Relay_In_78();
      }
      if ( test_2 == true )
      {
         Relay_In_78();
      }
   }
   
   void Relay_In_73()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptCon_StringSwitch_Targets_73.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_StringSwitch_Targets_73, index + 1);
            }
            logic_uScriptCon_StringSwitch_Targets_73[ index++ ] = local_SwipeDir_System_String;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_73.In(logic_uScriptCon_StringSwitch_Targets_73, logic_uScriptCon_StringSwitch_A_73, logic_uScriptCon_StringSwitch_B_73, logic_uScriptCon_StringSwitch_C_73, logic_uScriptCon_StringSwitch_D_73);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_StringSwitch_uScriptCon_StringSwitch_73.AMatch;
      
      if ( test_0 == true )
      {
         Relay_True_75();
      }
   }
   
   void Relay_True_75()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_75.True(out logic_uScriptAct_SetBool_Target_75);
      local_IsJump_System_Boolean = logic_uScriptAct_SetBool_Target_75;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_75()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_75.False(out logic_uScriptAct_SetBool_Target_75);
      local_IsJump_System_Boolean = logic_uScriptAct_SetBool_Target_75;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_77()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_77.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_77, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_77[ index++ ] = owner_Connection_70;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_77.In(logic_uScriptAct_SetGameObjectRotation_Target_77, logic_uScriptAct_SetGameObjectRotation_XDegrees_77, logic_uScriptAct_SetGameObjectRotation_YDegrees_77, logic_uScriptAct_SetGameObjectRotation_ZDegrees_77, logic_uScriptAct_SetGameObjectRotation_IgnoreX_77, logic_uScriptAct_SetGameObjectRotation_IgnoreY_77, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_77, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_77, logic_uScriptAct_SetGameObjectRotation_AsOffset_77);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_78()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_78.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_78, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_78[ index++ ] = owner_Connection_79;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_78.In(logic_uScriptAct_SetGameObjectRotation_Target_78, logic_uScriptAct_SetGameObjectRotation_XDegrees_78, logic_uScriptAct_SetGameObjectRotation_YDegrees_78, logic_uScriptAct_SetGameObjectRotation_ZDegrees_78, logic_uScriptAct_SetGameObjectRotation_IgnoreX_78, logic_uScriptAct_SetGameObjectRotation_IgnoreY_78, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_78, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_78, logic_uScriptAct_SetGameObjectRotation_AsOffset_78);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_80()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_80 = local_Right_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_80 = local_FloatResult01_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_80.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_80, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_80, out logic_uScriptAct_MultiplyVector3WithFloat_Result_80);
      local_RightResult_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_80;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_80.Out;
      
      if ( test_0 == true )
      {
         Relay_In_94();
      }
   }
   
   void Relay_OnButtonClick_84()
   {
      Relay_True_87();
   }
   
   void Relay_True_87()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_87.True(out logic_uScriptAct_SetBool_Target_87);
      StartBall = logic_uScriptAct_SetBool_Target_87;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_87()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_87.False(out logic_uScriptAct_SetBool_Target_87);
      StartBall = logic_uScriptAct_SetBool_Target_87;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_88()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_88.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_88, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_88[ index++ ] = owner_Connection_89;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_88.In(logic_uScriptAct_SetGameObjectRotation_Target_88, logic_uScriptAct_SetGameObjectRotation_XDegrees_88, logic_uScriptAct_SetGameObjectRotation_YDegrees_88, logic_uScriptAct_SetGameObjectRotation_ZDegrees_88, logic_uScriptAct_SetGameObjectRotation_IgnoreX_88, logic_uScriptAct_SetGameObjectRotation_IgnoreY_88, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_88, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_88, logic_uScriptAct_SetGameObjectRotation_AsOffset_88);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_88.Out;
      
      if ( test_0 == true )
      {
         Relay_False_62();
      }
   }
   
   void Relay_In_90()
   {
      {
         {
            logic_RN_ReturnTransformDirection_go_GameObject_90 = owner_Connection_4;
            
         }
         {
         }
         {
         }
      }
      logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_90.In(logic_RN_ReturnTransformDirection_go_GameObject_90, logic_RN_ReturnTransformDirection_d_Direction_90, out logic_RN_ReturnTransformDirection_v3_Direction_90);
      local_ObjectDirection_UnityEngine_Vector3 = logic_RN_ReturnTransformDirection_v3_Direction_90;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_90.Out;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_93()
   {
      {
         {
            logic_RN_MoveCharacterController_go_GameObject_93 = owner_Connection_2;
            
         }
         {
         }
         {
            logic_RN_MoveCharacterController_v3_Motion_93 = local_MoveDirection_UnityEngine_Vector3;
            
         }
      }
      logic_RN_MoveCharacterController_RN_MoveCharacterController_93.In(logic_RN_MoveCharacterController_go_GameObject_93, out logic_RN_MoveCharacterController_cf_CollisionFlags_93, logic_RN_MoveCharacterController_v3_Motion_93);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_MoveCharacterController_RN_MoveCharacterController_93.Out;
      
      if ( test_0 == true )
      {
         Relay_In_51();
      }
   }
   
   void Relay_In_94()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_59_UnityEngine_GameObject_previous != local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_59_UnityEngine_GameObject_previous = local_59_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_TransformRotate_go_GameObject_94 = local_59_UnityEngine_GameObject;
            
         }
         {
            logic_RN_TransformRotate_v3_EulerAngles_94 = local_RightResult_UnityEngine_Vector3;
            
         }
      }
      logic_RN_TransformRotate_RN_TransformRotate_94.In(logic_RN_TransformRotate_go_GameObject_94, logic_RN_TransformRotate_v3_EulerAngles_94);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnSwipeStart_128()
   {
      local_SwipeDir_System_String = event_UnityEngine_GameObject_s_SwipeDirection_128;
   }
   
   void Relay_OnSwipe_128()
   {
      local_SwipeDir_System_String = event_UnityEngine_GameObject_s_SwipeDirection_128;
   }
   
   void Relay_OnSwipeEnd_128()
   {
      local_SwipeDir_System_String = event_UnityEngine_GameObject_s_SwipeDirection_128;
      Relay_In_66();
   }
   
}
