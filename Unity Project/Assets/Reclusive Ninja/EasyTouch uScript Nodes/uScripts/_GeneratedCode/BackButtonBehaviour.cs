//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class BackButtonBehaviour : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   [Multiline(3)]
   public System.String LevelName = "";
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_2 = "";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_2 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_2 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_2 = true;
   
   //event nodes
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Button component = owner_Connection_1.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Button component = owner_Connection_1.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            uScript_Button component = owner_Connection_1.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.SetParent(g);
      owner_Connection_1 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded += uScriptAct_LoadLevel_Loaded_2;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded -= uScriptAct_LoadLevel_Loaded_2;
   }
   
   void Instance_OnButtonClick_0(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_0( );
   }
   
   void uScriptAct_LoadLevel_Loaded_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_2( );
   }
   
   void Relay_OnButtonClick_0()
   {
      Relay_In_2();
   }
   
   void Relay_Loaded_2()
   {
   }
   
   void Relay_In_2()
   {
      {
         {
            logic_uScriptAct_LoadLevel_name_2 = LevelName;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.In(logic_uScriptAct_LoadLevel_name_2, logic_uScriptAct_LoadLevel_destroyOtherObjects_2, logic_uScriptAct_LoadLevel_blockUntilLoaded_2);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
