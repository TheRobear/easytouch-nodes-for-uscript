//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Camera", "")]
public class MultiCamTouch : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_13_System_String = " on camera : ";
   UnityEngine.GameObject local_21_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_21_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_22_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_22_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_24_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_24_UnityEngine_GameObject_previous = null;
   UnityEngine.Camera local_28_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.Camera local_29_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.Camera local_30_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.Camera local_35_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.GameObject local_36_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_36_UnityEngine_GameObject_previous = null;
   System.String local_9_System_String = "You touched : ";
   System.String local_ActionText_System_String = "";
   System.Boolean local_Cam02Toggle_System_Boolean = (bool) false;
   System.Boolean local_Cam03Toggle_System_Boolean = (bool) false;
   System.String local_CameraName_System_String = "";
   System.String local_CatResult01_System_String = "";
   System.String local_CatResult02_System_String = "";
   UnityEngine.Camera local_PickedCamera_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   System.String local_PickedObjectName_System_String = "";
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_2 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_2 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_2;
   bool logic_uScriptAct_GetGameObjectName_Out_2 = true;
   //pointer to script instanced logic node
   RN_GetCameraName logic_RN_GetCameraName_RN_GetCameraName_5 = new RN_GetCameraName( );
   UnityEngine.Camera logic_RN_GetCameraName_c_Camera_5 = default(UnityEngine.Camera);
   System.String logic_RN_GetCameraName_s_CameraName_5;
   bool logic_RN_GetCameraName_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_8 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_8 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_8 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_8 = "";
   System.String logic_uScriptAct_Concatenate_Result_8;
   bool logic_uScriptAct_Concatenate_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_12 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_12 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_12 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_12 = "";
   System.String logic_uScriptAct_Concatenate_Result_12;
   bool logic_uScriptAct_Concatenate_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_16 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_16 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_16 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_16 = "";
   System.String logic_uScriptAct_Concatenate_Result_16;
   bool logic_uScriptAct_Concatenate_Out_16 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_26 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_26 = true;
   bool logic_uScriptCon_CompareBool_False_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_32 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_32 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_32 = true;
   bool logic_uScriptCon_CompareBool_False_32 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_37 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_37 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_37 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_37 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_39 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_39 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_39 = "";
   bool logic_RN_SetUnityUIText_Out_39 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_40 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_40 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_40 = "";
   bool logic_RN_SetUnityUIText_Out_40 = true;
   //pointer to script instanced logic node
   EasyTouch_AddCamera logic_EasyTouch_AddCamera_EasyTouch_AddCamera_58 = new EasyTouch_AddCamera( );
   UnityEngine.Camera logic_EasyTouch_AddCamera_c_Camera_58 = default(UnityEngine.Camera);
   System.Boolean logic_EasyTouch_AddCamera_b_IsGUICamera_58 = (bool) false;
   bool logic_EasyTouch_AddCamera_Out_58 = true;
   //pointer to script instanced logic node
   EasyTouch_RemoveCamera logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_59 = new EasyTouch_RemoveCamera( );
   UnityEngine.Camera logic_EasyTouch_RemoveCamera_c_Camera_59 = default(UnityEngine.Camera);
   bool logic_EasyTouch_RemoveCamera_Out_59 = true;
   //pointer to script instanced logic node
   EasyTouch_AddCamera logic_EasyTouch_AddCamera_EasyTouch_AddCamera_60 = new EasyTouch_AddCamera( );
   UnityEngine.Camera logic_EasyTouch_AddCamera_c_Camera_60 = default(UnityEngine.Camera);
   System.Boolean logic_EasyTouch_AddCamera_b_IsGUICamera_60 = (bool) false;
   bool logic_EasyTouch_AddCamera_Out_60 = true;
   //pointer to script instanced logic node
   EasyTouch_RemoveCamera logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_61 = new EasyTouch_RemoveCamera( );
   UnityEngine.Camera logic_EasyTouch_RemoveCamera_c_Camera_61 = default(UnityEngine.Camera);
   bool logic_EasyTouch_RemoveCamera_Out_61 = true;
   
   //event nodes
   System.Boolean event_UnityEngine_GameObject_ToggleValue_23 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_ToggleValue_34 = (bool) false;
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_62 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_62 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_62 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_62 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_62 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_62 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_62 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_62 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_62 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_62 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_62 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_62 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_62 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_62 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_62 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_62 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_62 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_62 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_62 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_62 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_62 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_62 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_62 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_62 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_62 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_62 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_62 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_62 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_62 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_62 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_62 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_21_UnityEngine_GameObject = GameObject.Find( "Action_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_22_UnityEngine_GameObject = GameObject.Find( "Action_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_24_UnityEngine_GameObject = GameObject.Find( "Cam2_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_24_UnityEngine_GameObject_previous != local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_24_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_24_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_23;
               }
            }
         }
         
         local_24_UnityEngine_GameObject_previous = local_24_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_24_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_24_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_24_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_23;
               }
            }
         }
      }
      if ( null == local_28_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Camera02" );
         if ( null != gameObject )
         {
            local_28_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_29_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Camera02" );
         if ( null != gameObject )
         {
            local_29_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_30_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Camera03" );
         if ( null != gameObject )
         {
            local_30_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_35_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Camera03" );
         if ( null != gameObject )
         {
            local_35_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_36_UnityEngine_GameObject = GameObject.Find( "Cam3_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_36_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_36_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
               }
            }
         }
         
         local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_36_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_36_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_36_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_34;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_24_UnityEngine_GameObject_previous != local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_24_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_24_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_23;
               }
            }
         }
         
         local_24_UnityEngine_GameObject_previous = local_24_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_24_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_24_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_24_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_23;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_36_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_36_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
               }
            }
         }
         
         local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_36_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_36_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_36_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_34;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_62 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_62 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_62 )
         {
            {
               EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_62.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_62.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_62;
                  component.OnTouchDown += Instance_OnTouchDown_62;
                  component.OnTouchUp += Instance_OnTouchUp_62;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_24_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_24_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_23;
            }
         }
      }
      if ( null != local_36_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_36_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_62 )
      {
         {
            EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_62.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_62;
               component.OnTouchDown -= Instance_OnTouchDown_62;
               component.OnTouchUp -= Instance_OnTouchUp_62;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_2.SetParent(g);
      logic_RN_GetCameraName_RN_GetCameraName_5.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_8.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_12.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_16.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_32.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_39.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_40.SetParent(g);
      logic_EasyTouch_AddCamera_EasyTouch_AddCamera_58.SetParent(g);
      logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_59.SetParent(g);
      logic_EasyTouch_AddCamera_EasyTouch_AddCamera_60.SetParent(g);
      logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_61.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnToggleValueChanged_23(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_23 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_23( );
   }
   
   void Instance_OnToggleValueChanged_34(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_34 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_34( );
   }
   
   void Instance_OnTouchStart_62(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_62 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_62 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_62 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_62 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_62 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_62 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_62 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_62 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_62 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_62 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_62 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_62 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_62 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_62 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_62 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_62 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_62 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_62 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_62 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_62 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_62 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_62 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_62 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_62 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_62 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_62 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_62 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_62 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_62 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_62 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_62 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_62 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_62 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_62 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_62 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_62 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_62 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_62 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_62 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_62 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_62 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_62 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_62 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_62( );
   }
   
   void Instance_OnTouchDown_62(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_62 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_62 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_62 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_62 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_62 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_62 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_62 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_62 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_62 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_62 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_62 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_62 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_62 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_62 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_62 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_62 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_62 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_62 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_62 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_62 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_62 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_62 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_62 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_62 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_62 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_62 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_62 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_62 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_62 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_62 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_62 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_62 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_62 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_62 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_62 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_62 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_62 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_62 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_62 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_62 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_62 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_62 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_62 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_62( );
   }
   
   void Instance_OnTouchUp_62(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_62 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_62 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_62 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_62 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_62 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_62 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_62 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_62 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_62 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_62 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_62 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_62 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_62 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_62 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_62 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_62 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_62 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_62 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_62 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_62 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_62 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_62 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_62 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_62 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_62 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_62 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_62 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_62 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_62 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_62 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_62 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_62 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_62 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_62 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_62 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_62 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_62 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_62 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_62 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_62 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_62 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_62 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_62 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_62( );
   }
   
   void Relay_In_2()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetGameObjectName_gameObject_2 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_2.In(logic_uScriptAct_GetGameObjectName_gameObject_2, out logic_uScriptAct_GetGameObjectName_name_2);
      local_PickedObjectName_System_String = logic_uScriptAct_GetGameObjectName_name_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_5();
      }
   }
   
   void Relay_In_5()
   {
      {
         {
            logic_RN_GetCameraName_c_Camera_5 = local_PickedCamera_UnityEngine_Camera;
            
         }
         {
         }
      }
      logic_RN_GetCameraName_RN_GetCameraName_5.In(logic_RN_GetCameraName_c_Camera_5, out logic_RN_GetCameraName_s_CameraName_5);
      local_CameraName_System_String = logic_RN_GetCameraName_s_CameraName_5;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_GetCameraName_RN_GetCameraName_5.Out;
      
      if ( test_0 == true )
      {
         Relay_In_8();
      }
   }
   
   void Relay_In_8()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_8.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_8, index + 1);
            }
            logic_uScriptAct_Concatenate_A_8[ index++ ] = local_9_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_8.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_8, index + 1);
            }
            logic_uScriptAct_Concatenate_B_8[ index++ ] = local_PickedObjectName_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_8.In(logic_uScriptAct_Concatenate_A_8, logic_uScriptAct_Concatenate_B_8, logic_uScriptAct_Concatenate_Separator_8, out logic_uScriptAct_Concatenate_Result_8);
      local_CatResult01_System_String = logic_uScriptAct_Concatenate_Result_8;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_8.Out;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_12, index + 1);
            }
            logic_uScriptAct_Concatenate_A_12[ index++ ] = local_13_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_12, index + 1);
            }
            logic_uScriptAct_Concatenate_B_12[ index++ ] = local_CameraName_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_12.In(logic_uScriptAct_Concatenate_A_12, logic_uScriptAct_Concatenate_B_12, logic_uScriptAct_Concatenate_Separator_12, out logic_uScriptAct_Concatenate_Result_12);
      local_CatResult02_System_String = logic_uScriptAct_Concatenate_Result_12;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_12.Out;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_16.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_16, index + 1);
            }
            logic_uScriptAct_Concatenate_A_16[ index++ ] = local_CatResult01_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_16.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_16, index + 1);
            }
            logic_uScriptAct_Concatenate_B_16[ index++ ] = local_CatResult02_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_16.In(logic_uScriptAct_Concatenate_A_16, logic_uScriptAct_Concatenate_B_16, logic_uScriptAct_Concatenate_Separator_16, out logic_uScriptAct_Concatenate_Result_16);
      local_ActionText_System_String = logic_uScriptAct_Concatenate_Result_16;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_16.Out;
      
      if ( test_0 == true )
      {
         Relay_In_40();
      }
   }
   
   void Relay_OnToggleValueChanged_23()
   {
      local_Cam02Toggle_System_Boolean = event_UnityEngine_GameObject_ToggleValue_23;
      Relay_In_26();
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_26 = local_Cam02Toggle_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.In(logic_uScriptCon_CompareBool_Bool_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.False;
      
      if ( test_0 == true )
      {
         Relay_In_58();
      }
      if ( test_1 == true )
      {
         Relay_In_59();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_32 = local_Cam03Toggle_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_32.In(logic_uScriptCon_CompareBool_Bool_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_32.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_32.False;
      
      if ( test_0 == true )
      {
         Relay_In_60();
      }
      if ( test_1 == true )
      {
         Relay_In_61();
      }
   }
   
   void Relay_OnToggleValueChanged_34()
   {
      local_Cam03Toggle_System_Boolean = event_UnityEngine_GameObject_ToggleValue_34;
      Relay_In_32();
   }
   
   void Relay_In_37()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_37.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_37, index + 1);
            }
            logic_uScriptCon_IsNull_Target_37[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.In(logic_uScriptCon_IsNull_Target_37);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_37.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_In_39()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_39 = local_22_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_39.In(logic_RN_SetUnityUIText_go_GameObject_39, logic_RN_SetUnityUIText_s_Text_39);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_40()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_40 = local_21_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetUnityUIText_s_Text_40 = local_ActionText_System_String;
            
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_40.In(logic_RN_SetUnityUIText_go_GameObject_40, logic_RN_SetUnityUIText_s_Text_40);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_58()
   {
      {
         {
            logic_EasyTouch_AddCamera_c_Camera_58 = local_28_UnityEngine_Camera;
            
         }
         {
         }
      }
      logic_EasyTouch_AddCamera_EasyTouch_AddCamera_58.In(logic_EasyTouch_AddCamera_c_Camera_58, logic_EasyTouch_AddCamera_b_IsGUICamera_58);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_59()
   {
      {
         {
            logic_EasyTouch_RemoveCamera_c_Camera_59 = local_29_UnityEngine_Camera;
            
         }
      }
      logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_59.In(logic_EasyTouch_RemoveCamera_c_Camera_59);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_60()
   {
      {
         {
            logic_EasyTouch_AddCamera_c_Camera_60 = local_30_UnityEngine_Camera;
            
         }
         {
         }
      }
      logic_EasyTouch_AddCamera_EasyTouch_AddCamera_60.In(logic_EasyTouch_AddCamera_c_Camera_60, logic_EasyTouch_AddCamera_b_IsGUICamera_60);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_61()
   {
      {
         {
            logic_EasyTouch_RemoveCamera_c_Camera_61 = local_35_UnityEngine_Camera;
            
         }
      }
      logic_EasyTouch_RemoveCamera_EasyTouch_RemoveCamera_61.In(logic_EasyTouch_RemoveCamera_c_Camera_61);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart_62()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_62;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_PickedCamera_UnityEngine_Camera = event_UnityEngine_GameObject_c_PickedCamera_62;
   }
   
   void Relay_OnTouchDown_62()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_62;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_PickedCamera_UnityEngine_Camera = event_UnityEngine_GameObject_c_PickedCamera_62;
      Relay_In_37();
   }
   
   void Relay_OnTouchUp_62()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_62;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_PickedCamera_UnityEngine_Camera = event_UnityEngine_GameObject_c_PickedCamera_62;
      Relay_In_39();
   }
   
}
