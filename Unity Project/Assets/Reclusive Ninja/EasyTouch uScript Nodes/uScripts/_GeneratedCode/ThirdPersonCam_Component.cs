//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is the component script that you should assign to GameObjects to use this graph on them. Use the uScript/Graphs section of Unity's "Component" menu to assign this graph to a selected GameObject.

[AddComponentMenu("uScript/Graphs/ThirdPersonCam")]
public class ThirdPersonCam_Component : uScriptCode
{
   #pragma warning disable 414
   public ThirdPersonCam ExposedVariables = new ThirdPersonCam( ); 
   #pragma warning restore 414
   
   public System.Single DistanceUp { get { return ExposedVariables.DistanceUp; } set { ExposedVariables.DistanceUp = value; } } 
   public System.Single DistanceAway { get { return ExposedVariables.DistanceAway; } set { ExposedVariables.DistanceAway = value; } } 
   public System.Single Smooth { get { return ExposedVariables.Smooth; } set { ExposedVariables.Smooth = value; } } 
   
   void Awake( )
   {
      #if !(UNITY_FLASH)
      useGUILayout = false;
      #endif
      ExposedVariables.Awake( );
      ExposedVariables.SetParent( this.gameObject );
      if ( "1.PLE" != uScript_MasterComponent.Version )
      {
         uScriptDebug.Log( "The generated code is not compatible with your current uScript Runtime " + uScript_MasterComponent.Version, uScriptDebug.Type.Error );
         ExposedVariables = null;
         UnityEngine.Debug.Break();
      }
   }
   void Start( )
   {
      ExposedVariables.Start( );
   }
   void OnEnable( )
   {
      ExposedVariables.OnEnable( );
   }
   void OnDisable( )
   {
      ExposedVariables.OnDisable( );
   }
   void Update( )
   {
      ExposedVariables.Update( );
   }
   void OnDestroy( )
   {
      ExposedVariables.OnDestroy( );
   }
   void LateUpdate( )
   {
      ExposedVariables.LateUpdate( );
   }
   #if UNITY_EDITOR
      void OnDrawGizmos( )
      {
      }
   #endif
}
