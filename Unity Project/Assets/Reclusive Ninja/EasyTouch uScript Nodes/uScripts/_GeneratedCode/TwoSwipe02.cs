//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoSwipe02 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_10_System_String = " / Vector : ";
   System.String local_18_System_String = "/ Angle : ";
   UnityEngine.GameObject local_3_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_3_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_33_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_33_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_35_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_35_UnityEngine_GameObject_previous = null;
   System.String local_7_System_String = "Last Swipe : ";
   System.Single local_Angle_System_Single = (float) 0;
   System.String local_AngleFormatted_System_String = "";
   System.String local_Result01_System_String = "";
   System.String local_Result02_System_String = "";
   System.String local_Result03_System_String = "";
   System.String local_Result04_System_String = "";
   System.String local_Result05_System_String = "";
   System.String local_Swipe_System_String = "";
   UnityEngine.Vector2 local_SwipeVector_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_2 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_2 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_2 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_2 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_5 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_5 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_5 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_5 = "";
   System.String logic_uScriptAct_Concatenate_Result_5;
   bool logic_uScriptAct_Concatenate_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_9 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_9 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_9 = "";
   System.String logic_uScriptAct_Concatenate_Result_9;
   bool logic_uScriptAct_Concatenate_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_13 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_13 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_13 = "";
   System.String logic_uScriptAct_Concatenate_Result_13;
   bool logic_uScriptAct_Concatenate_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_17 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_17 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_17 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_17 = "";
   System.String logic_uScriptAct_Concatenate_Result_17;
   bool logic_uScriptAct_Concatenate_Out_17 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_19 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_19 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_19 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_19;
   bool logic_RN_FormatFloatString_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_25 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_25 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_25 = "";
   System.String logic_uScriptAct_Concatenate_Result_25;
   bool logic_uScriptAct_Concatenate_Out_25 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_32 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_32 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_32 = "You started a swipe.";
   bool logic_RN_SetTextMeshText_Out_32 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_34 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_34 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_34 = "";
   bool logic_RN_SetTextMeshText_Out_34 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_73 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_73 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_73 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_73 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_73 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_73 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_73 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_73 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_73 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_73 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_73 = new Vector2( );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_73 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_73 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_73 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_73 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_73 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_73 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_73 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_73 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_73 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_73 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_73 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_73 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_73 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_73 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_73 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_73 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_73 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_73 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_73 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_73 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_3_UnityEngine_GameObject = GameObject.Find( "Trail" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_33_UnityEngine_GameObject = GameObject.Find( "Swipe_Results" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_35_UnityEngine_GameObject = GameObject.Find( "Swipe_Results" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Swipe2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Swipe2Fingers>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart2Fingers += Instance_OnSwipeStart2Fingers_73;
                  component.OnSwipe2Fingers += Instance_OnSwipe2Fingers_73;
                  component.OnSwipeEnd2Fingers += Instance_OnSwipeEnd2Fingers_73;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Swipe2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Swipe2Fingers>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart2Fingers += Instance_OnSwipeStart2Fingers_73;
                  component.OnSwipe2Fingers += Instance_OnSwipe2Fingers_73;
                  component.OnSwipeEnd2Fingers += Instance_OnSwipeEnd2Fingers_73;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Swipe2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe2Fingers>();
            if ( null != component )
            {
               component.OnSwipeStart2Fingers -= Instance_OnSwipeStart2Fingers_73;
               component.OnSwipe2Fingers -= Instance_OnSwipe2Fingers_73;
               component.OnSwipeEnd2Fingers -= Instance_OnSwipeEnd2Fingers_73;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_5.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_17.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_19.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_32.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_34.SetParent(g);
      owner_Connection_0 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnSwipeStart2Fingers_73(object o, EasyTouch_On_Swipe2Fingers.OnSwipe2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_73 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_73 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_73 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_73 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_73 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_73 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_73 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_73 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_73 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_73 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_73 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_73 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_73 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_73 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_73 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_73 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_73 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_73 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_73 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_73 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_73 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_73 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_73 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_73 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_73 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_73 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_73 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_73 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_73 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_73 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_73 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_73 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_73 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_73 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_73 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_73 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_73 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_73 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_73 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_73 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_73 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_73 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_73 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart2Fingers_73( );
   }
   
   void Instance_OnSwipe2Fingers_73(object o, EasyTouch_On_Swipe2Fingers.OnSwipe2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_73 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_73 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_73 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_73 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_73 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_73 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_73 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_73 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_73 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_73 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_73 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_73 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_73 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_73 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_73 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_73 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_73 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_73 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_73 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_73 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_73 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_73 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_73 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_73 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_73 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_73 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_73 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_73 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_73 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_73 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_73 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_73 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_73 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_73 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_73 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_73 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_73 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_73 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_73 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_73 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_73 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_73 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_73 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe2Fingers_73( );
   }
   
   void Instance_OnSwipeEnd2Fingers_73(object o, EasyTouch_On_Swipe2Fingers.OnSwipe2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_73 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_73 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_73 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_73 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_73 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_73 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_73 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_73 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_73 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_73 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_73 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_73 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_73 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_73 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_73 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_73 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_73 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_73 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_73 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_73 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_73 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_73 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_73 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_73 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_73 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_73 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_73 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_73 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_73 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_73 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_73 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_73 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_73 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_73 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_73 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_73 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_73 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_73 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_73 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_73 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_73 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_73 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_73 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd2Fingers_73( );
   }
   
   void Relay_In_2()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_2.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_2, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_2[ index++ ] = local_3_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_2 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2.In(logic_uScriptAct_SetGameObjectPosition_Target_2, logic_uScriptAct_SetGameObjectPosition_Position_2, logic_uScriptAct_SetGameObjectPosition_AsOffset_2, logic_uScriptAct_SetGameObjectPosition_AsLocal_2);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_5()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_5.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_5, index + 1);
            }
            logic_uScriptAct_Concatenate_A_5[ index++ ] = local_7_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_5.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_5, index + 1);
            }
            logic_uScriptAct_Concatenate_B_5[ index++ ] = local_Swipe_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_5.In(logic_uScriptAct_Concatenate_A_5, logic_uScriptAct_Concatenate_B_5, logic_uScriptAct_Concatenate_Separator_5, out logic_uScriptAct_Concatenate_Result_5);
      local_Result01_System_String = logic_uScriptAct_Concatenate_Result_5;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_5.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_9, index + 1);
            }
            logic_uScriptAct_Concatenate_A_9[ index++ ] = local_10_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_9, index + 1);
            }
            logic_uScriptAct_Concatenate_B_9[ index++ ] = local_SwipeVector_UnityEngine_Vector2;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.In(logic_uScriptAct_Concatenate_A_9, logic_uScriptAct_Concatenate_B_9, logic_uScriptAct_Concatenate_Separator_9, out logic_uScriptAct_Concatenate_Result_9);
      local_Result02_System_String = logic_uScriptAct_Concatenate_Result_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_13.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_13, index + 1);
            }
            logic_uScriptAct_Concatenate_A_13[ index++ ] = local_Result01_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_13.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_13, index + 1);
            }
            logic_uScriptAct_Concatenate_B_13[ index++ ] = local_Result02_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.In(logic_uScriptAct_Concatenate_A_13, logic_uScriptAct_Concatenate_B_13, logic_uScriptAct_Concatenate_Separator_13, out logic_uScriptAct_Concatenate_Result_13);
      local_Result03_System_String = logic_uScriptAct_Concatenate_Result_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_19();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_17.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_17, index + 1);
            }
            logic_uScriptAct_Concatenate_A_17[ index++ ] = local_18_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_17.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_17, index + 1);
            }
            logic_uScriptAct_Concatenate_B_17[ index++ ] = local_AngleFormatted_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_17.In(logic_uScriptAct_Concatenate_A_17, logic_uScriptAct_Concatenate_B_17, logic_uScriptAct_Concatenate_Separator_17, out logic_uScriptAct_Concatenate_Result_17);
      local_Result04_System_String = logic_uScriptAct_Concatenate_Result_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_In_19()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_19 = local_Angle_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_19.In(logic_RN_FormatFloatString_f_Float_19, logic_RN_FormatFloatString_s_Modifier_19, out logic_RN_FormatFloatString_s_Result_19);
      local_AngleFormatted_System_String = logic_RN_FormatFloatString_s_Result_19;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_19.Out;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_25()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_25, index + 1);
            }
            logic_uScriptAct_Concatenate_A_25[ index++ ] = local_Result03_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_25, index + 1);
            }
            logic_uScriptAct_Concatenate_B_25[ index++ ] = local_Result04_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.In(logic_uScriptAct_Concatenate_A_25, logic_uScriptAct_Concatenate_B_25, logic_uScriptAct_Concatenate_Separator_25, out logic_uScriptAct_Concatenate_Result_25);
      local_Result05_System_String = logic_uScriptAct_Concatenate_Result_25;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.Out;
      
      if ( test_0 == true )
      {
         Relay_In_34();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_32 = local_33_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_32.In(logic_RN_SetTextMeshText_go_GameObject_32, logic_RN_SetTextMeshText_s_Text_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_34()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_34 = local_35_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_34 = local_Result05_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_34.In(logic_RN_SetTextMeshText_go_GameObject_34, logic_RN_SetTextMeshText_s_Text_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnSwipeStart2Fingers_73()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_73;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_73;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_73;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_73;
      Relay_In_32();
   }
   
   void Relay_OnSwipe2Fingers_73()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_73;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_73;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_73;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_73;
      Relay_In_2();
   }
   
   void Relay_OnSwipeEnd2Fingers_73()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_73;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_73;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_73;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_73;
      Relay_In_5();
   }
   
}
