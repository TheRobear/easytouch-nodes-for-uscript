//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class UIDragWindow : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_DeltaPosition_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Boolean local_Drag_System_Boolean = (bool) false;
   System.Int32 local_FingerID_System_Int32 = (int) 0;
   System.Int32 local_FingerIndex_System_Int32 = (int) 0;
   System.Boolean local_OverGUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_Parent_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Parent_UnityEngine_GameObject_previous = null;
   public UnityEngine.GameObject PickedUIElement = default(UnityEngine.GameObject);
   UnityEngine.GameObject PickedUIElement_previous = null;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_3 = null;
   UnityEngine.GameObject owner_Connection_13 = null;
   UnityEngine.GameObject owner_Connection_18 = null;
   UnityEngine.GameObject owner_Connection_25 = null;
   UnityEngine.GameObject owner_Connection_34 = null;
   UnityEngine.GameObject owner_Connection_38 = null;
   UnityEngine.GameObject owner_Connection_40 = null;
   UnityEngine.GameObject owner_Connection_46 = null;
   UnityEngine.GameObject owner_Connection_55 = null;
   UnityEngine.GameObject owner_Connection_60 = null;
   UnityEngine.GameObject owner_Connection_62 = null;
   UnityEngine.GameObject owner_Connection_68 = null;
   UnityEngine.GameObject owner_Connection_70 = null;
   UnityEngine.GameObject owner_Connection_71 = null;
   UnityEngine.GameObject owner_Connection_76 = null;
   UnityEngine.GameObject owner_Connection_78 = null;
   UnityEngine.GameObject owner_Connection_83 = null;
   UnityEngine.GameObject owner_Connection_88 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_4 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_4;
   bool logic_uScriptAct_SetBool_Out_4 = true;
   bool logic_uScriptAct_SetBool_SetTrue_4 = true;
   bool logic_uScriptAct_SetBool_SetFalse_4 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_6 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_6 = (int) -1;
   System.Int32 logic_uScriptAct_SetInt_Target_6;
   bool logic_uScriptAct_SetInt_Out_6 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_8 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_8 = true;
   bool logic_uScriptCon_CompareBool_False_8 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_10 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_10 = true;
   bool logic_uScriptCon_CompareBool_False_10 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_12 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_12 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_12 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_12 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_12 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_12 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_12 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_12 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_15 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_15 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_15 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_15 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_15 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_19 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_19 = (int) -1;
   bool logic_uScriptCon_CompareInt_GreaterThan_19 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_LessThan_19 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_21 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_21 = (int) 0;
   System.Int32 logic_uScriptAct_SetInt_Target_21;
   bool logic_uScriptAct_SetInt_Out_21 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_26 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_26 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_26 = (int) 0;
   bool logic_uScriptCon_CompareInt_GreaterThan_26 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_26 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_26 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_26 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_26 = true;
   bool logic_uScriptCon_CompareInt_LessThan_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_29 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_29 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_29 = true;
   bool logic_uScriptCon_CompareBool_False_29 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_31 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_31 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_31 = true;
   bool logic_uScriptCon_CompareBool_False_31 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_36 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_36 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_36 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_36 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_36 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_36 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_36 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_37 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_37 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_37 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_37 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_37 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_39 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_39 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_39 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_39 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_39 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_39 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_43 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_43 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_43 = (int) 0;
   bool logic_uScriptCon_CompareInt_GreaterThan_43 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_43 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_43 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_43 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_43 = true;
   bool logic_uScriptCon_CompareInt_LessThan_43 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_45 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_45 = (int) -1;
   System.Int32 logic_uScriptAct_SetInt_Target_45;
   bool logic_uScriptAct_SetInt_Out_45 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_50 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_50 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_50 = true;
   bool logic_uScriptCon_CompareBool_False_50 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_51 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_51 = true;
   bool logic_uScriptCon_CompareBool_False_51 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_57 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_57 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_57 = (int) -1;
   bool logic_uScriptCon_CompareInt_GreaterThan_57 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_57 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_57 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_57 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_57 = true;
   bool logic_uScriptCon_CompareInt_LessThan_57 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_58 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_58 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_58 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_58 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_58 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_58 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_58 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_58 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_59 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_59 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_59 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_59 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_59 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_63 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_63 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_63 = true;
   bool logic_uScriptCon_CompareBool_False_63 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_65 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_65 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_65 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_65 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_65 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_65 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_65 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_65 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_69 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_69 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_69 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_69 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_69 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_72 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_72 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_72 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_72 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_72 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_72 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_72 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_74 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_74 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_74 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_74 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_74 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_77 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_77 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_77 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_77 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_77 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_77 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_80 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_80;
   bool logic_uScriptAct_SetBool_Out_80 = true;
   bool logic_uScriptAct_SetBool_SetTrue_80 = true;
   bool logic_uScriptAct_SetBool_SetFalse_80 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_82 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_82 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_82 = true;
   bool logic_uScriptCon_CompareBool_False_82 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_84 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_84 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_84 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_84 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_84 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_84 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_84 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_84 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_86 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_86 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_86 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_86 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_86 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_89 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_89;
   bool logic_uScriptAct_SetBool_Out_89 = true;
   bool logic_uScriptAct_SetBool_SetTrue_89 = true;
   bool logic_uScriptAct_SetBool_SetFalse_89 = true;
   //pointer to script instanced logic node
   uScriptAct_GetParent logic_uScriptAct_GetParent_uScriptAct_GetParent_95 = new uScriptAct_GetParent( );
   UnityEngine.GameObject logic_uScriptAct_GetParent_Target_95 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptAct_GetParent_Parent_95;
   bool logic_uScriptAct_GetParent_Out_95 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_99 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_99;
   bool logic_uScriptAct_SetBool_Out_99 = true;
   bool logic_uScriptAct_SetBool_SetTrue_99 = true;
   bool logic_uScriptAct_SetBool_SetFalse_99 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_101 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_101 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_101 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_101 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_101 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_101 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_106 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_106 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_106 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_106 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_106 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_106 = true;
   //pointer to script instanced logic node
   uScriptAct_GetParent logic_uScriptAct_GetParent_uScriptAct_GetParent_107 = new uScriptAct_GetParent( );
   UnityEngine.GameObject logic_uScriptAct_GetParent_Target_107 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptAct_GetParent_Parent_107;
   bool logic_uScriptAct_GetParent_Out_107 = true;
   //pointer to script instanced logic node
   RN_SetAsLastSibling logic_RN_SetAsLastSibling_RN_SetAsLastSibling_109 = new RN_SetAsLastSibling( );
   UnityEngine.GameObject logic_RN_SetAsLastSibling_go_GameObject_109 = default(UnityEngine.GameObject);
   bool logic_RN_SetAsLastSibling_Out_109 = true;
   //pointer to script instanced logic node
   RN_SetAsLastSibling logic_RN_SetAsLastSibling_RN_SetAsLastSibling_110 = new RN_SetAsLastSibling( );
   UnityEngine.GameObject logic_RN_SetAsLastSibling_go_GameObject_110 = default(UnityEngine.GameObject);
   bool logic_RN_SetAsLastSibling_Out_110 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_153 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_153 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_153 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_153 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_153 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_153 = new Vector3( );
   System.Single event_UnityEngine_GameObject_f_ActionTime_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_153 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_153 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_153 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_153 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_153 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_153 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_153 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_153 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_153 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_153 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_153 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_153 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_153 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_153 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_153 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_153 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_153 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_153 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_153 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_153 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_153 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_153 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_153 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_153 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_259 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_259 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_259 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_259 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_259 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_259 = new Vector3( );
   System.Single event_UnityEngine_GameObject_f_ActionTime_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_259 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_259 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_259 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_259 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_259 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_259 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_259 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_259 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_259 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_259 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_259 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_259 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_259 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_259 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_259 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_259 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_259 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_259 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_259 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_259 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_259 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_259 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_259 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_259 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedUIElement_previous = PickedUIElement;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_153;
                  component.OnTouchDown += Instance_OnTouchDown_153;
                  component.OnTouchUp += Instance_OnTouchUp_153;
               }
            }
         }
      }
      if ( null == owner_Connection_3 || false == m_RegisteredForEvents )
      {
         owner_Connection_3 = parentGameObject;
         if ( null != owner_Connection_3 )
         {
            {
               uScript_Global component = owner_Connection_3.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_3.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_2;
                  component.uScriptLateStart += Instance_uScriptLateStart_2;
               }
            }
         }
      }
      if ( null == owner_Connection_13 || false == m_RegisteredForEvents )
      {
         owner_Connection_13 = parentGameObject;
      }
      if ( null == owner_Connection_18 || false == m_RegisteredForEvents )
      {
         owner_Connection_18 = parentGameObject;
      }
      if ( null == owner_Connection_25 || false == m_RegisteredForEvents )
      {
         owner_Connection_25 = parentGameObject;
      }
      if ( null == owner_Connection_34 || false == m_RegisteredForEvents )
      {
         owner_Connection_34 = parentGameObject;
      }
      if ( null == owner_Connection_38 || false == m_RegisteredForEvents )
      {
         owner_Connection_38 = parentGameObject;
      }
      if ( null == owner_Connection_40 || false == m_RegisteredForEvents )
      {
         owner_Connection_40 = parentGameObject;
      }
      if ( null == owner_Connection_46 || false == m_RegisteredForEvents )
      {
         owner_Connection_46 = parentGameObject;
         if ( null != owner_Connection_46 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_46.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_46.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_259;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_259;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_259;
               }
            }
         }
      }
      if ( null == owner_Connection_55 || false == m_RegisteredForEvents )
      {
         owner_Connection_55 = parentGameObject;
      }
      if ( null == owner_Connection_60 || false == m_RegisteredForEvents )
      {
         owner_Connection_60 = parentGameObject;
      }
      if ( null == owner_Connection_62 || false == m_RegisteredForEvents )
      {
         owner_Connection_62 = parentGameObject;
      }
      if ( null == owner_Connection_68 || false == m_RegisteredForEvents )
      {
         owner_Connection_68 = parentGameObject;
      }
      if ( null == owner_Connection_70 || false == m_RegisteredForEvents )
      {
         owner_Connection_70 = parentGameObject;
      }
      if ( null == owner_Connection_71 || false == m_RegisteredForEvents )
      {
         owner_Connection_71 = parentGameObject;
      }
      if ( null == owner_Connection_76 || false == m_RegisteredForEvents )
      {
         owner_Connection_76 = parentGameObject;
      }
      if ( null == owner_Connection_78 || false == m_RegisteredForEvents )
      {
         owner_Connection_78 = parentGameObject;
      }
      if ( null == owner_Connection_83 || false == m_RegisteredForEvents )
      {
         owner_Connection_83 = parentGameObject;
      }
      if ( null == owner_Connection_88 || false == m_RegisteredForEvents )
      {
         owner_Connection_88 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedUIElement_previous = PickedUIElement;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_153;
                  component.OnTouchDown += Instance_OnTouchDown_153;
                  component.OnTouchUp += Instance_OnTouchUp_153;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_3 )
         {
            {
               uScript_Global component = owner_Connection_3.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_3.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_2;
                  component.uScriptLateStart += Instance_uScriptLateStart_2;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_46 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_46.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_46.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_259;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_259;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_259;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_153;
               component.OnTouchDown -= Instance_OnTouchDown_153;
               component.OnTouchUp -= Instance_OnTouchUp_153;
            }
         }
      }
      if ( null != owner_Connection_3 )
      {
         {
            uScript_Global component = owner_Connection_3.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_2;
               component.uScriptLateStart -= Instance_uScriptLateStart_2;
            }
         }
      }
      if ( null != owner_Connection_46 )
      {
         {
            EasyTouch_On_Touch2Fingers component = owner_Connection_46.GetComponent<EasyTouch_On_Touch2Fingers>();
            if ( null != component )
            {
               component.OnTouchStart2Fingers -= Instance_OnTouchStart2Fingers_259;
               component.OnTouchDown2Fingers -= Instance_OnTouchDown2Fingers_259;
               component.OnTouchUp2Fingers -= Instance_OnTouchUp2Fingers_259;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_6.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_12.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_15.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_21.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_26.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_29.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_31.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_37.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_39.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_43.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_45.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_50.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_57.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_58.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_59.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_63.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_65.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_69.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_74.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_77.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_80.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_82.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_84.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_86.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_89.SetParent(g);
      logic_uScriptAct_GetParent_uScriptAct_GetParent_95.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_99.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_101.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_106.SetParent(g);
      logic_uScriptAct_GetParent_uScriptAct_GetParent_107.SetParent(g);
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_109.SetParent(g);
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_110.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_3 = parentGameObject;
      owner_Connection_13 = parentGameObject;
      owner_Connection_18 = parentGameObject;
      owner_Connection_25 = parentGameObject;
      owner_Connection_34 = parentGameObject;
      owner_Connection_38 = parentGameObject;
      owner_Connection_40 = parentGameObject;
      owner_Connection_46 = parentGameObject;
      owner_Connection_55 = parentGameObject;
      owner_Connection_60 = parentGameObject;
      owner_Connection_62 = parentGameObject;
      owner_Connection_68 = parentGameObject;
      owner_Connection_70 = parentGameObject;
      owner_Connection_71 = parentGameObject;
      owner_Connection_76 = parentGameObject;
      owner_Connection_78 = parentGameObject;
      owner_Connection_83 = parentGameObject;
      owner_Connection_88 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_uScriptStart_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_2( );
   }
   
   void Instance_uScriptLateStart_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_2( );
   }
   
   void Instance_OnTouchStart_153(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_153 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_153 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_153 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_153 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_153 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_153 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_153 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_153 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_153 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_153 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_153 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_153 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_153 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_153 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_153 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_153 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_153 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_153 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_153 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_153 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_153 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_153 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_153 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_153 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_153 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_153 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_153 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_153 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_153 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_153 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_153 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_153 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_153 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_153 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_153 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_153 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_153 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_153 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_153 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_153 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_153 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_153 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_153 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_153( );
   }
   
   void Instance_OnTouchDown_153(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_153 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_153 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_153 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_153 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_153 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_153 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_153 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_153 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_153 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_153 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_153 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_153 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_153 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_153 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_153 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_153 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_153 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_153 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_153 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_153 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_153 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_153 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_153 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_153 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_153 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_153 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_153 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_153 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_153 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_153 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_153 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_153 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_153 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_153 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_153 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_153 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_153 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_153 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_153 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_153 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_153 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_153 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_153 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_153( );
   }
   
   void Instance_OnTouchUp_153(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_153 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_153 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_153 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_153 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_153 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_153 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_153 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_153 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_153 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_153 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_153 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_153 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_153 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_153 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_153 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_153 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_153 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_153 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_153 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_153 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_153 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_153 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_153 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_153 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_153 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_153 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_153 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_153 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_153 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_153 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_153 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_153 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_153 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_153 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_153 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_153 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_153 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_153 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_153 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_153 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_153 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_153 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_153 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_153( );
   }
   
   void Instance_OnTouchStart2Fingers_259(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_259 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_259 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_259 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_259 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_259 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_259 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_259 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_259 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_259 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_259 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_259 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_259 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_259 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_259 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_259 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_259 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_259 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_259 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_259 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_259 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_259 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_259 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_259 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_259 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_259 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_259 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_259 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_259 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_259 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_259 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_259 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_259 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_259 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_259 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_259 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_259 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_259 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_259 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_259 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_259 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_259 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_259 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_259 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart2Fingers_259( );
   }
   
   void Instance_OnTouchDown2Fingers_259(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_259 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_259 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_259 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_259 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_259 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_259 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_259 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_259 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_259 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_259 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_259 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_259 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_259 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_259 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_259 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_259 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_259 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_259 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_259 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_259 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_259 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_259 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_259 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_259 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_259 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_259 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_259 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_259 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_259 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_259 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_259 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_259 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_259 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_259 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_259 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_259 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_259 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_259 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_259 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_259 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_259 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_259 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_259 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown2Fingers_259( );
   }
   
   void Instance_OnTouchUp2Fingers_259(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_259 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_259 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_259 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_259 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_259 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_259 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_259 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_259 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_259 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_259 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_259 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_259 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_259 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_259 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_259 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_259 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_259 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_259 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_259 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_259 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_259 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_259 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_259 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_259 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_259 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_259 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_259 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_259 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_259 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_259 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_259 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_259 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_259 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_259 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_259 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_259 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_259 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_259 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_259 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_259 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_259 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_259 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_259 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp2Fingers_259( );
   }
   
   void Relay_uScriptStart_2()
   {
      Relay_True_4();
   }
   
   void Relay_uScriptLateStart_2()
   {
   }
   
   void Relay_True_4()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.True(out logic_uScriptAct_SetBool_Target_4);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_4.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_False_4()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.False(out logic_uScriptAct_SetBool_Target_4);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_4.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_6.In(logic_uScriptAct_SetInt_Value_6, out logic_uScriptAct_SetInt_Target_6);
      local_FingerID_System_Int32 = logic_uScriptAct_SetInt_Target_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_8()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_8 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.In(logic_uScriptCon_CompareBool_Bool_8);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.True;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_In_10()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_10 = local_Drag_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.In(logic_uScriptCon_CompareBool_Bool_10);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.True;
      
      if ( test_0 == true )
      {
         Relay_In_12();
         Relay_In_15();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_12 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_12 = owner_Connection_13;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_12.In(logic_uScriptCon_CompareGameObjects_A_12, logic_uScriptCon_CompareGameObjects_B_12, logic_uScriptCon_CompareGameObjects_CompareByTag_12, logic_uScriptCon_CompareGameObjects_CompareByName_12, logic_uScriptCon_CompareGameObjects_ReportNull_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_12.Same;
      
      if ( test_0 == true )
      {
         Relay_In_19();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_15 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_15 = owner_Connection_18;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_15.In(logic_uScriptCon_IsChild_Child_15, logic_uScriptCon_IsChild_Parent_15);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_15.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_19();
      }
   }
   
   void Relay_In_19()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_19 = local_FingerID_System_Int32;
            
         }
         {
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.In(logic_uScriptCon_CompareInt_A_19, logic_uScriptCon_CompareInt_B_19);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_21();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            logic_uScriptAct_SetInt_Value_21 = local_FingerIndex_System_Int32;
            
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_21.In(logic_uScriptAct_SetInt_Value_21, out logic_uScriptAct_SetInt_Target_21);
      local_FingerID_System_Int32 = logic_uScriptAct_SetInt_Target_21;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetInt_uScriptAct_SetInt_21.Out;
      
      if ( test_0 == true )
      {
         Relay_In_109();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_26 = local_FingerID_System_Int32;
            
         }
         {
            logic_uScriptCon_CompareInt_B_26 = local_FingerIndex_System_Int32;
            
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_26.In(logic_uScriptCon_CompareInt_A_26, logic_uScriptCon_CompareInt_B_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_26.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_29 = local_Drag_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_29.In(logic_uScriptCon_CompareBool_Bool_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_29.True;
      
      if ( test_0 == true )
      {
         Relay_In_31();
      }
   }
   
   void Relay_In_31()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_31 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_31.In(logic_uScriptCon_CompareBool_Bool_31);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_31.True;
      
      if ( test_0 == true )
      {
         Relay_In_36();
         Relay_In_37();
      }
   }
   
   void Relay_In_36()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_36 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_36 = owner_Connection_34;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.In(logic_uScriptCon_CompareGameObjects_A_36, logic_uScriptCon_CompareGameObjects_B_36, logic_uScriptCon_CompareGameObjects_CompareByTag_36, logic_uScriptCon_CompareGameObjects_CompareByName_36, logic_uScriptCon_CompareGameObjects_ReportNull_36);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.Same;
      
      if ( test_0 == true )
      {
         Relay_In_39();
      }
   }
   
   void Relay_In_37()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_37 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_37 = owner_Connection_38;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_37.In(logic_uScriptCon_IsChild_Child_37, logic_uScriptCon_IsChild_Parent_37);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_37.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_107();
      }
   }
   
   void Relay_In_39()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_39.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_39, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_39[ index++ ] = owner_Connection_40;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_39 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_39.In(logic_uScriptAct_SetGameObjectPosition_Target_39, logic_uScriptAct_SetGameObjectPosition_Position_39, logic_uScriptAct_SetGameObjectPosition_AsOffset_39, logic_uScriptAct_SetGameObjectPosition_AsLocal_39);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_43()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_43 = local_FingerID_System_Int32;
            
         }
         {
            logic_uScriptCon_CompareInt_B_43 = local_FingerIndex_System_Int32;
            
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_43.In(logic_uScriptCon_CompareInt_A_43, logic_uScriptCon_CompareInt_B_43);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_43.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_In_45()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_45.In(logic_uScriptAct_SetInt_Value_45, out logic_uScriptAct_SetInt_Target_45);
      local_FingerID_System_Int32 = logic_uScriptAct_SetInt_Target_45;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_50()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_50 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_50.In(logic_uScriptCon_CompareBool_Bool_50);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_50.True;
      
      if ( test_0 == true )
      {
         Relay_In_51();
      }
   }
   
   void Relay_In_51()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_51 = local_Drag_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.In(logic_uScriptCon_CompareBool_Bool_51);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.True;
      
      if ( test_0 == true )
      {
         Relay_In_58();
         Relay_In_59();
      }
   }
   
   void Relay_In_57()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_57 = local_FingerID_System_Int32;
            
         }
         {
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_57.In(logic_uScriptCon_CompareInt_A_57, logic_uScriptCon_CompareInt_B_57);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_57.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_110();
      }
   }
   
   void Relay_In_58()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_58 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_58 = owner_Connection_55;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_58.In(logic_uScriptCon_CompareGameObjects_A_58, logic_uScriptCon_CompareGameObjects_B_58, logic_uScriptCon_CompareGameObjects_CompareByTag_58, logic_uScriptCon_CompareGameObjects_CompareByName_58, logic_uScriptCon_CompareGameObjects_ReportNull_58);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_58.Same;
      
      if ( test_0 == true )
      {
         Relay_In_57();
      }
   }
   
   void Relay_In_59()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_59 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_59 = owner_Connection_60;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_59.In(logic_uScriptCon_IsChild_Child_59, logic_uScriptCon_IsChild_Parent_59);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_59.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_57();
      }
   }
   
   void Relay_In_63()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_63 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_63.In(logic_uScriptCon_CompareBool_Bool_63);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_63.True;
      
      if ( test_0 == true )
      {
         Relay_In_65();
         Relay_In_69();
      }
   }
   
   void Relay_In_65()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_65 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_65 = owner_Connection_70;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_65.In(logic_uScriptCon_CompareGameObjects_A_65, logic_uScriptCon_CompareGameObjects_B_65, logic_uScriptCon_CompareGameObjects_CompareByTag_65, logic_uScriptCon_CompareGameObjects_CompareByName_65, logic_uScriptCon_CompareGameObjects_ReportNull_65);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_65.Same;
      
      if ( test_0 == true )
      {
         Relay_In_72();
      }
   }
   
   void Relay_In_69()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_69 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_69 = owner_Connection_68;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_69.In(logic_uScriptCon_IsChild_Child_69, logic_uScriptCon_IsChild_Parent_69);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_69.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_74();
      }
   }
   
   void Relay_In_72()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_72 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_72 = owner_Connection_71;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.In(logic_uScriptCon_CompareGameObjects_A_72, logic_uScriptCon_CompareGameObjects_B_72, logic_uScriptCon_CompareGameObjects_CompareByTag_72, logic_uScriptCon_CompareGameObjects_CompareByName_72, logic_uScriptCon_CompareGameObjects_ReportNull_72);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_72.Same;
      
      if ( test_0 == true )
      {
         Relay_In_77();
      }
   }
   
   void Relay_In_74()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_74 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_74 = owner_Connection_76;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_74.In(logic_uScriptCon_IsChild_Child_74, logic_uScriptCon_IsChild_Parent_74);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_74.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_95();
      }
   }
   
   void Relay_In_77()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_77.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_77, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_77[ index++ ] = owner_Connection_78;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_77 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_77.In(logic_uScriptAct_SetGameObjectPosition_Target_77, logic_uScriptAct_SetGameObjectPosition_Position_77, logic_uScriptAct_SetGameObjectPosition_AsOffset_77, logic_uScriptAct_SetGameObjectPosition_AsLocal_77);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_77.Out;
      
      if ( test_0 == true )
      {
         Relay_False_80();
      }
   }
   
   void Relay_True_80()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_80.True(out logic_uScriptAct_SetBool_Target_80);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_80;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_80()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_80.False(out logic_uScriptAct_SetBool_Target_80);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_80;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_82()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_82 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_82.In(logic_uScriptCon_CompareBool_Bool_82);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_82.True;
      
      if ( test_0 == true )
      {
         Relay_In_84();
         Relay_In_86();
      }
   }
   
   void Relay_In_84()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_84 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_84 = owner_Connection_83;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_84.In(logic_uScriptCon_CompareGameObjects_A_84, logic_uScriptCon_CompareGameObjects_B_84, logic_uScriptCon_CompareGameObjects_CompareByTag_84, logic_uScriptCon_CompareGameObjects_CompareByName_84, logic_uScriptCon_CompareGameObjects_ReportNull_84);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_84.Same;
      
      if ( test_0 == true )
      {
         Relay_True_89();
      }
   }
   
   void Relay_In_86()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_86 = PickedUIElement;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_86 = owner_Connection_88;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_86.In(logic_uScriptCon_IsChild_Child_86, logic_uScriptCon_IsChild_Parent_86);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_86.IsChild;
      
      if ( test_0 == true )
      {
         Relay_True_89();
      }
   }
   
   void Relay_True_89()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_89.True(out logic_uScriptAct_SetBool_Target_89);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_89;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_89()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_89.False(out logic_uScriptAct_SetBool_Target_89);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_89;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_95()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetParent_Target_95 = PickedUIElement;
            
         }
         {
         }
      }
      logic_uScriptAct_GetParent_uScriptAct_GetParent_95.In(logic_uScriptAct_GetParent_Target_95, out logic_uScriptAct_GetParent_Parent_95);
      local_Parent_UnityEngine_GameObject = logic_uScriptAct_GetParent_Parent_95;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetParent_uScriptAct_GetParent_95.Out;
      
      if ( test_0 == true )
      {
         Relay_In_101();
      }
   }
   
   void Relay_True_99()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_99.True(out logic_uScriptAct_SetBool_Target_99);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_99;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_99()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_99.False(out logic_uScriptAct_SetBool_Target_99);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_99;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_101()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_101.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_101, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_101[ index++ ] = local_Parent_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_101 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_101.In(logic_uScriptAct_SetGameObjectPosition_Target_101, logic_uScriptAct_SetGameObjectPosition_Position_101, logic_uScriptAct_SetGameObjectPosition_AsOffset_101, logic_uScriptAct_SetGameObjectPosition_AsLocal_101);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_101.Out;
      
      if ( test_0 == true )
      {
         Relay_False_99();
      }
   }
   
   void Relay_In_106()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_106.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_106, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_106[ index++ ] = local_Parent_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_106 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_106.In(logic_uScriptAct_SetGameObjectPosition_Target_106, logic_uScriptAct_SetGameObjectPosition_Position_106, logic_uScriptAct_SetGameObjectPosition_AsOffset_106, logic_uScriptAct_SetGameObjectPosition_AsLocal_106);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_107()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedUIElement_previous = PickedUIElement;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetParent_Target_107 = PickedUIElement;
            
         }
         {
         }
      }
      logic_uScriptAct_GetParent_uScriptAct_GetParent_107.In(logic_uScriptAct_GetParent_Target_107, out logic_uScriptAct_GetParent_Parent_107);
      local_Parent_UnityEngine_GameObject = logic_uScriptAct_GetParent_Parent_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetParent_uScriptAct_GetParent_107.Out;
      
      if ( test_0 == true )
      {
         Relay_In_106();
      }
   }
   
   void Relay_In_109()
   {
      {
         {
            logic_RN_SetAsLastSibling_go_GameObject_109 = owner_Connection_25;
            
         }
      }
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_109.In(logic_RN_SetAsLastSibling_go_GameObject_109);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_110()
   {
      {
         {
            logic_RN_SetAsLastSibling_go_GameObject_110 = owner_Connection_62;
            
         }
      }
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_110.In(logic_RN_SetAsLastSibling_go_GameObject_110);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart_153()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_153;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_153;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_153;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_153;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_8();
   }
   
   void Relay_OnTouchDown_153()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_153;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_153;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_153;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_153;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_26();
   }
   
   void Relay_OnTouchUp_153()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_153;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_153;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_153;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_153;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_43();
   }
   
   void Relay_OnTouchStart2Fingers_259()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_259;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_259;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_259;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_259;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_50();
   }
   
   void Relay_OnTouchDown2Fingers_259()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_259;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_259;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_259;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_259;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_63();
   }
   
   void Relay_OnTouchUp2Fingers_259()
   {
      local_FingerIndex_System_Int32 = event_UnityEngine_GameObject_i_FingerIndex_259;
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_259;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_259;
      PickedUIElement = event_UnityEngine_GameObject_go_PickedUIElement_259;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedUIElement_previous != PickedUIElement || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedUIElement_previous = PickedUIElement;
            
            //setup new listeners
         }
      }
      Relay_In_82();
   }
   
}
