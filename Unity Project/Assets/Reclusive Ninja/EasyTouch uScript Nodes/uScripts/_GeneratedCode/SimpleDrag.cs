//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class SimpleDrag : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_39_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_39_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_43_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_43_UnityEngine_GameObject_previous = null;
   System.Single local_Angle_System_Single = (float) 0;
   System.String local_DragText_System_String = "";
   UnityEngine.GameObject local_PickedOBject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedOBject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   System.String local_Result_System_String = "";
   System.String local_Swipe_System_String = "";
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_8 = null;
   UnityEngine.GameObject owner_Connection_13 = null;
   UnityEngine.GameObject owner_Connection_26 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_36 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_1 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_1;
   bool logic_uScriptAct_SetRandomColor_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_3 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_3 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_3 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_7 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_7 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_7 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_7 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_9 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_9 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_9 = " / ";
   System.String logic_uScriptAct_Concatenate_Result_9;
   bool logic_uScriptAct_Concatenate_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_SetString logic_uScriptAct_SetString_uScriptAct_SetString_11 = new uScriptAct_SetString( );
   System.String logic_uScriptAct_SetString_Value_11 = "Drag Me";
   System.Boolean logic_uScriptAct_SetString_ToUpperCase_11 = (bool) false;
   System.Boolean logic_uScriptAct_SetString_ToLowerCase_11 = (bool) false;
   System.Boolean logic_uScriptAct_SetString_TrimWhitespace_11 = (bool) false;
   System.String logic_uScriptAct_SetString_Target_11;
   bool logic_uScriptAct_SetString_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_12 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_12 = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_12 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_12 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_16 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_16 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_16 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_16;
   bool logic_RN_FormatFloatString_Out_16 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_21 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_21 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_21 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_21 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_24 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_24 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_24 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_24 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_24 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_28 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_28 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_28 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_28 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_28 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_28 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_28 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_28 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_31 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_31 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_31 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_31 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_32 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_32 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_32 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_32 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_32 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_32 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_32 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_32 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_33 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_33 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_33 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_33 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_37 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_37 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_37 = "";
   bool logic_RN_SetTextMeshText_Out_37 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_42 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_42 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_42 = "";
   bool logic_RN_SetTextMeshText_Out_42 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_98 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_98 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_98 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_98 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_98 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_98 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_98 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_98 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_98 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_98 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_98 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_98 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_98 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_98 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_98 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_98 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_98 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_98 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_98 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_98 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_98 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_98 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_98 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_98 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_98 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_98 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_98 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_98 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_98 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_98 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_39_UnityEngine_GameObject = GameObject.Find( "Sphere_Drag_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_43_UnityEngine_GameObject = GameObject.Find( "Sphere_Drag_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Drag component = owner_Connection_0.GetComponent<EasyTouch_On_Drag>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Drag>();
               }
               if ( null != component )
               {
                  component.OnDragStart += Instance_OnDragStart_98;
                  component.OnDrag += Instance_OnDrag_98;
                  component.OnDragEnd += Instance_OnDragEnd_98;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
      if ( null == owner_Connection_8 || false == m_RegisteredForEvents )
      {
         owner_Connection_8 = parentGameObject;
      }
      if ( null == owner_Connection_13 || false == m_RegisteredForEvents )
      {
         owner_Connection_13 = parentGameObject;
      }
      if ( null == owner_Connection_26 || false == m_RegisteredForEvents )
      {
         owner_Connection_26 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
      if ( null == owner_Connection_36 || false == m_RegisteredForEvents )
      {
         owner_Connection_36 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Drag component = owner_Connection_0.GetComponent<EasyTouch_On_Drag>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Drag>();
               }
               if ( null != component )
               {
                  component.OnDragStart += Instance_OnDragStart_98;
                  component.OnDrag += Instance_OnDrag_98;
                  component.OnDragEnd += Instance_OnDragEnd_98;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Drag component = owner_Connection_0.GetComponent<EasyTouch_On_Drag>();
            if ( null != component )
            {
               component.OnDragStart -= Instance_OnDragStart_98;
               component.OnDrag -= Instance_OnDrag_98;
               component.OnDragEnd -= Instance_OnDragEnd_98;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.SetParent(g);
      logic_uScriptAct_SetString_uScriptAct_SetString_11.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_16.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_28.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_32.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_33.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_37.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_42.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_8 = parentGameObject;
      owner_Connection_13 = parentGameObject;
      owner_Connection_26 = parentGameObject;
      owner_Connection_27 = parentGameObject;
      owner_Connection_36 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnDragStart_98(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_98 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_98 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_98 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_98 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_98 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_98 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_98 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_98 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_98 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_98 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_98 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_98 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_98 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_98 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_98 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_98 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_98 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_98 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_98 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_98 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_98 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_98 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_98 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_98 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_98 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_98 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_98 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_98 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_98 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_98 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_98 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_98 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_98 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_98 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_98 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_98 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_98 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_98 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_98 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_98 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_98 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_98 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_98 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragStart_98( );
   }
   
   void Instance_OnDrag_98(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_98 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_98 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_98 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_98 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_98 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_98 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_98 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_98 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_98 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_98 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_98 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_98 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_98 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_98 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_98 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_98 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_98 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_98 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_98 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_98 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_98 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_98 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_98 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_98 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_98 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_98 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_98 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_98 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_98 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_98 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_98 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_98 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_98 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_98 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_98 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_98 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_98 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_98 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_98 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_98 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_98 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_98 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_98 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDrag_98( );
   }
   
   void Instance_OnDragEnd_98(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_98 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_98 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_98 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_98 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_98 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_98 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_98 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_98 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_98 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_98 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_98 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_98 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_98 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_98 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_98 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_98 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_98 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_98 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_98 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_98 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_98 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_98 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_98 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_98 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_98 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_98 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_98 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_98 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_98 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_98 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_98 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_98 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_98 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_98 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_98 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_98 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_98 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_98 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_98 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_98 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_98 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_98 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_98 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragEnd_98( );
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.In(logic_uScriptAct_SetRandomColor_RedMin_1, logic_uScriptAct_SetRandomColor_RedMax_1, logic_uScriptAct_SetRandomColor_GreenMin_1, logic_uScriptAct_SetRandomColor_GreenMax_1, logic_uScriptAct_SetRandomColor_BlueMin_1, logic_uScriptAct_SetRandomColor_BlueMax_1, logic_uScriptAct_SetRandomColor_AlphaMin_1, logic_uScriptAct_SetRandomColor_AlphaMax_1, out logic_uScriptAct_SetRandomColor_TargetColor_1);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_3.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_3, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_3[ index++ ] = owner_Connection_4;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_3 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3.In(logic_uScriptAct_AssignMaterialColor_Target_3, logic_uScriptAct_AssignMaterialColor_MatColor_3, logic_uScriptAct_AssignMaterialColor_MatChannel_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_7()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_7, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_7[ index++ ] = owner_Connection_8;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_7 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.In(logic_uScriptAct_SetGameObjectPosition_Target_7, logic_uScriptAct_SetGameObjectPosition_Position_7, logic_uScriptAct_SetGameObjectPosition_AsOffset_7, logic_uScriptAct_SetGameObjectPosition_AsLocal_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_9, index + 1);
            }
            logic_uScriptAct_Concatenate_A_9[ index++ ] = local_Result_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_9, index + 1);
            }
            logic_uScriptAct_Concatenate_B_9[ index++ ] = local_Swipe_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.In(logic_uScriptAct_Concatenate_A_9, logic_uScriptAct_Concatenate_B_9, logic_uScriptAct_Concatenate_Separator_9, out logic_uScriptAct_Concatenate_Result_9);
      local_DragText_System_String = logic_uScriptAct_Concatenate_Result_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetString_uScriptAct_SetString_11.In(logic_uScriptAct_SetString_Value_11, logic_uScriptAct_SetString_ToUpperCase_11, logic_uScriptAct_SetString_ToLowerCase_11, logic_uScriptAct_SetString_TrimWhitespace_11, out logic_uScriptAct_SetString_Target_11);
      local_DragText_System_String = logic_uScriptAct_SetString_Target_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetString_uScriptAct_SetString_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_37();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_12, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_12[ index++ ] = owner_Connection_13;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.In(logic_uScriptAct_AssignMaterialColor_Target_12, logic_uScriptAct_AssignMaterialColor_MatColor_12, logic_uScriptAct_AssignMaterialColor_MatChannel_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_16()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_16 = local_Angle_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_16.In(logic_RN_FormatFloatString_f_Float_16, logic_RN_FormatFloatString_s_Modifier_16, out logic_RN_FormatFloatString_s_Result_16);
      local_Result_System_String = logic_RN_FormatFloatString_s_Result_16;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_16.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_21.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_21, index + 1);
            }
            logic_uScriptCon_IsNull_Target_21[ index++ ] = local_PickedOBject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.In(logic_uScriptCon_IsNull_Target_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_21.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_24 = local_PickedOBject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_24 = owner_Connection_26;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.In(logic_uScriptCon_CompareGameObjects_A_24, logic_uScriptCon_CompareGameObjects_B_24, logic_uScriptCon_CompareGameObjects_CompareByTag_24, logic_uScriptCon_CompareGameObjects_CompareByName_24, logic_uScriptCon_CompareGameObjects_ReportNull_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.Same;
      
      if ( test_0 == true )
      {
         Relay_In_1();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_28 = local_PickedOBject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_28 = owner_Connection_27;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_28.In(logic_uScriptCon_CompareGameObjects_A_28, logic_uScriptCon_CompareGameObjects_B_28, logic_uScriptCon_CompareGameObjects_CompareByTag_28, logic_uScriptCon_CompareGameObjects_CompareByName_28, logic_uScriptCon_CompareGameObjects_ReportNull_28);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_28.Same;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_31()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_31.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_31, index + 1);
            }
            logic_uScriptCon_IsNull_Target_31[ index++ ] = local_PickedOBject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.In(logic_uScriptCon_IsNull_Target_31);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_31.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_32 = local_PickedOBject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_32 = owner_Connection_36;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_32.In(logic_uScriptCon_CompareGameObjects_A_32, logic_uScriptCon_CompareGameObjects_B_32, logic_uScriptCon_CompareGameObjects_CompareByTag_32, logic_uScriptCon_CompareGameObjects_CompareByName_32, logic_uScriptCon_CompareGameObjects_ReportNull_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_32.Same;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_33()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_33.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_33, index + 1);
            }
            logic_uScriptCon_IsNull_Target_33[ index++ ] = local_PickedOBject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_33.In(logic_uScriptCon_IsNull_Target_33);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_33.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_32();
      }
   }
   
   void Relay_In_37()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_37 = local_39_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_37 = local_DragText_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_37.In(logic_RN_SetTextMeshText_go_GameObject_37, logic_RN_SetTextMeshText_s_Text_37);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetTextMeshText_RN_SetTextMeshText_37.Out;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_42()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_42 = local_43_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_42 = local_DragText_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_42.In(logic_RN_SetTextMeshText_go_GameObject_42, logic_RN_SetTextMeshText_s_Text_42);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnDragStart_98()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_98;
      local_PickedOBject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_98;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_98;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_98;
      Relay_In_21();
   }
   
   void Relay_OnDrag_98()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_98;
      local_PickedOBject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_98;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_98;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_98;
      Relay_In_31();
   }
   
   void Relay_OnDragEnd_98()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_98;
      local_PickedOBject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_98;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedOBject_UnityEngine_GameObject_previous != local_PickedOBject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedOBject_UnityEngine_GameObject_previous = local_PickedOBject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_98;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_98;
      Relay_In_33();
   }
   
}
