//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class UIPinchWindow : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_DeltaFloat_System_Single = (float) 0;
   System.Single local_DeltaPinch_System_Single = (float) 0;
   System.Single local_DeltaTime_System_Single = (float) 0;
   UnityEngine.Vector3 local_NewScale_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_NewX_System_Single = (float) 0;
   System.Single local_NewY_System_Single = (float) 0;
   System.Boolean local_OverGUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_PickedUIElement_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedUIElement_UnityEngine_GameObject_previous = null;
   System.Single local_ScaleX_System_Single = (float) 0;
   System.Single local_ScaleY_System_Single = (float) 0;
   System.Single local_ScaleZ_System_Single = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_8 = null;
   UnityEngine.GameObject owner_Connection_16 = null;
   UnityEngine.GameObject owner_Connection_34 = null;
   UnityEngine.GameObject owner_Connection_38 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_3 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_3 = true;
   bool logic_uScriptCon_CompareBool_False_3 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_6 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_6 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_6 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_6 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_6 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_9 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_9;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_9;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_9;
   bool logic_uScriptAct_GetDeltaTime_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_11 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_11 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_11;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_11;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectScale logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_15 = new uScriptAct_GetGameObjectScale( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectScale_Target_15 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_GetGameObjectScale_Scale_15;
   System.Single logic_uScriptAct_GetGameObjectScale_X_15;
   System.Single logic_uScriptAct_GetGameObjectScale_Y_15;
   System.Single logic_uScriptAct_GetGameObjectScale_Z_15;
   bool logic_uScriptAct_GetGameObjectScale_Out_15 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_20 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_20 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_20 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_20;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_20;
   bool logic_uScriptAct_AddFloat_v2_Out_20 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_24 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_24 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_24 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_24;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_24;
   bool logic_uScriptAct_AddFloat_v2_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_28 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_28 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_28 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_28 = true;
   //pointer to script instanced logic node
   RN_FloatsToVector3 logic_RN_FloatsToVector3_RN_FloatsToVector3_29 = new RN_FloatsToVector3( );
   System.Single logic_RN_FloatsToVector3_f_XValue_29 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_YValue_29 = (float) 0;
   System.Single logic_RN_FloatsToVector3_f_ZValue_29 = (float) 0;
   UnityEngine.Vector3 logic_RN_FloatsToVector3_v3_Result_29;
   bool logic_RN_FloatsToVector3_Out_29 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_36 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_36 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_36 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_36 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_36 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_36 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_36 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_85 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_85 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_85 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_85 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_85 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_85 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_85 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_85 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_85 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_85 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_85 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_85 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_85 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_85 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_85 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_85 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_85 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_85 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_85 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_85 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_85 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_85 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_85 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_85 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_85 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_85 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_85 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_85 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_85 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_85 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_85;
                  component.OnPinchIn += Instance_OnPinchIn_85;
                  component.OnPinchOut += Instance_OnPinchOut_85;
                  component.OnPinchEnd += Instance_OnPinchEnd_85;
               }
            }
         }
      }
      if ( null == owner_Connection_8 || false == m_RegisteredForEvents )
      {
         owner_Connection_8 = parentGameObject;
      }
      if ( null == owner_Connection_16 || false == m_RegisteredForEvents )
      {
         owner_Connection_16 = parentGameObject;
      }
      if ( null == owner_Connection_34 || false == m_RegisteredForEvents )
      {
         owner_Connection_34 = parentGameObject;
      }
      if ( null == owner_Connection_38 || false == m_RegisteredForEvents )
      {
         owner_Connection_38 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_85;
                  component.OnPinchIn += Instance_OnPinchIn_85;
                  component.OnPinchOut += Instance_OnPinchOut_85;
                  component.OnPinchEnd += Instance_OnPinchEnd_85;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Pinch component = owner_Connection_0.GetComponent<EasyTouch_On_Pinch>();
            if ( null != component )
            {
               component.OnPinch -= Instance_OnPinch_85;
               component.OnPinchIn -= Instance_OnPinchIn_85;
               component.OnPinchOut -= Instance_OnPinchOut_85;
               component.OnPinchEnd -= Instance_OnPinchEnd_85;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_6.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_9.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.SetParent(g);
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_15.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_20.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_24.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_28.SetParent(g);
      logic_RN_FloatsToVector3_RN_FloatsToVector3_29.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_8 = parentGameObject;
      owner_Connection_16 = parentGameObject;
      owner_Connection_34 = parentGameObject;
      owner_Connection_38 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnPinch_85(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_85 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_85 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_85 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_85 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_85 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_85 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_85 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_85 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_85 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_85 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_85 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_85 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_85 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_85 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_85 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_85 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_85 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_85 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_85 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_85 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_85 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_85 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_85 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_85 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_85 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_85 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_85 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_85 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_85 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_85 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_85 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_85 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_85 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_85 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_85 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_85 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_85 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_85 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_85 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_85 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_85 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_85 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_85 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinch_85( );
   }
   
   void Instance_OnPinchIn_85(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_85 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_85 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_85 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_85 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_85 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_85 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_85 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_85 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_85 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_85 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_85 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_85 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_85 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_85 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_85 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_85 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_85 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_85 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_85 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_85 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_85 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_85 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_85 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_85 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_85 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_85 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_85 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_85 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_85 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_85 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_85 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_85 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_85 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_85 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_85 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_85 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_85 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_85 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_85 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_85 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_85 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_85 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_85 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchIn_85( );
   }
   
   void Instance_OnPinchOut_85(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_85 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_85 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_85 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_85 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_85 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_85 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_85 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_85 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_85 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_85 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_85 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_85 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_85 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_85 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_85 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_85 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_85 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_85 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_85 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_85 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_85 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_85 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_85 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_85 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_85 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_85 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_85 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_85 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_85 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_85 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_85 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_85 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_85 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_85 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_85 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_85 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_85 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_85 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_85 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_85 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_85 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_85 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_85 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchOut_85( );
   }
   
   void Instance_OnPinchEnd_85(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_85 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_85 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_85 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_85 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_85 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_85 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_85 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_85 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_85 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_85 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_85 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_85 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_85 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_85 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_85 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_85 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_85 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_85 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_85 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_85 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_85 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_85 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_85 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_85 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_85 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_85 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_85 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_85 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_85 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_85 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_85 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_85 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_85 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_85 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_85 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_85 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_85 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_85 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_85 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_85 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_85 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_85 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_85 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchEnd_85( );
   }
   
   void Relay_In_3()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_3 = local_OverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.In(logic_uScriptCon_CompareBool_Bool_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.True;
      
      if ( test_0 == true )
      {
         Relay_In_6();
         Relay_In_36();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_6 = local_PickedUIElement_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_6 = owner_Connection_8;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_6.In(logic_uScriptCon_IsChild_Child_6, logic_uScriptCon_IsChild_Parent_6);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_6.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_9.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_9, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_9, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_9);
      local_DeltaTime_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_11 = local_DeltaPinch_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_11 = local_DeltaTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.In(logic_uScriptAct_MultiplyFloat_v2_A_11, logic_uScriptAct_MultiplyFloat_v2_B_11, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_11, out logic_uScriptAct_MultiplyFloat_v2_IntResult_11);
      local_DeltaFloat_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_15();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            logic_uScriptAct_GetGameObjectScale_Target_15 = owner_Connection_16;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_15.In(logic_uScriptAct_GetGameObjectScale_Target_15, out logic_uScriptAct_GetGameObjectScale_Scale_15, out logic_uScriptAct_GetGameObjectScale_X_15, out logic_uScriptAct_GetGameObjectScale_Y_15, out logic_uScriptAct_GetGameObjectScale_Z_15);
      local_ScaleX_System_Single = logic_uScriptAct_GetGameObjectScale_X_15;
      local_ScaleY_System_Single = logic_uScriptAct_GetGameObjectScale_Y_15;
      local_ScaleZ_System_Single = logic_uScriptAct_GetGameObjectScale_Z_15;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_15.Out;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_20 = local_ScaleX_System_Single;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_20 = local_DeltaFloat_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_20.In(logic_uScriptAct_AddFloat_v2_A_20, logic_uScriptAct_AddFloat_v2_B_20, out logic_uScriptAct_AddFloat_v2_FloatResult_20, out logic_uScriptAct_AddFloat_v2_IntResult_20);
      local_NewX_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_20;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_20.Out;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_24 = local_ScaleY_System_Single;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_24 = local_DeltaFloat_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_24.In(logic_uScriptAct_AddFloat_v2_A_24, logic_uScriptAct_AddFloat_v2_B_24, out logic_uScriptAct_AddFloat_v2_FloatResult_24, out logic_uScriptAct_AddFloat_v2_IntResult_24);
      local_NewY_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_24;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_24.Out;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectScale_Target_28.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_28, index + 1);
            }
            logic_uScriptAct_SetGameObjectScale_Target_28[ index++ ] = owner_Connection_34;
            
         }
         {
            logic_uScriptAct_SetGameObjectScale_Scale_28 = local_NewScale_UnityEngine_Vector3;
            
         }
      }
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_28.In(logic_uScriptAct_SetGameObjectScale_Target_28, logic_uScriptAct_SetGameObjectScale_Scale_28);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_29()
   {
      {
         {
            logic_RN_FloatsToVector3_f_XValue_29 = local_NewX_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_YValue_29 = local_NewY_System_Single;
            
         }
         {
            logic_RN_FloatsToVector3_f_ZValue_29 = local_ScaleZ_System_Single;
            
         }
         {
         }
      }
      logic_RN_FloatsToVector3_RN_FloatsToVector3_29.In(logic_RN_FloatsToVector3_f_XValue_29, logic_RN_FloatsToVector3_f_YValue_29, logic_RN_FloatsToVector3_f_ZValue_29, out logic_RN_FloatsToVector3_v3_Result_29);
      local_NewScale_UnityEngine_Vector3 = logic_RN_FloatsToVector3_v3_Result_29;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FloatsToVector3_RN_FloatsToVector3_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_In_36()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_36 = local_PickedUIElement_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_36 = owner_Connection_38;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.In(logic_uScriptCon_CompareGameObjects_A_36, logic_uScriptCon_CompareGameObjects_B_36, logic_uScriptCon_CompareGameObjects_CompareByTag_36, logic_uScriptCon_CompareGameObjects_CompareByName_36, logic_uScriptCon_CompareGameObjects_ReportNull_36);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_36.Same;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_OnPinch_85()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_85;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_85;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_85;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_3();
   }
   
   void Relay_OnPinchIn_85()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_85;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_85;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_85;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnPinchOut_85()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_85;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_85;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_85;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnPinchEnd_85()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_85;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_85;
      local_PickedUIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_85;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedUIElement_UnityEngine_GameObject_previous != local_PickedUIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedUIElement_UnityEngine_GameObject_previous = local_PickedUIElement_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
}
