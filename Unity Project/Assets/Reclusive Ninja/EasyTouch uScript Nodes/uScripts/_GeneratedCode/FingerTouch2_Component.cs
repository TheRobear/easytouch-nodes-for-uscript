//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is the component script that you should assign to GameObjects to use this graph on them. Use the uScript/Graphs section of Unity's "Component" menu to assign this graph to a selected GameObject.

[AddComponentMenu("uScript/Graphs/FingerTouch2")]
public class FingerTouch2_Component : uScriptCode
{
   #pragma warning disable 414
   public FingerTouch2 ExposedVariables = new FingerTouch2( ); 
   #pragma warning restore 414
   
   public System.String FingerIDText { get { return ExposedVariables.FingerIDText; } set { ExposedVariables.FingerIDText = value; } } 
   public System.Int32 TouchFingerIndex { get { return ExposedVariables.TouchFingerIndex; } set { ExposedVariables.TouchFingerIndex = value; } } 
   public UnityEngine.GameObject FingerIDObj { get { return ExposedVariables.FingerIDObj; } set { ExposedVariables.FingerIDObj = value; } } 
   public UnityEngine.Vector3 NewPos { get { return ExposedVariables.NewPos; } set { ExposedVariables.NewPos = value; } } 
   public UnityEngine.Vector3 SwipePos { get { return ExposedVariables.SwipePos; } set { ExposedVariables.SwipePos = value; } } 
   public UnityEngine.GameObject PickedObject { get { return ExposedVariables.PickedObject; } set { ExposedVariables.PickedObject = value; } } 
   public UnityEngine.Vector3 DeltaPos { get { return ExposedVariables.DeltaPos; } set { ExposedVariables.DeltaPos = value; } } 
   public System.String ObjectName { get { return ExposedVariables.ObjectName; } set { ExposedVariables.ObjectName = value; } } 
   
   void Awake( )
   {
      #if !(UNITY_FLASH)
      useGUILayout = false;
      #endif
      ExposedVariables.Awake( );
      ExposedVariables.SetParent( this.gameObject );
      if ( "1.PLE" != uScript_MasterComponent.Version )
      {
         uScriptDebug.Log( "The generated code is not compatible with your current uScript Runtime " + uScript_MasterComponent.Version, uScriptDebug.Type.Error );
         ExposedVariables = null;
         UnityEngine.Debug.Break();
      }
   }
   void Start( )
   {
      ExposedVariables.Start( );
   }
   void OnEnable( )
   {
      ExposedVariables.OnEnable( );
   }
   void OnDisable( )
   {
      ExposedVariables.OnDisable( );
   }
   void Update( )
   {
      ExposedVariables.Update( );
   }
   void OnDestroy( )
   {
      ExposedVariables.OnDestroy( );
   }
   #if UNITY_EDITOR
      void OnDrawGizmos( )
      {
      }
   #endif
}
