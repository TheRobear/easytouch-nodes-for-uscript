//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class GlobalETEvent : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   System.String local_13_System_String = " (from On_OverUIElement event.)";
   UnityEngine.GameObject local_15_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_15_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_2_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_2_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_20_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_20_UnityEngine_GameObject_previous = null;
   System.String local_22_System_String = "(from On_OverUIElement event.)";
   UnityEngine.GameObject local_30_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_30_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_37_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_37_UnityEngine_GameObject_previous = null;
   System.String local_8_System_String = "You touched UI Element : ";
   System.String local_Concat_System_String = "";
   System.Boolean local_IsOverGUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   System.Boolean local_UICompatability_System_Boolean = (bool) false;
   UnityEngine.GameObject local_UIElement_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_UIElement_UnityEngine_GameObject_previous = null;
   System.String local_UIElementName_System_String = "";
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_4 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_4 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_4;
   bool logic_uScriptAct_GetGameObjectName_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_7 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_7 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_7 = "";
   System.String logic_uScriptAct_Concatenate_Result_7;
   bool logic_uScriptAct_Concatenate_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_11 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_11 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_11 = "";
   System.String logic_uScriptAct_Concatenate_Result_11;
   bool logic_uScriptAct_Concatenate_Out_11 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_16 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_16 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_16 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_16 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_18 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_18 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_18 = "";
   System.String logic_uScriptAct_Concatenate_Result_18;
   bool logic_uScriptAct_Concatenate_Out_18 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_24 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_24 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_24;
   bool logic_uScriptAct_GetGameObjectName_Out_24 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_26 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_26 = true;
   bool logic_uScriptCon_CompareBool_False_26 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_29 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_29 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_29 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_29 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_31 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_31 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_31 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_31 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_33 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_33 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_33 = true;
   bool logic_uScriptCon_CompareBool_False_33 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_38 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_38 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_38 = true;
   bool logic_uScriptCon_CompareBool_False_38 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_41 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_41 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_41 = "";
   bool logic_RN_SetUnityUIText_Out_41 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_42 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_42 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_42 = "You touched an empty area.";
   bool logic_RN_SetUnityUIText_Out_42 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_43 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_43 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_43 = "You touched a 3D object.";
   bool logic_RN_SetUnityUIText_Out_43 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_44 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_44 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_44 = "";
   bool logic_RN_SetUnityUIText_Out_44 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_45 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_45 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_45 = "";
   bool logic_RN_SetUnityUIText_Out_45 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_47 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_47 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_47 = "";
   bool logic_RN_SetUnityUIText_Out_47 = true;
   //pointer to script instanced logic node
   EasyTouch_GetUICompatibily logic_EasyTouch_GetUICompatibily_EasyTouch_GetUICompatibily_114 = new EasyTouch_GetUICompatibily( );
   System.Boolean logic_EasyTouch_GetUICompatibily_b_UICompatibilyEnabled_114;
   bool logic_EasyTouch_GetUICompatibily_Out_114 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_107 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_107 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_107 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_107 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_107 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_107 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_107 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_107 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_107 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_107 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_107 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_107 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_107 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_107 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_107 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_107 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_107 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_107 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_107 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_107 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_107 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_107 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_107 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_107 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_107 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_107 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_107 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_107 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_107 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_107 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_107 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_118 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_118 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_118 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_118 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_118 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_118 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_118 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_118 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_118 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_118 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_118 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_118 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_118 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_118 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_118 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_118 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_118 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_118 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_118 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_118 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_118 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_118 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_118 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_118 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_118 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_118 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_118 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_118 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_118 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_118 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_2_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_15_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_20_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_UIElement_UnityEngine_GameObject_previous )
         {
            {
               EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
               if ( null != component )
               {
                  component.OnOverUIElement -= Instance_OnOverUIElement_118;
                  component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
               }
            }
         }
         
         local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_UIElement_UnityEngine_GameObject )
         {
            {
               EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
               if ( null == component )
               {
                  component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
               }
               if ( null != component )
               {
                  component.OnOverUIElement += Instance_OnOverUIElement_118;
                  component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
               }
            }
         }
      }
      if ( null == local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_30_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_37_UnityEngine_GameObject = GameObject.Find( "Display_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_UIElement_UnityEngine_GameObject_previous )
         {
            {
               EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
               if ( null != component )
               {
                  component.OnOverUIElement -= Instance_OnOverUIElement_118;
                  component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
               }
            }
         }
         
         local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_UIElement_UnityEngine_GameObject )
         {
            {
               EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
               if ( null == component )
               {
                  component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
               }
               if ( null != component )
               {
                  component.OnOverUIElement += Instance_OnOverUIElement_118;
                  component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_107 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_107 = GameObject.Find( "/_uScript" ) as UnityEngine.GameObject;
         if ( null != event_UnityEngine_GameObject_Instance_107 )
         {
            {
               EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_107.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_107.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_107;
                  component.OnTouchDown += Instance_OnTouchDown_107;
                  component.OnTouchUp += Instance_OnTouchUp_107;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_UIElement_UnityEngine_GameObject )
      {
         {
            EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
            if ( null != component )
            {
               component.OnOverUIElement -= Instance_OnOverUIElement_118;
               component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_107 )
      {
         {
            EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_107.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_107;
               component.OnTouchDown -= Instance_OnTouchDown_107;
               component.OnTouchUp -= Instance_OnTouchUp_107;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_4.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_16.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.SetParent(g);
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_24.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_29.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_33.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_38.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_41.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_42.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_43.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_44.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_45.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_47.SetParent(g);
      logic_EasyTouch_GetUICompatibily_EasyTouch_GetUICompatibily_114.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart_107(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_107 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_107 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_107 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_107 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_107 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_107 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_107 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_107 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_107 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_107 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_107 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_107 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_107 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_107 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_107 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_107 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_107 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_107 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_107 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_107 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_107 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_107 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_107 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_107 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_107 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_107 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_107 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_107 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_107 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_107 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_107 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_107 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_107 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_107 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_107 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_107 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_107 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_107 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_107 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_107 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_107 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_107 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_107 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_107( );
   }
   
   void Instance_OnTouchDown_107(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_107 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_107 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_107 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_107 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_107 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_107 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_107 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_107 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_107 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_107 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_107 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_107 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_107 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_107 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_107 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_107 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_107 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_107 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_107 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_107 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_107 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_107 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_107 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_107 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_107 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_107 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_107 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_107 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_107 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_107 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_107 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_107 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_107 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_107 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_107 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_107 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_107 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_107 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_107 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_107 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_107 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_107 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_107 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_107( );
   }
   
   void Instance_OnTouchUp_107(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_107 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_107 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_107 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_107 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_107 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_107 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_107 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_107 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_107 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_107 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_107 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_107 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_107 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_107 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_107 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_107 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_107 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_107 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_107 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_107 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_107 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_107 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_107 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_107 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_107 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_107 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_107 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_107 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_107 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_107 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_107 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_107 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_107 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_107 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_107 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_107 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_107 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_107 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_107 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_107 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_107 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_107 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_107 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_107( );
   }
   
   void Instance_OnOverUIElement_118(object o, EasyTouch_On_UIElement.OnUIElementEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_118 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_118 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_118 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_118 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_118 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_118 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_118 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_118 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_118 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_118 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_118 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_118 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_118 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_118 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_118 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_118 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_118 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_118 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_118 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_118 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_118 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_118 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_118 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_118 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_118 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_118 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_118 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_118 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_118 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_118 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_118 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_118 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_118 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_118 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_118 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_118 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_118 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_118 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_118 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_118 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_118 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_118 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_118 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnOverUIElement_118( );
   }
   
   void Instance_OnUIElementTouchUp_118(object o, EasyTouch_On_UIElement.OnUIElementEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_118 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_118 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_118 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_118 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_118 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_118 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_118 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_118 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_118 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_118 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_118 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_118 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_118 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_118 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_118 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_118 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_118 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_118 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_118 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_118 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_118 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_118 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_118 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_118 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_118 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_118 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_118 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_118 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_118 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_118 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_118 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_118 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_118 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_118 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_118 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_118 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_118 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_118 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_118 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_118 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_118 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_118 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_118 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnUIElementTouchUp_118( );
   }
   
   void Relay_In_4()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  if ( null != local_UIElement_UnityEngine_GameObject_previous )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                        if ( null != component )
                        {
                           component.OnOverUIElement -= Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
                  
                  local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
                  if ( null != local_UIElement_UnityEngine_GameObject )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                        if ( null == component )
                        {
                           component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                        }
                        if ( null != component )
                        {
                           component.OnOverUIElement += Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
               }
            }
            logic_uScriptAct_GetGameObjectName_gameObject_4 = local_UIElement_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_4.In(logic_uScriptAct_GetGameObjectName_gameObject_4, out logic_uScriptAct_GetGameObjectName_name_4);
      local_UIElementName_System_String = logic_uScriptAct_GetGameObjectName_name_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_4.Out;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_7()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_7, index + 1);
            }
            logic_uScriptAct_Concatenate_A_7[ index++ ] = local_8_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_7, index + 1);
            }
            logic_uScriptAct_Concatenate_B_7[ index++ ] = local_UIElementName_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.In(logic_uScriptAct_Concatenate_A_7, logic_uScriptAct_Concatenate_B_7, logic_uScriptAct_Concatenate_Separator_7, out logic_uScriptAct_Concatenate_Result_7);
      local_Concat_System_String = logic_uScriptAct_Concatenate_Result_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_11, index + 1);
            }
            logic_uScriptAct_Concatenate_A_11[ index++ ] = local_Concat_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_11, index + 1);
            }
            logic_uScriptAct_Concatenate_B_11[ index++ ] = local_13_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.In(logic_uScriptAct_Concatenate_A_11, logic_uScriptAct_Concatenate_B_11, logic_uScriptAct_Concatenate_Separator_11, out logic_uScriptAct_Concatenate_Result_11);
      local_Concat_System_String = logic_uScriptAct_Concatenate_Result_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  if ( null != local_UIElement_UnityEngine_GameObject_previous )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                        if ( null != component )
                        {
                           component.OnOverUIElement -= Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
                  
                  local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
                  if ( null != local_UIElement_UnityEngine_GameObject )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                        if ( null == component )
                        {
                           component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                        }
                        if ( null != component )
                        {
                           component.OnOverUIElement += Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
               }
            }
            if ( logic_uScriptCon_IsNull_Target_16.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_16, index + 1);
            }
            logic_uScriptCon_IsNull_Target_16[ index++ ] = local_UIElement_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_16.In(logic_uScriptCon_IsNull_Target_16);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_16.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_18, index + 1);
            }
            logic_uScriptAct_Concatenate_A_18[ index++ ] = local_UIElementName_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_18, index + 1);
            }
            logic_uScriptAct_Concatenate_B_18[ index++ ] = local_22_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.In(logic_uScriptAct_Concatenate_A_18, logic_uScriptAct_Concatenate_B_18, logic_uScriptAct_Concatenate_Separator_18, out logic_uScriptAct_Concatenate_Result_18);
      local_Concat_System_String = logic_uScriptAct_Concatenate_Result_18;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.Out;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  if ( null != local_UIElement_UnityEngine_GameObject_previous )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                        if ( null != component )
                        {
                           component.OnOverUIElement -= Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
                  
                  local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
                  
                  //setup new listeners
                  if ( null != local_UIElement_UnityEngine_GameObject )
                  {
                     {
                        EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                        if ( null == component )
                        {
                           component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                        }
                        if ( null != component )
                        {
                           component.OnOverUIElement += Instance_OnOverUIElement_118;
                           component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                        }
                     }
                  }
               }
            }
            logic_uScriptAct_GetGameObjectName_gameObject_24 = local_UIElement_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_24.In(logic_uScriptAct_GetGameObjectName_gameObject_24, out logic_uScriptAct_GetGameObjectName_name_24);
      local_UIElementName_System_String = logic_uScriptAct_GetGameObjectName_name_24;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_24.Out;
      
      if ( test_0 == true )
      {
         Relay_In_18();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_26 = local_IsOverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.In(logic_uScriptCon_CompareBool_Bool_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_26.False;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_29.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_29, index + 1);
            }
            logic_uScriptCon_IsNull_Target_29[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_29.In(logic_uScriptCon_IsNull_Target_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_29.IsNull;
      
      if ( test_0 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_In_31()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_31.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_31, index + 1);
            }
            logic_uScriptCon_IsNull_Target_31[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.In(logic_uScriptCon_IsNull_Target_31);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_31.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_33();
      }
   }
   
   void Relay_In_33()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_33 = local_IsOverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_33.In(logic_uScriptCon_CompareBool_Bool_33);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_33.False;
      
      if ( test_0 == true )
      {
         Relay_In_43();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_38 = local_UICompatability_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_38.In(logic_uScriptCon_CompareBool_Bool_38);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_38.True;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_41()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_41 = local_20_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetUnityUIText_s_Text_41 = local_Concat_System_String;
            
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_41.In(logic_RN_SetUnityUIText_go_GameObject_41, logic_RN_SetUnityUIText_s_Text_41);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_42()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_42 = local_30_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_42.In(logic_RN_SetUnityUIText_go_GameObject_42, logic_RN_SetUnityUIText_s_Text_42);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_43()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_43 = local_37_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_43.In(logic_RN_SetUnityUIText_go_GameObject_43, logic_RN_SetUnityUIText_s_Text_43);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_44()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_44 = local_2_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_44.In(logic_RN_SetUnityUIText_go_GameObject_44, logic_RN_SetUnityUIText_s_Text_44);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_45()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_45 = local_15_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetUnityUIText_s_Text_45 = local_Concat_System_String;
            
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_45.In(logic_RN_SetUnityUIText_go_GameObject_45, logic_RN_SetUnityUIText_s_Text_45);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_47()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_47 = local_1_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_47.In(logic_RN_SetUnityUIText_go_GameObject_47, logic_RN_SetUnityUIText_s_Text_47);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart_107()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_107;
      local_UIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_UIElement_UnityEngine_GameObject_previous )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                  if ( null != component )
                  {
                     component.OnOverUIElement -= Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                  }
               }
            }
            
            local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_UIElement_UnityEngine_GameObject )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                  if ( null == component )
                  {
                     component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                  }
                  if ( null != component )
                  {
                     component.OnOverUIElement += Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                  }
               }
            }
         }
      }
      Relay_In_114();
   }
   
   void Relay_OnTouchDown_107()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_107;
      local_UIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_UIElement_UnityEngine_GameObject_previous )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                  if ( null != component )
                  {
                     component.OnOverUIElement -= Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                  }
               }
            }
            
            local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_UIElement_UnityEngine_GameObject )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                  if ( null == component )
                  {
                     component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                  }
                  if ( null != component )
                  {
                     component.OnOverUIElement += Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                  }
               }
            }
         }
      }
      Relay_In_26();
      Relay_In_31();
   }
   
   void Relay_OnTouchUp_107()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_107;
      local_UIElement_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedUIElement_107;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_UIElement_UnityEngine_GameObject_previous != local_UIElement_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_UIElement_UnityEngine_GameObject_previous )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_UIElement>();
                  if ( null != component )
                  {
                     component.OnOverUIElement -= Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp -= Instance_OnUIElementTouchUp_118;
                  }
               }
            }
            
            local_UIElement_UnityEngine_GameObject_previous = local_UIElement_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_UIElement_UnityEngine_GameObject )
            {
               {
                  EasyTouch_On_UIElement component = local_UIElement_UnityEngine_GameObject.GetComponent<EasyTouch_On_UIElement>();
                  if ( null == component )
                  {
                     component = local_UIElement_UnityEngine_GameObject.AddComponent<EasyTouch_On_UIElement>();
                  }
                  if ( null != component )
                  {
                     component.OnOverUIElement += Instance_OnOverUIElement_118;
                     component.OnUIElementTouchUp += Instance_OnUIElementTouchUp_118;
                  }
               }
            }
         }
      }
      Relay_In_44();
   }
   
   void Relay_In_114()
   {
      {
         {
         }
      }
      logic_EasyTouch_GetUICompatibily_EasyTouch_GetUICompatibily_114.In(out logic_EasyTouch_GetUICompatibily_b_UICompatibilyEnabled_114);
      local_UICompatability_System_Boolean = logic_EasyTouch_GetUICompatibily_b_UICompatibilyEnabled_114;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_GetUICompatibily_EasyTouch_GetUICompatibily_114.Out;
      
      if ( test_0 == true )
      {
         Relay_In_38();
      }
   }
   
   void Relay_OnOverUIElement_118()
   {
      Relay_In_4();
   }
   
   void Relay_OnUIElementTouchUp_118()
   {
      Relay_In_47();
   }
   
}
