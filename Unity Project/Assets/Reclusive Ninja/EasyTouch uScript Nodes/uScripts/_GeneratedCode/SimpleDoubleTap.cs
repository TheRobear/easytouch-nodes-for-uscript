//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class SimpleDoubleTap : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_11 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_1 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_1;
   bool logic_uScriptAct_SetRandomColor_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_3 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_3 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_3 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_3 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_7 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_7 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_7 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_7 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_9 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_9 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_9 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_9 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_9 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_9 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_9 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_9 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_17 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_17 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_17 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_17 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_17 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_17 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_17 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_17 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_17 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_17 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_17 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_17 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_17 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_17 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_17 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_17 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_17 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_17 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_17 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_17 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_17 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_17 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_17 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_17 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_17 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_17 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_17 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_17 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_17 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_17 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_DoubleTap component = owner_Connection_0.GetComponent<EasyTouch_On_DoubleTap>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_DoubleTap>();
               }
               if ( null != component )
               {
                  component.OnDoubleTap += Instance_OnDoubleTap_17;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_DoubleTap component = owner_Connection_0.GetComponent<EasyTouch_On_DoubleTap>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_DoubleTap>();
               }
               if ( null != component )
               {
                  component.OnDoubleTap += Instance_OnDoubleTap_17;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_DoubleTap component = owner_Connection_0.GetComponent<EasyTouch_On_DoubleTap>();
            if ( null != component )
            {
               component.OnDoubleTap -= Instance_OnDoubleTap_17;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_7.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_9.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_11 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnDoubleTap_17(object o, EasyTouch_On_DoubleTap.OnDoubleTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_17 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_17 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_17 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_17 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_17 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_17 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_17 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_17 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_17 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_17 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_17 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_17 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_17 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_17 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_17 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_17 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_17 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_17 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_17 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_17 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_17 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_17 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_17 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_17 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_17 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_17 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_17 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_17 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_17 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_17 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_17 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_17 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_17 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_17 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_17 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_17 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_17 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_17 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_17 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_17 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_17 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_17 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_17 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDoubleTap_17( );
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.In(logic_uScriptAct_SetRandomColor_RedMin_1, logic_uScriptAct_SetRandomColor_RedMax_1, logic_uScriptAct_SetRandomColor_GreenMin_1, logic_uScriptAct_SetRandomColor_GreenMax_1, logic_uScriptAct_SetRandomColor_BlueMin_1, logic_uScriptAct_SetRandomColor_BlueMax_1, logic_uScriptAct_SetRandomColor_AlphaMin_1, logic_uScriptAct_SetRandomColor_AlphaMax_1, out logic_uScriptAct_SetRandomColor_TargetColor_1);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_3.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_3, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_3[ index++ ] = owner_Connection_4;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_3 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_3.In(logic_uScriptAct_AssignMaterialColor_Target_3, logic_uScriptAct_AssignMaterialColor_MatColor_3, logic_uScriptAct_AssignMaterialColor_MatChannel_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_7()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_7, index + 1);
            }
            logic_uScriptCon_IsNull_Target_7[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_7.In(logic_uScriptCon_IsNull_Target_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_7.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_9 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_9 = owner_Connection_11;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_9.In(logic_uScriptCon_CompareGameObjects_A_9, logic_uScriptCon_CompareGameObjects_B_9, logic_uScriptCon_CompareGameObjects_CompareByTag_9, logic_uScriptCon_CompareGameObjects_CompareByName_9, logic_uScriptCon_CompareGameObjects_ReportNull_9);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_9.Same;
      
      if ( test_0 == true )
      {
         Relay_In_1();
      }
   }
   
   void Relay_OnDoubleTap_17()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_17;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_7();
   }
   
}
