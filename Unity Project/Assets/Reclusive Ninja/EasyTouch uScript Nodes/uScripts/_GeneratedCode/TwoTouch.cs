//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoTouch : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_11_System_String = "Down since : ";
   UnityEngine.Color local_15_UnityEngine_Color = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.String local_18_System_String = "Touch Me";
   UnityEngine.GameObject local_37_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_37_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_38_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_38_UnityEngine_GameObject_previous = null;
   System.Single local_ActionTime_System_Single = (float) 0;
   System.String local_ActionTimeFormatted_System_String = "";
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   System.String local_Result_System_String = "";
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_17 = null;
   UnityEngine.GameObject owner_Connection_24 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_32 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_2 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_2;
   bool logic_uScriptAct_SetRandomColor_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_4 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_4 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_4 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_4 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_7 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_7 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_7 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_7;
   bool logic_RN_FormatFloatString_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_10 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_10 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_10 = "";
   System.String logic_uScriptAct_Concatenate_Result_10;
   bool logic_uScriptAct_Concatenate_Out_10 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_16 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_16 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_16 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_16 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_16 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_20 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_20 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_20 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_20 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_21 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_21 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_21 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_21 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_21 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_21 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_21 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_21 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_26 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_26 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_26 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_29 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_29 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_29 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_29 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_29 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_31 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_31 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_31 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_31 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_34 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_34 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_34 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_34 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_34 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_34 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_34 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_34 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_35 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_35 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_35 = "";
   bool logic_RN_SetTextMeshText_Out_35 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_36 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_36 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_36 = "";
   bool logic_RN_SetTextMeshText_Out_36 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_86 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_86 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_86 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_86 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_86 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_86 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_86 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_86 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_86 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_86 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_86 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_86 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_86 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_86 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_86 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_86 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_86 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_86 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_86 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_86 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_86 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_86 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_86 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_86 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_86 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_86 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_86 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_86 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_86 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_86 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_37_UnityEngine_GameObject = GameObject.Find( "Sphere_Touch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_38_UnityEngine_GameObject = GameObject.Find( "Sphere_Touch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_86;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_86;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_86;
               }
            }
         }
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
      }
      if ( null == owner_Connection_17 || false == m_RegisteredForEvents )
      {
         owner_Connection_17 = parentGameObject;
      }
      if ( null == owner_Connection_24 || false == m_RegisteredForEvents )
      {
         owner_Connection_24 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
      if ( null == owner_Connection_32 || false == m_RegisteredForEvents )
      {
         owner_Connection_32 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_86;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_86;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_86;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Touch2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_Touch2Fingers>();
            if ( null != component )
            {
               component.OnTouchStart2Fingers -= Instance_OnTouchStart2Fingers_86;
               component.OnTouchDown2Fingers -= Instance_OnTouchDown2Fingers_86;
               component.OnTouchUp2Fingers -= Instance_OnTouchUp2Fingers_86;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_7.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_16.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_20.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_21.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_34.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_35.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_36.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_17 = parentGameObject;
      owner_Connection_24 = parentGameObject;
      owner_Connection_27 = parentGameObject;
      owner_Connection_32 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart2Fingers_86(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_86 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_86 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_86 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_86 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_86 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_86 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_86 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_86 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_86 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_86 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_86 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_86 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_86 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_86 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_86 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_86 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_86 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_86 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_86 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_86 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_86 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_86 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_86 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_86 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_86 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_86 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_86 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_86 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_86 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_86 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_86 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_86 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_86 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_86 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_86 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_86 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_86 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_86 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_86 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_86 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_86 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_86 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_86 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart2Fingers_86( );
   }
   
   void Instance_OnTouchDown2Fingers_86(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_86 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_86 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_86 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_86 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_86 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_86 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_86 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_86 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_86 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_86 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_86 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_86 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_86 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_86 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_86 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_86 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_86 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_86 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_86 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_86 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_86 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_86 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_86 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_86 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_86 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_86 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_86 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_86 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_86 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_86 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_86 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_86 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_86 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_86 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_86 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_86 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_86 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_86 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_86 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_86 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_86 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_86 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_86 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown2Fingers_86( );
   }
   
   void Instance_OnTouchUp2Fingers_86(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_86 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_86 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_86 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_86 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_86 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_86 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_86 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_86 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_86 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_86 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_86 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_86 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_86 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_86 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_86 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_86 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_86 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_86 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_86 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_86 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_86 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_86 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_86 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_86 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_86 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_86 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_86 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_86 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_86 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_86 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_86 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_86 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_86 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_86 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_86 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_86 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_86 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_86 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_86 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_86 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_86 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_86 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_86 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp2Fingers_86( );
   }
   
   void Relay_In_2()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.In(logic_uScriptAct_SetRandomColor_RedMin_2, logic_uScriptAct_SetRandomColor_RedMax_2, logic_uScriptAct_SetRandomColor_GreenMin_2, logic_uScriptAct_SetRandomColor_GreenMax_2, logic_uScriptAct_SetRandomColor_BlueMin_2, logic_uScriptAct_SetRandomColor_BlueMax_2, logic_uScriptAct_SetRandomColor_AlphaMin_2, logic_uScriptAct_SetRandomColor_AlphaMax_2, out logic_uScriptAct_SetRandomColor_TargetColor_2);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_4.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_4, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_4[ index++ ] = owner_Connection_5;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_4 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.In(logic_uScriptAct_AssignMaterialColor_Target_4, logic_uScriptAct_AssignMaterialColor_MatColor_4, logic_uScriptAct_AssignMaterialColor_MatChannel_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_7()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_7 = local_ActionTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_7.In(logic_RN_FormatFloatString_f_Float_7, logic_RN_FormatFloatString_s_Modifier_7, out logic_RN_FormatFloatString_s_Result_7);
      local_ActionTimeFormatted_System_String = logic_RN_FormatFloatString_s_Result_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_In_10()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_10, index + 1);
            }
            logic_uScriptAct_Concatenate_A_10[ index++ ] = local_11_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_10, index + 1);
            }
            logic_uScriptAct_Concatenate_B_10[ index++ ] = local_ActionTimeFormatted_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.In(logic_uScriptAct_Concatenate_A_10, logic_uScriptAct_Concatenate_B_10, logic_uScriptAct_Concatenate_Separator_10, out logic_uScriptAct_Concatenate_Result_10);
      local_Result_System_String = logic_uScriptAct_Concatenate_Result_10;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.Out;
      
      if ( test_0 == true )
      {
         Relay_In_36();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_16.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_16, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_16[ index++ ] = owner_Connection_17;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_16 = local_15_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_16.In(logic_uScriptAct_AssignMaterialColor_Target_16, logic_uScriptAct_AssignMaterialColor_MatColor_16, logic_uScriptAct_AssignMaterialColor_MatChannel_16);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_16.Out;
      
      if ( test_0 == true )
      {
         Relay_In_35();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_20.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_20, index + 1);
            }
            logic_uScriptCon_IsNull_Target_20[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_20.In(logic_uScriptCon_IsNull_Target_20);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_20.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_21();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_21 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_21 = owner_Connection_24;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_21.In(logic_uScriptCon_CompareGameObjects_A_21, logic_uScriptCon_CompareGameObjects_B_21, logic_uScriptCon_CompareGameObjects_CompareByTag_21, logic_uScriptCon_CompareGameObjects_CompareByName_21, logic_uScriptCon_CompareGameObjects_ReportNull_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_21.Same;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_26.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_26, index + 1);
            }
            logic_uScriptCon_IsNull_Target_26[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.In(logic_uScriptCon_IsNull_Target_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_26.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_29 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_29 = owner_Connection_27;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.In(logic_uScriptCon_CompareGameObjects_A_29, logic_uScriptCon_CompareGameObjects_B_29, logic_uScriptCon_CompareGameObjects_CompareByTag_29, logic_uScriptCon_CompareGameObjects_CompareByName_29, logic_uScriptCon_CompareGameObjects_ReportNull_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.Same;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_31()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_31.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_31, index + 1);
            }
            logic_uScriptCon_IsNull_Target_31[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_31.In(logic_uScriptCon_IsNull_Target_31);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_31.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_34();
      }
   }
   
   void Relay_In_34()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_34 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_34 = owner_Connection_32;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_34.In(logic_uScriptCon_CompareGameObjects_A_34, logic_uScriptCon_CompareGameObjects_B_34, logic_uScriptCon_CompareGameObjects_CompareByTag_34, logic_uScriptCon_CompareGameObjects_CompareByName_34, logic_uScriptCon_CompareGameObjects_ReportNull_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_34.Same;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_35()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_35 = local_37_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_35 = local_18_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_35.In(logic_RN_SetTextMeshText_go_GameObject_35, logic_RN_SetTextMeshText_s_Text_35);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_36()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_36 = local_38_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_36 = local_Result_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_36.In(logic_RN_SetTextMeshText_go_GameObject_36, logic_RN_SetTextMeshText_s_Text_36);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart2Fingers_86()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_86;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_86;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_20();
   }
   
   void Relay_OnTouchDown2Fingers_86()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_86;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_86;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_26();
   }
   
   void Relay_OnTouchUp2Fingers_86()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_86;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_86;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_31();
   }
   
}
