//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class SimpleSwipe : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_12_System_String = " / Vector : ";
   System.String local_23_System_String = " / Angle : ";
   UnityEngine.GameObject local_37_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_37_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_38_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_38_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_4_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_4_UnityEngine_GameObject_previous = null;
   System.String local_8_System_String = "Last Swipe: ";
   System.Single local_Angle_System_Single = (float) 0;
   System.String local_Result01_System_String = "";
   System.String local_Result02_System_String = "";
   System.String local_Result03_System_String = "";
   System.String local_Result04_System_String = "";
   System.String local_Result05_System_String = "";
   System.String local_Result06_System_String = "";
   System.String local_Swipe_System_String = "";
   System.String local_SwipeResults_System_String = "";
   UnityEngine.Vector2 local_SwipeVector_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector2 local_SwipeVectorNormalized_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_1 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_1 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_1 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_1 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_1 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_SetString logic_uScriptAct_SetString_uScriptAct_SetString_6 = new uScriptAct_SetString( );
   System.String logic_uScriptAct_SetString_Value_6 = "You start a swipe.";
   System.Boolean logic_uScriptAct_SetString_ToUpperCase_6 = (bool) false;
   System.Boolean logic_uScriptAct_SetString_ToLowerCase_6 = (bool) false;
   System.Boolean logic_uScriptAct_SetString_TrimWhitespace_6 = (bool) false;
   System.String logic_uScriptAct_SetString_Target_6;
   bool logic_uScriptAct_SetString_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_7 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_7 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_7 = "";
   System.String logic_uScriptAct_Concatenate_Result_7;
   bool logic_uScriptAct_Concatenate_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_11 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_11 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_11 = "";
   System.String logic_uScriptAct_Concatenate_Result_11;
   bool logic_uScriptAct_Concatenate_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_NormalizeVector2 logic_uScriptAct_NormalizeVector2_uScriptAct_NormalizeVector2_13 = new uScriptAct_NormalizeVector2( );
   UnityEngine.Vector2 logic_uScriptAct_NormalizeVector2_Target_13 = new Vector2( );
   UnityEngine.Vector2 logic_uScriptAct_NormalizeVector2_NormalizedVector_13;
   bool logic_uScriptAct_NormalizeVector2_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_18 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_18 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_18 = "";
   System.String logic_uScriptAct_Concatenate_Result_18;
   bool logic_uScriptAct_Concatenate_Out_18 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_22 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_22 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_22 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_22 = "";
   System.String logic_uScriptAct_Concatenate_Result_22;
   bool logic_uScriptAct_Concatenate_Out_22 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_25 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_25 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_25 = "";
   System.String logic_uScriptAct_Concatenate_Result_25;
   bool logic_uScriptAct_Concatenate_Out_25 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_29 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_29 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_29 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_29;
   bool logic_RN_FormatFloatString_Out_29 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_34 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_34 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_34 = "";
   bool logic_RN_SetTextMeshText_Out_34 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_39 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_39 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_39 = "";
   bool logic_RN_SetTextMeshText_Out_39 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_88 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_88 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_88 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_88 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_88 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_88 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_88 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_88 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_88 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_88 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_88 = new Vector2( );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_88 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_88 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_88 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_88 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_88 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_88 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_88 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_88 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_88 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_88 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_88 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_88 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_88 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_88 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_88 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_88 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_88 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_88 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_88 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_88 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_4_UnityEngine_GameObject = GameObject.Find( "Trail" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_37_UnityEngine_GameObject = GameObject.Find( "Sphere_Swipe_Results_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_38_UnityEngine_GameObject = GameObject.Find( "Sphere_Swipe_Results_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_88;
                  component.OnSwipe += Instance_OnSwipe_88;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_88;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_88;
                  component.OnSwipe += Instance_OnSwipe_88;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_88;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Swipe component = owner_Connection_0.GetComponent<EasyTouch_On_Swipe>();
            if ( null != component )
            {
               component.OnSwipeStart -= Instance_OnSwipeStart_88;
               component.OnSwipe -= Instance_OnSwipe_88;
               component.OnSwipeEnd -= Instance_OnSwipeEnd_88;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_1.SetParent(g);
      logic_uScriptAct_SetString_uScriptAct_SetString_6.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.SetParent(g);
      logic_uScriptAct_NormalizeVector2_uScriptAct_NormalizeVector2_13.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_22.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_29.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_34.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_39.SetParent(g);
      owner_Connection_0 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnSwipeStart_88(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_88 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_88 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_88 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_88 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_88 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_88 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_88 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_88 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_88 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_88 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_88 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_88 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_88 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_88 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_88 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_88 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_88 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_88 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_88 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_88 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_88 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_88 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_88 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_88 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_88 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_88 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_88 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_88 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_88 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_88 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_88 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_88 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_88 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_88 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_88 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_88 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_88 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_88 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_88 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_88 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_88 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_88 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_88 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart_88( );
   }
   
   void Instance_OnSwipe_88(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_88 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_88 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_88 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_88 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_88 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_88 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_88 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_88 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_88 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_88 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_88 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_88 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_88 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_88 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_88 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_88 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_88 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_88 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_88 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_88 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_88 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_88 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_88 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_88 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_88 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_88 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_88 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_88 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_88 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_88 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_88 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_88 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_88 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_88 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_88 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_88 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_88 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_88 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_88 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_88 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_88 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_88 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_88 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe_88( );
   }
   
   void Instance_OnSwipeEnd_88(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_88 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_88 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_88 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_88 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_88 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_88 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_88 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_88 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_88 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_88 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_88 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_88 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_88 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_88 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_88 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_88 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_88 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_88 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_88 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_88 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_88 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_88 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_88 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_88 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_88 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_88 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_88 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_88 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_88 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_88 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_88 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_88 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_88 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_88 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_88 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_88 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_88 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_88 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_88 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_88 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_88 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_88 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_88 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd_88( );
   }
   
   void Relay_In_1()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_1.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_1, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_1[ index++ ] = local_4_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_1 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_1.In(logic_uScriptAct_SetGameObjectPosition_Target_1, logic_uScriptAct_SetGameObjectPosition_Position_1, logic_uScriptAct_SetGameObjectPosition_AsOffset_1, logic_uScriptAct_SetGameObjectPosition_AsLocal_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_6()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetString_uScriptAct_SetString_6.In(logic_uScriptAct_SetString_Value_6, logic_uScriptAct_SetString_ToUpperCase_6, logic_uScriptAct_SetString_ToLowerCase_6, logic_uScriptAct_SetString_TrimWhitespace_6, out logic_uScriptAct_SetString_Target_6);
      local_SwipeResults_System_String = logic_uScriptAct_SetString_Target_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetString_uScriptAct_SetString_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_34();
      }
   }
   
   void Relay_In_7()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_7, index + 1);
            }
            logic_uScriptAct_Concatenate_A_7[ index++ ] = local_8_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_7, index + 1);
            }
            logic_uScriptAct_Concatenate_B_7[ index++ ] = local_Swipe_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.In(logic_uScriptAct_Concatenate_A_7, logic_uScriptAct_Concatenate_B_7, logic_uScriptAct_Concatenate_Separator_7, out logic_uScriptAct_Concatenate_Result_7);
      local_Result01_System_String = logic_uScriptAct_Concatenate_Result_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_11, index + 1);
            }
            logic_uScriptAct_Concatenate_A_11[ index++ ] = local_12_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_11, index + 1);
            }
            logic_uScriptAct_Concatenate_B_11[ index++ ] = local_SwipeVectorNormalized_UnityEngine_Vector2;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.In(logic_uScriptAct_Concatenate_A_11, logic_uScriptAct_Concatenate_B_11, logic_uScriptAct_Concatenate_Separator_11, out logic_uScriptAct_Concatenate_Result_11);
      local_Result02_System_String = logic_uScriptAct_Concatenate_Result_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_18();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptAct_NormalizeVector2_Target_13 = local_SwipeVector_UnityEngine_Vector2;
            
         }
         {
         }
      }
      logic_uScriptAct_NormalizeVector2_uScriptAct_NormalizeVector2_13.In(logic_uScriptAct_NormalizeVector2_Target_13, out logic_uScriptAct_NormalizeVector2_NormalizedVector_13);
      local_SwipeVectorNormalized_UnityEngine_Vector2 = logic_uScriptAct_NormalizeVector2_NormalizedVector_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_NormalizeVector2_uScriptAct_NormalizeVector2_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_18, index + 1);
            }
            logic_uScriptAct_Concatenate_A_18[ index++ ] = local_Result01_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_18, index + 1);
            }
            logic_uScriptAct_Concatenate_B_18[ index++ ] = local_Result02_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.In(logic_uScriptAct_Concatenate_A_18, logic_uScriptAct_Concatenate_B_18, logic_uScriptAct_Concatenate_Separator_18, out logic_uScriptAct_Concatenate_Result_18);
      local_Result03_System_String = logic_uScriptAct_Concatenate_Result_18;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_18.Out;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_22()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_22.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_22, index + 1);
            }
            logic_uScriptAct_Concatenate_A_22[ index++ ] = local_23_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_22.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_22, index + 1);
            }
            logic_uScriptAct_Concatenate_B_22[ index++ ] = local_Result04_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_22.In(logic_uScriptAct_Concatenate_A_22, logic_uScriptAct_Concatenate_B_22, logic_uScriptAct_Concatenate_Separator_22, out logic_uScriptAct_Concatenate_Result_22);
      local_Result05_System_String = logic_uScriptAct_Concatenate_Result_22;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_22.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_In_25()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_25, index + 1);
            }
            logic_uScriptAct_Concatenate_A_25[ index++ ] = local_Result03_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_25, index + 1);
            }
            logic_uScriptAct_Concatenate_B_25[ index++ ] = local_Result05_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.In(logic_uScriptAct_Concatenate_A_25, logic_uScriptAct_Concatenate_B_25, logic_uScriptAct_Concatenate_Separator_25, out logic_uScriptAct_Concatenate_Result_25);
      local_Result06_System_String = logic_uScriptAct_Concatenate_Result_25;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_25.Out;
      
      if ( test_0 == true )
      {
         Relay_In_39();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_29 = local_Angle_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_29.In(logic_RN_FormatFloatString_f_Float_29, logic_RN_FormatFloatString_s_Modifier_29, out logic_RN_FormatFloatString_s_Result_29);
      local_Result04_System_String = logic_RN_FormatFloatString_s_Result_29;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_22();
      }
   }
   
   void Relay_In_34()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_34 = local_37_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_34 = local_SwipeResults_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_34.In(logic_RN_SetTextMeshText_go_GameObject_34, logic_RN_SetTextMeshText_s_Text_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_39()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_39 = local_38_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_39 = local_Result06_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_39.In(logic_RN_SetTextMeshText_go_GameObject_39, logic_RN_SetTextMeshText_s_Text_39);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnSwipeStart_88()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_88;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_88;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_88;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_88;
      Relay_In_6();
   }
   
   void Relay_OnSwipe_88()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_88;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_88;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_88;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_88;
      Relay_In_1();
   }
   
   void Relay_OnSwipeEnd_88()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_88;
      local_SwipeVector_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_SwipeVector_88;
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_88;
      local_Angle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_88;
      Relay_In_7();
   }
   
}
