//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class AutoSelect : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_35_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_35_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_37_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_37_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_43_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_43_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_52_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_52_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_58_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_58_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_84_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_84_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_86_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_86_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_88_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_88_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_90_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_90_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_93_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_93_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_95_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_95_UnityEngine_GameObject_previous = null;
   System.Boolean local_AutoSelect_System_Boolean = (bool) false;
   System.Boolean local_AutoUpdate_System_Boolean = (bool) false;
   System.Boolean local_AutoUpdatePickedObject_System_Boolean = (bool) false;
   System.String local_Cat01_System_String = "";
   System.String local_Cat02_System_String = "";
   System.String local_Cat03_System_String = "";
   System.String local_CurrentPickedObjectName_System_String = "";
   UnityEngine.GameObject local_CurrentPickedObjext_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_CurrentPickedObjext_UnityEngine_GameObject_previous = null;
   System.Boolean local_EnableLayer1_System_Boolean = (bool) false;
   System.Boolean local_EnableLayer2_System_Boolean = (bool) false;
   System.Boolean local_EnableLayer3_System_Boolean = (bool) false;
   System.String local_Label01_System_String = "Picked object from event : ";
   System.String local_Label02_System_String = "Picked object from GetCurrentPickedObject";
   UnityEngine.LayerMask local_LayerMaskResult_UnityEngine_LayerMask = 1;
   System.String local_PickedObjectName_System_String = "";
   UnityEngine.Vector2 local_TouchPosition_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   public UnityEngine.GameObject PickedObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject PickedObject_previous = null;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_1 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_1 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_1 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_1 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_4 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_4 = true;
   bool logic_uScriptCon_CompareBool_False_4 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_6 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_6 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_6 = "";
   System.String logic_uScriptAct_Concatenate_Result_6;
   bool logic_uScriptAct_Concatenate_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_10 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_10 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_10 = " : ";
   System.String logic_uScriptAct_Concatenate_Result_10;
   bool logic_uScriptAct_Concatenate_Out_10 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_17 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_17 = true;
   bool logic_uScriptCon_CompareBool_False_17 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_18 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_18 = true;
   bool logic_uScriptCon_CompareBool_False_18 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_20 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_20 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_20 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_20 = " : ";
   System.String logic_uScriptAct_Concatenate_Result_20;
   bool logic_uScriptAct_Concatenate_Out_20 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_24 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_24 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_24 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_24 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_26 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_26 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_26;
   bool logic_uScriptAct_GetGameObjectName_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_29 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_29 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_29;
   bool logic_uScriptAct_GetGameObjectName_Out_29 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_45 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_45 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_45 = true;
   bool logic_uScriptCon_CompareBool_False_45 = true;
   //pointer to script instanced logic node
   uScriptAct_CombineLayerMasks logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_47 = new uScriptAct_CombineLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_A_47 = 0;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_B_47 = 256;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_LayerMask_47;
   bool logic_uScriptAct_CombineLayerMasks_Out_47 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_51 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_51 = true;
   bool logic_uScriptCon_CompareBool_False_51 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_56 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_56 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_56 = true;
   bool logic_uScriptCon_CompareBool_False_56 = true;
   //pointer to script instanced logic node
   uScriptAct_RemoveLayerMasks logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_63 = new uScriptAct_RemoveLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_A_63 = 0;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_B_63 = 256;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_LayerMask_63;
   bool logic_uScriptAct_RemoveLayerMasks_Out_63 = true;
   //pointer to script instanced logic node
   uScriptAct_CombineLayerMasks logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_71 = new uScriptAct_CombineLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_A_71 = 0;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_B_71 = 512;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_LayerMask_71;
   bool logic_uScriptAct_CombineLayerMasks_Out_71 = true;
   //pointer to script instanced logic node
   uScriptAct_RemoveLayerMasks logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_74 = new uScriptAct_RemoveLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_A_74 = 0;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_B_74 = 512;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_LayerMask_74;
   bool logic_uScriptAct_RemoveLayerMasks_Out_74 = true;
   //pointer to script instanced logic node
   uScriptAct_CombineLayerMasks logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_78 = new uScriptAct_CombineLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_A_78 = 0;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_B_78 = 1024;
   UnityEngine.LayerMask logic_uScriptAct_CombineLayerMasks_LayerMask_78;
   bool logic_uScriptAct_CombineLayerMasks_Out_78 = true;
   //pointer to script instanced logic node
   uScriptAct_RemoveLayerMasks logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_81 = new uScriptAct_RemoveLayerMasks( );
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_A_81 = 0;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_B_81 = 1024;
   UnityEngine.LayerMask logic_uScriptAct_RemoveLayerMasks_LayerMask_81;
   bool logic_uScriptAct_RemoveLayerMasks_Out_81 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_83 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_83 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_83 = "Picked object from event : none";
   bool logic_RN_SetUnityUIText_Out_83 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_85 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_85 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_85 = "";
   bool logic_RN_SetUnityUIText_Out_85 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_87 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_87 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_87 = "Picked object from GetCurrentPickedObject : none";
   bool logic_RN_SetUnityUIText_Out_87 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_89 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_89 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_89 = "";
   bool logic_RN_SetUnityUIText_Out_89 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_92 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_92 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_92 = "";
   bool logic_RN_SetUnityUIText_Out_92 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_94 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_94 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_94 = "";
   bool logic_RN_SetUnityUIText_Out_94 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnableAutoSelect logic_EasyTouch_SetEnableAutoSelect_EasyTouch_SetEnableAutoSelect_137 = new EasyTouch_SetEnableAutoSelect( );
   System.Boolean logic_EasyTouch_SetEnableAutoSelect_b_EnableAutoSelect_137 = (bool) false;
   bool logic_EasyTouch_SetEnableAutoSelect_Out_137 = true;
   //pointer to script instanced logic node
   EasyTouch_SetAutoUpdatePickedObject logic_EasyTouch_SetAutoUpdatePickedObject_EasyTouch_SetAutoUpdatePickedObject_138 = new EasyTouch_SetAutoUpdatePickedObject( );
   System.Boolean logic_EasyTouch_SetAutoUpdatePickedObject_b_AutoUpdatePickedObject_138 = (bool) false;
   bool logic_EasyTouch_SetAutoUpdatePickedObject_Out_138 = true;
   //pointer to script instanced logic node
   EasyTouch_Get3DPickableLayer logic_EasyTouch_Get3DPickableLayer_EasyTouch_Get3DPickableLayer_139 = new EasyTouch_Get3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Get3DPickableLayer_lm_LayerMask_139;
   bool logic_EasyTouch_Get3DPickableLayer_Out_139 = true;
   //pointer to script instanced logic node
   EasyTouch_GetAutoUpdatePickedObject logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_140 = new EasyTouch_GetAutoUpdatePickedObject( );
   System.Boolean logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_140;
   bool logic_EasyTouch_GetAutoUpdatePickedObject_Out_140 = true;
   //pointer to script instanced logic node
   EasyTouch_GetAutoUpdatePickedObject logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_141 = new EasyTouch_GetAutoUpdatePickedObject( );
   System.Boolean logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_141;
   bool logic_EasyTouch_GetAutoUpdatePickedObject_Out_141 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_142 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_142 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_142 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_143 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_143 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_143 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_144 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_144 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_144 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_145 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_145 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_145 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_146 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_146 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_146 = true;
   //pointer to script instanced logic node
   EasyTouch_Set3DPickableLayer logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_147 = new EasyTouch_Set3DPickableLayer( );
   UnityEngine.LayerMask logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_147 = 0;
   bool logic_EasyTouch_Set3DPickableLayer_Out_147 = true;
   
   //event nodes
   System.Boolean event_UnityEngine_GameObject_ToggleValue_34 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_ToggleValue_36 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_ToggleValue_42 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_ToggleValue_50 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_ToggleValue_55 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_59 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_136 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_136 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_136 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_136 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_136 = new Vector2( );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_136 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_136 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_136 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_136 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_136 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_136 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_136 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_136 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_136 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_136 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_136 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_136 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_136 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_136 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_136 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_136 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_136 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_136 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_136 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_136 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_136 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_136 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_136 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_136 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_136 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_136 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_136 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_35_UnityEngine_GameObject = GameObject.Find( "AutoSelect_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_35_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_35_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
               }
            }
         }
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_35_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_35_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_35_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_34;
               }
            }
         }
      }
      if ( null == local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_37_UnityEngine_GameObject = GameObject.Find( "AutoUpdate_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_37_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_37_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_36;
               }
            }
         }
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_37_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_37_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_37_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_36;
               }
            }
         }
      }
      if ( null == local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_43_UnityEngine_GameObject = GameObject.Find( "Layer1_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_43_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_43_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_42;
               }
            }
         }
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_43_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_43_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_43_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_42;
               }
            }
         }
      }
      if ( null == local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_52_UnityEngine_GameObject = GameObject.Find( "Layer2_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_52_UnityEngine_GameObject_previous != local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_52_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_52_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_50;
               }
            }
         }
         
         local_52_UnityEngine_GameObject_previous = local_52_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_52_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_52_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_52_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_50;
               }
            }
         }
      }
      if ( null == local_58_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_58_UnityEngine_GameObject = GameObject.Find( "Layer3_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_58_UnityEngine_GameObject_previous != local_58_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_58_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_58_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_55;
               }
            }
         }
         
         local_58_UnityEngine_GameObject_previous = local_58_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_58_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_58_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_58_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_55;
               }
            }
         }
      }
      if ( null == local_84_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_84_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_01" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_84_UnityEngine_GameObject_previous != local_84_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_84_UnityEngine_GameObject_previous = local_84_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_86_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_01" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_88_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_02" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_88_UnityEngine_GameObject_previous != local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_88_UnityEngine_GameObject_previous = local_88_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_90_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_90_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_02" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_90_UnityEngine_GameObject_previous != local_90_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_90_UnityEngine_GameObject_previous = local_90_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_93_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_01" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_95_UnityEngine_GameObject = GameObject.Find( "TouchData_Text_02" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_35_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_35_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
               }
            }
         }
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_35_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_35_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_35_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_34;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_37_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_37_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_36;
               }
            }
         }
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_37_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_37_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_37_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_36;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_43_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_43_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_42;
               }
            }
         }
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_43_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_43_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_43_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_42;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_52_UnityEngine_GameObject_previous != local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_52_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_52_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_50;
               }
            }
         }
         
         local_52_UnityEngine_GameObject_previous = local_52_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_52_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_52_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_52_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_50;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_58_UnityEngine_GameObject_previous != local_58_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_58_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_58_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_55;
               }
            }
         }
         
         local_58_UnityEngine_GameObject_previous = local_58_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_58_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_58_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_58_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_55;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_84_UnityEngine_GameObject_previous != local_84_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_84_UnityEngine_GameObject_previous = local_84_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_88_UnityEngine_GameObject_previous != local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_88_UnityEngine_GameObject_previous = local_88_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_90_UnityEngine_GameObject_previous != local_90_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_90_UnityEngine_GameObject_previous = local_90_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_59 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_59 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_59 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_59.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_59.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_59;
                  component.uScriptLateStart += Instance_uScriptLateStart_59;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_136 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_136 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_136 )
         {
            {
               EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_136.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_136.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_136;
                  component.OnTouchDown += Instance_OnTouchDown_136;
                  component.OnTouchUp += Instance_OnTouchUp_136;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_35_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_35_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_34;
            }
         }
      }
      if ( null != local_37_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_37_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_36;
            }
         }
      }
      if ( null != local_43_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_43_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_42;
            }
         }
      }
      if ( null != local_52_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_52_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_50;
            }
         }
      }
      if ( null != local_58_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_58_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_55;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_59 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_59.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_59;
               component.uScriptLateStart -= Instance_uScriptLateStart_59;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_136 )
      {
         {
            EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_136.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_136;
               component.OnTouchDown -= Instance_OnTouchDown_136;
               component.OnTouchUp -= Instance_OnTouchUp_136;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptCon_IsNull_uScriptCon_IsNull_1.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_20.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_24.SetParent(g);
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_26.SetParent(g);
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_29.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_45.SetParent(g);
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_47.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_56.SetParent(g);
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_63.SetParent(g);
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_71.SetParent(g);
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_74.SetParent(g);
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_78.SetParent(g);
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_81.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_83.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_85.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_87.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_89.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_92.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_94.SetParent(g);
      logic_EasyTouch_SetEnableAutoSelect_EasyTouch_SetEnableAutoSelect_137.SetParent(g);
      logic_EasyTouch_SetAutoUpdatePickedObject_EasyTouch_SetAutoUpdatePickedObject_138.SetParent(g);
      logic_EasyTouch_Get3DPickableLayer_EasyTouch_Get3DPickableLayer_139.SetParent(g);
      logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_140.SetParent(g);
      logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_141.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_142.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_143.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_144.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_145.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_146.SetParent(g);
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_147.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnToggleValueChanged_34(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_34 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_34( );
   }
   
   void Instance_OnToggleValueChanged_36(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_36 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_36( );
   }
   
   void Instance_OnToggleValueChanged_42(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_42 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_42( );
   }
   
   void Instance_OnToggleValueChanged_50(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_50 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_50( );
   }
   
   void Instance_OnToggleValueChanged_55(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_55 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_55( );
   }
   
   void Instance_uScriptStart_59(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_59( );
   }
   
   void Instance_uScriptLateStart_59(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_59( );
   }
   
   void Instance_OnTouchStart_136(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_136 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_136 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_136 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_136 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_136 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_136 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_136 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_136 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_136 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_136 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_136 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_136 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_136 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_136 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_136 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_136 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_136 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_136 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_136 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_136 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_136 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_136 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_136 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_136 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_136 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_136 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_136 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_136 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_136 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_136 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_136 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_136 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_136 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_136 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_136 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_136 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_136 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_136 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_136 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_136 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_136 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_136 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_136 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_136( );
   }
   
   void Instance_OnTouchDown_136(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_136 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_136 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_136 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_136 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_136 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_136 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_136 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_136 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_136 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_136 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_136 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_136 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_136 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_136 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_136 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_136 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_136 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_136 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_136 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_136 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_136 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_136 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_136 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_136 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_136 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_136 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_136 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_136 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_136 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_136 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_136 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_136 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_136 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_136 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_136 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_136 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_136 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_136 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_136 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_136 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_136 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_136 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_136 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_136( );
   }
   
   void Instance_OnTouchUp_136(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_136 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_136 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_136 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_136 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_136 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_136 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_136 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_136 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_136 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_136 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_136 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_136 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_136 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_136 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_136 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_136 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_136 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_136 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_136 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_136 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_136 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_136 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_136 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_136 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_136 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_136 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_136 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_136 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_136 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_136 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_136 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_136 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_136 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_136 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_136 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_136 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_136 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_136 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_136 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_136 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_136 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_136 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_136 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_136( );
   }
   
   void Relay_In_1()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_1.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_1, index + 1);
            }
            logic_uScriptCon_IsNull_Target_1[ index++ ] = PickedObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_1.In(logic_uScriptCon_IsNull_Target_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_1.IsNull;
      bool test_1 = logic_uScriptCon_IsNull_uScriptCon_IsNull_1.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_140();
      }
      if ( test_1 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_4 = local_AutoUpdatePickedObject_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.In(logic_uScriptCon_CompareBool_Bool_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.False;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
      if ( test_1 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_6.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_6, index + 1);
            }
            logic_uScriptAct_Concatenate_A_6[ index++ ] = local_Label01_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_6.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_6, index + 1);
            }
            logic_uScriptAct_Concatenate_B_6[ index++ ] = local_PickedObjectName_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.In(logic_uScriptAct_Concatenate_A_6, logic_uScriptAct_Concatenate_B_6, logic_uScriptAct_Concatenate_Separator_6, out logic_uScriptAct_Concatenate_Result_6);
      local_Cat01_System_String = logic_uScriptAct_Concatenate_Result_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_In_10()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_10, index + 1);
            }
            logic_uScriptAct_Concatenate_A_10[ index++ ] = local_Cat01_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_10, index + 1);
            }
            logic_uScriptAct_Concatenate_B_10[ index++ ] = local_TouchPosition_UnityEngine_Vector2;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.In(logic_uScriptAct_Concatenate_A_10, logic_uScriptAct_Concatenate_B_10, logic_uScriptAct_Concatenate_Separator_10, out logic_uScriptAct_Concatenate_Result_10);
      local_Cat02_System_String = logic_uScriptAct_Concatenate_Result_10;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_10.Out;
      
      if ( test_0 == true )
      {
         Relay_In_85();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_17 = local_AutoUpdatePickedObject_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.In(logic_uScriptCon_CompareBool_Bool_17);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.False;
      
      if ( test_0 == true )
      {
         Relay_In_83();
      }
      if ( test_1 == true )
      {
         Relay_In_83();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_18 = local_AutoUpdatePickedObject_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.In(logic_uScriptCon_CompareBool_Bool_18);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.False;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_20.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_20, index + 1);
            }
            logic_uScriptAct_Concatenate_A_20[ index++ ] = local_Label02_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_20.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_20, index + 1);
            }
            logic_uScriptAct_Concatenate_B_20[ index++ ] = local_CurrentPickedObjectName_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_20.In(logic_uScriptAct_Concatenate_A_20, logic_uScriptAct_Concatenate_B_20, logic_uScriptAct_Concatenate_Separator_20, out logic_uScriptAct_Concatenate_Result_20);
      local_Cat03_System_String = logic_uScriptAct_Concatenate_Result_20;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_20.Out;
      
      if ( test_0 == true )
      {
         Relay_In_89();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_24.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_24, index + 1);
            }
            logic_uScriptCon_IsNull_Target_24[ index++ ] = local_CurrentPickedObjext_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_24.In(logic_uScriptCon_IsNull_Target_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_24.IsNull;
      bool test_1 = logic_uScriptCon_IsNull_uScriptCon_IsNull_24.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_87();
      }
      if ( test_1 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetGameObjectName_gameObject_26 = PickedObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_26.In(logic_uScriptAct_GetGameObjectName_gameObject_26, out logic_uScriptAct_GetGameObjectName_name_26);
      local_PickedObjectName_System_String = logic_uScriptAct_GetGameObjectName_name_26;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_26.Out;
      
      if ( test_0 == true )
      {
         Relay_In_141();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetGameObjectName_gameObject_29 = local_CurrentPickedObjext_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_29.In(logic_uScriptAct_GetGameObjectName_gameObject_29, out logic_uScriptAct_GetGameObjectName_name_29);
      local_CurrentPickedObjectName_System_String = logic_uScriptAct_GetGameObjectName_name_29;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_OnToggleValueChanged_34()
   {
      local_AutoSelect_System_Boolean = event_UnityEngine_GameObject_ToggleValue_34;
      Relay_In_137();
   }
   
   void Relay_OnToggleValueChanged_36()
   {
      local_AutoUpdate_System_Boolean = event_UnityEngine_GameObject_ToggleValue_36;
      Relay_In_138();
   }
   
   void Relay_OnToggleValueChanged_42()
   {
      local_EnableLayer1_System_Boolean = event_UnityEngine_GameObject_ToggleValue_42;
      Relay_In_45();
   }
   
   void Relay_In_45()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_45 = local_EnableLayer1_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_45.In(logic_uScriptCon_CompareBool_Bool_45);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_45.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_45.False;
      
      if ( test_0 == true )
      {
         Relay_In_47();
      }
      if ( test_1 == true )
      {
         Relay_In_63();
      }
   }
   
   void Relay_In_47()
   {
      {
         {
            logic_uScriptAct_CombineLayerMasks_A_47 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_47.In(logic_uScriptAct_CombineLayerMasks_A_47, logic_uScriptAct_CombineLayerMasks_B_47, out logic_uScriptAct_CombineLayerMasks_LayerMask_47);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_CombineLayerMasks_LayerMask_47;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_47.Out;
      
      if ( test_0 == true )
      {
         Relay_In_142();
      }
   }
   
   void Relay_OnToggleValueChanged_50()
   {
      local_EnableLayer2_System_Boolean = event_UnityEngine_GameObject_ToggleValue_50;
      Relay_In_51();
   }
   
   void Relay_In_51()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_51 = local_EnableLayer2_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.In(logic_uScriptCon_CompareBool_Bool_51);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_51.False;
      
      if ( test_0 == true )
      {
         Relay_In_71();
      }
      if ( test_1 == true )
      {
         Relay_In_74();
      }
   }
   
   void Relay_OnToggleValueChanged_55()
   {
      local_EnableLayer3_System_Boolean = event_UnityEngine_GameObject_ToggleValue_55;
      Relay_In_56();
   }
   
   void Relay_In_56()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_56 = local_EnableLayer3_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_56.In(logic_uScriptCon_CompareBool_Bool_56);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_56.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_56.False;
      
      if ( test_0 == true )
      {
         Relay_In_78();
      }
      if ( test_1 == true )
      {
         Relay_In_81();
      }
   }
   
   void Relay_uScriptStart_59()
   {
      Relay_In_139();
   }
   
   void Relay_uScriptLateStart_59()
   {
   }
   
   void Relay_In_63()
   {
      {
         {
            logic_uScriptAct_RemoveLayerMasks_A_63 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_63.In(logic_uScriptAct_RemoveLayerMasks_A_63, logic_uScriptAct_RemoveLayerMasks_B_63, out logic_uScriptAct_RemoveLayerMasks_LayerMask_63);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_RemoveLayerMasks_LayerMask_63;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_63.Out;
      
      if ( test_0 == true )
      {
         Relay_In_143();
      }
   }
   
   void Relay_In_71()
   {
      {
         {
            logic_uScriptAct_CombineLayerMasks_A_71 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_71.In(logic_uScriptAct_CombineLayerMasks_A_71, logic_uScriptAct_CombineLayerMasks_B_71, out logic_uScriptAct_CombineLayerMasks_LayerMask_71);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_CombineLayerMasks_LayerMask_71;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_71.Out;
      
      if ( test_0 == true )
      {
         Relay_In_144();
      }
   }
   
   void Relay_In_74()
   {
      {
         {
            logic_uScriptAct_RemoveLayerMasks_A_74 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_74.In(logic_uScriptAct_RemoveLayerMasks_A_74, logic_uScriptAct_RemoveLayerMasks_B_74, out logic_uScriptAct_RemoveLayerMasks_LayerMask_74);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_RemoveLayerMasks_LayerMask_74;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_74.Out;
      
      if ( test_0 == true )
      {
         Relay_In_145();
      }
   }
   
   void Relay_In_78()
   {
      {
         {
            logic_uScriptAct_CombineLayerMasks_A_78 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_78.In(logic_uScriptAct_CombineLayerMasks_A_78, logic_uScriptAct_CombineLayerMasks_B_78, out logic_uScriptAct_CombineLayerMasks_LayerMask_78);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_CombineLayerMasks_LayerMask_78;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_CombineLayerMasks_uScriptAct_CombineLayerMasks_78.Out;
      
      if ( test_0 == true )
      {
         Relay_In_146();
      }
   }
   
   void Relay_In_81()
   {
      {
         {
            logic_uScriptAct_RemoveLayerMasks_A_81 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_81.In(logic_uScriptAct_RemoveLayerMasks_A_81, logic_uScriptAct_RemoveLayerMasks_B_81, out logic_uScriptAct_RemoveLayerMasks_LayerMask_81);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_uScriptAct_RemoveLayerMasks_LayerMask_81;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_RemoveLayerMasks_uScriptAct_RemoveLayerMasks_81.Out;
      
      if ( test_0 == true )
      {
         Relay_In_147();
      }
   }
   
   void Relay_In_83()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_84_UnityEngine_GameObject_previous != local_84_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_84_UnityEngine_GameObject_previous = local_84_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_83 = local_84_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_83.In(logic_RN_SetUnityUIText_go_GameObject_83, logic_RN_SetUnityUIText_s_Text_83);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_85()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_85 = local_86_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetUnityUIText_s_Text_85 = local_Cat02_System_String;
            
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_85.In(logic_RN_SetUnityUIText_go_GameObject_85, logic_RN_SetUnityUIText_s_Text_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_87()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_88_UnityEngine_GameObject_previous != local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_88_UnityEngine_GameObject_previous = local_88_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_87 = local_88_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_87.In(logic_RN_SetUnityUIText_go_GameObject_87, logic_RN_SetUnityUIText_s_Text_87);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_89()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_90_UnityEngine_GameObject_previous != local_90_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_90_UnityEngine_GameObject_previous = local_90_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_89 = local_90_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetUnityUIText_s_Text_89 = local_Cat03_System_String;
            
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_89.In(logic_RN_SetUnityUIText_go_GameObject_89, logic_RN_SetUnityUIText_s_Text_89);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_92()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_93_UnityEngine_GameObject_previous != local_93_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_93_UnityEngine_GameObject_previous = local_93_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_92 = local_93_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_92.In(logic_RN_SetUnityUIText_go_GameObject_92, logic_RN_SetUnityUIText_s_Text_92);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetUnityUIText_RN_SetUnityUIText_92.Out;
      
      if ( test_0 == true )
      {
         Relay_In_94();
      }
   }
   
   void Relay_In_94()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_94 = local_95_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_94.In(logic_RN_SetUnityUIText_go_GameObject_94, logic_RN_SetUnityUIText_s_Text_94);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart_136()
   {
      local_TouchPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_Position_136;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_CurrentPickedObjext_UnityEngine_GameObject = event_UnityEngine_GameObject_go_CurrentPickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_1();
   }
   
   void Relay_OnTouchDown_136()
   {
      local_TouchPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_Position_136;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_CurrentPickedObjext_UnityEngine_GameObject = event_UnityEngine_GameObject_go_CurrentPickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_18();
   }
   
   void Relay_OnTouchUp_136()
   {
      local_TouchPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_Position_136;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_CurrentPickedObjext_UnityEngine_GameObject = event_UnityEngine_GameObject_go_CurrentPickedObject_136;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CurrentPickedObjext_UnityEngine_GameObject_previous != local_CurrentPickedObjext_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CurrentPickedObjext_UnityEngine_GameObject_previous = local_CurrentPickedObjext_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_92();
   }
   
   void Relay_In_137()
   {
      {
         {
            logic_EasyTouch_SetEnableAutoSelect_b_EnableAutoSelect_137 = local_AutoSelect_System_Boolean;
            
         }
      }
      logic_EasyTouch_SetEnableAutoSelect_EasyTouch_SetEnableAutoSelect_137.In(logic_EasyTouch_SetEnableAutoSelect_b_EnableAutoSelect_137);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_138()
   {
      {
         {
            logic_EasyTouch_SetAutoUpdatePickedObject_b_AutoUpdatePickedObject_138 = local_AutoUpdate_System_Boolean;
            
         }
      }
      logic_EasyTouch_SetAutoUpdatePickedObject_EasyTouch_SetAutoUpdatePickedObject_138.In(logic_EasyTouch_SetAutoUpdatePickedObject_b_AutoUpdatePickedObject_138);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_139()
   {
      {
         {
         }
      }
      logic_EasyTouch_Get3DPickableLayer_EasyTouch_Get3DPickableLayer_139.In(out logic_EasyTouch_Get3DPickableLayer_lm_LayerMask_139);
      local_LayerMaskResult_UnityEngine_LayerMask = logic_EasyTouch_Get3DPickableLayer_lm_LayerMask_139;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_140()
   {
      {
         {
         }
      }
      logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_140.In(out logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_140);
      local_AutoUpdatePickedObject_System_Boolean = logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_140;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_140.Out;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_141()
   {
      {
         {
         }
      }
      logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_141.In(out logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_141);
      local_AutoUpdatePickedObject_System_Boolean = logic_EasyTouch_GetAutoUpdatePickedObject_b_AutoUpdatePickedObject_141;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_GetAutoUpdatePickedObject_EasyTouch_GetAutoUpdatePickedObject_141.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_142()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_142 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_142.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_142);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_143()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_143 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_143.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_143);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_144()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_144 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_144.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_144);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_145()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_145 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_145.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_145);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_146()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_146 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_146.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_146);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_147()
   {
      {
         {
            logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_147 = local_LayerMaskResult_UnityEngine_LayerMask;
            
         }
      }
      logic_EasyTouch_Set3DPickableLayer_EasyTouch_Set3DPickableLayer_147.In(logic_EasyTouch_Set3DPickableLayer_lm_LayerMask_147);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
