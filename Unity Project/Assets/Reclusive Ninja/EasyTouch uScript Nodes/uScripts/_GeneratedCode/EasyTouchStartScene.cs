//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class EasyTouchStartScene : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_11_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_11_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_20_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_20_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_35_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_35_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_41_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_41_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_44_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_44_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_47_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_47_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_50_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_50_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_53_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_53_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_56_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_56_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_59_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_59_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_62_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_62_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_65_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_65_UnityEngine_GameObject_previous = null;
   UnityEngine.CanvasGroup local_AdvancedPanel_UnityEngine_CanvasGroup = default(UnityEngine.CanvasGroup);
   System.Boolean local_ShowAdvancedPanel_System_Boolean = (bool) false;
   System.Boolean local_ShowSimplePanel_System_Boolean = (bool) false;
   System.Boolean local_ShowUIPanel_System_Boolean = (bool) false;
   UnityEngine.CanvasGroup local_SimplePanel_UnityEngine_CanvasGroup = default(UnityEngine.CanvasGroup);
   UnityEngine.CanvasGroup local_UIPanel_UnityEngine_CanvasGroup = default(UnityEngine.CanvasGroup);
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_1 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_1 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_1 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_1 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_1 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_1 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_1 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_2 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_2 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_2 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_2 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_2 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_2 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_2 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_3 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_3 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_3 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_3 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_3 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_3 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_4 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_4;
   bool logic_uScriptAct_SetBool_Out_4 = true;
   bool logic_uScriptAct_SetBool_SetTrue_4 = true;
   bool logic_uScriptAct_SetBool_SetFalse_4 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_7 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_7;
   bool logic_uScriptAct_SetBool_Out_7 = true;
   bool logic_uScriptAct_SetBool_SetTrue_7 = true;
   bool logic_uScriptAct_SetBool_SetFalse_7 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_9 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_9;
   bool logic_uScriptAct_SetBool_Out_9 = true;
   bool logic_uScriptAct_SetBool_SetTrue_9 = true;
   bool logic_uScriptAct_SetBool_SetFalse_9 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_12 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_12 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_12 = true;
   bool logic_uScriptCon_CompareBool_False_12 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_15 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_15;
   bool logic_uScriptAct_SetBool_Out_15 = true;
   bool logic_uScriptAct_SetBool_SetTrue_15 = true;
   bool logic_uScriptAct_SetBool_SetFalse_15 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_17 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_17 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_17 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_17 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_17 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_17 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_17 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_18 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_18;
   bool logic_uScriptAct_SetBool_Out_18 = true;
   bool logic_uScriptAct_SetBool_SetTrue_18 = true;
   bool logic_uScriptAct_SetBool_SetFalse_18 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_21 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_21 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_21 = true;
   bool logic_uScriptCon_CompareBool_False_21 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_23 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_23 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_23 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_23 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_23 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_23 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_23 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_24 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_24 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_24 = (float) 1;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_24 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_24 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_24 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_25 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_25;
   bool logic_uScriptAct_SetBool_Out_25 = true;
   bool logic_uScriptAct_SetBool_SetTrue_25 = true;
   bool logic_uScriptAct_SetBool_SetFalse_25 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_26 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_26;
   bool logic_uScriptAct_SetBool_Out_26 = true;
   bool logic_uScriptAct_SetBool_SetTrue_26 = true;
   bool logic_uScriptAct_SetBool_SetFalse_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_30 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_30 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_30 = true;
   bool logic_uScriptCon_CompareBool_False_30 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_31 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_31 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_31 = (float) 1;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_31 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_31 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_31 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_31 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_32 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_32;
   bool logic_uScriptAct_SetBool_Out_32 = true;
   bool logic_uScriptAct_SetBool_SetTrue_32 = true;
   bool logic_uScriptAct_SetBool_SetFalse_32 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_33 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_33;
   bool logic_uScriptAct_SetBool_Out_33 = true;
   bool logic_uScriptAct_SetBool_SetTrue_33 = true;
   bool logic_uScriptAct_SetBool_SetFalse_33 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_34 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_34 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_34 = (float) 0;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_34 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_34 = (bool) false;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_34 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_34 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_39 = "ETOneFinger";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_39 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_39 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_39 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_42 = "ETTwoFinger";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_42 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_42 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_42 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_45 = "ETMultifinger";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_45 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_45 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_45 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_48 = "ETAutoSelect";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_48 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_48 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_48 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_52 = "ETMulticam";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_52 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_52 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_52 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_55 = "ETUICompat";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_55 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_55 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_57 = "ETUITwist";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_57 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_57 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_57 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_60 = "ETRTSExample";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_60 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_60 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_60 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_63 = "ETRunBall";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_63 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_63 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_63 = true;
   //pointer to script instanced logic node
   RN_CanvasGroupProperties logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_75 = new RN_CanvasGroupProperties( );
   UnityEngine.CanvasGroup logic_RN_CanvasGroupProperties_cg_CanvasGroup_75 = default(UnityEngine.CanvasGroup);
   System.Single logic_RN_CanvasGroupProperties_f_Alpha_75 = (float) 1;
   System.Boolean logic_RN_CanvasGroupProperties_b_Interactable_75 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_BlocksRaycasts_75 = (bool) true;
   System.Boolean logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_75 = (bool) false;
   bool logic_RN_CanvasGroupProperties_Out_75 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_11_UnityEngine_GameObject = GameObject.Find( "Simple_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_11_UnityEngine_GameObject_previous != local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_11_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_10;
               }
            }
         }
         
         local_11_UnityEngine_GameObject_previous = local_11_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_11_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_11_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_10;
               }
            }
         }
      }
      if ( null == local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_20_UnityEngine_GameObject = GameObject.Find( "/Start_Canvas/Advanced_Button " ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_20_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_20_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_19;
               }
            }
         }
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_20_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_20_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_20_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_19;
               }
            }
         }
      }
      if ( null == local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_35_UnityEngine_GameObject = GameObject.Find( "UI_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_35_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_35_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_29;
               }
            }
         }
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_35_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_35_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_35_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_29;
               }
            }
         }
      }
      if ( null == local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_41_UnityEngine_GameObject = GameObject.Find( "OneFinger_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_41_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_40;
               }
            }
         }
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_41_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_41_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_40;
               }
            }
         }
      }
      if ( null == local_44_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_44_UnityEngine_GameObject = GameObject.Find( "TwoFinger_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_44_UnityEngine_GameObject_previous != local_44_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_44_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_44_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_43;
               }
            }
         }
         
         local_44_UnityEngine_GameObject_previous = local_44_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_44_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_44_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_44_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_43;
               }
            }
         }
      }
      if ( null == local_47_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_47_UnityEngine_GameObject = GameObject.Find( "MultiFinger_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_47_UnityEngine_GameObject_previous != local_47_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_47_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_47_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_46;
               }
            }
         }
         
         local_47_UnityEngine_GameObject_previous = local_47_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_47_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_47_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_47_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_46;
               }
            }
         }
      }
      if ( null == local_50_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_50_UnityEngine_GameObject = GameObject.Find( "AutoSelect_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_50_UnityEngine_GameObject_previous != local_50_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_50_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_50_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_49;
               }
            }
         }
         
         local_50_UnityEngine_GameObject_previous = local_50_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_50_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_50_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_50_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_49;
               }
            }
         }
      }
      if ( null == local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_53_UnityEngine_GameObject = GameObject.Find( "MultiCam_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_53_UnityEngine_GameObject_previous != local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_53_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_51;
               }
            }
         }
         
         local_53_UnityEngine_GameObject_previous = local_53_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_53_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_53_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_51;
               }
            }
         }
      }
      if ( null == local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_56_UnityEngine_GameObject = GameObject.Find( "UIComp_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_56_UnityEngine_GameObject_previous != local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_56_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_54;
               }
            }
         }
         
         local_56_UnityEngine_GameObject_previous = local_56_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_56_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_56_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_54;
               }
            }
         }
      }
      if ( null == local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_59_UnityEngine_GameObject = GameObject.Find( "UITwist_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_59_UnityEngine_GameObject_previous != local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_59_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_59_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_58;
               }
            }
         }
         
         local_59_UnityEngine_GameObject_previous = local_59_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_59_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_59_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_59_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_58;
               }
            }
         }
      }
      if ( null == local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_62_UnityEngine_GameObject = GameObject.Find( "RTS_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_62_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_62_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_61;
               }
            }
         }
         
         local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_62_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_62_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_62_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_61;
               }
            }
         }
      }
      if ( null == local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_65_UnityEngine_GameObject = GameObject.Find( "Ball_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_65_UnityEngine_GameObject_previous != local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_65_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_64;
               }
            }
         }
         
         local_65_UnityEngine_GameObject_previous = local_65_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_65_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_65_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_64;
               }
            }
         }
      }
      if ( null == local_SimplePanel_UnityEngine_CanvasGroup || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "/Start_Canvas/Simple_Panel" );
         if ( null != gameObject )
         {
            local_SimplePanel_UnityEngine_CanvasGroup = gameObject.GetComponent<UnityEngine.CanvasGroup>();
         }
      }
      if ( null == local_AdvancedPanel_UnityEngine_CanvasGroup || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "/Start_Canvas/Advanced_Panel" );
         if ( null != gameObject )
         {
            local_AdvancedPanel_UnityEngine_CanvasGroup = gameObject.GetComponent<UnityEngine.CanvasGroup>();
         }
      }
      if ( null == local_UIPanel_UnityEngine_CanvasGroup || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "/Start_Canvas/UI_Panel" );
         if ( null != gameObject )
         {
            local_UIPanel_UnityEngine_CanvasGroup = gameObject.GetComponent<UnityEngine.CanvasGroup>();
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_11_UnityEngine_GameObject_previous != local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_11_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_10;
               }
            }
         }
         
         local_11_UnityEngine_GameObject_previous = local_11_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_11_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_11_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_10;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_20_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_20_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_19;
               }
            }
         }
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_20_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_20_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_20_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_19;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_35_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_35_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_29;
               }
            }
         }
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_35_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_35_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_35_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_29;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_41_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_40;
               }
            }
         }
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_41_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_41_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_40;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_44_UnityEngine_GameObject_previous != local_44_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_44_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_44_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_43;
               }
            }
         }
         
         local_44_UnityEngine_GameObject_previous = local_44_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_44_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_44_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_44_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_43;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_47_UnityEngine_GameObject_previous != local_47_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_47_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_47_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_46;
               }
            }
         }
         
         local_47_UnityEngine_GameObject_previous = local_47_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_47_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_47_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_47_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_46;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_50_UnityEngine_GameObject_previous != local_50_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_50_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_50_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_49;
               }
            }
         }
         
         local_50_UnityEngine_GameObject_previous = local_50_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_50_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_50_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_50_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_49;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_53_UnityEngine_GameObject_previous != local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_53_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_51;
               }
            }
         }
         
         local_53_UnityEngine_GameObject_previous = local_53_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_53_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_53_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_51;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_56_UnityEngine_GameObject_previous != local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_56_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_54;
               }
            }
         }
         
         local_56_UnityEngine_GameObject_previous = local_56_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_56_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_56_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_54;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_59_UnityEngine_GameObject_previous != local_59_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_59_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_59_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_58;
               }
            }
         }
         
         local_59_UnityEngine_GameObject_previous = local_59_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_59_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_59_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_59_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_58;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_62_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_62_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_61;
               }
            }
         }
         
         local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_62_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_62_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_62_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_61;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_65_UnityEngine_GameObject_previous != local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_65_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_64;
               }
            }
         }
         
         local_65_UnityEngine_GameObject_previous = local_65_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_65_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_65_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_64;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_0;
                  component.uScriptLateStart += Instance_uScriptLateStart_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_11_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_10;
            }
         }
      }
      if ( null != local_20_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_20_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_19;
            }
         }
      }
      if ( null != local_35_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_35_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_29;
            }
         }
      }
      if ( null != local_41_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_40;
            }
         }
      }
      if ( null != local_44_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_44_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_43;
            }
         }
      }
      if ( null != local_47_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_47_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_46;
            }
         }
      }
      if ( null != local_50_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_50_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_49;
            }
         }
      }
      if ( null != local_53_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_51;
            }
         }
      }
      if ( null != local_56_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_54;
            }
         }
      }
      if ( null != local_59_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_59_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_58;
            }
         }
      }
      if ( null != local_62_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_62_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_61;
            }
         }
      }
      if ( null != local_65_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_64;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_0;
               component.uScriptLateStart -= Instance_uScriptLateStart_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_1.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_2.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_3.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_7.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_12.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_17.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_18.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_21.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_23.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_24.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_25.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_26.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_30.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_31.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_32.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_33.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_34.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63.SetParent(g);
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_75.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Loaded += uScriptAct_LoadLevel_Loaded_39;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42.Loaded += uScriptAct_LoadLevel_Loaded_42;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45.Loaded += uScriptAct_LoadLevel_Loaded_45;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Loaded += uScriptAct_LoadLevel_Loaded_48;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52.Loaded += uScriptAct_LoadLevel_Loaded_52;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Loaded += uScriptAct_LoadLevel_Loaded_55;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57.Loaded += uScriptAct_LoadLevel_Loaded_57;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Loaded += uScriptAct_LoadLevel_Loaded_60;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63.Loaded += uScriptAct_LoadLevel_Loaded_63;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Loaded -= uScriptAct_LoadLevel_Loaded_39;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42.Loaded -= uScriptAct_LoadLevel_Loaded_42;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45.Loaded -= uScriptAct_LoadLevel_Loaded_45;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Loaded -= uScriptAct_LoadLevel_Loaded_48;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52.Loaded -= uScriptAct_LoadLevel_Loaded_52;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Loaded -= uScriptAct_LoadLevel_Loaded_55;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57.Loaded -= uScriptAct_LoadLevel_Loaded_57;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Loaded -= uScriptAct_LoadLevel_Loaded_60;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63.Loaded -= uScriptAct_LoadLevel_Loaded_63;
   }
   
   void Instance_uScriptStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_0( );
   }
   
   void Instance_uScriptLateStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_0( );
   }
   
   void Instance_OnButtonClick_10(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_10( );
   }
   
   void Instance_OnButtonClick_19(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_19( );
   }
   
   void Instance_OnButtonClick_29(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_29( );
   }
   
   void Instance_OnButtonClick_40(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_40( );
   }
   
   void Instance_OnButtonClick_43(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_43( );
   }
   
   void Instance_OnButtonClick_46(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_46( );
   }
   
   void Instance_OnButtonClick_49(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_49( );
   }
   
   void Instance_OnButtonClick_51(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_51( );
   }
   
   void Instance_OnButtonClick_54(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_54( );
   }
   
   void Instance_OnButtonClick_58(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_58( );
   }
   
   void Instance_OnButtonClick_61(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_61( );
   }
   
   void Instance_OnButtonClick_64(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_64( );
   }
   
   void uScriptAct_LoadLevel_Loaded_39(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_39( );
   }
   
   void uScriptAct_LoadLevel_Loaded_42(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_42( );
   }
   
   void uScriptAct_LoadLevel_Loaded_45(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_45( );
   }
   
   void uScriptAct_LoadLevel_Loaded_48(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_48( );
   }
   
   void uScriptAct_LoadLevel_Loaded_52(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_52( );
   }
   
   void uScriptAct_LoadLevel_Loaded_55(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_55( );
   }
   
   void uScriptAct_LoadLevel_Loaded_57(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_57( );
   }
   
   void uScriptAct_LoadLevel_Loaded_60(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_60( );
   }
   
   void uScriptAct_LoadLevel_Loaded_63(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_63( );
   }
   
   void Relay_uScriptStart_0()
   {
      Relay_In_1();
      Relay_False_4();
   }
   
   void Relay_uScriptLateStart_0()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_1 = local_SimplePanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_1.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_1, logic_RN_CanvasGroupProperties_f_Alpha_1, logic_RN_CanvasGroupProperties_b_Interactable_1, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_1, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_In_2()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_2 = local_AdvancedPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_2.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_2, logic_RN_CanvasGroupProperties_f_Alpha_2, logic_RN_CanvasGroupProperties_b_Interactable_2, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_2, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_2);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_3 = local_UIPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_3.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_3, logic_RN_CanvasGroupProperties_f_Alpha_3, logic_RN_CanvasGroupProperties_b_Interactable_3, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_3, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_4()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.True(out logic_uScriptAct_SetBool_Target_4);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_4.Out;
      
      if ( test_0 == true )
      {
         Relay_False_7();
      }
   }
   
   void Relay_False_4()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.False(out logic_uScriptAct_SetBool_Target_4);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_4;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_4.Out;
      
      if ( test_0 == true )
      {
         Relay_False_7();
      }
   }
   
   void Relay_True_7()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_7.True(out logic_uScriptAct_SetBool_Target_7);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_7.Out;
      
      if ( test_0 == true )
      {
         Relay_False_9();
      }
   }
   
   void Relay_False_7()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_7.False(out logic_uScriptAct_SetBool_Target_7);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_7.Out;
      
      if ( test_0 == true )
      {
         Relay_False_9();
      }
   }
   
   void Relay_True_9()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.True(out logic_uScriptAct_SetBool_Target_9);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_9()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.False(out logic_uScriptAct_SetBool_Target_9);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_10()
   {
      Relay_In_12();
   }
   
   void Relay_In_12()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_12 = local_ShowSimplePanel_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_12.In(logic_uScriptCon_CompareBool_Bool_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_12.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_12.False;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
      if ( test_1 == true )
      {
         Relay_In_75();
      }
   }
   
   void Relay_True_15()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.True(out logic_uScriptAct_SetBool_Target_15);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_15;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_15()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.False(out logic_uScriptAct_SetBool_Target_15);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_15;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_17()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_17 = local_SimplePanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_17.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_17, logic_RN_CanvasGroupProperties_f_Alpha_17, logic_RN_CanvasGroupProperties_b_Interactable_17, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_17, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_17);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_17.Out;
      
      if ( test_0 == true )
      {
         Relay_False_18();
      }
   }
   
   void Relay_True_18()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_18.True(out logic_uScriptAct_SetBool_Target_18);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_18;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_18()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_18.False(out logic_uScriptAct_SetBool_Target_18);
      local_ShowSimplePanel_System_Boolean = logic_uScriptAct_SetBool_Target_18;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_19()
   {
      Relay_In_21();
   }
   
   void Relay_In_21()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_21 = local_ShowAdvancedPanel_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_21.In(logic_uScriptCon_CompareBool_Bool_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_21.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_21.False;
      
      if ( test_0 == true )
      {
         Relay_In_23();
      }
      if ( test_1 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_23()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_23 = local_AdvancedPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_23.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_23, logic_RN_CanvasGroupProperties_f_Alpha_23, logic_RN_CanvasGroupProperties_b_Interactable_23, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_23, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_23);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_23.Out;
      
      if ( test_0 == true )
      {
         Relay_False_25();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_24 = local_AdvancedPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_24.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_24, logic_RN_CanvasGroupProperties_f_Alpha_24, logic_RN_CanvasGroupProperties_b_Interactable_24, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_24, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_24.Out;
      
      if ( test_0 == true )
      {
         Relay_True_26();
      }
   }
   
   void Relay_True_25()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_25.True(out logic_uScriptAct_SetBool_Target_25);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_25;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_25()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_25.False(out logic_uScriptAct_SetBool_Target_25);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_25;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_26()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_26.True(out logic_uScriptAct_SetBool_Target_26);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_26;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_26()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_26.False(out logic_uScriptAct_SetBool_Target_26);
      local_ShowAdvancedPanel_System_Boolean = logic_uScriptAct_SetBool_Target_26;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_29()
   {
      Relay_In_30();
   }
   
   void Relay_In_30()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_30 = local_ShowUIPanel_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_30.In(logic_uScriptCon_CompareBool_Bool_30);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_30.True;
      bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_30.False;
      
      if ( test_0 == true )
      {
         Relay_In_34();
      }
      if ( test_1 == true )
      {
         Relay_In_31();
      }
   }
   
   void Relay_In_31()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_31 = local_UIPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_31.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_31, logic_RN_CanvasGroupProperties_f_Alpha_31, logic_RN_CanvasGroupProperties_b_Interactable_31, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_31, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_31);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_31.Out;
      
      if ( test_0 == true )
      {
         Relay_True_33();
      }
   }
   
   void Relay_True_32()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_32.True(out logic_uScriptAct_SetBool_Target_32);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_32;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_32()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_32.False(out logic_uScriptAct_SetBool_Target_32);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_32;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_33()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_33.True(out logic_uScriptAct_SetBool_Target_33);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_33;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_33()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_33.False(out logic_uScriptAct_SetBool_Target_33);
      local_ShowUIPanel_System_Boolean = logic_uScriptAct_SetBool_Target_33;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_34()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_34 = local_UIPanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_34.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_34, logic_RN_CanvasGroupProperties_f_Alpha_34, logic_RN_CanvasGroupProperties_b_Interactable_34, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_34, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_34.Out;
      
      if ( test_0 == true )
      {
         Relay_False_32();
      }
   }
   
   void Relay_Loaded_39()
   {
   }
   
   void Relay_In_39()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.In(logic_uScriptAct_LoadLevel_name_39, logic_uScriptAct_LoadLevel_destroyOtherObjects_39, logic_uScriptAct_LoadLevel_blockUntilLoaded_39);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_40()
   {
      Relay_In_39();
   }
   
   void Relay_Loaded_42()
   {
   }
   
   void Relay_In_42()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_42.In(logic_uScriptAct_LoadLevel_name_42, logic_uScriptAct_LoadLevel_destroyOtherObjects_42, logic_uScriptAct_LoadLevel_blockUntilLoaded_42);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_43()
   {
      Relay_In_42();
   }
   
   void Relay_Loaded_45()
   {
   }
   
   void Relay_In_45()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_45.In(logic_uScriptAct_LoadLevel_name_45, logic_uScriptAct_LoadLevel_destroyOtherObjects_45, logic_uScriptAct_LoadLevel_blockUntilLoaded_45);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_46()
   {
      Relay_In_45();
   }
   
   void Relay_Loaded_48()
   {
   }
   
   void Relay_In_48()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.In(logic_uScriptAct_LoadLevel_name_48, logic_uScriptAct_LoadLevel_destroyOtherObjects_48, logic_uScriptAct_LoadLevel_blockUntilLoaded_48);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_49()
   {
      Relay_In_48();
   }
   
   void Relay_OnButtonClick_51()
   {
      Relay_In_52();
   }
   
   void Relay_Loaded_52()
   {
   }
   
   void Relay_In_52()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_52.In(logic_uScriptAct_LoadLevel_name_52, logic_uScriptAct_LoadLevel_destroyOtherObjects_52, logic_uScriptAct_LoadLevel_blockUntilLoaded_52);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_54()
   {
      Relay_In_55();
   }
   
   void Relay_Loaded_55()
   {
   }
   
   void Relay_In_55()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.In(logic_uScriptAct_LoadLevel_name_55, logic_uScriptAct_LoadLevel_destroyOtherObjects_55, logic_uScriptAct_LoadLevel_blockUntilLoaded_55);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Loaded_57()
   {
   }
   
   void Relay_In_57()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_57.In(logic_uScriptAct_LoadLevel_name_57, logic_uScriptAct_LoadLevel_destroyOtherObjects_57, logic_uScriptAct_LoadLevel_blockUntilLoaded_57);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_58()
   {
      Relay_In_57();
   }
   
   void Relay_Loaded_60()
   {
   }
   
   void Relay_In_60()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.In(logic_uScriptAct_LoadLevel_name_60, logic_uScriptAct_LoadLevel_destroyOtherObjects_60, logic_uScriptAct_LoadLevel_blockUntilLoaded_60);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_61()
   {
      Relay_In_60();
   }
   
   void Relay_Loaded_63()
   {
   }
   
   void Relay_In_63()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_63.In(logic_uScriptAct_LoadLevel_name_63, logic_uScriptAct_LoadLevel_destroyOtherObjects_63, logic_uScriptAct_LoadLevel_blockUntilLoaded_63);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnButtonClick_64()
   {
      Relay_In_63();
   }
   
   void Relay_In_75()
   {
      {
         {
            logic_RN_CanvasGroupProperties_cg_CanvasGroup_75 = local_SimplePanel_UnityEngine_CanvasGroup;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_75.In(logic_RN_CanvasGroupProperties_cg_CanvasGroup_75, logic_RN_CanvasGroupProperties_f_Alpha_75, logic_RN_CanvasGroupProperties_b_Interactable_75, logic_RN_CanvasGroupProperties_b_BlocksRaycasts_75, logic_RN_CanvasGroupProperties_b_IgnoreParentGroups_75);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_CanvasGroupProperties_RN_CanvasGroupProperties_75.Out;
      
      if ( test_0 == true )
      {
         Relay_True_15();
      }
   }
   
}
