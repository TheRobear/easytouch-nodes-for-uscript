//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class SimpleLongTap : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_12_System_String = "Long Tap Me";
   UnityEngine.GameObject local_31_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_31_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_33_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_33_UnityEngine_GameObject_previous = null;
   System.Single local_ActionTime_System_Single = (float) 0;
   System.String local_ActionTimeString_System_String = "";
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_3 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_11 = null;
   UnityEngine.GameObject owner_Connection_19 = null;
   UnityEngine.GameObject owner_Connection_20 = null;
   UnityEngine.GameObject owner_Connection_25 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_1 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_1 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_1 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_1;
   bool logic_uScriptAct_SetRandomColor_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_4 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_4 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_4 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_10 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_10 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_10 = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_10 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_10 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_13 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_13 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_13 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_13;
   bool logic_RN_FormatFloatString_Out_13 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_15 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_15 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_15 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_15 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_17 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_17 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_17 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_17 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_17 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_17 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_17 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_17 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_22 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_22 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_22 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_22 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_24 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_24 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_24 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_24 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_24 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_27 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_27 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_27 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_27 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_29 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_29 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_29 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_29 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_29 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_30 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_30 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_30 = "";
   bool logic_RN_SetTextMeshText_Out_30 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_32 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_32 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_32 = "";
   bool logic_RN_SetTextMeshText_Out_32 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_75 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_75 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_75 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_75 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_75 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_75 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_75 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_75 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_75 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_75 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_75 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_75 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_75 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_75 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_75 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_75 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_75 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_75 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_75 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_31_UnityEngine_GameObject = GameObject.Find( "Sphere_Long_Tap_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_33_UnityEngine_GameObject = GameObject.Find( "Sphere_Long_Tap_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_3 || false == m_RegisteredForEvents )
      {
         owner_Connection_3 = parentGameObject;
         if ( null != owner_Connection_3 )
         {
            {
               EasyTouch_On_LongTap component = owner_Connection_3.GetComponent<EasyTouch_On_LongTap>();
               if ( null == component )
               {
                  component = owner_Connection_3.AddComponent<EasyTouch_On_LongTap>();
               }
               if ( null != component )
               {
                  component.OnLongTapStart += Instance_OnLongTapStart_75;
                  component.OnLongTap += Instance_OnLongTap_75;
                  component.OnLongTapEnd += Instance_OnLongTapEnd_75;
               }
            }
         }
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
      }
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
      if ( null == owner_Connection_19 || false == m_RegisteredForEvents )
      {
         owner_Connection_19 = parentGameObject;
      }
      if ( null == owner_Connection_20 || false == m_RegisteredForEvents )
      {
         owner_Connection_20 = parentGameObject;
      }
      if ( null == owner_Connection_25 || false == m_RegisteredForEvents )
      {
         owner_Connection_25 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_3 )
         {
            {
               EasyTouch_On_LongTap component = owner_Connection_3.GetComponent<EasyTouch_On_LongTap>();
               if ( null == component )
               {
                  component = owner_Connection_3.AddComponent<EasyTouch_On_LongTap>();
               }
               if ( null != component )
               {
                  component.OnLongTapStart += Instance_OnLongTapStart_75;
                  component.OnLongTap += Instance_OnLongTap_75;
                  component.OnLongTapEnd += Instance_OnLongTapEnd_75;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_3 )
      {
         {
            EasyTouch_On_LongTap component = owner_Connection_3.GetComponent<EasyTouch_On_LongTap>();
            if ( null != component )
            {
               component.OnLongTapStart -= Instance_OnLongTapStart_75;
               component.OnLongTap -= Instance_OnLongTap_75;
               component.OnLongTapEnd -= Instance_OnLongTapEnd_75;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_10.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_13.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_15.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_17.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_22.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_27.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_30.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_32.SetParent(g);
      owner_Connection_3 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_11 = parentGameObject;
      owner_Connection_19 = parentGameObject;
      owner_Connection_20 = parentGameObject;
      owner_Connection_25 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnLongTapStart_75(object o, EasyTouch_On_LongTap.OnLongTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTapStart_75( );
   }
   
   void Instance_OnLongTap_75(object o, EasyTouch_On_LongTap.OnLongTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTap_75( );
   }
   
   void Instance_OnLongTapEnd_75(object o, EasyTouch_On_LongTap.OnLongTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTapEnd_75( );
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.In(logic_uScriptAct_SetRandomColor_RedMin_1, logic_uScriptAct_SetRandomColor_RedMax_1, logic_uScriptAct_SetRandomColor_GreenMin_1, logic_uScriptAct_SetRandomColor_GreenMax_1, logic_uScriptAct_SetRandomColor_BlueMin_1, logic_uScriptAct_SetRandomColor_BlueMax_1, logic_uScriptAct_SetRandomColor_AlphaMin_1, logic_uScriptAct_SetRandomColor_AlphaMax_1, out logic_uScriptAct_SetRandomColor_TargetColor_1);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_4.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_4, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_4[ index++ ] = owner_Connection_5;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_4 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.In(logic_uScriptAct_AssignMaterialColor_Target_4, logic_uScriptAct_AssignMaterialColor_MatColor_4, logic_uScriptAct_AssignMaterialColor_MatChannel_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_10()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_10, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_10[ index++ ] = owner_Connection_11;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_10.In(logic_uScriptAct_AssignMaterialColor_Target_10, logic_uScriptAct_AssignMaterialColor_MatColor_10, logic_uScriptAct_AssignMaterialColor_MatChannel_10);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_10.Out;
      
      if ( test_0 == true )
      {
         Relay_In_32();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_13 = local_ActionTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_13.In(logic_RN_FormatFloatString_f_Float_13, logic_RN_FormatFloatString_s_Modifier_13, out logic_RN_FormatFloatString_s_Result_13);
      local_ActionTimeString_System_String = logic_RN_FormatFloatString_s_Result_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_30();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_15.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_15, index + 1);
            }
            logic_uScriptCon_IsNull_Target_15[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_15.In(logic_uScriptCon_IsNull_Target_15);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_15.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_17 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_17 = owner_Connection_19;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_17.In(logic_uScriptCon_CompareGameObjects_A_17, logic_uScriptCon_CompareGameObjects_B_17, logic_uScriptCon_CompareGameObjects_CompareByTag_17, logic_uScriptCon_CompareGameObjects_CompareByName_17, logic_uScriptCon_CompareGameObjects_ReportNull_17);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_17.Same;
      
      if ( test_0 == true )
      {
         Relay_In_1();
      }
   }
   
   void Relay_In_22()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_22.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_22, index + 1);
            }
            logic_uScriptCon_IsNull_Target_22[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_22.In(logic_uScriptCon_IsNull_Target_22);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_22.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_24 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_24 = owner_Connection_20;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.In(logic_uScriptCon_CompareGameObjects_A_24, logic_uScriptCon_CompareGameObjects_B_24, logic_uScriptCon_CompareGameObjects_CompareByTag_24, logic_uScriptCon_CompareGameObjects_CompareByName_24, logic_uScriptCon_CompareGameObjects_ReportNull_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.Same;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_27()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_27.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_27, index + 1);
            }
            logic_uScriptCon_IsNull_Target_27[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_27.In(logic_uScriptCon_IsNull_Target_27);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_27.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_29 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_29 = owner_Connection_25;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.In(logic_uScriptCon_CompareGameObjects_A_29, logic_uScriptCon_CompareGameObjects_B_29, logic_uScriptCon_CompareGameObjects_CompareByTag_29, logic_uScriptCon_CompareGameObjects_CompareByName_29, logic_uScriptCon_CompareGameObjects_ReportNull_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.Same;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_In_30()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_30 = local_31_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_30 = local_ActionTimeString_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_30.In(logic_RN_SetTextMeshText_go_GameObject_30, logic_RN_SetTextMeshText_s_Text_30);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_32()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_32 = local_33_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_32 = local_12_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_32.In(logic_RN_SetTextMeshText_go_GameObject_32, logic_RN_SetTextMeshText_s_Text_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnLongTapStart_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_15();
   }
   
   void Relay_OnLongTap_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_22();
   }
   
   void Relay_OnLongTapEnd_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_27();
   }
   
}
