//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoDrag : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_39_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_39_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_41_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_41_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   System.String local_Result_System_String = "";
   System.String local_Swipe_System_String = "";
   System.Single local_SwipeAngle_System_Single = (float) 0;
   System.String local_SwipeFormatted_System_String = "";
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_2 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_9 = null;
   UnityEngine.GameObject owner_Connection_19 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_32 = null;
   UnityEngine.GameObject owner_Connection_37 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_0 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_0 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_0 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_0 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_0 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_0 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_0 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_0 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_0 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_0;
   bool logic_uScriptAct_SetRandomColor_Out_0 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_4 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_4 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_4 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_8 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_8 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_8 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_8 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_8 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_8 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_11 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_11 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_11 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_11;
   bool logic_RN_FormatFloatString_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_13 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_13 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_13 = " / ";
   System.String logic_uScriptAct_Concatenate_Result_13;
   bool logic_uScriptAct_Concatenate_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_18 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_18 = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_18 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_18 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_23 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_23 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_23 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_23 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_24 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_24 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_24 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_24 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_24 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_29 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_29 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_29 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_29 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_30 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_30 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_30 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_30 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_30 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_30 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_30 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_30 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_34 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_34 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_34 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_34 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_35 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_35 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_35 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_35 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_35 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_35 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_35 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_35 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_38 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_38 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_38 = "";
   bool logic_RN_SetTextMeshText_Out_38 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_40 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_40 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_40 = "Drag Me";
   bool logic_RN_SetTextMeshText_Out_40 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_91 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_91 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_91 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_91 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_91 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_91 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_91 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_91 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_91 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_91 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_91 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_91 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_91 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_91 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_91 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_91 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_91 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_91 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_91 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_91 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_91 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_91 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_91 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_91 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_91 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_91 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_91 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_91 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_91 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_91 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_39_UnityEngine_GameObject = GameObject.Find( "Sphere_Drag_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_41_UnityEngine_GameObject = GameObject.Find( "Sphere_Drag_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_2 || false == m_RegisteredForEvents )
      {
         owner_Connection_2 = parentGameObject;
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Drag2Fingers component = owner_Connection_2.GetComponent<EasyTouch_On_Drag2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Drag2Fingers>();
               }
               if ( null != component )
               {
                  component.OnDragStart2Fingers += Instance_OnDragStart2Fingers_91;
                  component.OnDrag2Fingers += Instance_OnDrag2Fingers_91;
                  component.OnDragEnd2Fingers += Instance_OnDragEnd2Fingers_91;
               }
            }
         }
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
      }
      if ( null == owner_Connection_9 || false == m_RegisteredForEvents )
      {
         owner_Connection_9 = parentGameObject;
      }
      if ( null == owner_Connection_19 || false == m_RegisteredForEvents )
      {
         owner_Connection_19 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
      if ( null == owner_Connection_32 || false == m_RegisteredForEvents )
      {
         owner_Connection_32 = parentGameObject;
      }
      if ( null == owner_Connection_37 || false == m_RegisteredForEvents )
      {
         owner_Connection_37 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Drag2Fingers component = owner_Connection_2.GetComponent<EasyTouch_On_Drag2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Drag2Fingers>();
               }
               if ( null != component )
               {
                  component.OnDragStart2Fingers += Instance_OnDragStart2Fingers_91;
                  component.OnDrag2Fingers += Instance_OnDrag2Fingers_91;
                  component.OnDragEnd2Fingers += Instance_OnDragEnd2Fingers_91;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_2 )
      {
         {
            EasyTouch_On_Drag2Fingers component = owner_Connection_2.GetComponent<EasyTouch_On_Drag2Fingers>();
            if ( null != component )
            {
               component.OnDragStart2Fingers -= Instance_OnDragStart2Fingers_91;
               component.OnDrag2Fingers -= Instance_OnDrag2Fingers_91;
               component.OnDragEnd2Fingers -= Instance_OnDragEnd2Fingers_91;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_0.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_8.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_11.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_23.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_29.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_30.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_34.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_35.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_38.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_40.SetParent(g);
      owner_Connection_2 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_9 = parentGameObject;
      owner_Connection_19 = parentGameObject;
      owner_Connection_27 = parentGameObject;
      owner_Connection_32 = parentGameObject;
      owner_Connection_37 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnDragStart2Fingers_91(object o, EasyTouch_On_Drag2Fingers.OnDrag2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_91 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_91 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_91 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_91 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_91 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_91 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_91 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_91 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_91 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_91 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_91 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_91 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_91 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_91 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_91 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_91 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_91 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_91 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_91 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_91 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_91 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_91 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_91 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_91 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_91 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_91 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_91 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_91 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_91 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_91 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_91 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_91 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_91 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_91 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_91 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_91 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_91 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_91 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_91 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_91 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_91 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_91 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_91 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragStart2Fingers_91( );
   }
   
   void Instance_OnDrag2Fingers_91(object o, EasyTouch_On_Drag2Fingers.OnDrag2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_91 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_91 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_91 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_91 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_91 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_91 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_91 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_91 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_91 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_91 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_91 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_91 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_91 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_91 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_91 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_91 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_91 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_91 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_91 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_91 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_91 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_91 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_91 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_91 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_91 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_91 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_91 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_91 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_91 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_91 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_91 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_91 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_91 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_91 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_91 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_91 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_91 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_91 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_91 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_91 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_91 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_91 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_91 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDrag2Fingers_91( );
   }
   
   void Instance_OnDragEnd2Fingers_91(object o, EasyTouch_On_Drag2Fingers.OnDrag2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_91 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_91 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_91 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_91 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_91 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_91 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_91 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_91 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_91 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_91 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_91 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_91 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_91 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_91 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_91 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_91 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_91 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_91 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_91 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_91 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_91 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_91 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_91 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_91 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_91 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_91 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_91 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_91 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_91 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_91 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_91 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_91 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_91 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_91 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_91 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_91 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_91 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_91 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_91 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_91 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_91 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_91 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_91 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragEnd2Fingers_91( );
   }
   
   void Relay_In_0()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_0.In(logic_uScriptAct_SetRandomColor_RedMin_0, logic_uScriptAct_SetRandomColor_RedMax_0, logic_uScriptAct_SetRandomColor_GreenMin_0, logic_uScriptAct_SetRandomColor_GreenMax_0, logic_uScriptAct_SetRandomColor_BlueMin_0, logic_uScriptAct_SetRandomColor_BlueMax_0, logic_uScriptAct_SetRandomColor_AlphaMin_0, logic_uScriptAct_SetRandomColor_AlphaMax_0, out logic_uScriptAct_SetRandomColor_TargetColor_0);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_0;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_0.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_4.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_4, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_4[ index++ ] = owner_Connection_5;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_4 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.In(logic_uScriptAct_AssignMaterialColor_Target_4, logic_uScriptAct_AssignMaterialColor_MatColor_4, logic_uScriptAct_AssignMaterialColor_MatChannel_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_8()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_8.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_8, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_8[ index++ ] = owner_Connection_9;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_8 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_8.In(logic_uScriptAct_SetGameObjectPosition_Target_8, logic_uScriptAct_SetGameObjectPosition_Position_8, logic_uScriptAct_SetGameObjectPosition_AsOffset_8, logic_uScriptAct_SetGameObjectPosition_AsLocal_8);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_8.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_11 = local_SwipeAngle_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_11.In(logic_RN_FormatFloatString_f_Float_11, logic_RN_FormatFloatString_s_Modifier_11, out logic_RN_FormatFloatString_s_Result_11);
      local_SwipeFormatted_System_String = logic_RN_FormatFloatString_s_Result_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_13.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_13, index + 1);
            }
            logic_uScriptAct_Concatenate_A_13[ index++ ] = local_SwipeFormatted_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_13.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_13, index + 1);
            }
            logic_uScriptAct_Concatenate_B_13[ index++ ] = local_Swipe_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.In(logic_uScriptAct_Concatenate_A_13, logic_uScriptAct_Concatenate_B_13, logic_uScriptAct_Concatenate_Separator_13, out logic_uScriptAct_Concatenate_Result_13);
      local_Result_System_String = logic_uScriptAct_Concatenate_Result_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_38();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_18, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_18[ index++ ] = owner_Connection_19;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.In(logic_uScriptAct_AssignMaterialColor_Target_18, logic_uScriptAct_AssignMaterialColor_MatColor_18, logic_uScriptAct_AssignMaterialColor_MatChannel_18);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.Out;
      
      if ( test_0 == true )
      {
         Relay_In_40();
      }
   }
   
   void Relay_In_23()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_23.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_23, index + 1);
            }
            logic_uScriptCon_IsNull_Target_23[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_23.In(logic_uScriptCon_IsNull_Target_23);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_23.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_24 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_24 = owner_Connection_27;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.In(logic_uScriptCon_CompareGameObjects_A_24, logic_uScriptCon_CompareGameObjects_B_24, logic_uScriptCon_CompareGameObjects_CompareByTag_24, logic_uScriptCon_CompareGameObjects_CompareByName_24, logic_uScriptCon_CompareGameObjects_ReportNull_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.Same;
      
      if ( test_0 == true )
      {
         Relay_In_0();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_29.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_29, index + 1);
            }
            logic_uScriptCon_IsNull_Target_29[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_29.In(logic_uScriptCon_IsNull_Target_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_29.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_30();
      }
   }
   
   void Relay_In_30()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_30 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_30 = owner_Connection_32;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_30.In(logic_uScriptCon_CompareGameObjects_A_30, logic_uScriptCon_CompareGameObjects_B_30, logic_uScriptCon_CompareGameObjects_CompareByTag_30, logic_uScriptCon_CompareGameObjects_CompareByName_30, logic_uScriptCon_CompareGameObjects_ReportNull_30);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_30.Same;
      
      if ( test_0 == true )
      {
         Relay_In_8();
      }
   }
   
   void Relay_In_34()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_34.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_34, index + 1);
            }
            logic_uScriptCon_IsNull_Target_34[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_34.In(logic_uScriptCon_IsNull_Target_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_34.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_35();
      }
   }
   
   void Relay_In_35()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_35 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_35 = owner_Connection_37;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_35.In(logic_uScriptCon_CompareGameObjects_A_35, logic_uScriptCon_CompareGameObjects_B_35, logic_uScriptCon_CompareGameObjects_CompareByTag_35, logic_uScriptCon_CompareGameObjects_CompareByName_35, logic_uScriptCon_CompareGameObjects_ReportNull_35);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_35.Same;
      
      if ( test_0 == true )
      {
         Relay_In_18();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_38 = local_39_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_38 = local_Result_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_38.In(logic_RN_SetTextMeshText_go_GameObject_38, logic_RN_SetTextMeshText_s_Text_38);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_40()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_40 = local_41_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_40.In(logic_RN_SetTextMeshText_go_GameObject_40, logic_RN_SetTextMeshText_s_Text_40);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnDragStart2Fingers_91()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_91;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_91;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_91;
      local_SwipeAngle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_91;
      Relay_In_23();
   }
   
   void Relay_OnDrag2Fingers_91()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_91;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_91;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_91;
      local_SwipeAngle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_91;
      Relay_In_29();
   }
   
   void Relay_OnDragEnd2Fingers_91()
   {
      local_Swipe_System_String = event_UnityEngine_GameObject_s_SwipeDirection_91;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_91;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_91;
      local_SwipeAngle_System_Single = event_UnityEngine_GameObject_f_SwipeDragAngle_91;
      Relay_In_34();
   }
   
}
