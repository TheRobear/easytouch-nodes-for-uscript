//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Player", "")]
public class ThirdPersonCam : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Single DistanceAway = (float) 0;
   public System.Single DistanceUp = (float) 0;
   UnityEngine.Vector3 local_AddResult01_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_DeltaTime_System_Single = (float) 0;
   System.Single local_MoveTime_System_Single = (float) 0;
   UnityEngine.Vector3 local_MultiplyResult01_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_MultiplyResult02_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject local_Player_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Player_UnityEngine_GameObject_previous = null;
   UnityEngine.Vector3 local_PlayerDir_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_PlayerPos_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_Subtract01_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_Up_UnityEngine_Vector3 = new Vector3( (float)0, (float)1, (float)0 );
   public System.Single Smooth = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_24 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_6;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_AddVector3_v2 logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12 = new uScriptAct_AddVector3_v2( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_A_12 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_B_12 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_Result_12;
   bool logic_uScriptAct_AddVector3_v2_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractVector3 logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_16 = new uScriptAct_SubtractVector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_A_16 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_B_16 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_Result_16;
   bool logic_uScriptAct_SubtractVector3_Out_16 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_20 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_20 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_20 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_20;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_20;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_20 = true;
   //pointer to script instanced logic node
   uScriptAct_LookAt logic_uScriptAct_LookAt_uScriptAct_LookAt_25 = new uScriptAct_LookAt( );
   UnityEngine.GameObject[] logic_uScriptAct_LookAt_Target_25 = new UnityEngine.GameObject[] {};
   System.Object logic_uScriptAct_LookAt_Focus_25 = "";
   System.Single logic_uScriptAct_LookAt_time_25 = (float) 0;
   uScriptAct_LookAt.LockAxis logic_uScriptAct_LookAt_lockAxis_25 = uScriptAct_LookAt.LockAxis.None;
   bool logic_uScriptAct_LookAt_Out_25 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_29 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_29;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_29;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_29;
   bool logic_uScriptAct_GetDeltaTime_Out_29 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectsByTag logic_uScriptAct_GetGameObjectsByTag_uScriptAct_GetGameObjectsByTag_32 = new uScriptAct_GetGameObjectsByTag( );
   System.String logic_uScriptAct_GetGameObjectsByTag_Tag_32 = "Player";
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectsByTag_FirstGameObject_32;
   UnityEngine.GameObject[] logic_uScriptAct_GetGameObjectsByTag_GameObjects_32;
   System.Int32 logic_uScriptAct_GetGameObjectsByTag_GameObjectCount_32;
   bool logic_uScriptAct_GetGameObjectsByTag_Out_32 = true;
   bool logic_uScriptAct_GetGameObjectsByTag_GameObjectsFound_32 = true;
   bool logic_uScriptAct_GetGameObjectsByTag_GameObjectsNotFound_32 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_33 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_33 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_33 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_33;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_33;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_33;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_33;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_33;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_33;
   bool logic_uScriptAct_GetPositionAndRotation_Out_33 = true;
   //pointer to script instanced logic node
   RN_ReturnTransformDirection logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_36 = new RN_ReturnTransformDirection( );
   UnityEngine.GameObject logic_RN_ReturnTransformDirection_go_GameObject_36 = default(UnityEngine.GameObject);
   RN_ReturnTransformDirection.Direction logic_RN_ReturnTransformDirection_d_Direction_36 = RN_ReturnTransformDirection.Direction.Forward;
   UnityEngine.Vector3 logic_RN_ReturnTransformDirection_v3_Direction_36;
   bool logic_RN_ReturnTransformDirection_Out_36 = true;
   //pointer to script instanced logic node
   RN_MoveToLocationLateUpdate logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85 = new RN_MoveToLocationLateUpdate( );
   UnityEngine.GameObject[] logic_RN_MoveToLocationLateUpdate_g_GameObject_85 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_RN_MoveToLocationLateUpdate_v3_Location_85 = new Vector3( );
   System.Boolean logic_RN_MoveToLocationLateUpdate_b_AsOffset_85 = (bool) false;
   System.Single logic_RN_MoveToLocationLateUpdate_f_Time_85 = (float) 0;
   bool logic_RN_MoveToLocationLateUpdate_Out_85 = true;
   bool logic_RN_MoveToLocationLateUpdate_Cancelled_85 = true;
   
   //event nodes
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Global component = owner_Connection_1.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_0;
                  component.uScriptLateStart += Instance_uScriptLateStart_0;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
         if ( null != owner_Connection_4 )
         {
            {
               uScript_Update component = owner_Connection_4.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = owner_Connection_4.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_3;
                  component.OnLateUpdate += Instance_OnLateUpdate_3;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_3;
               }
            }
         }
      }
      if ( null == owner_Connection_24 || false == m_RegisteredForEvents )
      {
         owner_Connection_24 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Global component = owner_Connection_1.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_0;
                  component.uScriptLateStart += Instance_uScriptLateStart_0;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_4 )
         {
            {
               uScript_Update component = owner_Connection_4.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = owner_Connection_4.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_3;
                  component.OnLateUpdate += Instance_OnLateUpdate_3;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_3;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            uScript_Global component = owner_Connection_1.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_0;
               component.uScriptLateStart -= Instance_uScriptLateStart_0;
            }
         }
      }
      if ( null != owner_Connection_4 )
      {
         {
            uScript_Update component = owner_Connection_4.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_3;
               component.OnLateUpdate -= Instance_OnLateUpdate_3;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_3;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.SetParent(g);
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12.SetParent(g);
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_16.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_20.SetParent(g);
      logic_uScriptAct_LookAt_uScriptAct_LookAt_25.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_29.SetParent(g);
      logic_uScriptAct_GetGameObjectsByTag_uScriptAct_GetGameObjectsByTag_32.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_33.SetParent(g);
      logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_36.SetParent(g);
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.SetParent(g);
      owner_Connection_1 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_24 = parentGameObject;
      owner_Connection_27 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_LookAt_uScriptAct_LookAt_25.Finished += uScriptAct_LookAt_Finished_25;
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.Finished += RN_MoveToLocationLateUpdate_Finished_85;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LookAt_uScriptAct_LookAt_25.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LookAt_uScriptAct_LookAt_25.Finished -= uScriptAct_LookAt_Finished_25;
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.Finished -= RN_MoveToLocationLateUpdate_Finished_85;
   }
   
   public void LateUpdate()
   {
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.LateUpdate( );
   }
   
   void Instance_uScriptStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_0( );
   }
   
   void Instance_uScriptLateStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_0( );
   }
   
   void Instance_OnUpdate_3(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_3( );
   }
   
   void Instance_OnLateUpdate_3(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_3( );
   }
   
   void Instance_OnFixedUpdate_3(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_3( );
   }
   
   void uScriptAct_LookAt_Finished_25(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_25( );
   }
   
   void RN_MoveToLocationLateUpdate_Finished_85(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_85( );
   }
   
   void Relay_uScriptStart_0()
   {
      Relay_In_32();
   }
   
   void Relay_uScriptLateStart_0()
   {
   }
   
   void Relay_OnUpdate_3()
   {
   }
   
   void Relay_OnLateUpdate_3()
   {
      Relay_In_29();
   }
   
   void Relay_OnFixedUpdate_3()
   {
   }
   
   void Relay_In_6()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6 = local_Up_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6 = DistanceUp;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6, out logic_uScriptAct_MultiplyVector3WithFloat_Result_6);
      local_MultiplyResult01_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_36();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9 = local_PlayerDir_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = DistanceAway;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9, out logic_uScriptAct_MultiplyVector3WithFloat_Result_9);
      local_MultiplyResult02_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_33();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            logic_uScriptAct_AddVector3_v2_A_12 = local_PlayerPos_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_AddVector3_v2_B_12 = local_MultiplyResult01_UnityEngine_Vector3;
            
         }
         {
         }
      }
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12.In(logic_uScriptAct_AddVector3_v2_A_12, logic_uScriptAct_AddVector3_v2_B_12, out logic_uScriptAct_AddVector3_v2_Result_12);
      local_AddResult01_UnityEngine_Vector3 = logic_uScriptAct_AddVector3_v2_Result_12;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12.Out;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            logic_uScriptAct_SubtractVector3_A_16 = local_AddResult01_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_SubtractVector3_B_16 = local_MultiplyResult02_UnityEngine_Vector3;
            
         }
         {
         }
      }
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_16.In(logic_uScriptAct_SubtractVector3_A_16, logic_uScriptAct_SubtractVector3_B_16, out logic_uScriptAct_SubtractVector3_Result_16);
      local_Subtract01_UnityEngine_Vector3 = logic_uScriptAct_SubtractVector3_Result_16;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_16.Out;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_20 = local_DeltaTime_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_20 = Smooth;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_20.In(logic_uScriptAct_MultiplyFloat_v2_A_20, logic_uScriptAct_MultiplyFloat_v2_B_20, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_20, out logic_uScriptAct_MultiplyFloat_v2_IntResult_20);
      local_MoveTime_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_20;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_20.Out;
      
      if ( test_0 == true )
      {
         Relay_In_85();
      }
   }
   
   void Relay_Finished_25()
   {
   }
   
   void Relay_In_25()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_LookAt_Target_25.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_LookAt_Target_25, index + 1);
            }
            logic_uScriptAct_LookAt_Target_25[ index++ ] = owner_Connection_24;
            
         }
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_LookAt_Focus_25 = local_Player_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LookAt_uScriptAct_LookAt_25.In(logic_uScriptAct_LookAt_Target_25, logic_uScriptAct_LookAt_Focus_25, logic_uScriptAct_LookAt_time_25, logic_uScriptAct_LookAt_lockAxis_25);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_29()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_29.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_29, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_29, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_29);
      local_DeltaTime_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_29;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectsByTag_uScriptAct_GetGameObjectsByTag_32.In(logic_uScriptAct_GetGameObjectsByTag_Tag_32, out logic_uScriptAct_GetGameObjectsByTag_FirstGameObject_32, out logic_uScriptAct_GetGameObjectsByTag_GameObjects_32, out logic_uScriptAct_GetGameObjectsByTag_GameObjectCount_32);
      local_Player_UnityEngine_GameObject = logic_uScriptAct_GetGameObjectsByTag_FirstGameObject_32;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_33()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetPositionAndRotation_Target_33 = local_Player_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_33.In(logic_uScriptAct_GetPositionAndRotation_Target_33, logic_uScriptAct_GetPositionAndRotation_GetLocal_33, out logic_uScriptAct_GetPositionAndRotation_Position_33, out logic_uScriptAct_GetPositionAndRotation_Rotation_33, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_33, out logic_uScriptAct_GetPositionAndRotation_Forward_33, out logic_uScriptAct_GetPositionAndRotation_Up_33, out logic_uScriptAct_GetPositionAndRotation_Right_33);
      local_PlayerPos_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_33;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_33.Out;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_36()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_ReturnTransformDirection_go_GameObject_36 = local_Player_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
      }
      logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_36.In(logic_RN_ReturnTransformDirection_go_GameObject_36, logic_RN_ReturnTransformDirection_d_Direction_36, out logic_RN_ReturnTransformDirection_v3_Direction_36);
      local_PlayerDir_UnityEngine_Vector3 = logic_RN_ReturnTransformDirection_v3_Direction_36;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_ReturnTransformDirection_RN_ReturnTransformDirection_36.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_Finished_85()
   {
   }
   
   void Relay_In_85()
   {
      {
         {
            int index = 0;
            if ( logic_RN_MoveToLocationLateUpdate_g_GameObject_85.Length <= index)
            {
               System.Array.Resize(ref logic_RN_MoveToLocationLateUpdate_g_GameObject_85, index + 1);
            }
            logic_RN_MoveToLocationLateUpdate_g_GameObject_85[ index++ ] = owner_Connection_27;
            
         }
         {
            logic_RN_MoveToLocationLateUpdate_v3_Location_85 = local_Subtract01_UnityEngine_Vector3;
            
         }
         {
         }
         {
            logic_RN_MoveToLocationLateUpdate_f_Time_85 = local_MoveTime_System_Single;
            
         }
      }
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.In(logic_RN_MoveToLocationLateUpdate_g_GameObject_85, logic_RN_MoveToLocationLateUpdate_v3_Location_85, logic_RN_MoveToLocationLateUpdate_b_AsOffset_85, logic_RN_MoveToLocationLateUpdate_f_Time_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_Cancel_85()
   {
      {
         {
            int index = 0;
            if ( logic_RN_MoveToLocationLateUpdate_g_GameObject_85.Length <= index)
            {
               System.Array.Resize(ref logic_RN_MoveToLocationLateUpdate_g_GameObject_85, index + 1);
            }
            logic_RN_MoveToLocationLateUpdate_g_GameObject_85[ index++ ] = owner_Connection_27;
            
         }
         {
            logic_RN_MoveToLocationLateUpdate_v3_Location_85 = local_Subtract01_UnityEngine_Vector3;
            
         }
         {
         }
         {
            logic_RN_MoveToLocationLateUpdate_f_Time_85 = local_MoveTime_System_Single;
            
         }
      }
      logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.Cancel(logic_RN_MoveToLocationLateUpdate_g_GameObject_85, logic_RN_MoveToLocationLateUpdate_v3_Location_85, logic_RN_MoveToLocationLateUpdate_b_AsOffset_85, logic_RN_MoveToLocationLateUpdate_f_Time_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_MoveToLocationLateUpdate_RN_MoveToLocationLateUpdate_85.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
}
