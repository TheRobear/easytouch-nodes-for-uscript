//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class FingerTouch2 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 DeltaPos = new Vector3( (float)0, (float)0, (float)0 );
   public UnityEngine.GameObject FingerIDObj = default(UnityEngine.GameObject);
   UnityEngine.GameObject FingerIDObj_previous = null;
   [Multiline(3)]
   public System.String FingerIDText = "";
   System.Int32 local_FingerID_System_Int32 = (int) 0;
   System.String local_ObjectFingerID_System_String = "FingerID";
   UnityEngine.Vector3 local_ObjPos_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_ObjWorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.String local_TFIndex_System_String = "";
   UnityEngine.GameObject local_ThisObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_ThisObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   public UnityEngine.Vector3 NewPos = new Vector3( (float)0, (float)0, (float)0 );
   [Multiline(3)]
   public System.String ObjectName = "";
   public UnityEngine.GameObject PickedObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject PickedObject_previous = null;
   public UnityEngine.Vector3 SwipePos = new Vector3( (float)0, (float)0, (float)0 );
   public System.Int32 TouchFingerIndex = (int) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_18 = null;
   UnityEngine.GameObject owner_Connection_33 = null;
   UnityEngine.GameObject owner_Connection_104 = null;
   UnityEngine.GameObject owner_Connection_105 = null;
   UnityEngine.GameObject owner_Connection_106 = null;
   UnityEngine.GameObject owner_Connection_107 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_IntToString logic_uScriptAct_IntToString_uScriptAct_IntToString_2 = new uScriptAct_IntToString( );
   System.Int32 logic_uScriptAct_IntToString_Target_2 = (int) 0;
   uScriptAct_IntToString.FormatType logic_uScriptAct_IntToString_StandardFormat_2 = uScriptAct_IntToString.FormatType.General;
   System.String logic_uScriptAct_IntToString_CustomFormat_2 = "";
   System.String logic_uScriptAct_IntToString_CustomCulture_2 = "";
   System.String logic_uScriptAct_IntToString_Result_2;
   bool logic_uScriptAct_IntToString_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractInt logic_uScriptAct_SubtractInt_uScriptAct_SubtractInt_3 = new uScriptAct_SubtractInt( );
   System.Int32 logic_uScriptAct_SubtractInt_A_3 = (int) 0;
   System.Int32 logic_uScriptAct_SubtractInt_B_3 = (int) 1;
   System.Int32 logic_uScriptAct_SubtractInt_IntResult_3;
   System.Single logic_uScriptAct_SubtractInt_FloatResult_3;
   bool logic_uScriptAct_SubtractInt_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_6 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_6 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_6 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_6 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_6 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_7 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_7 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_7 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_7 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Destroy logic_uScriptAct_Destroy_uScriptAct_Destroy_8 = new uScriptAct_Destroy( );
   UnityEngine.GameObject[] logic_uScriptAct_Destroy_Target_8 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_Destroy_DelayTime_8 = (float) 0;
   bool logic_uScriptAct_Destroy_Out_8 = true;
   bool logic_uScriptAct_Destroy_ObjectsDestroyed_8 = true;
   bool logic_uScriptAct_Destroy_WaitOneTick_8 = false;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_10 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_10 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_10 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_10 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_12 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_12 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_12 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_12 = true;
   //pointer to script instanced logic node
   uScriptAct_GetChildrenByName logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_15 = new uScriptAct_GetChildrenByName( );
   UnityEngine.GameObject logic_uScriptAct_GetChildrenByName_Target_15 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetChildrenByName_Name_15 = "";
   uScriptAct_GetChildrenByName.SearchType logic_uScriptAct_GetChildrenByName_SearchMethod_15 = uScriptAct_GetChildrenByName.SearchType.Matches;
   System.Boolean logic_uScriptAct_GetChildrenByName_recursive_15 = (bool) false;
   UnityEngine.GameObject logic_uScriptAct_GetChildrenByName_FirstChild_15;
   UnityEngine.GameObject[] logic_uScriptAct_GetChildrenByName_Children_15;
   System.Int32 logic_uScriptAct_GetChildrenByName_ChildrenCount_15;
   bool logic_uScriptAct_GetChildrenByName_Out_15 = true;
   bool logic_uScriptAct_GetChildrenByName_ChildrenFound_15 = true;
   bool logic_uScriptAct_GetChildrenByName_ChildrenNotFound_15 = true;
   //pointer to script instanced logic node
   uScriptAct_GetChildrenByName logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_16 = new uScriptAct_GetChildrenByName( );
   UnityEngine.GameObject logic_uScriptAct_GetChildrenByName_Target_16 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetChildrenByName_Name_16 = "";
   uScriptAct_GetChildrenByName.SearchType logic_uScriptAct_GetChildrenByName_SearchMethod_16 = uScriptAct_GetChildrenByName.SearchType.Matches;
   System.Boolean logic_uScriptAct_GetChildrenByName_recursive_16 = (bool) false;
   UnityEngine.GameObject logic_uScriptAct_GetChildrenByName_FirstChild_16;
   UnityEngine.GameObject[] logic_uScriptAct_GetChildrenByName_Children_16;
   System.Int32 logic_uScriptAct_GetChildrenByName_ChildrenCount_16;
   bool logic_uScriptAct_GetChildrenByName_Out_16 = true;
   bool logic_uScriptAct_GetChildrenByName_ChildrenFound_16 = true;
   bool logic_uScriptAct_GetChildrenByName_ChildrenNotFound_16 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_19 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_19 = (int) 0;
   bool logic_uScriptCon_CompareInt_GreaterThan_19 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_19 = true;
   bool logic_uScriptCon_CompareInt_LessThan_19 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_23 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_23 = (int) -1;
   System.Int32 logic_uScriptAct_SetInt_Target_23;
   bool logic_uScriptAct_SetInt_Out_23 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_27 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_27 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_27 = (int) 0;
   bool logic_uScriptCon_CompareInt_GreaterThan_27 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_27 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_27 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_27 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_27 = true;
   bool logic_uScriptCon_CompareInt_LessThan_27 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_28 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_28 = (int) 0;
   System.Int32 logic_uScriptAct_SetInt_Target_28;
   bool logic_uScriptAct_SetInt_Out_28 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_35 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_35 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_35 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_35 = "/";
   System.String logic_uScriptAct_Concatenate_Result_35;
   bool logic_uScriptAct_Concatenate_Out_35 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectName logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_46 = new uScriptAct_GetGameObjectName( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectName_gameObject_46 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_GetGameObjectName_name_46;
   bool logic_uScriptAct_GetGameObjectName_Out_46 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_47 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_47 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_47 = "";
   bool logic_RN_SetTextMeshText_Out_47 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_48 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_48 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_48 = "";
   bool logic_RN_SetTextMeshText_Out_48 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectByName logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_51 = new uScriptAct_GetGameObjectByName( );
   System.String logic_uScriptAct_GetGameObjectByName_Name_51 = "";
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectByName_gameObject_51;
   bool logic_uScriptAct_GetGameObjectByName_Out_51 = true;
   bool logic_uScriptAct_GetGameObjectByName_GameObjectFound_51 = true;
   bool logic_uScriptAct_GetGameObjectByName_GameObjectNotFound_51 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_55 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_55 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_55 = "";
   bool logic_RN_SetTextMeshText_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_IntToString logic_uScriptAct_IntToString_uScriptAct_IntToString_56 = new uScriptAct_IntToString( );
   System.Int32 logic_uScriptAct_IntToString_Target_56 = (int) 0;
   uScriptAct_IntToString.FormatType logic_uScriptAct_IntToString_StandardFormat_56 = uScriptAct_IntToString.FormatType.General;
   System.String logic_uScriptAct_IntToString_CustomFormat_56 = "";
   System.String logic_uScriptAct_IntToString_CustomCulture_56 = "";
   System.String logic_uScriptAct_IntToString_Result_56;
   bool logic_uScriptAct_IntToString_Out_56 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_61 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_61 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_61 = "";
   bool logic_RN_SetTextMeshText_Out_61 = true;
   //pointer to script instanced logic node
   uScriptAct_IntToString logic_uScriptAct_IntToString_uScriptAct_IntToString_62 = new uScriptAct_IntToString( );
   System.Int32 logic_uScriptAct_IntToString_Target_62 = (int) 0;
   uScriptAct_IntToString.FormatType logic_uScriptAct_IntToString_StandardFormat_62 = uScriptAct_IntToString.FormatType.General;
   System.String logic_uScriptAct_IntToString_CustomFormat_62 = "";
   System.String logic_uScriptAct_IntToString_CustomCulture_62 = "";
   System.String logic_uScriptAct_IntToString_Result_62;
   bool logic_uScriptAct_IntToString_Out_62 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_67 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_67 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_67 = "";
   bool logic_RN_SetTextMeshText_Out_67 = true;
   //pointer to script instanced logic node
   uScriptAct_IntToString logic_uScriptAct_IntToString_uScriptAct_IntToString_68 = new uScriptAct_IntToString( );
   System.Int32 logic_uScriptAct_IntToString_Target_68 = (int) 0;
   uScriptAct_IntToString.FormatType logic_uScriptAct_IntToString_StandardFormat_68 = uScriptAct_IntToString.FormatType.General;
   System.String logic_uScriptAct_IntToString_CustomFormat_68 = "";
   System.String logic_uScriptAct_IntToString_CustomCulture_68 = "";
   System.String logic_uScriptAct_IntToString_Result_68;
   bool logic_uScriptAct_IntToString_Out_68 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractVector3 logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_72 = new uScriptAct_SubtractVector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_A_72 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_B_72 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_Result_72;
   bool logic_uScriptAct_SubtractVector3_Out_72 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractVector3 logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_79 = new uScriptAct_SubtractVector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_A_79 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_B_79 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_Result_79;
   bool logic_uScriptAct_SubtractVector3_Out_79 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_85 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_85 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_85 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_85 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_85 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_86 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_86 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_86 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_86 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_86 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractVector3 logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_89 = new uScriptAct_SubtractVector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_A_89 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_B_89 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_SubtractVector3_Result_89;
   bool logic_uScriptAct_SubtractVector3_Out_89 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_90 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_90 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_90 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_90;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_90;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_90;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_90;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_90;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_90;
   bool logic_uScriptAct_GetPositionAndRotation_Out_90 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectByName logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_93 = new uScriptAct_GetGameObjectByName( );
   System.String logic_uScriptAct_GetGameObjectByName_Name_93 = "";
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectByName_gameObject_93;
   bool logic_uScriptAct_GetGameObjectByName_Out_93 = true;
   bool logic_uScriptAct_GetGameObjectByName_GameObjectFound_93 = true;
   bool logic_uScriptAct_GetGameObjectByName_GameObjectNotFound_93 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_109 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_109 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_109 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_109 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_109 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_201 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_201 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_201 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_201 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_201 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_201 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_201 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_201 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_201 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_201 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_201 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_201 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_201 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_201 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_201 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_201 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_201 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_201 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_201 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_201 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_201 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_201 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_201 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_201 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_201 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_201 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_201 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_201 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_201 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_201 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_208 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_208 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_208 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_208 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_208 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_208 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_208 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_208 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_208 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_208 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_208 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_208 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_208 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_208 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_208 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_208 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_208 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_208 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208 = new Vector3( );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_208 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_208 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_208 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_208 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_208 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_208 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_208 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_208 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_208 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_208 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_208 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_213 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_213 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_213 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_213 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_213 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_213 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_213 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_213 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_213 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_213 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_213 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_213 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_213 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_213 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_213 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_213 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_213 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_213 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_213 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_213 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_213 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_213 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_213 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_213 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_213 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_213 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_213 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_213 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_213 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_213 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_219 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_219 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_219 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_219 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_219 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_219 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_219 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_219 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_219 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_219 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_219 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_219 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_219 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_219 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_219 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_219 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_219 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_219 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_219 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_219 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_219 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_219 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_219 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_219 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_219 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_219 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_219 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_219 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_219 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_219 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         FingerIDObj_previous = FingerIDObj;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_18 || false == m_RegisteredForEvents )
      {
         owner_Connection_18 = parentGameObject;
         if ( null != owner_Connection_18 )
         {
            {
               uScript_Global component = owner_Connection_18.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_18.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_17;
                  component.uScriptLateStart += Instance_uScriptLateStart_17;
               }
            }
         }
      }
      if ( null == owner_Connection_33 || false == m_RegisteredForEvents )
      {
         owner_Connection_33 = parentGameObject;
      }
      if ( null == owner_Connection_104 || false == m_RegisteredForEvents )
      {
         owner_Connection_104 = parentGameObject;
         if ( null != owner_Connection_104 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_104.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_104.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_201;
                  component.OnTouchDown += Instance_OnTouchDown_201;
                  component.OnTouchUp += Instance_OnTouchUp_201;
               }
            }
         }
      }
      if ( null == owner_Connection_105 || false == m_RegisteredForEvents )
      {
         owner_Connection_105 = parentGameObject;
         if ( null != owner_Connection_105 )
         {
            {
               EasyTouch_On_Drag component = owner_Connection_105.GetComponent<EasyTouch_On_Drag>();
               if ( null == component )
               {
                  component = owner_Connection_105.AddComponent<EasyTouch_On_Drag>();
               }
               if ( null != component )
               {
                  component.OnDragStart += Instance_OnDragStart_208;
                  component.OnDrag += Instance_OnDrag_208;
                  component.OnDragEnd += Instance_OnDragEnd_208;
               }
            }
         }
      }
      if ( null == owner_Connection_106 || false == m_RegisteredForEvents )
      {
         owner_Connection_106 = parentGameObject;
         if ( null != owner_Connection_106 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_106.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_106.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_213;
                  component.OnSwipe += Instance_OnSwipe_213;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_213;
               }
            }
         }
      }
      if ( null == owner_Connection_107 || false == m_RegisteredForEvents )
      {
         owner_Connection_107 = parentGameObject;
         if ( null != owner_Connection_107 )
         {
            {
               EasyTouch_On_DoubleTap component = owner_Connection_107.GetComponent<EasyTouch_On_DoubleTap>();
               if ( null == component )
               {
                  component = owner_Connection_107.AddComponent<EasyTouch_On_DoubleTap>();
               }
               if ( null != component )
               {
                  component.OnDoubleTap += Instance_OnDoubleTap_219;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         FingerIDObj_previous = FingerIDObj;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_18 )
         {
            {
               uScript_Global component = owner_Connection_18.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_18.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_17;
                  component.uScriptLateStart += Instance_uScriptLateStart_17;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_104 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_104.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_104.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_201;
                  component.OnTouchDown += Instance_OnTouchDown_201;
                  component.OnTouchUp += Instance_OnTouchUp_201;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_105 )
         {
            {
               EasyTouch_On_Drag component = owner_Connection_105.GetComponent<EasyTouch_On_Drag>();
               if ( null == component )
               {
                  component = owner_Connection_105.AddComponent<EasyTouch_On_Drag>();
               }
               if ( null != component )
               {
                  component.OnDragStart += Instance_OnDragStart_208;
                  component.OnDrag += Instance_OnDrag_208;
                  component.OnDragEnd += Instance_OnDragEnd_208;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_106 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_106.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_106.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_213;
                  component.OnSwipe += Instance_OnSwipe_213;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_213;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_107 )
         {
            {
               EasyTouch_On_DoubleTap component = owner_Connection_107.GetComponent<EasyTouch_On_DoubleTap>();
               if ( null == component )
               {
                  component = owner_Connection_107.AddComponent<EasyTouch_On_DoubleTap>();
               }
               if ( null != component )
               {
                  component.OnDoubleTap += Instance_OnDoubleTap_219;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_18 )
      {
         {
            uScript_Global component = owner_Connection_18.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_17;
               component.uScriptLateStart -= Instance_uScriptLateStart_17;
            }
         }
      }
      if ( null != owner_Connection_104 )
      {
         {
            EasyTouch_On_Touch component = owner_Connection_104.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_201;
               component.OnTouchDown -= Instance_OnTouchDown_201;
               component.OnTouchUp -= Instance_OnTouchUp_201;
            }
         }
      }
      if ( null != owner_Connection_105 )
      {
         {
            EasyTouch_On_Drag component = owner_Connection_105.GetComponent<EasyTouch_On_Drag>();
            if ( null != component )
            {
               component.OnDragStart -= Instance_OnDragStart_208;
               component.OnDrag -= Instance_OnDrag_208;
               component.OnDragEnd -= Instance_OnDragEnd_208;
            }
         }
      }
      if ( null != owner_Connection_106 )
      {
         {
            EasyTouch_On_Swipe component = owner_Connection_106.GetComponent<EasyTouch_On_Swipe>();
            if ( null != component )
            {
               component.OnSwipeStart -= Instance_OnSwipeStart_213;
               component.OnSwipe -= Instance_OnSwipe_213;
               component.OnSwipeEnd -= Instance_OnSwipeEnd_213;
            }
         }
      }
      if ( null != owner_Connection_107 )
      {
         {
            EasyTouch_On_DoubleTap component = owner_Connection_107.GetComponent<EasyTouch_On_DoubleTap>();
            if ( null != component )
            {
               component.OnDoubleTap -= Instance_OnDoubleTap_219;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_IntToString_uScriptAct_IntToString_2.SetParent(g);
      logic_uScriptAct_SubtractInt_uScriptAct_SubtractInt_3.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_6.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.SetParent(g);
      logic_uScriptAct_Destroy_uScriptAct_Destroy_8.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_10.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_12.SetParent(g);
      logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_15.SetParent(g);
      logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_16.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_23.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_27.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_28.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_35.SetParent(g);
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_46.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_47.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_48.SetParent(g);
      logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_51.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_55.SetParent(g);
      logic_uScriptAct_IntToString_uScriptAct_IntToString_56.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_61.SetParent(g);
      logic_uScriptAct_IntToString_uScriptAct_IntToString_62.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_67.SetParent(g);
      logic_uScriptAct_IntToString_uScriptAct_IntToString_68.SetParent(g);
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_72.SetParent(g);
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_79.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_85.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_86.SetParent(g);
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_89.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_90.SetParent(g);
      logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_93.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_109.SetParent(g);
      owner_Connection_18 = parentGameObject;
      owner_Connection_33 = parentGameObject;
      owner_Connection_104 = parentGameObject;
      owner_Connection_105 = parentGameObject;
      owner_Connection_106 = parentGameObject;
      owner_Connection_107 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      if (true == logic_uScriptAct_Destroy_WaitOneTick_8)
      {
         Relay_WaitOneTick_8();
      }
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_uScriptStart_17(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_17( );
   }
   
   void Instance_uScriptLateStart_17(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_17( );
   }
   
   void Instance_OnTouchStart_201(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_201 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_201 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_201 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_201 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_201 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_201 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_201 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_201 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_201 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_201 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_201 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_201 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_201 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_201 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_201 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_201 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_201 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_201 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_201 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_201 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_201 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_201 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_201 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_201 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_201 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_201 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_201 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_201 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_201 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_201 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_201 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_201 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_201 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_201 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_201 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_201 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_201 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_201 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_201 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_201 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_201 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_201 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_201 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_201( );
   }
   
   void Instance_OnTouchDown_201(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_201 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_201 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_201 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_201 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_201 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_201 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_201 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_201 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_201 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_201 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_201 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_201 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_201 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_201 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_201 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_201 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_201 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_201 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_201 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_201 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_201 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_201 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_201 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_201 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_201 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_201 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_201 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_201 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_201 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_201 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_201 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_201 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_201 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_201 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_201 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_201 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_201 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_201 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_201 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_201 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_201 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_201 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_201 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_201( );
   }
   
   void Instance_OnTouchUp_201(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_201 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_201 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_201 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_201 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_201 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_201 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_201 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_201 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_201 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_201 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_201 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_201 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_201 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_201 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_201 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_201 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_201 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_201 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_201 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_201 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_201 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_201 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_201 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_201 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_201 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_201 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_201 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_201 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_201 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_201 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_201 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_201 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_201 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_201 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_201 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_201 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_201 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_201 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_201 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_201 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_201 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_201 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_201 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_201( );
   }
   
   void Instance_OnDragStart_208(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_208 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_208 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_208 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_208 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_208 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_208 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_208 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_208 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_208 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_208 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_208 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_208 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_208 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_208 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_208 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_208 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_208 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_208 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_208 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_208 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_208 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_208 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_208 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_208 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_208 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_208 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_208 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_208 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_208 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_208 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_208 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_208 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_208 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_208 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_208 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_208 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_208 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_208 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_208 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_208 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_208 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_208 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragStart_208( );
   }
   
   void Instance_OnDrag_208(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_208 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_208 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_208 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_208 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_208 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_208 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_208 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_208 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_208 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_208 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_208 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_208 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_208 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_208 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_208 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_208 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_208 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_208 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_208 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_208 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_208 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_208 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_208 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_208 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_208 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_208 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_208 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_208 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_208 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_208 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_208 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_208 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_208 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_208 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_208 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_208 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_208 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_208 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_208 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_208 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_208 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_208 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDrag_208( );
   }
   
   void Instance_OnDragEnd_208(object o, EasyTouch_On_Drag.OnDragEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_208 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_208 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_208 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_208 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_208 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_208 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_208 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_208 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_208 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_208 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_208 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_208 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_208 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_208 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_208 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_208 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_208 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_208 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_208 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_208 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_208 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_208 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_208 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_208 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_208 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_208 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_208 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_208 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_208 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_208 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_208 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_208 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_208 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_208 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_208 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_208 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_208 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_208 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_208 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_208 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_208 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_208 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDragEnd_208( );
   }
   
   void Instance_OnSwipeStart_213(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_213 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_213 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_213 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_213 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_213 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_213 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_213 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_213 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_213 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_213 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_213 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_213 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_213 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_213 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_213 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_213 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_213 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_213 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_213 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_213 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_213 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_213 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_213 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_213 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_213 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_213 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_213 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_213 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_213 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_213 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_213 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_213 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_213 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_213 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_213 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_213 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_213 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_213 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_213 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_213 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_213 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_213 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_213 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart_213( );
   }
   
   void Instance_OnSwipe_213(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_213 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_213 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_213 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_213 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_213 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_213 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_213 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_213 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_213 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_213 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_213 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_213 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_213 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_213 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_213 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_213 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_213 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_213 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_213 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_213 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_213 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_213 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_213 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_213 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_213 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_213 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_213 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_213 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_213 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_213 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_213 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_213 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_213 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_213 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_213 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_213 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_213 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_213 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_213 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_213 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_213 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_213 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_213 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe_213( );
   }
   
   void Instance_OnSwipeEnd_213(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_213 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_213 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_213 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_213 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_213 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_213 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_213 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_213 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_213 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_213 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_213 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_213 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_213 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_213 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_213 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_213 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_213 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_213 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_213 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_213 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_213 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_213 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_213 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_213 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_213 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_213 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_213 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_213 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_213 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_213 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_213 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_213 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_213 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_213 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_213 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_213 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_213 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_213 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_213 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_213 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_213 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_213 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_213 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd_213( );
   }
   
   void Instance_OnDoubleTap_219(object o, EasyTouch_On_DoubleTap.OnDoubleTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_219 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_219 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_219 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_219 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_219 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_219 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_219 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_219 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_219 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_219 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_219 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_219 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_219 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_219 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_219 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_219 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_219 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_219 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_219 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_219 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_219 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_219 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_219 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_219 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_219 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_219 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_219 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_219 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_219 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_219 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_219 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_219 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_219 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_219 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_219 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_219 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_219 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_219 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_219 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_219 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_219 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_219 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_219 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnDoubleTap_219( );
   }
   
   void Relay_In_2()
   {
      {
         {
            logic_uScriptAct_IntToString_Target_2 = TouchFingerIndex;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_IntToString_uScriptAct_IntToString_2.In(logic_uScriptAct_IntToString_Target_2, logic_uScriptAct_IntToString_StandardFormat_2, logic_uScriptAct_IntToString_CustomFormat_2, logic_uScriptAct_IntToString_CustomCulture_2, out logic_uScriptAct_IntToString_Result_2);
      local_TFIndex_System_String = logic_uScriptAct_IntToString_Result_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_IntToString_uScriptAct_IntToString_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_48();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
            logic_uScriptAct_SubtractInt_A_3 = TouchFingerIndex;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SubtractInt_uScriptAct_SubtractInt_3.In(logic_uScriptAct_SubtractInt_A_3, logic_uScriptAct_SubtractInt_B_3, out logic_uScriptAct_SubtractInt_IntResult_3, out logic_uScriptAct_SubtractInt_FloatResult_3);
      TouchFingerIndex = logic_uScriptAct_SubtractInt_IntResult_3;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractInt_uScriptAct_SubtractInt_3.Out;
      
      if ( test_0 == true )
      {
         Relay_In_23();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_6.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_6, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_6[ index++ ] = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_6 = NewPos;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_6.In(logic_uScriptAct_SetGameObjectPosition_Target_6, logic_uScriptAct_SetGameObjectPosition_Position_6, logic_uScriptAct_SetGameObjectPosition_AsOffset_6, logic_uScriptAct_SetGameObjectPosition_AsLocal_6);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_56();
      }
   }
   
   void Relay_In_7()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_7, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_7[ index++ ] = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_7 = SwipePos;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.In(logic_uScriptAct_SetGameObjectPosition_Target_7, logic_uScriptAct_SetGameObjectPosition_Position_7, logic_uScriptAct_SetGameObjectPosition_AsOffset_7, logic_uScriptAct_SetGameObjectPosition_AsLocal_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_62();
      }
   }
   
   void Relay_In_8()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_Destroy_Target_8.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Destroy_Target_8, index + 1);
            }
            logic_uScriptAct_Destroy_Target_8[ index++ ] = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_Destroy_uScriptAct_Destroy_8.In(logic_uScriptAct_Destroy_Target_8, logic_uScriptAct_Destroy_DelayTime_8);
      logic_uScriptAct_Destroy_WaitOneTick_8 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_WaitOneTick_8( )
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_Destroy_Target_8.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Destroy_Target_8, index + 1);
            }
            logic_uScriptAct_Destroy_Target_8[ index++ ] = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_Destroy_WaitOneTick_8 = logic_uScriptAct_Destroy_uScriptAct_Destroy_8.WaitOneTick();
      if ( true == logic_uScriptAct_Destroy_WaitOneTick_8 )
      {
      }
   }
   void Relay_In_10()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_10.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_10, index + 1);
            }
            logic_uScriptCon_IsNull_Target_10[ index++ ] = PickedObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_10.In(logic_uScriptCon_IsNull_Target_10);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_10.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_109();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_12, index + 1);
            }
            logic_uScriptCon_IsNull_Target_12[ index++ ] = PickedObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_12.In(logic_uScriptCon_IsNull_Target_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_12.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_86();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetChildrenByName_Target_15 = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_GetChildrenByName_Name_15 = local_ObjectFingerID_System_String;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_15.In(logic_uScriptAct_GetChildrenByName_Target_15, logic_uScriptAct_GetChildrenByName_Name_15, logic_uScriptAct_GetChildrenByName_SearchMethod_15, logic_uScriptAct_GetChildrenByName_recursive_15, out logic_uScriptAct_GetChildrenByName_FirstChild_15, out logic_uScriptAct_GetChildrenByName_Children_15, out logic_uScriptAct_GetChildrenByName_ChildrenCount_15);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_15.ChildrenFound;
      
      if ( test_0 == true )
      {
         Relay_In_47();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetChildrenByName_Target_16 = PickedObject;
            
         }
         {
            logic_uScriptAct_GetChildrenByName_Name_16 = local_ObjectFingerID_System_String;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_16.In(logic_uScriptAct_GetChildrenByName_Target_16, logic_uScriptAct_GetChildrenByName_Name_16, logic_uScriptAct_GetChildrenByName_SearchMethod_16, logic_uScriptAct_GetChildrenByName_recursive_16, out logic_uScriptAct_GetChildrenByName_FirstChild_16, out logic_uScriptAct_GetChildrenByName_Children_16, out logic_uScriptAct_GetChildrenByName_ChildrenCount_16);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetChildrenByName_uScriptAct_GetChildrenByName_16.ChildrenFound;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_uScriptStart_17()
   {
      Relay_In_46();
   }
   
   void Relay_uScriptLateStart_17()
   {
   }
   
   void Relay_In_19()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_19 = local_FingerID_System_Int32;
            
         }
         {
            logic_uScriptCon_CompareInt_B_19 = TouchFingerIndex;
            
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.In(logic_uScriptCon_CompareInt_A_19, logic_uScriptCon_CompareInt_B_19);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_19.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_79();
      }
   }
   
   void Relay_In_23()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_23.In(logic_uScriptAct_SetInt_Value_23, out logic_uScriptAct_SetInt_Target_23);
      local_FingerID_System_Int32 = logic_uScriptAct_SetInt_Target_23;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetInt_uScriptAct_SetInt_23.Out;
      
      if ( test_0 == true )
      {
         Relay_In_15();
      }
   }
   
   void Relay_In_27()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_27 = local_FingerID_System_Int32;
            
         }
         {
            logic_uScriptCon_CompareInt_B_27 = TouchFingerIndex;
            
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_27.In(logic_uScriptCon_CompareInt_A_27, logic_uScriptCon_CompareInt_B_27);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_27.EqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_72();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            logic_uScriptAct_SetInt_Value_28 = TouchFingerIndex;
            
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_28.In(logic_uScriptAct_SetInt_Value_28, out logic_uScriptAct_SetInt_Target_28);
      local_FingerID_System_Int32 = logic_uScriptAct_SetInt_Target_28;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetInt_uScriptAct_SetInt_28.Out;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_35()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_35.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_35, index + 1);
            }
            logic_uScriptAct_Concatenate_A_35[ index++ ] = ObjectName;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_35.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_35, index + 1);
            }
            logic_uScriptAct_Concatenate_B_35[ index++ ] = local_ObjectFingerID_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_35.In(logic_uScriptAct_Concatenate_A_35, logic_uScriptAct_Concatenate_B_35, logic_uScriptAct_Concatenate_Separator_35, out logic_uScriptAct_Concatenate_Result_35);
      FingerIDText = logic_uScriptAct_Concatenate_Result_35;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_35.Out;
      
      if ( test_0 == true )
      {
         Relay_In_51();
      }
   }
   
   void Relay_In_46()
   {
      {
         {
            logic_uScriptAct_GetGameObjectName_gameObject_46 = owner_Connection_33;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_46.In(logic_uScriptAct_GetGameObjectName_gameObject_46, out logic_uScriptAct_GetGameObjectName_name_46);
      ObjectName = logic_uScriptAct_GetGameObjectName_name_46;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectName_uScriptAct_GetGameObjectName_46.Out;
      
      if ( test_0 == true )
      {
         Relay_In_93();
      }
   }
   
   void Relay_In_47()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  FingerIDObj_previous = FingerIDObj;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_47 = FingerIDObj;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_47.In(logic_RN_SetTextMeshText_go_GameObject_47, logic_RN_SetTextMeshText_s_Text_47);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_48()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  FingerIDObj_previous = FingerIDObj;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_48 = FingerIDObj;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_48 = local_TFIndex_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_48.In(logic_RN_SetTextMeshText_go_GameObject_48, logic_RN_SetTextMeshText_s_Text_48);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetTextMeshText_RN_SetTextMeshText_48.Out;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_In_51()
   {
      {
         {
            logic_uScriptAct_GetGameObjectByName_Name_51 = FingerIDText;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_51.In(logic_uScriptAct_GetGameObjectByName_Name_51, out logic_uScriptAct_GetGameObjectByName_gameObject_51);
      FingerIDObj = logic_uScriptAct_GetGameObjectByName_gameObject_51;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            FingerIDObj_previous = FingerIDObj;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_51.GameObjectFound;
      
      if ( test_0 == true )
      {
         Relay_In_68();
      }
   }
   
   void Relay_In_55()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  FingerIDObj_previous = FingerIDObj;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_55 = FingerIDObj;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_55 = local_TFIndex_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_55.In(logic_RN_SetTextMeshText_go_GameObject_55, logic_RN_SetTextMeshText_s_Text_55);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_56()
   {
      {
         {
            logic_uScriptAct_IntToString_Target_56 = TouchFingerIndex;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_IntToString_uScriptAct_IntToString_56.In(logic_uScriptAct_IntToString_Target_56, logic_uScriptAct_IntToString_StandardFormat_56, logic_uScriptAct_IntToString_CustomFormat_56, logic_uScriptAct_IntToString_CustomCulture_56, out logic_uScriptAct_IntToString_Result_56);
      local_TFIndex_System_String = logic_uScriptAct_IntToString_Result_56;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_IntToString_uScriptAct_IntToString_56.Out;
      
      if ( test_0 == true )
      {
         Relay_In_55();
      }
   }
   
   void Relay_In_61()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  FingerIDObj_previous = FingerIDObj;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_61 = FingerIDObj;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_61 = local_TFIndex_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_61.In(logic_RN_SetTextMeshText_go_GameObject_61, logic_RN_SetTextMeshText_s_Text_61);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_62()
   {
      {
         {
            logic_uScriptAct_IntToString_Target_62 = TouchFingerIndex;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_IntToString_uScriptAct_IntToString_62.In(logic_uScriptAct_IntToString_Target_62, logic_uScriptAct_IntToString_StandardFormat_62, logic_uScriptAct_IntToString_CustomFormat_62, logic_uScriptAct_IntToString_CustomCulture_62, out logic_uScriptAct_IntToString_Result_62);
      local_TFIndex_System_String = logic_uScriptAct_IntToString_Result_62;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_IntToString_uScriptAct_IntToString_62.Out;
      
      if ( test_0 == true )
      {
         Relay_In_61();
      }
   }
   
   void Relay_In_67()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( FingerIDObj_previous != FingerIDObj || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  FingerIDObj_previous = FingerIDObj;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_67 = FingerIDObj;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_67 = local_TFIndex_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_67.In(logic_RN_SetTextMeshText_go_GameObject_67, logic_RN_SetTextMeshText_s_Text_67);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_68()
   {
      {
         {
            logic_uScriptAct_IntToString_Target_68 = TouchFingerIndex;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_IntToString_uScriptAct_IntToString_68.In(logic_uScriptAct_IntToString_Target_68, logic_uScriptAct_IntToString_StandardFormat_68, logic_uScriptAct_IntToString_CustomFormat_68, logic_uScriptAct_IntToString_CustomCulture_68, out logic_uScriptAct_IntToString_Result_68);
      local_TFIndex_System_String = logic_uScriptAct_IntToString_Result_68;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_IntToString_uScriptAct_IntToString_68.Out;
      
      if ( test_0 == true )
      {
         Relay_In_67();
      }
   }
   
   void Relay_In_72()
   {
      {
         {
            logic_uScriptAct_SubtractVector3_A_72 = local_ObjWorldPoint_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_SubtractVector3_B_72 = DeltaPos;
            
         }
         {
         }
      }
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_72.In(logic_uScriptAct_SubtractVector3_A_72, logic_uScriptAct_SubtractVector3_B_72, out logic_uScriptAct_SubtractVector3_Result_72);
      NewPos = logic_uScriptAct_SubtractVector3_Result_72;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_72.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_79()
   {
      {
         {
            logic_uScriptAct_SubtractVector3_A_79 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_SubtractVector3_B_79 = DeltaPos;
            
         }
         {
         }
      }
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_79.In(logic_uScriptAct_SubtractVector3_A_79, logic_uScriptAct_SubtractVector3_B_79, out logic_uScriptAct_SubtractVector3_Result_79);
      SwipePos = logic_uScriptAct_SubtractVector3_Result_79;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_79.Out;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_85()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_85 = PickedObject;
            
         }
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Parent_85 = local_ThisObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_85.In(logic_uScriptCon_IsChild_Child_85, logic_uScriptCon_IsChild_Parent_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_85.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_27();
      }
   }
   
   void Relay_In_86()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_86 = PickedObject;
            
         }
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Parent_86 = local_ThisObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_86.In(logic_uScriptCon_IsChild_Child_86, logic_uScriptCon_IsChild_Parent_86);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_86.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_8();
      }
   }
   
   void Relay_In_89()
   {
      {
         {
            logic_uScriptAct_SubtractVector3_A_89 = local_ObjWorldPoint_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_SubtractVector3_B_89 = local_ObjPos_UnityEngine_Vector3;
            
         }
         {
         }
      }
      logic_uScriptAct_SubtractVector3_uScriptAct_SubtractVector3_89.In(logic_uScriptAct_SubtractVector3_A_89, logic_uScriptAct_SubtractVector3_B_89, out logic_uScriptAct_SubtractVector3_Result_89);
      DeltaPos = logic_uScriptAct_SubtractVector3_Result_89;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_90()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetPositionAndRotation_Target_90 = local_ThisObject_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_90.In(logic_uScriptAct_GetPositionAndRotation_Target_90, logic_uScriptAct_GetPositionAndRotation_GetLocal_90, out logic_uScriptAct_GetPositionAndRotation_Position_90, out logic_uScriptAct_GetPositionAndRotation_Rotation_90, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_90, out logic_uScriptAct_GetPositionAndRotation_Forward_90, out logic_uScriptAct_GetPositionAndRotation_Up_90, out logic_uScriptAct_GetPositionAndRotation_Right_90);
      local_ObjPos_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_90;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_90.Out;
      
      if ( test_0 == true )
      {
         Relay_In_89();
      }
   }
   
   void Relay_In_93()
   {
      {
         {
            logic_uScriptAct_GetGameObjectByName_Name_93 = ObjectName;
            
         }
         {
         }
      }
      logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_93.In(logic_uScriptAct_GetGameObjectByName_Name_93, out logic_uScriptAct_GetGameObjectByName_gameObject_93);
      local_ThisObject_UnityEngine_GameObject = logic_uScriptAct_GetGameObjectByName_gameObject_93;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetGameObjectByName_uScriptAct_GetGameObjectByName_93.Out;
      
      if ( test_0 == true )
      {
         Relay_In_35();
      }
   }
   
   void Relay_In_109()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_109 = PickedObject;
            
         }
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_ThisObject_UnityEngine_GameObject_previous != local_ThisObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_ThisObject_UnityEngine_GameObject_previous = local_ThisObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Parent_109 = local_ThisObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_109.In(logic_uScriptCon_IsChild_Child_109, logic_uScriptCon_IsChild_Parent_109);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_109.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_OnTouchStart_201()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_201;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_201;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_201;
      Relay_In_10();
   }
   
   void Relay_OnTouchDown_201()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_201;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_201;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_201;
   }
   
   void Relay_OnTouchUp_201()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_201;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_201;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_201;
      Relay_In_3();
   }
   
   void Relay_OnDragStart_208()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_208;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_ObjWorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208;
   }
   
   void Relay_OnDrag_208()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_208;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_ObjWorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208;
      Relay_In_85();
   }
   
   void Relay_OnDragEnd_208()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_208;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_ObjWorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_208;
   }
   
   void Relay_OnSwipeStart_213()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_213;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_213;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_213;
   }
   
   void Relay_OnSwipe_213()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_213;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_213;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_213;
      Relay_In_19();
   }
   
   void Relay_OnSwipeEnd_213()
   {
      TouchFingerIndex = event_UnityEngine_GameObject_i_FingerIndex_213;
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_213;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_213;
   }
   
   void Relay_OnDoubleTap_219()
   {
      Relay_In_12();
   }
   
}
