//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class SimpleTouch : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_39_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_39_UnityEngine_GameObject_previous = null;
   System.String local_4_System_String = "Down since: ";
   UnityEngine.GameObject local_40_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_40_UnityEngine_GameObject_previous = null;
   System.Single local_ActionTime_System_Single = (float) 0;
   System.String local_CatResult_System_String = "";
   System.String local_Formatted_System_String = "";
   UnityEngine.Color local_ObjectColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_11 = null;
   UnityEngine.GameObject owner_Connection_13 = null;
   UnityEngine.GameObject owner_Connection_25 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_30 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_2 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_2 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_2 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_2 = "";
   System.String logic_uScriptAct_Concatenate_Result_2;
   bool logic_uScriptAct_Concatenate_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_7 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_7 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_7 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_7 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_7 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_7 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_7 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_7 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_7 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_7;
   bool logic_uScriptAct_SetRandomColor_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_9 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_9 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_9 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_9 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_12 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_12 = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_12 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_12 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_19 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_19 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_19 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_19;
   bool logic_RN_FormatFloatString_Out_19 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_23 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_23 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_23 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_23 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_23 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_23 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_23 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_23 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_26 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_26 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_26 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_26 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_26 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_26 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_26 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_29 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_29 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_29 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_29 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_29 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_32 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_32 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_32 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_32 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_35 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_35 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_35 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_35 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_37 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_37 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_37 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_37 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_38 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_38 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_38 = "Touch Me";
   bool logic_RN_SetTextMeshText_Out_38 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_41 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_41 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_41 = "";
   bool logic_RN_SetTextMeshText_Out_41 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_82 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_82 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_82 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_82 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_82 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_82 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_82 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_82 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_82 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_82 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_82 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_82 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_82 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_82 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_82 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_82 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_82 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_82 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_82 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_82 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_82 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_82 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_82 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_82 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_82 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_82 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_82 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_82 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_82 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_82 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_39_UnityEngine_GameObject = GameObject.Find( "Sphere_Touch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_40_UnityEngine_GameObject = GameObject.Find( "Sphere_Touch_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_1.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_82;
                  component.OnTouchDown += Instance_OnTouchDown_82;
                  component.OnTouchUp += Instance_OnTouchUp_82;
               }
            }
         }
      }
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
      if ( null == owner_Connection_13 || false == m_RegisteredForEvents )
      {
         owner_Connection_13 = parentGameObject;
      }
      if ( null == owner_Connection_25 || false == m_RegisteredForEvents )
      {
         owner_Connection_25 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
      if ( null == owner_Connection_30 || false == m_RegisteredForEvents )
      {
         owner_Connection_30 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_1.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_82;
                  component.OnTouchDown += Instance_OnTouchDown_82;
                  component.OnTouchUp += Instance_OnTouchUp_82;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            EasyTouch_On_Touch component = owner_Connection_1.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_82;
               component.OnTouchDown -= Instance_OnTouchDown_82;
               component.OnTouchUp -= Instance_OnTouchUp_82;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_2.SetParent(g);
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_7.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_9.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_19.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_23.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_26.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_32.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_35.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_38.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_41.SetParent(g);
      owner_Connection_1 = parentGameObject;
      owner_Connection_11 = parentGameObject;
      owner_Connection_13 = parentGameObject;
      owner_Connection_25 = parentGameObject;
      owner_Connection_27 = parentGameObject;
      owner_Connection_30 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart_82(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_82 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_82 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_82 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_82 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_82 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_82 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_82 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_82 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_82 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_82 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_82 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_82 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_82 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_82 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_82 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_82 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_82 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_82 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_82 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_82 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_82 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_82 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_82 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_82 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_82 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_82 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_82 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_82 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_82 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_82 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_82 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_82 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_82 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_82 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_82 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_82 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_82 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_82 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_82 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_82 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_82 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_82 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_82 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_82( );
   }
   
   void Instance_OnTouchDown_82(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_82 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_82 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_82 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_82 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_82 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_82 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_82 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_82 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_82 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_82 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_82 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_82 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_82 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_82 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_82 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_82 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_82 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_82 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_82 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_82 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_82 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_82 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_82 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_82 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_82 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_82 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_82 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_82 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_82 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_82 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_82 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_82 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_82 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_82 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_82 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_82 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_82 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_82 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_82 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_82 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_82 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_82 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_82 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_82( );
   }
   
   void Instance_OnTouchUp_82(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_82 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_82 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_82 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_82 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_82 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_82 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_82 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_82 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_82 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_82 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_82 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_82 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_82 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_82 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_82 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_82 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_82 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_82 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_82 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_82 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_82 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_82 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_82 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_82 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_82 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_82 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_82 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_82 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_82 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_82 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_82 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_82 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_82 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_82 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_82 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_82 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_82 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_82 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_82 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_82 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_82 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_82 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_82 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_82( );
   }
   
   void Relay_In_2()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_2.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_2, index + 1);
            }
            logic_uScriptAct_Concatenate_A_2[ index++ ] = local_4_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_2.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_2, index + 1);
            }
            logic_uScriptAct_Concatenate_B_2[ index++ ] = local_Formatted_System_String;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_2.In(logic_uScriptAct_Concatenate_A_2, logic_uScriptAct_Concatenate_B_2, logic_uScriptAct_Concatenate_Separator_2, out logic_uScriptAct_Concatenate_Result_2);
      local_CatResult_System_String = logic_uScriptAct_Concatenate_Result_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_In_7()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_7.In(logic_uScriptAct_SetRandomColor_RedMin_7, logic_uScriptAct_SetRandomColor_RedMax_7, logic_uScriptAct_SetRandomColor_GreenMin_7, logic_uScriptAct_SetRandomColor_GreenMax_7, logic_uScriptAct_SetRandomColor_BlueMin_7, logic_uScriptAct_SetRandomColor_BlueMax_7, logic_uScriptAct_SetRandomColor_AlphaMin_7, logic_uScriptAct_SetRandomColor_AlphaMax_7, out logic_uScriptAct_SetRandomColor_TargetColor_7);
      local_ObjectColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_9, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_9[ index++ ] = owner_Connection_11;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_9 = local_ObjectColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_9.In(logic_uScriptAct_AssignMaterialColor_Target_9, logic_uScriptAct_AssignMaterialColor_MatColor_9, logic_uScriptAct_AssignMaterialColor_MatChannel_9);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_12, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_12[ index++ ] = owner_Connection_13;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.In(logic_uScriptAct_AssignMaterialColor_Target_12, logic_uScriptAct_AssignMaterialColor_MatColor_12, logic_uScriptAct_AssignMaterialColor_MatChannel_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_19()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_19 = local_ActionTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_19.In(logic_RN_FormatFloatString_f_Float_19, logic_RN_FormatFloatString_s_Modifier_19, out logic_RN_FormatFloatString_s_Result_19);
      local_Formatted_System_String = logic_RN_FormatFloatString_s_Result_19;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_19.Out;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_In_23()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_23 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_23 = owner_Connection_25;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_23.In(logic_uScriptCon_CompareGameObjects_A_23, logic_uScriptCon_CompareGameObjects_B_23, logic_uScriptCon_CompareGameObjects_CompareByTag_23, logic_uScriptCon_CompareGameObjects_CompareByName_23, logic_uScriptCon_CompareGameObjects_ReportNull_23);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_23.Same;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_26 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_26 = owner_Connection_27;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_26.In(logic_uScriptCon_CompareGameObjects_A_26, logic_uScriptCon_CompareGameObjects_B_26, logic_uScriptCon_CompareGameObjects_CompareByTag_26, logic_uScriptCon_CompareGameObjects_CompareByName_26, logic_uScriptCon_CompareGameObjects_ReportNull_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_26.Same;
      
      if ( test_0 == true )
      {
         Relay_In_19();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_29 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_29 = owner_Connection_30;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.In(logic_uScriptCon_CompareGameObjects_A_29, logic_uScriptCon_CompareGameObjects_B_29, logic_uScriptCon_CompareGameObjects_CompareByTag_29, logic_uScriptCon_CompareGameObjects_CompareByName_29, logic_uScriptCon_CompareGameObjects_ReportNull_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.Same;
      
      if ( test_0 == true )
      {
         Relay_In_38();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_32.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_32, index + 1);
            }
            logic_uScriptCon_IsNull_Target_32[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_32.In(logic_uScriptCon_IsNull_Target_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_32.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_23();
      }
   }
   
   void Relay_In_35()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_35.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_35, index + 1);
            }
            logic_uScriptCon_IsNull_Target_35[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_35.In(logic_uScriptCon_IsNull_Target_35);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_35.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_37()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_37.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_37, index + 1);
            }
            logic_uScriptCon_IsNull_Target_37[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.In(logic_uScriptCon_IsNull_Target_37);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_37.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_38 = local_39_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_38.In(logic_RN_SetTextMeshText_go_GameObject_38, logic_RN_SetTextMeshText_s_Text_38);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetTextMeshText_RN_SetTextMeshText_38.Out;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_41()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_41 = local_40_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_41 = local_CatResult_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_41.In(logic_RN_SetTextMeshText_go_GameObject_41, logic_RN_SetTextMeshText_s_Text_41);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTouchStart_82()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_82;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_82;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_32();
   }
   
   void Relay_OnTouchDown_82()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_82;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_82;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_35();
   }
   
   void Relay_OnTouchUp_82()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_82;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_82;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_37();
   }
   
}
