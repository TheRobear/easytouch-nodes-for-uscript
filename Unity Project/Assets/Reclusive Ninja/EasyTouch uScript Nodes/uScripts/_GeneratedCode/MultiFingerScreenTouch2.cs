//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class MultiFingerScreenTouch2 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.String local_12_System_String = "Touch ";
   System.Int32 local_SpawnCounter_System_Int32 = (int) 0;
   System.String local_SpawnName_System_String = "";
   public UnityEngine.GameObject PickedObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject PickedObject_previous = null;
   public UnityEngine.Vector3 WorldPoint = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SpawnPrefab logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1 = new uScriptAct_SpawnPrefab( );
   System.String logic_uScriptAct_SpawnPrefab_PrefabName_1 = "Touch";
   System.String logic_uScriptAct_SpawnPrefab_ResourcePath_1 = "";
   UnityEngine.GameObject logic_uScriptAct_SpawnPrefab_SpawnPoint_1 = default(UnityEngine.GameObject);
   System.String logic_uScriptAct_SpawnPrefab_SpawnedName_1 = "";
   UnityEngine.Vector3 logic_uScriptAct_SpawnPrefab_LocationOffset_1 = new Vector3( );
   UnityEngine.GameObject logic_uScriptAct_SpawnPrefab_SpawnedGameObject_1;
   System.Int32 logic_uScriptAct_SpawnPrefab_SpawnedInstancedID_1;
   bool logic_uScriptAct_SpawnPrefab_Immediate_1 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_5 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_5 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_5 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_5 = true;
   //pointer to script instanced logic node
   uScriptAct_AddInt_v2 logic_uScriptAct_AddInt_v2_uScriptAct_AddInt_v2_9 = new uScriptAct_AddInt_v2( );
   System.Int32 logic_uScriptAct_AddInt_v2_A_9 = (int) 0;
   System.Int32 logic_uScriptAct_AddInt_v2_B_9 = (int) 1;
   System.Int32 logic_uScriptAct_AddInt_v2_IntResult_9;
   System.Single logic_uScriptAct_AddInt_v2_FloatResult_9;
   bool logic_uScriptAct_AddInt_v2_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_11 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_11 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_11 = "";
   System.String logic_uScriptAct_Concatenate_Result_11;
   bool logic_uScriptAct_Concatenate_Out_11 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_21 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_21 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_21 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_21 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_21 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_21 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_21 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_21 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_21 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_21 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_21 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_21 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_21 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_21 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_21 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_21 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_21 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_21 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_21 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_21 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_21 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_21 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_21 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_21 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_21 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_21 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_21 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_21 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_21 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_21 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_21;
                  component.OnTouchDown += Instance_OnTouchDown_21;
                  component.OnTouchUp += Instance_OnTouchUp_21;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_21;
                  component.OnTouchDown += Instance_OnTouchDown_21;
                  component.OnTouchUp += Instance_OnTouchUp_21;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Touch component = owner_Connection_0.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_21;
               component.OnTouchDown -= Instance_OnTouchDown_21;
               component.OnTouchUp -= Instance_OnTouchUp_21;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_5.SetParent(g);
      logic_uScriptAct_AddInt_v2_uScriptAct_AddInt_v2_9.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_4 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1.FinishedSpawning += uScriptAct_SpawnPrefab_FinishedSpawning_1;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1.FinishedSpawning -= uScriptAct_SpawnPrefab_FinishedSpawning_1;
   }
   
   void Instance_OnTouchStart_21(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_21 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_21 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_21 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_21 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_21 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_21 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_21 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_21 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_21 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_21 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_21 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_21 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_21 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_21 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_21 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_21 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_21 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_21 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_21 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_21 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_21 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_21 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_21 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_21 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_21 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_21 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_21 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_21 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_21 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_21 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_21 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_21 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_21 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_21 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_21 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_21 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_21 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_21 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_21 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_21 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_21 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_21 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_21 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_21( );
   }
   
   void Instance_OnTouchDown_21(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_21 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_21 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_21 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_21 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_21 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_21 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_21 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_21 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_21 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_21 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_21 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_21 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_21 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_21 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_21 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_21 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_21 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_21 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_21 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_21 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_21 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_21 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_21 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_21 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_21 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_21 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_21 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_21 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_21 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_21 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_21 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_21 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_21 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_21 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_21 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_21 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_21 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_21 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_21 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_21 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_21 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_21 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_21 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_21( );
   }
   
   void Instance_OnTouchUp_21(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_21 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_21 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_21 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_21 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_21 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_21 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_21 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_21 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_21 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_21 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_21 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_21 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_21 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_21 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_21 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_21 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_21 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_21 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_21 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_21 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_21 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_21 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_21 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_21 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_21 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_21 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_21 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_21 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_21 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_21 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_21 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_21 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_21 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_21 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_21 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_21 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_21 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_21 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_21 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_21 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_21 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_21 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_21 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_21( );
   }
   
   void uScriptAct_SpawnPrefab_FinishedSpawning_1(object o, System.EventArgs e)
   {
      //fill globals
      //links to SpawnedGameObject = 0
      //links to SpawnedInstancedID = 0
      //relay event to nodes
      Relay_FinishedSpawning_1( );
   }
   
   void Relay_FinishedSpawning_1()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
         {
            logic_uScriptAct_SpawnPrefab_SpawnPoint_1 = owner_Connection_4;
            
         }
         {
            logic_uScriptAct_SpawnPrefab_SpawnedName_1 = local_SpawnName_System_String;
            
         }
         {
            logic_uScriptAct_SpawnPrefab_LocationOffset_1 = WorldPoint;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SpawnPrefab_uScriptAct_SpawnPrefab_1.In(logic_uScriptAct_SpawnPrefab_PrefabName_1, logic_uScriptAct_SpawnPrefab_ResourcePath_1, logic_uScriptAct_SpawnPrefab_SpawnPoint_1, logic_uScriptAct_SpawnPrefab_SpawnedName_1, logic_uScriptAct_SpawnPrefab_LocationOffset_1, out logic_uScriptAct_SpawnPrefab_SpawnedGameObject_1, out logic_uScriptAct_SpawnPrefab_SpawnedInstancedID_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_5()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_5.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_5, index + 1);
            }
            logic_uScriptCon_IsNull_Target_5[ index++ ] = PickedObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_5.In(logic_uScriptCon_IsNull_Target_5);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_5.IsNull;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_uScriptAct_AddInt_v2_A_9 = local_SpawnCounter_System_Int32;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddInt_v2_uScriptAct_AddInt_v2_9.In(logic_uScriptAct_AddInt_v2_A_9, logic_uScriptAct_AddInt_v2_B_9, out logic_uScriptAct_AddInt_v2_IntResult_9, out logic_uScriptAct_AddInt_v2_FloatResult_9);
      local_SpawnCounter_System_Int32 = logic_uScriptAct_AddInt_v2_IntResult_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddInt_v2_uScriptAct_AddInt_v2_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_11, index + 1);
            }
            logic_uScriptAct_Concatenate_A_11[ index++ ] = local_12_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_11, index + 1);
            }
            logic_uScriptAct_Concatenate_B_11[ index++ ] = local_SpawnCounter_System_Int32;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.In(logic_uScriptAct_Concatenate_A_11, logic_uScriptAct_Concatenate_B_11, logic_uScriptAct_Concatenate_Separator_11, out logic_uScriptAct_Concatenate_Result_11);
      local_SpawnName_System_String = logic_uScriptAct_Concatenate_Result_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_1();
      }
   }
   
   void Relay_OnTouchStart_21()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_21;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      WorldPoint = event_UnityEngine_GameObject_v3_TouchToWorldPoint_21;
      Relay_In_5();
   }
   
   void Relay_OnTouchDown_21()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_21;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      WorldPoint = event_UnityEngine_GameObject_v3_TouchToWorldPoint_21;
   }
   
   void Relay_OnTouchUp_21()
   {
      PickedObject = event_UnityEngine_GameObject_go_PickedObject_21;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      WorldPoint = event_UnityEngine_GameObject_v3_TouchToWorldPoint_21;
   }
   
}
