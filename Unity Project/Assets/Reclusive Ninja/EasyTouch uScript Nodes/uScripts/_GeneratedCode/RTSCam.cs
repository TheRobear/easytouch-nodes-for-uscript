//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class RTSCam : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Single CameraFOV = (float) 0;
   UnityEngine.Vector3 local_18_UnityEngine_Vector3 = new Vector3( (float)-1, (float)0, (float)0 );
   UnityEngine.Vector3 local_24_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)-1 );
   UnityEngine.GameObject local_64_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_64_UnityEngine_GameObject_previous = null;
   UnityEngine.Camera local_68_UnityEngine_Camera = default(UnityEngine.Camera);
   UnityEngine.Camera local_74_UnityEngine_Camera = default(UnityEngine.Camera);
   System.Single local_DeltaPinch_System_Single = (float) 0;
   UnityEngine.Vector2 local_DeltaPosition_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_DeltaPosX_System_Single = (float) 0;
   System.Single local_DeltaPosY_System_Single = (float) 0;
   System.Single local_DeltaTime_System_Single = (float) 0;
   System.Single local_DivResult01_System_Single = (float) 0;
   System.Single local_DivResult02_System_Single = (float) 0;
   System.Boolean local_OverGUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   System.Single local_PinchTime_System_Single = (float) 0;
   UnityEngine.GameObject local_PreviousPicked_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PreviousPicked_UnityEngine_GameObject_previous = null;
   System.Single local_ScreenHeight_System_Single = (float) 0;
   System.Single local_ScreenWidth_System_Single = (float) 0;
   UnityEngine.Vector3 local_Translate_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_TwistAngle_System_Single = (float) 0;
   public System.Single Speed = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_2 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_25 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_35 = null;
   UnityEngine.GameObject owner_Connection_49 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_1 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_1 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_1 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_1 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_1 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_1 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_1 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_1 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_1 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_1 = (bool) true;
   bool logic_uScriptAct_SetGameObjectRotation_Out_1 = true;
   //pointer to script instanced logic node
   RN_Vector2ToFloats logic_RN_Vector2ToFloats_RN_Vector2ToFloats_8 = new RN_Vector2ToFloats( );
   UnityEngine.Vector2 logic_RN_Vector2ToFloats_v2_Vector2_8 = new Vector2( );
   System.Single logic_RN_Vector2ToFloats_f_XValue_8;
   System.Single logic_RN_Vector2ToFloats_f_YValue_8;
   bool logic_RN_Vector2ToFloats_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_DivideFloat logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_11 = new uScriptAct_DivideFloat( );
   System.Single logic_uScriptAct_DivideFloat_A_11 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_B_11 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_FloatResult_11;
   System.Int32 logic_uScriptAct_DivideFloat_IntResult_11;
   bool logic_uScriptAct_DivideFloat_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_DivideFloat logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_13 = new uScriptAct_DivideFloat( );
   System.Single logic_uScriptAct_DivideFloat_A_13 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_B_13 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_FloatResult_13;
   System.Int32 logic_uScriptAct_DivideFloat_IntResult_13;
   bool logic_uScriptAct_DivideFloat_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_17 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_17 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_17 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_17;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_17 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_21 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_21 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_21 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_21;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_21 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_30 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_30;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_30;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_30;
   bool logic_uScriptAct_GetDeltaTime_Out_30 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_32 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_32 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_32 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_32;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_32;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_32 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_37 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_37 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_37 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_37 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObject logic_uScriptAct_SetGameObject_uScriptAct_SetGameObject_40 = new uScriptAct_SetGameObject( );
   UnityEngine.GameObject logic_uScriptAct_SetGameObject_Value_40 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptAct_SetGameObject_TargetGameObject_40;
   bool logic_uScriptAct_SetGameObject_Out_40 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_41 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_41 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_41 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_41 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_42 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_42 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_42 = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_42 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_42 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_46 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_46 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_46 = new UnityEngine.Color( (float)1, (float)0, (float)0, (float)1 );
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_46 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_46 = true;
   //pointer to script instanced logic node
   uScriptAct_GetScreenSize logic_uScriptAct_GetScreenSize_uScriptAct_GetScreenSize_50 = new uScriptAct_GetScreenSize( );
   System.Int32 logic_uScriptAct_GetScreenSize_ScreenWidth_50;
   System.Int32 logic_uScriptAct_GetScreenSize_ScreenHeight_50;
   System.Single logic_uScriptAct_GetScreenSize_fScreenWidth_50;
   System.Single logic_uScriptAct_GetScreenSize_fScreenHeight_50;
   UnityEngine.Rect logic_uScriptAct_GetScreenSize_ScreenRect_50;
   UnityEngine.Vector2 logic_uScriptAct_GetScreenSize_ScreenCenter_50;
   bool logic_uScriptAct_GetScreenSize_Out_50 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_55 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_55 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_55 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_55;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_65 = "EasyTouchuScriptStart";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_65 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_65 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_65 = true;
   //pointer to script instanced logic node
   RN_GetCameraFOV logic_RN_GetCameraFOV_RN_GetCameraFOV_66 = new RN_GetCameraFOV( );
   UnityEngine.Camera logic_RN_GetCameraFOV_c_Camera_66 = default(UnityEngine.Camera);
   System.Single logic_RN_GetCameraFOV_f_FieldOfView_66;
   bool logic_RN_GetCameraFOV_Out_66 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_69 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_69 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_69 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_69;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_69;
   bool logic_uScriptAct_AddFloat_v2_Out_69 = true;
   //pointer to script instanced logic node
   RN_SetCameraFOV logic_RN_SetCameraFOV_RN_SetCameraFOV_73 = new RN_SetCameraFOV( );
   UnityEngine.Camera logic_RN_SetCameraFOV_c_Camera_73 = default(UnityEngine.Camera);
   System.Single logic_RN_SetCameraFOV_f_FieldOfView_73 = (float) 0;
   bool logic_RN_SetCameraFOV_Out_73 = true;
   //pointer to script instanced logic node
   RN_TransformTranslate logic_RN_TransformTranslate_RN_TransformTranslate_76 = new RN_TransformTranslate( );
   UnityEngine.GameObject logic_RN_TransformTranslate_go_GameObject_76 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_RN_TransformTranslate_v3_translation_76 = new Vector3( );
   bool logic_RN_TransformTranslate_Out_76 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_101 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_101 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_101 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_101 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_101 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_101 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_101 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_101 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_101 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_101 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_101 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_101 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_101 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_101 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_101 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_101 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_101 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_101 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_101 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_101 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_101 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_101 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_101 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_101 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_101 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_101 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_101 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_101 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_101 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_101 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_167 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_167 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_167 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_167 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_167 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_167 = new Vector2( );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_167 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_167 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_167 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_167 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_167 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_167 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_167 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_167 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_167 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_167 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_167 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_167 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_167 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_167 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_167 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_167 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_167 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_167 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_167 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_167 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_167 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_167 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_167 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_167 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_167 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_172 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_172 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_172 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_172 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_172 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_172 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_172 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_172 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_172 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_172 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_172 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_172 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_172 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_172 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_172 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_172 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_172 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_172 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_172 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_172 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_172 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_172 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_172 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_172 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_172 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_172 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_172 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_172 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_172 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_172 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_177 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_177 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_177 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_177 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_177 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_177 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_177 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_177 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_177 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_177 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_177 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_177 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_177 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_177 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_177 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_177 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_177 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_177 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_177 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_177 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_177 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_177 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_177 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_177 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_177 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_177 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_177 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_177 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_177 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_177 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PreviousPicked_UnityEngine_GameObject_previous != local_PreviousPicked_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PreviousPicked_UnityEngine_GameObject_previous = local_PreviousPicked_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_64_UnityEngine_GameObject = GameObject.Find( "Back_Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_64_UnityEngine_GameObject_previous != local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_64_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_63;
               }
            }
         }
         
         local_64_UnityEngine_GameObject_previous = local_64_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_64_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_64_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_63;
               }
            }
         }
      }
      if ( null == local_68_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Main Camera" );
         if ( null != gameObject )
         {
            local_68_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_74_UnityEngine_Camera || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Main Camera" );
         if ( null != gameObject )
         {
            local_74_UnityEngine_Camera = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_101;
                  component.OnTwistEnd += Instance_OnTwistEnd_101;
               }
            }
         }
      }
      if ( null == owner_Connection_2 || false == m_RegisteredForEvents )
      {
         owner_Connection_2 = parentGameObject;
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
         if ( null != owner_Connection_5 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_5.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_5.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_167;
                  component.OnSwipe += Instance_OnSwipe_167;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_167;
               }
            }
         }
      }
      if ( null == owner_Connection_25 || false == m_RegisteredForEvents )
      {
         owner_Connection_25 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
         if ( null != owner_Connection_27 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_27.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_27.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_172;
                  component.OnPinchIn += Instance_OnPinchIn_172;
                  component.OnPinchOut += Instance_OnPinchOut_172;
                  component.OnPinchEnd += Instance_OnPinchEnd_172;
               }
            }
         }
      }
      if ( null == owner_Connection_35 || false == m_RegisteredForEvents )
      {
         owner_Connection_35 = parentGameObject;
         if ( null != owner_Connection_35 )
         {
            {
               EasyTouch_On_SimpleTap component = owner_Connection_35.GetComponent<EasyTouch_On_SimpleTap>();
               if ( null == component )
               {
                  component = owner_Connection_35.AddComponent<EasyTouch_On_SimpleTap>();
               }
               if ( null != component )
               {
                  component.OnSimpleTap += Instance_OnSimpleTap_177;
               }
            }
         }
      }
      if ( null == owner_Connection_49 || false == m_RegisteredForEvents )
      {
         owner_Connection_49 = parentGameObject;
         if ( null != owner_Connection_49 )
         {
            {
               uScript_Global component = owner_Connection_49.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_49.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_48;
                  component.uScriptLateStart += Instance_uScriptLateStart_48;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PreviousPicked_UnityEngine_GameObject_previous != local_PreviousPicked_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PreviousPicked_UnityEngine_GameObject_previous = local_PreviousPicked_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_64_UnityEngine_GameObject_previous != local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_64_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_63;
               }
            }
         }
         
         local_64_UnityEngine_GameObject_previous = local_64_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_64_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_64_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_63;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_101;
                  component.OnTwistEnd += Instance_OnTwistEnd_101;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_5 )
         {
            {
               EasyTouch_On_Swipe component = owner_Connection_5.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = owner_Connection_5.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_167;
                  component.OnSwipe += Instance_OnSwipe_167;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_167;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_27 )
         {
            {
               EasyTouch_On_Pinch component = owner_Connection_27.GetComponent<EasyTouch_On_Pinch>();
               if ( null == component )
               {
                  component = owner_Connection_27.AddComponent<EasyTouch_On_Pinch>();
               }
               if ( null != component )
               {
                  component.OnPinch += Instance_OnPinch_172;
                  component.OnPinchIn += Instance_OnPinchIn_172;
                  component.OnPinchOut += Instance_OnPinchOut_172;
                  component.OnPinchEnd += Instance_OnPinchEnd_172;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_35 )
         {
            {
               EasyTouch_On_SimpleTap component = owner_Connection_35.GetComponent<EasyTouch_On_SimpleTap>();
               if ( null == component )
               {
                  component = owner_Connection_35.AddComponent<EasyTouch_On_SimpleTap>();
               }
               if ( null != component )
               {
                  component.OnSimpleTap += Instance_OnSimpleTap_177;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_49 )
         {
            {
               uScript_Global component = owner_Connection_49.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = owner_Connection_49.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_48;
                  component.uScriptLateStart += Instance_uScriptLateStart_48;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_64_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_63;
            }
         }
      }
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_Twist component = owner_Connection_0.GetComponent<EasyTouch_On_Twist>();
            if ( null != component )
            {
               component.OnTwist -= Instance_OnTwist_101;
               component.OnTwistEnd -= Instance_OnTwistEnd_101;
            }
         }
      }
      if ( null != owner_Connection_5 )
      {
         {
            EasyTouch_On_Swipe component = owner_Connection_5.GetComponent<EasyTouch_On_Swipe>();
            if ( null != component )
            {
               component.OnSwipeStart -= Instance_OnSwipeStart_167;
               component.OnSwipe -= Instance_OnSwipe_167;
               component.OnSwipeEnd -= Instance_OnSwipeEnd_167;
            }
         }
      }
      if ( null != owner_Connection_27 )
      {
         {
            EasyTouch_On_Pinch component = owner_Connection_27.GetComponent<EasyTouch_On_Pinch>();
            if ( null != component )
            {
               component.OnPinch -= Instance_OnPinch_172;
               component.OnPinchIn -= Instance_OnPinchIn_172;
               component.OnPinchOut -= Instance_OnPinchOut_172;
               component.OnPinchEnd -= Instance_OnPinchEnd_172;
            }
         }
      }
      if ( null != owner_Connection_35 )
      {
         {
            EasyTouch_On_SimpleTap component = owner_Connection_35.GetComponent<EasyTouch_On_SimpleTap>();
            if ( null != component )
            {
               component.OnSimpleTap -= Instance_OnSimpleTap_177;
            }
         }
      }
      if ( null != owner_Connection_49 )
      {
         {
            uScript_Global component = owner_Connection_49.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_48;
               component.uScriptLateStart -= Instance_uScriptLateStart_48;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_1.SetParent(g);
      logic_RN_Vector2ToFloats_RN_Vector2ToFloats_8.SetParent(g);
      logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_11.SetParent(g);
      logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_13.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_17.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_21.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_30.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_32.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.SetParent(g);
      logic_uScriptAct_SetGameObject_uScriptAct_SetGameObject_40.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_41.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_42.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_46.SetParent(g);
      logic_uScriptAct_GetScreenSize_uScriptAct_GetScreenSize_50.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_55.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65.SetParent(g);
      logic_RN_GetCameraFOV_RN_GetCameraFOV_66.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_69.SetParent(g);
      logic_RN_SetCameraFOV_RN_SetCameraFOV_73.SetParent(g);
      logic_RN_TransformTranslate_RN_TransformTranslate_76.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_2 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_25 = parentGameObject;
      owner_Connection_27 = parentGameObject;
      owner_Connection_35 = parentGameObject;
      owner_Connection_49 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65.Loaded += uScriptAct_LoadLevel_Loaded_65;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65.Loaded -= uScriptAct_LoadLevel_Loaded_65;
   }
   
   void Instance_uScriptStart_48(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_48( );
   }
   
   void Instance_uScriptLateStart_48(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_48( );
   }
   
   void Instance_OnButtonClick_63(object o, uScript_Button.ClickEventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_63( );
   }
   
   void Instance_OnTwist_101(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_101 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_101 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_101 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_101 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_101 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_101 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_101 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_101 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_101 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_101 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_101 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_101 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_101 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_101 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_101 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_101 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_101 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_101 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_101 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_101 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_101 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_101 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_101 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_101 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_101 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_101 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_101 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_101 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_101 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_101 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_101 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_101 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_101 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_101 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_101 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_101 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_101 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_101 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_101 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_101 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_101 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_101 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_101 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwist_101( );
   }
   
   void Instance_OnTwistEnd_101(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_101 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_101 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_101 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_101 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_101 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_101 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_101 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_101 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_101 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_101 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_101 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_101 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_101 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_101 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_101 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_101 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_101 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_101 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_101 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_101 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_101 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_101 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_101 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_101 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_101 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_101 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_101 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_101 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_101 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_101 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_101 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_101 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_101 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_101 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_101 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_101 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_101 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_101 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_101 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_101 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_101 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_101 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_101 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwistEnd_101( );
   }
   
   void Instance_OnSwipeStart_167(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_167 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_167 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_167 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_167 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_167 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_167 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_167 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_167 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_167 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_167 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_167 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_167 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_167 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_167 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_167 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_167 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_167 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_167 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_167 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_167 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_167 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_167 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_167 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_167 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_167 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_167 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_167 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_167 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_167 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_167 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_167 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_167 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_167 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_167 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_167 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_167 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_167 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_167 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_167 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_167 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_167 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_167 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_167 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart_167( );
   }
   
   void Instance_OnSwipe_167(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_167 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_167 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_167 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_167 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_167 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_167 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_167 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_167 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_167 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_167 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_167 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_167 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_167 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_167 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_167 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_167 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_167 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_167 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_167 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_167 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_167 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_167 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_167 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_167 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_167 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_167 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_167 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_167 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_167 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_167 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_167 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_167 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_167 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_167 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_167 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_167 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_167 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_167 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_167 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_167 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_167 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_167 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_167 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe_167( );
   }
   
   void Instance_OnSwipeEnd_167(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_167 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_167 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_167 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_167 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_167 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_167 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_167 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_167 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_167 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_167 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_167 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_167 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_167 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_167 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_167 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_167 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_167 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_167 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_167 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_167 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_167 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_167 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_167 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_167 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_167 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_167 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_167 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_167 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_167 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_167 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_167 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_167 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_167 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_167 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_167 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_167 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_167 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_167 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_167 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_167 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_167 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_167 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_167 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd_167( );
   }
   
   void Instance_OnPinch_172(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_172 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_172 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_172 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_172 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_172 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_172 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_172 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_172 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_172 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_172 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_172 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_172 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_172 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_172 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_172 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_172 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_172 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_172 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_172 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_172 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_172 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_172 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_172 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_172 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_172 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_172 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_172 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_172 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_172 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_172 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_172 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_172 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_172 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_172 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_172 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_172 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_172 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_172 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_172 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_172 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_172 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_172 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_172 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinch_172( );
   }
   
   void Instance_OnPinchIn_172(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_172 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_172 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_172 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_172 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_172 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_172 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_172 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_172 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_172 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_172 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_172 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_172 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_172 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_172 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_172 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_172 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_172 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_172 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_172 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_172 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_172 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_172 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_172 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_172 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_172 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_172 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_172 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_172 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_172 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_172 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_172 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_172 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_172 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_172 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_172 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_172 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_172 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_172 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_172 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_172 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_172 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_172 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_172 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchIn_172( );
   }
   
   void Instance_OnPinchOut_172(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_172 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_172 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_172 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_172 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_172 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_172 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_172 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_172 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_172 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_172 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_172 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_172 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_172 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_172 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_172 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_172 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_172 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_172 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_172 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_172 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_172 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_172 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_172 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_172 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_172 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_172 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_172 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_172 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_172 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_172 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_172 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_172 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_172 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_172 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_172 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_172 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_172 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_172 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_172 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_172 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_172 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_172 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_172 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchOut_172( );
   }
   
   void Instance_OnPinchEnd_172(object o, EasyTouch_On_Pinch.OnPinchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_172 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_172 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_172 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_172 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_172 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_172 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_172 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_172 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_172 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_172 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_172 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_172 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_172 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_172 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_172 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_172 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_172 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_172 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_172 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_172 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_172 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_172 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_172 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_172 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_172 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_172 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_172 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_172 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_172 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_172 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_172 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_172 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_172 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_172 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_172 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_172 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_172 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_172 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_172 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_172 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_172 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_172 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_172 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnPinchEnd_172( );
   }
   
   void Instance_OnSimpleTap_177(object o, EasyTouch_On_SimpleTap.OnSimpleTapEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_177 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_177 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_177 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_177 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_177 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_177 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_177 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_177 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_177 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_177 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_177 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_177 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_177 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_177 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_177 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_177 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_177 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_177 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_177 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_177 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_177 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_177 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_177 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_177 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_177 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_177 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_177 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_177 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_177 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_177 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_177 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_177 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_177 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_177 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_177 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_177 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_177 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_177 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_177 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_177 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_177 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_177 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_177 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSimpleTap_177( );
   }
   
   void uScriptAct_LoadLevel_Loaded_65(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_65( );
   }
   
   void Relay_In_1()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_1.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_1, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_1[ index++ ] = owner_Connection_2;
            
         }
         {
         }
         {
            logic_uScriptAct_SetGameObjectRotation_YDegrees_1 = local_TwistAngle_System_Single;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_1.In(logic_uScriptAct_SetGameObjectRotation_Target_1, logic_uScriptAct_SetGameObjectRotation_XDegrees_1, logic_uScriptAct_SetGameObjectRotation_YDegrees_1, logic_uScriptAct_SetGameObjectRotation_ZDegrees_1, logic_uScriptAct_SetGameObjectRotation_IgnoreX_1, logic_uScriptAct_SetGameObjectRotation_IgnoreY_1, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_1, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_1, logic_uScriptAct_SetGameObjectRotation_AsOffset_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_8()
   {
      {
         {
            logic_RN_Vector2ToFloats_v2_Vector2_8 = local_DeltaPosition_UnityEngine_Vector2;
            
         }
         {
         }
         {
         }
      }
      logic_RN_Vector2ToFloats_RN_Vector2ToFloats_8.In(logic_RN_Vector2ToFloats_v2_Vector2_8, out logic_RN_Vector2ToFloats_f_XValue_8, out logic_RN_Vector2ToFloats_f_YValue_8);
      local_DeltaPosX_System_Single = logic_RN_Vector2ToFloats_f_XValue_8;
      local_DeltaPosY_System_Single = logic_RN_Vector2ToFloats_f_YValue_8;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_Vector2ToFloats_RN_Vector2ToFloats_8.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
         Relay_In_13();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            logic_uScriptAct_DivideFloat_A_11 = local_DeltaPosX_System_Single;
            
         }
         {
            logic_uScriptAct_DivideFloat_B_11 = local_ScreenWidth_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_11.In(logic_uScriptAct_DivideFloat_A_11, logic_uScriptAct_DivideFloat_B_11, out logic_uScriptAct_DivideFloat_FloatResult_11, out logic_uScriptAct_DivideFloat_IntResult_11);
      local_DivResult01_System_Single = logic_uScriptAct_DivideFloat_FloatResult_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_17();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptAct_DivideFloat_A_13 = local_DeltaPosY_System_Single;
            
         }
         {
            logic_uScriptAct_DivideFloat_B_13 = local_ScreenHeight_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_13.In(logic_uScriptAct_DivideFloat_A_13, logic_uScriptAct_DivideFloat_B_13, out logic_uScriptAct_DivideFloat_FloatResult_13, out logic_uScriptAct_DivideFloat_IntResult_13);
      local_DivResult02_System_Single = logic_uScriptAct_DivideFloat_FloatResult_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_21();
      }
   }
   
   void Relay_In_17()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_17 = local_18_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_17 = local_DivResult01_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_17.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_17, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_17, out logic_uScriptAct_MultiplyVector3WithFloat_Result_17);
      local_Translate_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_55();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_21 = local_24_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_21 = local_DivResult02_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_21.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_21, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_21, out logic_uScriptAct_MultiplyVector3WithFloat_Result_21);
      local_Translate_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_21;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_21.Out;
      
      if ( test_0 == true )
      {
         Relay_In_55();
      }
   }
   
   void Relay_In_30()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_30.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_30, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_30, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_30);
      local_DeltaTime_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_30;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_30.Out;
      
      if ( test_0 == true )
      {
         Relay_In_32();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_32 = local_DeltaPinch_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_32 = local_DeltaTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_32.In(logic_uScriptAct_MultiplyFloat_v2_A_32, logic_uScriptAct_MultiplyFloat_v2_B_32, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_32, out logic_uScriptAct_MultiplyFloat_v2_IntResult_32);
      local_PinchTime_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_32;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_32.Out;
      
      if ( test_0 == true )
      {
         Relay_In_66();
      }
   }
   
   void Relay_In_37()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_37.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_37, index + 1);
            }
            logic_uScriptCon_IsNull_Target_37[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_37.In(logic_uScriptCon_IsNull_Target_37);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_37.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_In_40()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_SetGameObject_Value_40 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_SetGameObject_uScriptAct_SetGameObject_40.In(logic_uScriptAct_SetGameObject_Value_40, out logic_uScriptAct_SetGameObject_TargetGameObject_40);
      local_PreviousPicked_UnityEngine_GameObject = logic_uScriptAct_SetGameObject_TargetGameObject_40;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PreviousPicked_UnityEngine_GameObject_previous != local_PreviousPicked_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PreviousPicked_UnityEngine_GameObject_previous = local_PreviousPicked_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObject_uScriptAct_SetGameObject_40.Out;
      
      if ( test_0 == true )
      {
         Relay_In_46();
      }
   }
   
   void Relay_In_41()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PreviousPicked_UnityEngine_GameObject_previous != local_PreviousPicked_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PreviousPicked_UnityEngine_GameObject_previous = local_PreviousPicked_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_41.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_41, index + 1);
            }
            logic_uScriptCon_IsNull_Target_41[ index++ ] = local_PreviousPicked_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_41.In(logic_uScriptCon_IsNull_Target_41);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_41.IsNull;
      bool test_1 = logic_uScriptCon_IsNull_uScriptCon_IsNull_41.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_40();
      }
      if ( test_1 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_In_42()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PreviousPicked_UnityEngine_GameObject_previous != local_PreviousPicked_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PreviousPicked_UnityEngine_GameObject_previous = local_PreviousPicked_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_42.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_42, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_42[ index++ ] = local_PreviousPicked_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_42.In(logic_uScriptAct_AssignMaterialColor_Target_42, logic_uScriptAct_AssignMaterialColor_MatColor_42, logic_uScriptAct_AssignMaterialColor_MatChannel_42);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_42.Out;
      
      if ( test_0 == true )
      {
         Relay_In_40();
      }
   }
   
   void Relay_In_46()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_46.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_46, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_46[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_46.In(logic_uScriptAct_AssignMaterialColor_Target_46, logic_uScriptAct_AssignMaterialColor_MatColor_46, logic_uScriptAct_AssignMaterialColor_MatChannel_46);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_uScriptStart_48()
   {
      Relay_In_50();
   }
   
   void Relay_uScriptLateStart_48()
   {
   }
   
   void Relay_In_50()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetScreenSize_uScriptAct_GetScreenSize_50.In(out logic_uScriptAct_GetScreenSize_ScreenWidth_50, out logic_uScriptAct_GetScreenSize_ScreenHeight_50, out logic_uScriptAct_GetScreenSize_fScreenWidth_50, out logic_uScriptAct_GetScreenSize_fScreenHeight_50, out logic_uScriptAct_GetScreenSize_ScreenRect_50, out logic_uScriptAct_GetScreenSize_ScreenCenter_50);
      local_ScreenWidth_System_Single = logic_uScriptAct_GetScreenSize_fScreenWidth_50;
      local_ScreenHeight_System_Single = logic_uScriptAct_GetScreenSize_fScreenHeight_50;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_55()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_55 = local_Translate_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_55 = Speed;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_55.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_55, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_55, out logic_uScriptAct_MultiplyVector3WithFloat_Result_55);
      local_Translate_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_55;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_55.Out;
      
      if ( test_0 == true )
      {
         Relay_In_76();
      }
   }
   
   void Relay_OnButtonClick_63()
   {
      Relay_In_65();
   }
   
   void Relay_Loaded_65()
   {
   }
   
   void Relay_In_65()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_65.In(logic_uScriptAct_LoadLevel_name_65, logic_uScriptAct_LoadLevel_destroyOtherObjects_65, logic_uScriptAct_LoadLevel_blockUntilLoaded_65);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_66()
   {
      {
         {
            logic_RN_GetCameraFOV_c_Camera_66 = local_68_UnityEngine_Camera;
            
         }
         {
         }
      }
      logic_RN_GetCameraFOV_RN_GetCameraFOV_66.In(logic_RN_GetCameraFOV_c_Camera_66, out logic_RN_GetCameraFOV_f_FieldOfView_66);
      CameraFOV = logic_RN_GetCameraFOV_f_FieldOfView_66;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_GetCameraFOV_RN_GetCameraFOV_66.Out;
      
      if ( test_0 == true )
      {
         Relay_In_69();
      }
   }
   
   void Relay_In_69()
   {
      {
         {
            logic_uScriptAct_AddFloat_v2_A_69 = CameraFOV;
            
         }
         {
            logic_uScriptAct_AddFloat_v2_B_69 = local_PinchTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_69.In(logic_uScriptAct_AddFloat_v2_A_69, logic_uScriptAct_AddFloat_v2_B_69, out logic_uScriptAct_AddFloat_v2_FloatResult_69, out logic_uScriptAct_AddFloat_v2_IntResult_69);
      CameraFOV = logic_uScriptAct_AddFloat_v2_FloatResult_69;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_69.Out;
      
      if ( test_0 == true )
      {
         Relay_In_73();
      }
   }
   
   void Relay_In_73()
   {
      {
         {
            logic_RN_SetCameraFOV_c_Camera_73 = local_74_UnityEngine_Camera;
            
         }
         {
            logic_RN_SetCameraFOV_f_FieldOfView_73 = CameraFOV;
            
         }
      }
      logic_RN_SetCameraFOV_RN_SetCameraFOV_73.In(logic_RN_SetCameraFOV_c_Camera_73, logic_RN_SetCameraFOV_f_FieldOfView_73);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_76()
   {
      {
         {
            logic_RN_TransformTranslate_go_GameObject_76 = owner_Connection_25;
            
         }
         {
            logic_RN_TransformTranslate_v3_translation_76 = local_Translate_UnityEngine_Vector3;
            
         }
      }
      logic_RN_TransformTranslate_RN_TransformTranslate_76.In(logic_RN_TransformTranslate_go_GameObject_76, logic_RN_TransformTranslate_v3_translation_76);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTwist_101()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_101;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_101;
      Relay_In_1();
   }
   
   void Relay_OnTwistEnd_101()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_101;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_101;
   }
   
   void Relay_OnSwipeStart_167()
   {
      local_DeltaPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_DeltaPosition_167;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_167;
   }
   
   void Relay_OnSwipe_167()
   {
      local_DeltaPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_DeltaPosition_167;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_167;
      Relay_In_8();
   }
   
   void Relay_OnSwipeEnd_167()
   {
      local_DeltaPosition_UnityEngine_Vector2 = event_UnityEngine_GameObject_v2_DeltaPosition_167;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_167;
   }
   
   void Relay_OnPinch_172()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_172;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_172;
      Relay_In_30();
   }
   
   void Relay_OnPinchIn_172()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_172;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_172;
   }
   
   void Relay_OnPinchOut_172()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_172;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_172;
   }
   
   void Relay_OnPinchEnd_172()
   {
      local_DeltaPinch_System_Single = event_UnityEngine_GameObject_f_DeltaPinch_172;
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_172;
   }
   
   void Relay_OnSimpleTap_177()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_177;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      local_OverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_177;
      Relay_In_37();
   }
   
}
