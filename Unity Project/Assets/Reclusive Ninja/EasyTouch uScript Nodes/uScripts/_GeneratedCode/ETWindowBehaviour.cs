//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class ETWindowBehaviour : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_DeltaPosition_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Boolean local_Drag_System_Boolean = (bool) false;
   System.Boolean local_IsOverGUI_System_Boolean = (bool) false;
   UnityEngine.GameObject local_Parent_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Parent_UnityEngine_GameObject_previous = null;
   public UnityEngine.GameObject PickedObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject PickedObject_previous = null;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_2 = null;
   UnityEngine.GameObject owner_Connection_9 = null;
   UnityEngine.GameObject owner_Connection_10 = null;
   UnityEngine.GameObject owner_Connection_16 = null;
   UnityEngine.GameObject owner_Connection_21 = null;
   UnityEngine.GameObject owner_Connection_25 = null;
   UnityEngine.GameObject owner_Connection_38 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_0 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_0;
   bool logic_uScriptAct_SetBool_Out_0 = true;
   bool logic_uScriptAct_SetBool_SetTrue_0 = true;
   bool logic_uScriptAct_SetBool_SetFalse_0 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_5 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_5 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_5 = true;
   bool logic_uScriptCon_CompareBool_False_5 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_8 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_8 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_8 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_8 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_8 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_8 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_8 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_8 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_11 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_11;
   bool logic_uScriptAct_SetBool_Out_11 = true;
   bool logic_uScriptAct_SetBool_SetTrue_11 = true;
   bool logic_uScriptAct_SetBool_SetFalse_11 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_13 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_13 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_13 = true;
   bool logic_uScriptCon_CompareBool_False_13 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_15 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_15 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_15 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_15 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_15 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_15 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_15 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_15 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_18 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_18 = true;
   bool logic_uScriptCon_CompareBool_False_18 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_20 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_20 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_20 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_20 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_20 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_20 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_24 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_24 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_24 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_24 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_24 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_28 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_28 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_28 = true;
   bool logic_uScriptCon_CompareBool_False_28 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_29 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_29 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_29 = (bool) true;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_29 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_29 = true;
   //pointer to script instanced logic node
   uScriptAct_GetParent logic_uScriptAct_GetParent_uScriptAct_GetParent_32 = new uScriptAct_GetParent( );
   UnityEngine.GameObject logic_uScriptAct_GetParent_Target_32 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptAct_GetParent_Parent_32;
   bool logic_uScriptAct_GetParent_Out_32 = true;
   //pointer to script instanced logic node
   RN_SetAsLastSibling logic_RN_SetAsLastSibling_RN_SetAsLastSibling_35 = new RN_SetAsLastSibling( );
   UnityEngine.GameObject logic_RN_SetAsLastSibling_go_GameObject_35 = default(UnityEngine.GameObject);
   bool logic_RN_SetAsLastSibling_Out_35 = true;
   //pointer to script instanced logic node
   uScriptCon_IsChild logic_uScriptCon_IsChild_uScriptCon_IsChild_36 = new uScriptCon_IsChild( );
   UnityEngine.GameObject logic_uScriptCon_IsChild_Child_36 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_IsChild_Parent_36 = default(UnityEngine.GameObject);
   bool logic_uScriptCon_IsChild_IsChild_36 = true;
   bool logic_uScriptCon_IsChild_IsNotChild_36 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_54 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_54 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_54 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_54 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_54 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_54 = new Vector3( );
   System.Single event_UnityEngine_GameObject_f_ActionTime_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_54 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_54 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_54 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_54 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_54 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_54 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_54 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_54 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_54 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_54 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_54 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_54 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_54 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_54 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_54 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_54 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_54 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_54 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_54 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_54 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_54 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_54 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_54 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_54 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_2 || false == m_RegisteredForEvents )
      {
         owner_Connection_2 = parentGameObject;
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_2.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_54;
                  component.OnTouchDown += Instance_OnTouchDown_54;
                  component.OnTouchUp += Instance_OnTouchUp_54;
               }
            }
         }
      }
      if ( null == owner_Connection_9 || false == m_RegisteredForEvents )
      {
         owner_Connection_9 = parentGameObject;
      }
      if ( null == owner_Connection_10 || false == m_RegisteredForEvents )
      {
         owner_Connection_10 = parentGameObject;
      }
      if ( null == owner_Connection_16 || false == m_RegisteredForEvents )
      {
         owner_Connection_16 = parentGameObject;
      }
      if ( null == owner_Connection_21 || false == m_RegisteredForEvents )
      {
         owner_Connection_21 = parentGameObject;
      }
      if ( null == owner_Connection_25 || false == m_RegisteredForEvents )
      {
         owner_Connection_25 = parentGameObject;
      }
      if ( null == owner_Connection_38 || false == m_RegisteredForEvents )
      {
         owner_Connection_38 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         PickedObject_previous = PickedObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Touch component = owner_Connection_2.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_54;
                  component.OnTouchDown += Instance_OnTouchDown_54;
                  component.OnTouchUp += Instance_OnTouchUp_54;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_2 )
      {
         {
            EasyTouch_On_Touch component = owner_Connection_2.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_54;
               component.OnTouchDown -= Instance_OnTouchDown_54;
               component.OnTouchUp -= Instance_OnTouchUp_54;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetBool_uScriptAct_SetBool_0.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_5.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_8.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_13.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_15.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_20.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_24.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_28.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29.SetParent(g);
      logic_uScriptAct_GetParent_uScriptAct_GetParent_32.SetParent(g);
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_35.SetParent(g);
      logic_uScriptCon_IsChild_uScriptCon_IsChild_36.SetParent(g);
      owner_Connection_2 = parentGameObject;
      owner_Connection_9 = parentGameObject;
      owner_Connection_10 = parentGameObject;
      owner_Connection_16 = parentGameObject;
      owner_Connection_21 = parentGameObject;
      owner_Connection_25 = parentGameObject;
      owner_Connection_38 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart_54(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_54 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_54 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_54 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_54 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_54 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_54 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_54 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_54 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_54 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_54 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_54 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_54 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_54 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_54 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_54 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_54 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_54 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_54 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_54 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_54 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_54 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_54 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_54 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_54 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_54 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_54 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_54 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_54 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_54 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_54 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_54 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_54 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_54 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_54 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_54 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_54 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_54 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_54 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_54 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_54 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_54 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_54 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_54 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_54( );
   }
   
   void Instance_OnTouchDown_54(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_54 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_54 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_54 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_54 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_54 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_54 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_54 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_54 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_54 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_54 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_54 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_54 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_54 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_54 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_54 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_54 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_54 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_54 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_54 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_54 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_54 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_54 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_54 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_54 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_54 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_54 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_54 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_54 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_54 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_54 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_54 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_54 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_54 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_54 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_54 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_54 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_54 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_54 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_54 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_54 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_54 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_54 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_54 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_54( );
   }
   
   void Instance_OnTouchUp_54(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_54 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_54 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_54 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_54 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_54 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_54 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_54 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_54 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_54 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_54 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_54 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_54 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_54 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_54 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_54 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_54 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_54 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_54 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_54 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_54 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_54 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_54 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_54 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_54 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_54 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_54 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_54 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_54 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_54 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_54 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_54 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_54 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_54 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_54 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_54 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_54 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_54 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_54 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_54 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_54 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_54 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_54 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_54 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_54( );
   }
   
   void Relay_True_0()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_0.True(out logic_uScriptAct_SetBool_Target_0);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_0;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_0.Out;
      
      if ( test_0 == true )
      {
         Relay_In_5();
      }
   }
   
   void Relay_False_0()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_0.False(out logic_uScriptAct_SetBool_Target_0);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_0;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_0.Out;
      
      if ( test_0 == true )
      {
         Relay_In_5();
      }
   }
   
   void Relay_In_5()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_5 = local_IsOverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_5.In(logic_uScriptCon_CompareBool_Bool_5);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_5.True;
      
      if ( test_0 == true )
      {
         Relay_In_8();
         Relay_In_36();
      }
   }
   
   void Relay_In_8()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_8 = PickedObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_8 = owner_Connection_9;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_8.In(logic_uScriptCon_CompareGameObjects_A_8, logic_uScriptCon_CompareGameObjects_B_8, logic_uScriptCon_CompareGameObjects_CompareByTag_8, logic_uScriptCon_CompareGameObjects_CompareByName_8, logic_uScriptCon_CompareGameObjects_ReportNull_8);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_8.Same;
      
      if ( test_0 == true )
      {
         Relay_In_35();
      }
   }
   
   void Relay_True_11()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.True(out logic_uScriptAct_SetBool_Target_11);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_11()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.False(out logic_uScriptAct_SetBool_Target_11);
      local_Drag_System_Boolean = logic_uScriptAct_SetBool_Target_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_13 = local_IsOverGUI_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_13.In(logic_uScriptCon_CompareBool_Bool_13);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_13.True;
      
      if ( test_0 == true )
      {
         Relay_In_15();
         Relay_In_24();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_15 = PickedObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_15 = owner_Connection_16;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_15.In(logic_uScriptCon_CompareGameObjects_A_15, logic_uScriptCon_CompareGameObjects_B_15, logic_uScriptCon_CompareGameObjects_CompareByTag_15, logic_uScriptCon_CompareGameObjects_CompareByName_15, logic_uScriptCon_CompareGameObjects_ReportNull_15);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_15.Same;
      
      if ( test_0 == true )
      {
         Relay_In_18();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_18 = local_Drag_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.In(logic_uScriptCon_CompareBool_Bool_18);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_18.True;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectPosition_Target_20.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_20, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_20[ index++ ] = owner_Connection_21;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_20 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_20.In(logic_uScriptAct_SetGameObjectPosition_Target_20, logic_uScriptAct_SetGameObjectPosition_Position_20, logic_uScriptAct_SetGameObjectPosition_AsOffset_20, logic_uScriptAct_SetGameObjectPosition_AsLocal_20);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_24 = PickedObject;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_24 = owner_Connection_25;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_24.In(logic_uScriptCon_IsChild_Child_24, logic_uScriptCon_IsChild_Parent_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_24.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_28 = local_Drag_System_Boolean;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_28.In(logic_uScriptCon_CompareBool_Bool_28);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_28.True;
      
      if ( test_0 == true )
      {
         Relay_In_32();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_29.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_29, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_29[ index++ ] = local_Parent_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_29 = local_DeltaPosition_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29.In(logic_uScriptAct_SetGameObjectPosition_Target_29, logic_uScriptAct_SetGameObjectPosition_Position_29, logic_uScriptAct_SetGameObjectPosition_AsOffset_29, logic_uScriptAct_SetGameObjectPosition_AsLocal_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_32()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptAct_GetParent_Target_32 = PickedObject;
            
         }
         {
         }
      }
      logic_uScriptAct_GetParent_uScriptAct_GetParent_32.In(logic_uScriptAct_GetParent_Target_32, out logic_uScriptAct_GetParent_Parent_32);
      local_Parent_UnityEngine_GameObject = logic_uScriptAct_GetParent_Parent_32;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_Parent_UnityEngine_GameObject_previous != local_Parent_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_Parent_UnityEngine_GameObject_previous = local_Parent_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetParent_uScriptAct_GetParent_32.Out;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_35()
   {
      {
         {
            logic_RN_SetAsLastSibling_go_GameObject_35 = owner_Connection_10;
            
         }
      }
      logic_RN_SetAsLastSibling_RN_SetAsLastSibling_35.In(logic_RN_SetAsLastSibling_go_GameObject_35);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetAsLastSibling_RN_SetAsLastSibling_35.Out;
      
      if ( test_0 == true )
      {
         Relay_True_11();
      }
   }
   
   void Relay_In_36()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  PickedObject_previous = PickedObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_IsChild_Child_36 = PickedObject;
            
         }
         {
            logic_uScriptCon_IsChild_Parent_36 = owner_Connection_38;
            
         }
      }
      logic_uScriptCon_IsChild_uScriptCon_IsChild_36.In(logic_uScriptCon_IsChild_Child_36, logic_uScriptCon_IsChild_Parent_36);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsChild_uScriptCon_IsChild_36.IsChild;
      
      if ( test_0 == true )
      {
         Relay_In_35();
      }
   }
   
   void Relay_OnTouchStart_54()
   {
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_54;
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_54;
      PickedObject = event_UnityEngine_GameObject_go_PickedUIElement_54;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      Relay_False_0();
   }
   
   void Relay_OnTouchDown_54()
   {
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_54;
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_54;
      PickedObject = event_UnityEngine_GameObject_go_PickedUIElement_54;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
      Relay_In_13();
   }
   
   void Relay_OnTouchUp_54()
   {
      local_DeltaPosition_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_DeltaPosition_54;
      local_IsOverGUI_System_Boolean = event_UnityEngine_GameObject_b_IsOverGUI_54;
      PickedObject = event_UnityEngine_GameObject_go_PickedUIElement_54;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( PickedObject_previous != PickedObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            PickedObject_previous = PickedObject;
            
            //setup new listeners
         }
      }
   }
   
}
