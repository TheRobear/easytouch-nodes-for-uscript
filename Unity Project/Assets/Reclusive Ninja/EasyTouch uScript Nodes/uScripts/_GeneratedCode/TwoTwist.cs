//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoTwist : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_30_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_30_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_31_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_31_UnityEngine_GameObject_previous = null;
   System.String local_7_System_String = "Delta Angle : ";
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   System.Single local_TwistAngle_System_Single = (float) 0;
   System.String local_TwistText_System_String = "";
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   UnityEngine.GameObject owner_Connection_2 = null;
   UnityEngine.GameObject owner_Connection_4 = null;
   UnityEngine.GameObject owner_Connection_10 = null;
   UnityEngine.GameObject owner_Connection_16 = null;
   UnityEngine.GameObject owner_Connection_18 = null;
   UnityEngine.GameObject owner_Connection_23 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_3 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_3 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_3 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_3 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_3 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_3 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_3 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_3 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_3 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_3 = (bool) true;
   bool logic_uScriptAct_SetGameObjectRotation_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_Concatenate logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6 = new uScriptAct_Concatenate( );
   System.Object[] logic_uScriptAct_Concatenate_A_6 = new System.Object[] {};
   System.Object[] logic_uScriptAct_Concatenate_B_6 = new System.Object[] {};
   System.String logic_uScriptAct_Concatenate_Separator_6 = "";
   System.String logic_uScriptAct_Concatenate_Result_6;
   bool logic_uScriptAct_Concatenate_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_9 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_9 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_9 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_9 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_9 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_9 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_9 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_9 = UnityEngine.Space.World;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_9 = (bool) false;
   bool logic_uScriptAct_SetGameObjectRotation_Out_9 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_11 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_11 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_11 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_11 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_14 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_14 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_14 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_14 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_14 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_14 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_14 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_14 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_20 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_20 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_20 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_20 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_20 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_20 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_20 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_20 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_21 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_21 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_21 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_21 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_25 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_25 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_25 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_25 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_25 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_25 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_25 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_25 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_26 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_26 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_26 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_26 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_28 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_28 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_28 = "";
   bool logic_RN_SetTextMeshText_Out_28 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_29 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_29 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_29 = "Twist Me";
   bool logic_RN_SetTextMeshText_Out_29 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnablePinch logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_80 = new EasyTouch_SetEnablePinch( );
   System.Boolean logic_EasyTouch_SetEnablePinch_b_EnablePinch_80 = (bool) false;
   bool logic_EasyTouch_SetEnablePinch_Out_80 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnableTwist logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_82 = new EasyTouch_SetEnableTwist( );
   System.Boolean logic_EasyTouch_SetEnableTwist_b_EnableTwist_82 = (bool) true;
   bool logic_EasyTouch_SetEnableTwist_Out_82 = true;
   //pointer to script instanced logic node
   EasyTouch_SetEnablePinch logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_90 = new EasyTouch_SetEnablePinch( );
   System.Boolean logic_EasyTouch_SetEnablePinch_b_EnablePinch_90 = (bool) true;
   bool logic_EasyTouch_SetEnablePinch_Out_90 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_76 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_76 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_76 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_76 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_76 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_76 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_76 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_76 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_76 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_76 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_76 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_76 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_76 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_76 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_76 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_76 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_76 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_76 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_76 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_76 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_76 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_76 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_76 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_76 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_76 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_76 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_76 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_76 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_76 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_76 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_84 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_84 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_84 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_84 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_84 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_84 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_84 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_84 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_84 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_84 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_84 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_84 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_84 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_84 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_84 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_84 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_84 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_84 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_84 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_84 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_84 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_84 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_84 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_84 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_84 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_84 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_84 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_84 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_84 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_84 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_30_UnityEngine_GameObject = GameObject.Find( "Sphere_Twist_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_31_UnityEngine_GameObject = GameObject.Find( "Sphere_Twist_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_76;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_76;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_76;
               }
            }
         }
      }
      if ( null == owner_Connection_2 || false == m_RegisteredForEvents )
      {
         owner_Connection_2 = parentGameObject;
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_2.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_84;
                  component.OnTwistEnd += Instance_OnTwistEnd_84;
               }
            }
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
      if ( null == owner_Connection_10 || false == m_RegisteredForEvents )
      {
         owner_Connection_10 = parentGameObject;
      }
      if ( null == owner_Connection_16 || false == m_RegisteredForEvents )
      {
         owner_Connection_16 = parentGameObject;
      }
      if ( null == owner_Connection_18 || false == m_RegisteredForEvents )
      {
         owner_Connection_18 = parentGameObject;
      }
      if ( null == owner_Connection_23 || false == m_RegisteredForEvents )
      {
         owner_Connection_23 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<EasyTouch_On_Touch2Fingers>();
               }
               if ( null != component )
               {
                  component.OnTouchStart2Fingers += Instance_OnTouchStart2Fingers_76;
                  component.OnTouchDown2Fingers += Instance_OnTouchDown2Fingers_76;
                  component.OnTouchUp2Fingers += Instance_OnTouchUp2Fingers_76;
               }
            }
         }
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_2 )
         {
            {
               EasyTouch_On_Twist component = owner_Connection_2.GetComponent<EasyTouch_On_Twist>();
               if ( null == component )
               {
                  component = owner_Connection_2.AddComponent<EasyTouch_On_Twist>();
               }
               if ( null != component )
               {
                  component.OnTwist += Instance_OnTwist_84;
                  component.OnTwistEnd += Instance_OnTwistEnd_84;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            EasyTouch_On_Touch2Fingers component = owner_Connection_1.GetComponent<EasyTouch_On_Touch2Fingers>();
            if ( null != component )
            {
               component.OnTouchStart2Fingers -= Instance_OnTouchStart2Fingers_76;
               component.OnTouchDown2Fingers -= Instance_OnTouchDown2Fingers_76;
               component.OnTouchUp2Fingers -= Instance_OnTouchUp2Fingers_76;
            }
         }
      }
      if ( null != owner_Connection_2 )
      {
         {
            EasyTouch_On_Twist component = owner_Connection_2.GetComponent<EasyTouch_On_Twist>();
            if ( null != component )
            {
               component.OnTwist -= Instance_OnTwist_84;
               component.OnTwistEnd -= Instance_OnTwistEnd_84;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_3.SetParent(g);
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_11.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_14.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_20.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_25.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_28.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_29.SetParent(g);
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_80.SetParent(g);
      logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_82.SetParent(g);
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_90.SetParent(g);
      owner_Connection_1 = parentGameObject;
      owner_Connection_2 = parentGameObject;
      owner_Connection_4 = parentGameObject;
      owner_Connection_10 = parentGameObject;
      owner_Connection_16 = parentGameObject;
      owner_Connection_18 = parentGameObject;
      owner_Connection_23 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnTouchStart2Fingers_76(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_76 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_76 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_76 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_76 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_76 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_76 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_76 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_76 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_76 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_76 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_76 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_76 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_76 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_76 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_76 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_76 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_76 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_76 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_76 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_76 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_76 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_76 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_76 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_76 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_76 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_76 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_76 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_76 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_76 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_76 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_76 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_76 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_76 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_76 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_76 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_76 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_76 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_76 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_76 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_76 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_76 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_76 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_76 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart2Fingers_76( );
   }
   
   void Instance_OnTouchDown2Fingers_76(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_76 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_76 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_76 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_76 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_76 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_76 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_76 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_76 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_76 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_76 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_76 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_76 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_76 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_76 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_76 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_76 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_76 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_76 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_76 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_76 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_76 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_76 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_76 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_76 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_76 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_76 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_76 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_76 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_76 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_76 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_76 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_76 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_76 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_76 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_76 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_76 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_76 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_76 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_76 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_76 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_76 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_76 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_76 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown2Fingers_76( );
   }
   
   void Instance_OnTouchUp2Fingers_76(object o, EasyTouch_On_Touch2Fingers.OnTouch2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_76 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_76 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_76 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_76 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_76 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_76 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_76 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_76 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_76 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_76 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_76 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_76 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_76 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_76 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_76 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_76 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_76 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_76 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_76 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_76 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_76 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_76 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_76 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_76 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_76 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_76 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_76 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_76 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_76 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_76 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_76 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_76 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_76 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_76 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_76 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_76 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_76 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_76 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_76 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_76 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_76 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_76 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_76 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp2Fingers_76( );
   }
   
   void Instance_OnTwist_84(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_84 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_84 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_84 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_84 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_84 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_84 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_84 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_84 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_84 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_84 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_84 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_84 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_84 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_84 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_84 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_84 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_84 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_84 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_84 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_84 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_84 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_84 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_84 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_84 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_84 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_84 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_84 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_84 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_84 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_84 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_84 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_84 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_84 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_84 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_84 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_84 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_84 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_84 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_84 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_84 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_84 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_84 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_84 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwist_84( );
   }
   
   void Instance_OnTwistEnd_84(object o, EasyTouch_On_Twist.OnTwistEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_84 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_84 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_84 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_84 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_84 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_84 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_84 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_84 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_84 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_84 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_84 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_84 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_84 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_84 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_84 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_84 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_84 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_84 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_84 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_84 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_84 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_84 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_84 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_84 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_84 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_84 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_84 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_84 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_84 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_84 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_84 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_84 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_84 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_84 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_84 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_84 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_84 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_84 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_84 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_84 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_84 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_84 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_84 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTwistEnd_84( );
   }
   
   void Relay_In_3()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_3.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_3, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_3[ index++ ] = owner_Connection_4;
            
         }
         {
         }
         {
         }
         {
            logic_uScriptAct_SetGameObjectRotation_ZDegrees_3 = local_TwistAngle_System_Single;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_3.In(logic_uScriptAct_SetGameObjectRotation_Target_3, logic_uScriptAct_SetGameObjectRotation_XDegrees_3, logic_uScriptAct_SetGameObjectRotation_YDegrees_3, logic_uScriptAct_SetGameObjectRotation_ZDegrees_3, logic_uScriptAct_SetGameObjectRotation_IgnoreX_3, logic_uScriptAct_SetGameObjectRotation_IgnoreY_3, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_3, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_3, logic_uScriptAct_SetGameObjectRotation_AsOffset_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_3.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_A_6.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_A_6, index + 1);
            }
            logic_uScriptAct_Concatenate_A_6[ index++ ] = local_7_System_String;
            
         }
         {
            int index = 0;
            if ( logic_uScriptAct_Concatenate_B_6.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_Concatenate_B_6, index + 1);
            }
            logic_uScriptAct_Concatenate_B_6[ index++ ] = local_TwistAngle_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.In(logic_uScriptAct_Concatenate_A_6, logic_uScriptAct_Concatenate_B_6, logic_uScriptAct_Concatenate_Separator_6, out logic_uScriptAct_Concatenate_Result_6);
      local_TwistText_System_String = logic_uScriptAct_Concatenate_Result_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Concatenate_uScriptAct_Concatenate_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectRotation_Target_9.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_9, index + 1);
            }
            logic_uScriptAct_SetGameObjectRotation_Target_9[ index++ ] = owner_Connection_10;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9.In(logic_uScriptAct_SetGameObjectRotation_Target_9, logic_uScriptAct_SetGameObjectRotation_XDegrees_9, logic_uScriptAct_SetGameObjectRotation_YDegrees_9, logic_uScriptAct_SetGameObjectRotation_ZDegrees_9, logic_uScriptAct_SetGameObjectRotation_IgnoreX_9, logic_uScriptAct_SetGameObjectRotation_IgnoreY_9, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_9, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_9, logic_uScriptAct_SetGameObjectRotation_AsOffset_9);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_11, index + 1);
            }
            logic_uScriptCon_IsNull_Target_11[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_11.In(logic_uScriptCon_IsNull_Target_11);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_11.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_14();
      }
   }
   
   void Relay_In_14()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_14 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_14 = owner_Connection_16;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_14.In(logic_uScriptCon_CompareGameObjects_A_14, logic_uScriptCon_CompareGameObjects_B_14, logic_uScriptCon_CompareGameObjects_CompareByTag_14, logic_uScriptCon_CompareGameObjects_CompareByName_14, logic_uScriptCon_CompareGameObjects_ReportNull_14);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_14.Same;
      
      if ( test_0 == true )
      {
         Relay_In_80();
      }
   }
   
   void Relay_In_20()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_20 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_20 = owner_Connection_18;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_20.In(logic_uScriptCon_CompareGameObjects_A_20, logic_uScriptCon_CompareGameObjects_B_20, logic_uScriptCon_CompareGameObjects_CompareByTag_20, logic_uScriptCon_CompareGameObjects_CompareByName_20, logic_uScriptCon_CompareGameObjects_ReportNull_20);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_20.Same;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_21.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_21, index + 1);
            }
            logic_uScriptCon_IsNull_Target_21[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.In(logic_uScriptCon_IsNull_Target_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_21.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_20();
      }
   }
   
   void Relay_In_25()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_25 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_25 = owner_Connection_23;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_25.In(logic_uScriptCon_CompareGameObjects_A_25, logic_uScriptCon_CompareGameObjects_B_25, logic_uScriptCon_CompareGameObjects_CompareByTag_25, logic_uScriptCon_CompareGameObjects_CompareByName_25, logic_uScriptCon_CompareGameObjects_ReportNull_25);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_25.Same;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_26.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_26, index + 1);
            }
            logic_uScriptCon_IsNull_Target_26[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.In(logic_uScriptCon_IsNull_Target_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_26.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_28 = local_31_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_28 = local_TwistText_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_28.In(logic_RN_SetTextMeshText_go_GameObject_28, logic_RN_SetTextMeshText_s_Text_28);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_29 = local_30_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_29.In(logic_RN_SetTextMeshText_go_GameObject_29, logic_RN_SetTextMeshText_s_Text_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_SetTextMeshText_RN_SetTextMeshText_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_OnTouchStart2Fingers_76()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_76;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_11();
   }
   
   void Relay_OnTouchDown2Fingers_76()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_76;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnTouchUp2Fingers_76()
   {
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_76;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_In_80()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_80.In(logic_EasyTouch_SetEnablePinch_b_EnablePinch_80);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_80.Out;
      
      if ( test_0 == true )
      {
         Relay_In_82();
      }
   }
   
   void Relay_In_82()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnableTwist_EasyTouch_SetEnableTwist_82.In(logic_EasyTouch_SetEnableTwist_b_EnableTwist_82);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnTwist_84()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_84;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_84;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_21();
   }
   
   void Relay_OnTwistEnd_84()
   {
      local_TwistAngle_System_Single = event_UnityEngine_GameObject_f_TwistAngle_84;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_84;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_26();
   }
   
   void Relay_In_90()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetEnablePinch_EasyTouch_SetEnablePinch_90.In(logic_EasyTouch_SetEnablePinch_b_EnablePinch_90);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
