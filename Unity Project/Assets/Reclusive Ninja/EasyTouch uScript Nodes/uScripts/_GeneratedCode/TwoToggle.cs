//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoToggle : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_18_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_18_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_19_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_19_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_20_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_20_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_21_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_21_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_22_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_22_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_23_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_23_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_30_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_30_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_31_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_31_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_33_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_33_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_7_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_7_UnityEngine_GameObject_previous = null;
   System.Boolean local_Average_System_Boolean = (bool) false;
   System.Boolean local_Finger_System_Boolean = (bool) false;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptCon_CompareBoolState logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_3 = new uScriptCon_CompareBoolState( );
   System.Boolean logic_uScriptCon_CompareBoolState_target_3 = (bool) false;
   System.Boolean logic_uScriptCon_CompareBoolState_previousState_3 = (bool) false;
   bool logic_uScriptCon_CompareBoolState_Out_3 = true;
   bool logic_uScriptCon_CompareBoolState_IsTrue_3 = true;
   bool logic_uScriptCon_CompareBoolState_IsFalse_3 = true;
   bool logic_uScriptCon_CompareBoolState_WasTrue_3 = true;
   bool logic_uScriptCon_CompareBoolState_WasFalse_3 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBoolState logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_6 = new uScriptCon_CompareBoolState( );
   System.Boolean logic_uScriptCon_CompareBoolState_target_6 = (bool) false;
   System.Boolean logic_uScriptCon_CompareBoolState_previousState_6 = (bool) false;
   bool logic_uScriptCon_CompareBoolState_Out_6 = true;
   bool logic_uScriptCon_CompareBoolState_IsTrue_6 = true;
   bool logic_uScriptCon_CompareBoolState_IsFalse_6 = true;
   bool logic_uScriptCon_CompareBoolState_WasTrue_6 = true;
   bool logic_uScriptCon_CompareBoolState_WasFalse_6 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_9 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_9;
   bool logic_uScriptAct_SetBool_Out_9 = true;
   bool logic_uScriptAct_SetBool_SetTrue_9 = true;
   bool logic_uScriptAct_SetBool_SetFalse_9 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_11 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_11;
   bool logic_uScriptAct_SetBool_Out_11 = true;
   bool logic_uScriptAct_SetBool_SetTrue_11 = true;
   bool logic_uScriptAct_SetBool_SetFalse_11 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_15 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_15;
   bool logic_uScriptAct_SetBool_Out_15 = true;
   bool logic_uScriptAct_SetBool_SetTrue_15 = true;
   bool logic_uScriptAct_SetBool_SetFalse_15 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_17 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_17;
   bool logic_uScriptAct_SetBool_Out_17 = true;
   bool logic_uScriptAct_SetBool_SetTrue_17 = true;
   bool logic_uScriptAct_SetBool_SetFalse_17 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_24 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_24 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_24 = (bool) true;
   bool logic_RN_SetUnityUIToggle_Out_24 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_25 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_25 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_25 = (bool) false;
   bool logic_RN_SetUnityUIToggle_Out_25 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_26 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_26 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_26 = (bool) false;
   bool logic_RN_SetUnityUIToggle_Out_26 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_27 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_27 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_27 = (bool) true;
   bool logic_RN_SetUnityUIToggle_Out_27 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_28 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_28 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_28 = (bool) false;
   bool logic_RN_SetUnityUIToggle_Out_28 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIToggle logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_29 = new RN_SetUnityUIToggle( );
   UnityEngine.GameObject logic_RN_SetUnityUIToggle_go_GameObject_29 = default(UnityEngine.GameObject);
   System.Boolean logic_RN_SetUnityUIToggle_b_Toggle_29 = (bool) true;
   bool logic_RN_SetUnityUIToggle_Out_29 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_32 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_32 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_32 = "The two fingers should be positioned on the object.";
   bool logic_RN_SetUnityUIText_Out_32 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_34 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_34 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_34 = "The average position of the two fingers is used to determine if an object is located in this position.";
   bool logic_RN_SetUnityUIText_Out_34 = true;
   //pointer to script instanced logic node
   RN_SetUnityUIText logic_RN_SetUnityUIText_RN_SetUnityUIText_35 = new RN_SetUnityUIText( );
   UnityEngine.GameObject logic_RN_SetUnityUIText_go_GameObject_35 = default(UnityEngine.GameObject);
   System.String logic_RN_SetUnityUIText_s_Text_35 = "The two fingers should be positioned on the object.";
   bool logic_RN_SetUnityUIText_Out_35 = true;
   //pointer to script instanced logic node
   EasyTouch_SetTwoFingerPickMethod logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_85 = new EasyTouch_SetTwoFingerPickMethod( );
   HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod logic_EasyTouch_SetTwoFingerPickMethod_ettfpm_TwoFingPickMethod_85 = HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod.Finger;
   bool logic_EasyTouch_SetTwoFingerPickMethod_Out_85 = true;
   //pointer to script instanced logic node
   EasyTouch_SetTwoFingerPickMethod logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_90 = new EasyTouch_SetTwoFingerPickMethod( );
   HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod logic_EasyTouch_SetTwoFingerPickMethod_ettfpm_TwoFingPickMethod_90 = HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod.Average;
   bool logic_EasyTouch_SetTwoFingerPickMethod_Out_90 = true;
   
   //event nodes
   System.Boolean event_UnityEngine_GameObject_ToggleValue_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_13 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_ToggleValue_81 = (bool) false;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = GameObject.Find( "Fingers_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_1_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_0;
               }
            }
         }
      }
      if ( null == local_7_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_7_UnityEngine_GameObject = GameObject.Find( "Average_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_7_UnityEngine_GameObject_previous != local_7_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_7_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_7_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_81;
               }
            }
         }
         
         local_7_UnityEngine_GameObject_previous = local_7_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_7_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_7_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_7_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_81;
               }
            }
         }
      }
      if ( null == local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_18_UnityEngine_GameObject = GameObject.Find( "Average_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_19_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_19_UnityEngine_GameObject = GameObject.Find( "Fingers_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_19_UnityEngine_GameObject_previous != local_19_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_19_UnityEngine_GameObject_previous = local_19_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_20_UnityEngine_GameObject = GameObject.Find( "Fingers_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_21_UnityEngine_GameObject = GameObject.Find( "Average_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_22_UnityEngine_GameObject = GameObject.Find( "Average_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_23_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_23_UnityEngine_GameObject = GameObject.Find( "Fingers_Toggle" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_23_UnityEngine_GameObject_previous != local_23_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_23_UnityEngine_GameObject_previous = local_23_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_30_UnityEngine_GameObject = GameObject.Find( "Info_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_31_UnityEngine_GameObject = GameObject.Find( "Info_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_33_UnityEngine_GameObject = GameObject.Find( "Info_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_1_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_0;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_7_UnityEngine_GameObject_previous != local_7_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_7_UnityEngine_GameObject_previous )
         {
            {
               uScript_Toggle component = local_7_UnityEngine_GameObject_previous.GetComponent<uScript_Toggle>();
               if ( null != component )
               {
                  component.OnToggleValueChanged -= Instance_OnToggleValueChanged_81;
               }
            }
         }
         
         local_7_UnityEngine_GameObject_previous = local_7_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_7_UnityEngine_GameObject )
         {
            {
               uScript_Toggle component = local_7_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = local_7_UnityEngine_GameObject.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_81;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_19_UnityEngine_GameObject_previous != local_19_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_19_UnityEngine_GameObject_previous = local_19_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_23_UnityEngine_GameObject_previous != local_23_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_23_UnityEngine_GameObject_previous = local_23_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_13 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_13 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_13 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_13.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_13.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_13;
                  component.uScriptLateStart += Instance_uScriptLateStart_13;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_1_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_1_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_0;
            }
         }
      }
      if ( null != local_7_UnityEngine_GameObject )
      {
         {
            uScript_Toggle component = local_7_UnityEngine_GameObject.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_81;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_13 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_13.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_13;
               component.uScriptLateStart -= Instance_uScriptLateStart_13;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_3.SetParent(g);
      logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_6.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_17.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_24.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_25.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_26.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_27.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_28.SetParent(g);
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_29.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_32.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_34.SetParent(g);
      logic_RN_SetUnityUIText_RN_SetUnityUIText_35.SetParent(g);
      logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_85.SetParent(g);
      logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_90.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnToggleValueChanged_0(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_0 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_0( );
   }
   
   void Instance_uScriptStart_13(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_13( );
   }
   
   void Instance_uScriptLateStart_13(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_13( );
   }
   
   void Instance_OnToggleValueChanged_81(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_81 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_81( );
   }
   
   void Relay_OnToggleValueChanged_0()
   {
      local_Finger_System_Boolean = event_UnityEngine_GameObject_ToggleValue_0;
      Relay_In_3();
   }
   
   void Relay_In_3()
   {
      {
         {
            logic_uScriptCon_CompareBoolState_target_3 = local_Finger_System_Boolean;
            
         }
         {
         }
      }
      logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_3.In(logic_uScriptCon_CompareBoolState_target_3, ref logic_uScriptCon_CompareBoolState_previousState_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_3.IsTrue;
      
      if ( test_0 == true )
      {
         Relay_In_85();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            logic_uScriptCon_CompareBoolState_target_6 = local_Average_System_Boolean;
            
         }
         {
         }
      }
      logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_6.In(logic_uScriptCon_CompareBoolState_target_6, ref logic_uScriptCon_CompareBoolState_previousState_6);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBoolState_uScriptCon_CompareBoolState_6.IsTrue;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_True_9()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.True(out logic_uScriptAct_SetBool_Target_9);
      local_Average_System_Boolean = logic_uScriptAct_SetBool_Target_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_False_9()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_9.False(out logic_uScriptAct_SetBool_Target_9);
      local_Average_System_Boolean = logic_uScriptAct_SetBool_Target_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_True_11()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.True(out logic_uScriptAct_SetBool_Target_11);
      local_Finger_System_Boolean = logic_uScriptAct_SetBool_Target_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_False_11()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_11.False(out logic_uScriptAct_SetBool_Target_11);
      local_Finger_System_Boolean = logic_uScriptAct_SetBool_Target_11;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_11.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_uScriptStart_13()
   {
      Relay_False_15();
      Relay_True_17();
      Relay_In_32();
   }
   
   void Relay_uScriptLateStart_13()
   {
   }
   
   void Relay_True_15()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.True(out logic_uScriptAct_SetBool_Target_15);
      local_Average_System_Boolean = logic_uScriptAct_SetBool_Target_15;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_15.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_False_15()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_15.False(out logic_uScriptAct_SetBool_Target_15);
      local_Average_System_Boolean = logic_uScriptAct_SetBool_Target_15;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_15.Out;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_True_17()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_17.True(out logic_uScriptAct_SetBool_Target_17);
      local_Finger_System_Boolean = logic_uScriptAct_SetBool_Target_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_False_17()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_17.False(out logic_uScriptAct_SetBool_Target_17);
      local_Finger_System_Boolean = logic_uScriptAct_SetBool_Target_17;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_17.Out;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_19_UnityEngine_GameObject_previous != local_19_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_19_UnityEngine_GameObject_previous = local_19_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_24 = local_19_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_24.In(logic_RN_SetUnityUIToggle_go_GameObject_24, logic_RN_SetUnityUIToggle_b_Toggle_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_25()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_25 = local_18_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_25.In(logic_RN_SetUnityUIToggle_go_GameObject_25, logic_RN_SetUnityUIToggle_b_Toggle_25);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_26()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_23_UnityEngine_GameObject_previous != local_23_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_23_UnityEngine_GameObject_previous = local_23_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_26 = local_23_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_26.In(logic_RN_SetUnityUIToggle_go_GameObject_26, logic_RN_SetUnityUIToggle_b_Toggle_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_27()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_27 = local_22_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_27.In(logic_RN_SetUnityUIToggle_go_GameObject_27, logic_RN_SetUnityUIToggle_b_Toggle_27);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_28()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_28 = local_21_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_28.In(logic_RN_SetUnityUIToggle_go_GameObject_28, logic_RN_SetUnityUIToggle_b_Toggle_28);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_20_UnityEngine_GameObject_previous != local_20_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_20_UnityEngine_GameObject_previous = local_20_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIToggle_go_GameObject_29 = local_20_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIToggle_RN_SetUnityUIToggle_29.In(logic_RN_SetUnityUIToggle_go_GameObject_29, logic_RN_SetUnityUIToggle_b_Toggle_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_32()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_30_UnityEngine_GameObject_previous != local_30_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_30_UnityEngine_GameObject_previous = local_30_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_32 = local_30_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_32.In(logic_RN_SetUnityUIText_go_GameObject_32, logic_RN_SetUnityUIText_s_Text_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_34()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_33_UnityEngine_GameObject_previous != local_33_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_33_UnityEngine_GameObject_previous = local_33_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_34 = local_33_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_34.In(logic_RN_SetUnityUIText_go_GameObject_34, logic_RN_SetUnityUIText_s_Text_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_35()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetUnityUIText_go_GameObject_35 = local_31_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetUnityUIText_RN_SetUnityUIText_35.In(logic_RN_SetUnityUIText_go_GameObject_35, logic_RN_SetUnityUIText_s_Text_35);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnToggleValueChanged_81()
   {
      local_Average_System_Boolean = event_UnityEngine_GameObject_ToggleValue_81;
      Relay_In_6();
   }
   
   void Relay_In_85()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_85.In(logic_EasyTouch_SetTwoFingerPickMethod_ettfpm_TwoFingPickMethod_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_85.Out;
      
      if ( test_0 == true )
      {
         Relay_In_29();
         Relay_In_35();
         Relay_False_9();
      }
   }
   
   void Relay_In_90()
   {
      {
         {
         }
      }
      logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_90.In(logic_EasyTouch_SetTwoFingerPickMethod_ettfpm_TwoFingPickMethod_90);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_EasyTouch_SetTwoFingerPickMethod_EasyTouch_SetTwoFingerPickMethod_90.Out;
      
      if ( test_0 == true )
      {
         Relay_In_27();
         Relay_In_34();
         Relay_False_11();
      }
   }
   
}
