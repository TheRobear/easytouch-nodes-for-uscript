//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class TwoLongTap : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Color local_11_UnityEngine_Color = new UnityEngine.Color( (float)1, (float)1, (float)1, (float)1 );
   UnityEngine.GameObject local_31_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_31_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_32_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_32_UnityEngine_GameObject_previous = null;
   System.Single local_ActionTime_System_Single = (float) 0;
   System.String local_ActionTimeFormatted_System_String = "";
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_PickedObject_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_0 = null;
   UnityEngine.GameObject owner_Connection_5 = null;
   UnityEngine.GameObject owner_Connection_13 = null;
   UnityEngine.GameObject owner_Connection_19 = null;
   UnityEngine.GameObject owner_Connection_22 = null;
   UnityEngine.GameObject owner_Connection_27 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_2 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_2 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_2 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_2;
   bool logic_uScriptAct_SetRandomColor_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_4 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_4 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_4 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_4 = true;
   //pointer to script instanced logic node
   RN_FormatFloatString logic_RN_FormatFloatString_RN_FormatFloatString_7 = new RN_FormatFloatString( );
   System.Single logic_RN_FormatFloatString_f_Float_7 = (float) 0;
   System.String logic_RN_FormatFloatString_s_Modifier_7 = "f2";
   System.String logic_RN_FormatFloatString_s_Result_7;
   bool logic_RN_FormatFloatString_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_12 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_12 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_12 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_12 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_15 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_15 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_15 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_15 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_16 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_16 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_16 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_16 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_16 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_16 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_16 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_16 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_21 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_21 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_21 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_21 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_24 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_24 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_24 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_24 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_24 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_24 = true;
   //pointer to script instanced logic node
   uScriptCon_IsNull logic_uScriptCon_IsNull_uScriptCon_IsNull_26 = new uScriptCon_IsNull( );
   UnityEngine.GameObject[] logic_uScriptCon_IsNull_Target_26 = new UnityEngine.GameObject[] {};
   bool logic_uScriptCon_IsNull_IsNull_26 = true;
   bool logic_uScriptCon_IsNull_IsNotNull_26 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareGameObjects logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29 = new uScriptCon_CompareGameObjects( );
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_A_29 = default(UnityEngine.GameObject);
   UnityEngine.GameObject logic_uScriptCon_CompareGameObjects_B_29 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByTag_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_CompareByName_29 = (bool) false;
   System.Boolean logic_uScriptCon_CompareGameObjects_ReportNull_29 = (bool) true;
   bool logic_uScriptCon_CompareGameObjects_Same_29 = true;
   bool logic_uScriptCon_CompareGameObjects_Different_29 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_30 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_30 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_30 = "";
   bool logic_RN_SetTextMeshText_Out_30 = true;
   //pointer to script instanced logic node
   RN_SetTextMeshText logic_RN_SetTextMeshText_RN_SetTextMeshText_33 = new RN_SetTextMeshText( );
   UnityEngine.GameObject logic_RN_SetTextMeshText_go_GameObject_33 = default(UnityEngine.GameObject);
   System.String logic_RN_SetTextMeshText_s_Text_33 = "Long Tap Me";
   bool logic_RN_SetTextMeshText_Out_33 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_75 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_75 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_75 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_75 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_75 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_75 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_75 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_75 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_75 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_75 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_75 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_75 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_75 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_75 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_75 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_75 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_75 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_75 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_75 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_75 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_75 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_75 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_31_UnityEngine_GameObject = GameObject.Find( "Sphere_Long_Tap_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_32_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_32_UnityEngine_GameObject = GameObject.Find( "Sphere_Long_Tap_Text" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_32_UnityEngine_GameObject_previous != local_32_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_32_UnityEngine_GameObject_previous = local_32_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_0 || false == m_RegisteredForEvents )
      {
         owner_Connection_0 = parentGameObject;
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_LongTap2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_LongTap2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_LongTap2Fingers>();
               }
               if ( null != component )
               {
                  component.OnLongTapStart2Fingers += Instance_OnLongTapStart2Fingers_75;
                  component.OnLongTap2Fingers += Instance_OnLongTap2Fingers_75;
                  component.OnLongTapEnd2Fingers += Instance_OnLongTapEnd2Fingers_75;
               }
            }
         }
      }
      if ( null == owner_Connection_5 || false == m_RegisteredForEvents )
      {
         owner_Connection_5 = parentGameObject;
      }
      if ( null == owner_Connection_13 || false == m_RegisteredForEvents )
      {
         owner_Connection_13 = parentGameObject;
      }
      if ( null == owner_Connection_19 || false == m_RegisteredForEvents )
      {
         owner_Connection_19 = parentGameObject;
      }
      if ( null == owner_Connection_22 || false == m_RegisteredForEvents )
      {
         owner_Connection_22 = parentGameObject;
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_32_UnityEngine_GameObject_previous != local_32_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_32_UnityEngine_GameObject_previous = local_32_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_0 )
         {
            {
               EasyTouch_On_LongTap2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_LongTap2Fingers>();
               if ( null == component )
               {
                  component = owner_Connection_0.AddComponent<EasyTouch_On_LongTap2Fingers>();
               }
               if ( null != component )
               {
                  component.OnLongTapStart2Fingers += Instance_OnLongTapStart2Fingers_75;
                  component.OnLongTap2Fingers += Instance_OnLongTap2Fingers_75;
                  component.OnLongTapEnd2Fingers += Instance_OnLongTapEnd2Fingers_75;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_0 )
      {
         {
            EasyTouch_On_LongTap2Fingers component = owner_Connection_0.GetComponent<EasyTouch_On_LongTap2Fingers>();
            if ( null != component )
            {
               component.OnLongTapStart2Fingers -= Instance_OnLongTapStart2Fingers_75;
               component.OnLongTap2Fingers -= Instance_OnLongTap2Fingers_75;
               component.OnLongTapEnd2Fingers -= Instance_OnLongTapEnd2Fingers_75;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.SetParent(g);
      logic_RN_FormatFloatString_RN_FormatFloatString_7.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_15.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_16.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.SetParent(g);
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.SetParent(g);
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_30.SetParent(g);
      logic_RN_SetTextMeshText_RN_SetTextMeshText_33.SetParent(g);
      owner_Connection_0 = parentGameObject;
      owner_Connection_5 = parentGameObject;
      owner_Connection_13 = parentGameObject;
      owner_Connection_19 = parentGameObject;
      owner_Connection_22 = parentGameObject;
      owner_Connection_27 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnLongTapStart2Fingers_75(object o, EasyTouch_On_LongTap2Fingers.OnLongTap2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTapStart2Fingers_75( );
   }
   
   void Instance_OnLongTap2Fingers_75(object o, EasyTouch_On_LongTap2Fingers.OnLongTap2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTap2Fingers_75( );
   }
   
   void Instance_OnLongTapEnd2Fingers_75(object o, EasyTouch_On_LongTap2Fingers.OnLongTap2FingersEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_75 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_75 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_75 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_75 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_75 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_75 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_75 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_75 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_75 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_75 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_75 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_75 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_75 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_75 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_75 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_75 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_75 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_75 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_75 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_75 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_75 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_75 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_75 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_75 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_75 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_75 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_75 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_75 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_75 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_75 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_75 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_75 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_75 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_75 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_75 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_75 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_75 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_75 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_75 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_75 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_75 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_75 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_75 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnLongTapEnd2Fingers_75( );
   }
   
   void Relay_In_2()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.In(logic_uScriptAct_SetRandomColor_RedMin_2, logic_uScriptAct_SetRandomColor_RedMax_2, logic_uScriptAct_SetRandomColor_GreenMin_2, logic_uScriptAct_SetRandomColor_GreenMax_2, logic_uScriptAct_SetRandomColor_BlueMin_2, logic_uScriptAct_SetRandomColor_BlueMax_2, logic_uScriptAct_SetRandomColor_AlphaMin_2, logic_uScriptAct_SetRandomColor_AlphaMax_2, out logic_uScriptAct_SetRandomColor_TargetColor_2);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_4.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_4, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_4[ index++ ] = owner_Connection_5;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_4 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_4.In(logic_uScriptAct_AssignMaterialColor_Target_4, logic_uScriptAct_AssignMaterialColor_MatColor_4, logic_uScriptAct_AssignMaterialColor_MatChannel_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_7()
   {
      {
         {
            logic_RN_FormatFloatString_f_Float_7 = local_ActionTime_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_RN_FormatFloatString_RN_FormatFloatString_7.In(logic_RN_FormatFloatString_f_Float_7, logic_RN_FormatFloatString_s_Modifier_7, out logic_RN_FormatFloatString_s_Result_7);
      local_ActionTimeFormatted_System_String = logic_RN_FormatFloatString_s_Result_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_RN_FormatFloatString_RN_FormatFloatString_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_30();
      }
   }
   
   void Relay_In_12()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_AssignMaterialColor_Target_12.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_12, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_12[ index++ ] = owner_Connection_13;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_12 = local_11_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.In(logic_uScriptAct_AssignMaterialColor_Target_12, logic_uScriptAct_AssignMaterialColor_MatColor_12, logic_uScriptAct_AssignMaterialColor_MatChannel_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_12.Out;
      
      if ( test_0 == true )
      {
         Relay_In_33();
      }
   }
   
   void Relay_In_15()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_15.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_15, index + 1);
            }
            logic_uScriptCon_IsNull_Target_15[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_15.In(logic_uScriptCon_IsNull_Target_15);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_15.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_16 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_16 = owner_Connection_19;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_16.In(logic_uScriptCon_CompareGameObjects_A_16, logic_uScriptCon_CompareGameObjects_B_16, logic_uScriptCon_CompareGameObjects_CompareByTag_16, logic_uScriptCon_CompareGameObjects_CompareByName_16, logic_uScriptCon_CompareGameObjects_ReportNull_16);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_16.Same;
      
      if ( test_0 == true )
      {
         Relay_In_2();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_21.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_21, index + 1);
            }
            logic_uScriptCon_IsNull_Target_21[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_21.In(logic_uScriptCon_IsNull_Target_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_21.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_24 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_24 = owner_Connection_22;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.In(logic_uScriptCon_CompareGameObjects_A_24, logic_uScriptCon_CompareGameObjects_B_24, logic_uScriptCon_CompareGameObjects_CompareByTag_24, logic_uScriptCon_CompareGameObjects_CompareByName_24, logic_uScriptCon_CompareGameObjects_ReportNull_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_24.Same;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptCon_IsNull_Target_26.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptCon_IsNull_Target_26, index + 1);
            }
            logic_uScriptCon_IsNull_Target_26[ index++ ] = local_PickedObject_UnityEngine_GameObject;
            
         }
      }
      logic_uScriptCon_IsNull_uScriptCon_IsNull_26.In(logic_uScriptCon_IsNull_Target_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_IsNull_uScriptCon_IsNull_26.IsNotNull;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_uScriptCon_CompareGameObjects_A_29 = local_PickedObject_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptCon_CompareGameObjects_B_29 = owner_Connection_27;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.In(logic_uScriptCon_CompareGameObjects_A_29, logic_uScriptCon_CompareGameObjects_B_29, logic_uScriptCon_CompareGameObjects_CompareByTag_29, logic_uScriptCon_CompareGameObjects_CompareByName_29, logic_uScriptCon_CompareGameObjects_ReportNull_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareGameObjects_uScriptCon_CompareGameObjects_29.Same;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_In_30()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_31_UnityEngine_GameObject_previous != local_31_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_31_UnityEngine_GameObject_previous = local_31_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_30 = local_31_UnityEngine_GameObject;
            
         }
         {
            logic_RN_SetTextMeshText_s_Text_30 = local_ActionTimeFormatted_System_String;
            
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_30.In(logic_RN_SetTextMeshText_go_GameObject_30, logic_RN_SetTextMeshText_s_Text_30);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_33()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_32_UnityEngine_GameObject_previous != local_32_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_32_UnityEngine_GameObject_previous = local_32_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_RN_SetTextMeshText_go_GameObject_33 = local_32_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_RN_SetTextMeshText_RN_SetTextMeshText_33.In(logic_RN_SetTextMeshText_go_GameObject_33, logic_RN_SetTextMeshText_s_Text_33);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_OnLongTapStart2Fingers_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_15();
   }
   
   void Relay_OnLongTap2Fingers_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_21();
   }
   
   void Relay_OnLongTapEnd2Fingers_75()
   {
      local_ActionTime_System_Single = event_UnityEngine_GameObject_f_ActionTime_75;
      local_PickedObject_UnityEngine_GameObject = event_UnityEngine_GameObject_go_PickedObject_75;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_PickedObject_UnityEngine_GameObject_previous != local_PickedObject_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_PickedObject_UnityEngine_GameObject_previous = local_PickedObject_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_26();
   }
   
}
