//uScript Generated Code - Build 1.0.3018
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class ToggleUIComp : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Boolean UIComp = (bool) false;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   EasyTouch_SetUICompatibily logic_EasyTouch_SetUICompatibily_EasyTouch_SetUICompatibily_7 = new EasyTouch_SetUICompatibily( );
   System.Boolean logic_EasyTouch_SetUICompatibily_b_EnableUIComp_7 = (bool) false;
   bool logic_EasyTouch_SetUICompatibily_Out_7 = true;
   
   //event nodes
   System.Boolean event_UnityEngine_GameObject_ToggleValue_0 = (bool) false;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Toggle component = owner_Connection_1.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_0;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_1 )
         {
            {
               uScript_Toggle component = owner_Connection_1.GetComponent<uScript_Toggle>();
               if ( null == component )
               {
                  component = owner_Connection_1.AddComponent<uScript_Toggle>();
               }
               if ( null != component )
               {
                  component.OnToggleValueChanged += Instance_OnToggleValueChanged_0;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_1 )
      {
         {
            uScript_Toggle component = owner_Connection_1.GetComponent<uScript_Toggle>();
            if ( null != component )
            {
               component.OnToggleValueChanged -= Instance_OnToggleValueChanged_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_EasyTouch_SetUICompatibily_EasyTouch_SetUICompatibily_7.SetParent(g);
      owner_Connection_1 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnToggleValueChanged_0(object o, uScript_Toggle.ToggleEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_ToggleValue_0 = e.ToggleValue;
      //relay event to nodes
      Relay_OnToggleValueChanged_0( );
   }
   
   void Relay_OnToggleValueChanged_0()
   {
      UIComp = event_UnityEngine_GameObject_ToggleValue_0;
      Relay_In_7();
   }
   
   void Relay_In_7()
   {
      {
         {
            logic_EasyTouch_SetUICompatibily_b_EnableUIComp_7 = UIComp;
            
         }
      }
      logic_EasyTouch_SetUICompatibily_EasyTouch_SetUICompatibily_7.In(logic_EasyTouch_SetUICompatibily_b_EnableUIComp_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
