﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Set the text value of a Unity UI Text.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Set Unity UI Text", "Set the text value of a Unity UI Text.")]

public class RN_SetUnityUIText : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject,

        [FriendlyName("Text", "The value to set the Unity UI text to.")]
        string s_Text)
    {
        go_GameObject.GetComponent<Text>().text = s_Text;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}