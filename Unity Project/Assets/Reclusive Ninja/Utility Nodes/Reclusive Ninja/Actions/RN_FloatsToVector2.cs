﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Allows two float values to be used for X and Y for a Vector2 variable.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Floats To Vector2", "Allows two float values to be used for X and Y for a Vector2 variable.")]
public class RN_FloatsToVector2 : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("X Value", "The float value for the X value.")]
        float f_XValue,

        [FriendlyName("Y Value", "The float value for the Y Value.")]
        float f_YValue,

        [FriendlyName("Vector2 Result", "The resulting Vector2 values.")]
        out Vector2 v2_Result
        )
    {
        v2_Result = new Vector3(f_XValue, f_YValue);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}