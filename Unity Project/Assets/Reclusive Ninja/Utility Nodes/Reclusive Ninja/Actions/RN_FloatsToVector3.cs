﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Allows three float values to be used for X, Y and Z for a Vector3 variable.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Floats To Vector3", "Allows three float values to be used for X, Y and Z for a Vector3 variable.")]
public class RN_FloatsToVector3 : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("X Value", "The float value for the X value.")]
        float f_XValue,

        [FriendlyName("Y Value", "The float value for the Y Value.")]
        float f_YValue,

        [FriendlyName("Z Value", "The float value for the Z Value.")]
        float f_ZValue,

        [FriendlyName("Vector3 Result", "The resulting Vector3 values.")]
        out Vector3 v3_Result
        )
    {
        v3_Result = new Vector3(f_XValue, f_YValue, f_ZValue);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}