﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Set the value of a Unity UI toggle object.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Set Unity UI Toggle", "Set the value of a Unity UI toggle object.")]
public class RN_SetUnityUIToggle : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject,
        
        [FriendlyName("Toggle Value", "The boolean value to set the toggle object to.")]
        bool b_Toggle)
    {
        go_GameObject.GetComponent<Toggle>().isOn = b_Toggle;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}