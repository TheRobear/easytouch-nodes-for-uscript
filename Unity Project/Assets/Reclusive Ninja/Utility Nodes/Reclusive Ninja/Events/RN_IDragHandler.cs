﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[NodePath("Reclusive Ninja/Events")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Uses the Unity Event System IDragHandler.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - IDragHandler", "Uses the Unity Event System IDragHandler.")]
public class RN_IDragHandler : uScriptEvent, IDragHandler
{
#region uScript Event Handlers ------------------------------------------------------------------------
    //uScript event declaration.
    public delegate void uScriptEventHandler(object sender, OnDragEventData eventData);

    //Defining additional read-only variable sockets.
    public class OnDragEventData : System.EventArgs
    {
        private PointerEventData e_EventData;

        [FriendlyName("Button", "The InputButton for this event.")]
        [SocketState(false, false)]
        public PointerEventData.InputButton ib_Button
        {
            get { return e_EventData.button; }
        }

        [FriendlyName("Click Count", "Number of clicks in a row.")]
        [SocketState(false, false)]
        public int i_ClickCount
        {
            get { return e_EventData.clickCount; }
        }

        [FriendlyName("Click Time", "The last time a click event was sent.")]
        [SocketState(false, false)]
        public float f_ClickTime
        {
            get { return e_EventData.clickTime; }
        }

        [FriendlyName("Click Count", "Number of clicks in a row.")]
        [SocketState(false, false)]
        public BaseInputModule bim_CurrentInput
        {
            get { return e_EventData.currentInputModule; }
        }

        [FriendlyName("Delta Vector2", "Pointer delta since last update as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_Delta
        {
            get { return e_EventData.delta; }
        }

        [FriendlyName("Delta Vector3", "Pointer delta since last update as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_Delta
        {
            get { return (Vector3)e_EventData.delta; }
        }

        [FriendlyName("Dragging", "Is a drag operation currently occuring.")]
        [SocketState(false, false)]
        public bool b_Dragging
        {
            get { return e_EventData.dragging; }
        }

        [FriendlyName("Eligible For Click", "Is the element eligible for click.")]
        [SocketState(false, false)]
        public bool b_EligibleForClick
        {
            get { return e_EventData.eligibleForClick; }
        }

        [FriendlyName("Enter Event Camera", "The camera associated with the last OnPointerEnter event.")]
        [SocketState(false, false)]
        public Camera c_EnterEventCam
        {
            get { return e_EventData.enterEventCamera; }
        }

        [FriendlyName("Hovered", "List of objects in the hover stack.")]
        [SocketState(false, false)]
        public List<GameObject> l_Hovered
        {
            get { return e_EventData.hovered; }
        }

        [FriendlyName("Is Pointer Moving", "Is the pointer moving.")]
        [SocketState(false, false)]
        public bool b_IsPointerMoving
        {
            get { return e_EventData.IsPointerMoving(); }
        }

        [FriendlyName("Is Scrolling", "Is scroll being used on the input device.")]
        [SocketState(false, false)]
        public bool b_IsScrolling
        {
            get { return e_EventData.IsScrolling(); }
        }

        [FriendlyName("Last Press", "The GameObject for the last press event.")]
        [SocketState(false, false)]
        public GameObject go_LastPress
        {
            get { return e_EventData.lastPress; }
        }

        [FriendlyName("Pointer Current Raycast", "RaycastResult associated with the current event.")]
        [SocketState(false, false)]
        public RaycastResult rc_PointerCurrentRaycast
        {
            get { return e_EventData.pointerCurrentRaycast; }
        }

        [FriendlyName("Pointer Drag", "The object that is receiving OnDrag.")]
        [SocketState(false, false)]
        public GameObject go_PointerDrag
        {
            get { return e_EventData.pointerDrag; }
        }

        [FriendlyName("Pointer Enter", "The object that received OnPointerEnter.")]
        [SocketState(false, false)]
        public GameObject go_PointerEnter
        {
            get { return e_EventData.pointerEnter; }
        }

        [FriendlyName("Pointer ID", "Id of the pointer (touch id).")]
        [SocketState(false, false)]
        public int i_PointerID
        {
            get { return e_EventData.pointerId; }
        }

        [FriendlyName("Pointer Press", "The GameObject that received the OnPointerDown.")]
        [SocketState(false, false)]
        public GameObject go_PointerPress
        {
            get { return e_EventData.pointerPress; }
        }

        [FriendlyName("Pointer Press Raycast", "RaycastResult associated with the pointer press.")]
        [SocketState(false, false)]
        public RaycastResult rc_PointerPressRaycast
        {
            get { return e_EventData.pointerPressRaycast; }
        }

        [FriendlyName("Position Vector2", "Current pointer position as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_Position
        {
            get { return e_EventData.position; }
        }

        [FriendlyName("Position Vector3", "Current pointer position as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_Position
        {
            get { return (Vector3)e_EventData.position; }
        }

        [FriendlyName("Press Event Camera", "The camera associated with the last OnPointerPress event.")]
        [SocketState(false, false)]
        public Camera c_PressEventCamera
        {
            get { return e_EventData.pressEventCamera; }
        }

        [FriendlyName("Press Position Vector2", "Position of the press as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_PressPosition
        {
            get { return e_EventData.pressPosition; }
        }

        [FriendlyName("Press Position Vector3", "Position of the press as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_PressPosition
        {
            get { return (Vector3)e_EventData.pressPosition; }
        }

        [FriendlyName("Raw Pointer Press", "The object that the press happened on even if it can not handle the press event.")]
        [SocketState(false, false)]
        public GameObject go_RawPointerPress
        {
            get { return e_EventData.rawPointerPress; }
        }

        [FriendlyName("Scroll Delta Vector2", "The amount of scroll since the last update as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_ScrollDelta
        {
            get { return e_EventData.scrollDelta; }
        }

        [FriendlyName("Scroll Delta Vector3", "The amount of scroll since the last update as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_ScrollData
        {
            get { return e_EventData.scrollDelta; }
        }

        [FriendlyName("Selected Object", "The object currently considered selected by the EventSystem.")]
        [SocketState(false, false)]
        public GameObject go_SelectedObject
        {
            get { return e_EventData.selectedObject; }
        }

        [FriendlyName("Used", "Is the event used?")]
        [SocketState(false, false)]
        public bool b_Used
        {
            get { return e_EventData.used; }
        }

        [FriendlyName("Used Drag Threshold", "Should a drag threshold be used?")]
        [SocketState(false, false)]
        public bool b_UsedDragThreshold
        {
            get { return e_EventData.useDragThreshold; }
        }

        public OnDragEventData(PointerEventData eventData)
        {
            e_EventData = eventData;
        }
    }

    //Unity Event System declaration
    [FriendlyName("OnDrag")]
    public event uScriptEventHandler On_Drag;
#endregion

#region OnDrag Functions ------------------------------------------------------------------------
    public void OnDrag(PointerEventData eventData)
    {
        if (On_Drag != null)
        {
            On_Drag(this, new OnDragEventData(eventData));
        }
    }
#endregion
}